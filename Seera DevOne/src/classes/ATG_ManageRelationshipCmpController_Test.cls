/**
 * Created by mnazir001 on 11/27/18.
 */

@IsTest
private class ATG_ManageRelationshipCmpController_Test {
    @IsTest
    static void testGetSelectableValues() {

        Integer masterRecordCount = 10;
        List<ATG_Master__c> masterPrograms = ATG_TestDataFactory.createMaster(masterRecordCount, 'p');
        upsert masterPrograms;

        List<ATG_Master__c> masterDepartments = ATG_TestDataFactory.createMaster(masterRecordCount, 'd');
        upsert masterDepartments;

        List<ATG_Master__c> masterCCGR = ATG_TestDataFactory.createMaster(masterRecordCount, 'c');
        upsert masterCCGR;

        List<ATG_Department_Relationship__c> departmentRelationships = new List<ATG_Department_Relationship__c>();


        List<Case> casesList = ATG_TestDataFactory.CreateETOCase(masterRecordCount);
        for(Integer i = 0; i < casesList.size(); i++){

            casesList[i].ATG_Program__c = masterPrograms[i].Id;
            casesList[i].ATG_Department__c = masterDepartments[i].Id;
            casesList[i].ATG_CC_CCGR_Number__c = masterCCGR[i].Id;
            ATG_Department_Relationship__c departmentRelationship = new ATG_Department_Relationship__c( Department__c= masterDepartments[i].Id,Program__c=masterPrograms[i].Id );
            departmentRelationships.add(departmentRelationship);
            casesList[i].Status = ATG_CommunityListViewController.STATUS_APPROVAL_PENDING;
            casesList[i].ATG_ProgramName__c = masterPrograms[i].Name;
        }

        upsert departmentRelationships;
        upsert casesList;

        ATG_ManageRelationshipCmpController.GetSelectableValues(masterPrograms[0].Id, '1');
        ATG_ManageRelationshipCmpController.GetSelectableValues(masterDepartments[0].Id, '2');
        ATG_ManageRelationshipCmpController.GetSelectableValues(masterDepartments[0].Id, '3');
        ATG_ManageRelationshipCmpController.GetSelectableValues(masterCCGR[0].Id, '4');
    }

    @IsTest
    static void testSaveSelectedValues() {

        Integer masterRecordCount = 10;
        List<ATG_Master__c> masterPrograms = ATG_TestDataFactory.createMaster(masterRecordCount, 'p');
        upsert masterPrograms;

        List<ATG_Master__c> masterDepartments = ATG_TestDataFactory.createMaster(masterRecordCount, 'd');
        upsert masterDepartments;

        List<ATG_Master__c> masterCCGR = ATG_TestDataFactory.createMaster(masterRecordCount, 'c');
        upsert masterCCGR;

        List<ATG_Department_Relationship__c> departmentRelationships = new List<ATG_Department_Relationship__c>();


        List<Case> casesList = ATG_TestDataFactory.CreateETOCase(masterRecordCount);
        for(Integer i = 0; i < casesList.size(); i++){

            casesList[i].ATG_Program__c = masterPrograms[i].Id;
            casesList[i].ATG_Department__c = masterDepartments[i].Id;
            casesList[i].ATG_CC_CCGR_Number__c = masterCCGR[i].Id;
            ATG_Department_Relationship__c departmentRelationship = new ATG_Department_Relationship__c( Department__c= masterDepartments[i].Id,Program__c=masterPrograms[i].Id );
            departmentRelationships.add(departmentRelationship);
            casesList[i].Status = ATG_CommunityListViewController.STATUS_APPROVAL_PENDING;
            casesList[i].ATG_ProgramName__c = masterPrograms[i].Name;
        }

        upsert departmentRelationships;
        upsert casesList;


        List<String> depsForPrograms = new List<String>();
        for(Integer i = 0; i < masterRecordCount - 5; i++){
            depsForPrograms.add(masterDepartments[i].Id);
        }
        ATG_ManageRelationshipCmpController.SaveSelectedValues(masterPrograms[0].Id, depsForPrograms, '1');


        List<String> programsForDeps = new List<String>();
        for(Integer i = 0; i < masterRecordCount - 5; i++){
            programsForDeps.add(masterPrograms[i].Id);
        }
        ATG_ManageRelationshipCmpController.SaveSelectedValues(masterDepartments[0].Id, programsForDeps, '2');


        List<String> ccGRForDeps = new List<String>();
        for(Integer i = 0; i < masterRecordCount - 5; i++){
            ccGRForDeps.add(masterCCGR[i].Id);
        }
        ATG_ManageRelationshipCmpController.SaveSelectedValues(masterDepartments[0].Id, ccGRForDeps, '3');

        List<String> deptsForCCGR = new List<String>();
        for(Integer i = 0; i < masterRecordCount - 5; i++){
            deptsForCCGR.add(masterDepartments[i].Id);
        }
        ATG_ManageRelationshipCmpController.SaveSelectedValues(masterCCGR[0].Id, deptsForCCGR, '4');

    }
}