/**
 * Created by mnazir001 on 11/26/18.
 */

@IsTest
private class ATG_ETORequestPdfController_Test {
    @IsTest
    static void testPdfGenerator() {

        List<Case> casesList = ATG_TestDataFactory.CreateETOCase(1);
        insert casesList;

        List<ATG_Requested_Traveller__c> travellers = ATG_TestDataFactory.CreateRTraveller(1);

        List<ATG_Requested_Traveller__c> etoRequestedTravellers = new List<ATG_Requested_Traveller__c>();
        for(ATG_Requested_Traveller__c traveller: travellers){
            traveller.Request_Number__c= casesList[0].Id;
            etoRequestedTravellers.add(traveller);
        }
        insert etoRequestedTravellers;


        ApexPages.StandardController sc = new ApexPages.StandardController(casesList[0]);
        ATG_ETORequestPdfController pdfController = new ATG_ETORequestPdfController(sc);

    }
}