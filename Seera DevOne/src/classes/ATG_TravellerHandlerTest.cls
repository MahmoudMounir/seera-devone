/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class ATG_TravellerHandlerTest {

    static testMethod void ATG_TravellerHandler1() {
        ATG_TestUtility objTest = new ATG_TestUtility();
        
        List<Case> caseList = ATG_TestDataFactory.CreateETOCase(1); 
        
        insert caseList;
        
        
        string bookingID = objTest.createflightbooking();
        ATG_Booking__c boookDtls = [select Id,Name from ATG_Booking__c where id = : bookingID  limit 1];
        
        
        caseList[0].PNR_Number__c = boookDtls.Name;
        update caseList;
        
        
        list<ATG_Requested_Traveller__c> rtList = ATG_TestDataFactory.CreateRTraveller(1);
        rtList[0].Request_Number__c = caseList[0].Id;
        rtList[0].ATG_PNR_Downloaded__c = true;
        insert rtList;
        
        
        rtList[0].ATG_Ticket_Number__c = '123456';
        update rtList;
      
        ATG_Traveller__c objTraveller = new ATG_Traveller__c();
        objTraveller.ATG_Booking__c = bookingID;
        objTraveller.ATG_Email__c = 'tets@test.test';
        objTraveller.ATG_Last_Name__c = 'sadsad';        
        objTraveller.ATG_Ticket_Number__c = '123456';
        objTraveller.ATG_AirLine_Fee__c = 20;
        objTraveller.ATG_Selling_Amount__c = '20';
        
        test.startTest();
            list<ATG_Traveller__c> soNewList = new list<ATG_Traveller__c>();
            soNewList.add(objTraveller);
            insert soNewList;
            
            soNewList[0].ATG_Void_Flag__c = true;
            Update soNewList;
            
            
            map<id,ATG_Traveller__c> soOldMap = new map<id,ATG_Traveller__c>();
            
            ATG_TravellerHandler objClass = new ATG_TravellerHandler();
            objClass.beforeInsert (soNewList);
            objClass.beforeUpdate (soNewList,soOldMap);
            objClass.beforeDelete (soNewList);
            objClass.afterInsert (soNewList);
           // objClass.afterUpdate (soNewList,soOldMap);
            objClass.afterDelete (soNewList);
        test.stopTest();
    }
}