/**
 * Created by mnazir001 on 9/26/18.
 */

public with sharing class ATG_ETORequestPdfController {

    public Case caseObj{set;get;}
    public Case translatedETODetails{set;get;}
    public list<ATG_Case_Details__c> caseDetails{get;set;}
    public list<ATG_Requested_Traveller__c> rtList{get;set;}

    public ATG_Requested_Traveller__c    employeeTraveller {set; get;}

    public String generatedDate {get; set;}
    public String generateTime {get; set;}
    public Integer totalPassengers {get; set;}


    public ATG_ETORequestPdfController(Apexpages.StandardController sc){


//        ATG_CommunityETORequestController.ETORequestDetail etoDetails = ATG_CommunityETORequestController.getBooking(sc.getId());


        Case etoDetails = getCaseETO(sc.getId());
        translatedETODetails = getTranslatedCaseETO(sc.getId());

        System.debug('Temp Case ETO ==>' +  etoDetails);
        System.debug('Temp Case ETO Travellers ==>' +  etoDetails.Requested_Travelers__r);
        System.debug('Temp Case ETO Flight ==>' +  etoDetails.Case_Details__r);


        if(etoDetails !=null){
//            caseObj = etoDetails.bookingCase;
//            caseDetails = etoDetails.caseDetails;
            caseObj = etoDetails;

            caseDetails = new List<ATG_Case_Details__c>();
            for(ATG_Case_Details__c flightCaseDetails: etoDetails.Case_Details__r){
                caseDetails.add(flightCaseDetails);
                System.debug('Flight City FROM =>' + flightCaseDetails.From_Arabic_Airport_Name__c);
                System.debug('Flight City TO =>' + flightCaseDetails.To_Arabic_Airport_Name__c);
            }
            System.debug('Flight Cities ==>' +  caseDetails);



            //  rtList = etoDetails.requestedTravelerInfo;

            String employeeId = apexpages.currentpage().getparameters().get('employeeId');
            System.debug('Case ETO ==>' + caseObj);


            if(employeeId != null){

//                for(ATG_Requested_Traveller__c     traveller: etoDetails.requestedTravelerInfo){
                for(ATG_Requested_Traveller__c   traveller: etoDetails.Requested_Travelers__r){
                    if(traveller.RecordType.DeveloperName != 'Dependent' && traveller.Id == employeeId){
                        employeeTraveller = traveller;
                        System.debug('Traveller 1 ==>' + traveller);
                        break;
                    }
                }
            }else{
//                for(ATG_Requested_Traveller__c     traveller: etoDetails.requestedTravelerInfo){
                for(ATG_Requested_Traveller__c   traveller: etoDetails.Requested_Travelers__r){
                    if(traveller.RecordType.DeveloperName != 'Dependent'){
                        employeeTraveller = traveller;
                        System.debug('Traveller 2 ==>' + traveller);
                        break;
                    }
                }
            }

            System.debug('Traveller Selected ==>' + employeeTraveller);

            rtList = new List<ATG_Requested_Traveller__c>();

            System.debug('Dependents ==>' + etoDetails.Requested_Travelers__r.size());
            for(ATG_Requested_Traveller__c traveller: etoDetails.Requested_Travelers__r){

                if(traveller.RecordType.DeveloperName == 'Dependant' &&
                        traveller.ATG_Employee_Traveler__c == employeeTraveller.Id){
                    rtList.add(traveller);
                }

            }

        }

       totalPassengers = rtList.size() + 1;

//        Datetime dt = Datetime.now();
//
//
//
//        HijriCalendar cal = new HijriCalendar(dt.year(), dt.month(), dt.day());
//        Date hijriDate = cal.getDate();
//
//        String strConvertedDate =
//                hijriDate.format();
//
//
//        generatedDate = formateToArabicNumerals(strConvertedDate);
//        generateTime = formateToArabicNumerals(dt.hourGmt() + ':' + dt.minuteGmt() + ':' + dt.secondGmt());
//
    }

    public case getCaseETO(String caseId){

        Case etoCase = [
                SELECT id, Status, ATG_Reference_Number__c, ATG_Reference_Date__c, MOH_Request_Number__c, ATG_CC_CCGR_Number_Value__c, ATG_Approved_Date__c,
                        ATG_Trip_Type__c, toLabel(ATG_Purpose_of_Visit__c), toLabel (Cabin_Class__c), ATG_Program_Name_Arabic__c,
                (SELECT id, RecordType.DeveloperName, ATG_Employee_Id__c, ATG_Traveller_Full_Name__c, ATG_Grade__c, ATG_Level__c, ATG_Job_Title__c,
                        ATG_Date_of_Birth__c, ATG_ID_Type__c, ATG_Passport_ID__c, ATG_National_ID__c, ATG_Employee_Traveler__c, ATG_Employee__r.ATG_Job_Title__c,
                        ATG_Date_of_Issue__c, ATG_Ticket_Amount__c,ATG_Ticket_Number__c, ATG_Total_Amount__c,
                        toLabel(Relationship__c), toLabel(ATG_Nationality__c), toLabel(ATG_Gender__c)

                    FROM Requested_Travelers__r
                ),
                (SELECT id, From_Arabic_Airport_Name__c, To_Arabic_Airport_Name__c FROM Case_Details__r)

                FROM Case
                WHERE id=: caseId
        ];

        return  etoCase;
    }

    public case getTranslatedCaseETO(String caseId){
        return [SELECT toLabel(Status), toLabel(ATG_Trip_Type__c) FROM Case WHERE id=: caseId];
    }


//    private String formateToArabicNumerals(String str){
//        // String str = 'MOH1440';
//
//        String[] arabicChars = new String[]{'٠','١','٢','٣','٤','٥','٦','٧','٨','٩'};
//
//        String builder = '';
//        for(Integer i =0;i<str.length();i++)
//        {
//
//            //System.debug('IsNumeric =>' + String.valueOf(str[i]));
//
//            if(str.charAt(i) - 48 >= 0 && str.charAt(i) - 48 <= 9)
//            {
//                Integer arabicInteger = ((Integer)(str.charAt(i))) - 48;
//                builder += String.valueOf(arabicChars[arabicInteger]);
//            }
//            else
//            {
//                builder +=  String.valueOf(String.fromCharArray(new Integer[]{str.charAt(i)}));
//            }
//        }
//
//        return builder;
//    }
}