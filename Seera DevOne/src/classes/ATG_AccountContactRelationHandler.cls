/**************************************************************************************************
* Name               : ATG_AccountContactRelationHandler                                                               
* Description        : Trigger Handler for the AccountContactRelation SObject. This class implements the ITrigger
					   interface to help ensure the trigger code is bulkified and all in one place.                                       
* Created Date       : 16th August 2018                                                                *
* Created By         : Swati Sehrawat (PWC Digital)                                              
* -------------------------------------------------------------------------------------------------
* Version		Author		Date		Comment                                                           *

**************************************************************************************************/
public without sharing class ATG_AccountContactRelationHandler implements ATG_TriggerInterface{
	 
	// Constructor
	public void ATG_AccountContactRelationHandler(){}
	
	public map<id,id> CorporateToPrimaryContact = new map<id,id>();
	
	public void beforeInsert(list<SObject> soNewList){ 
		try{
			list<AccountContactRelation> tempAccountContactRelation = new list<AccountContactRelation>();
			map<id,id> CompanyNameMap = new map<id,id>();
			
			for(AccountContactRelation obj : (list<AccountContactRelation>)soNewList){
				if(obj.AccountId!=null && obj.ContactId!=null && obj.IsActive==true &&
				   string.isNotBlank(obj.Roles) && obj.Roles.contains('Primary Contact')){
					tempAccountContactRelation.add(obj);	
					CorporateToPrimaryContact.put(obj.AccountId, obj.contactID);
				}	
				
				if(obj.AccountId!=null && obj.ContactId!=null && obj.IsActive==true  &&
				   string.isNotBlank(obj.Roles) && obj.Roles.contains('Employee') && obj.AccountID!=Label.MOH_Account){
					CompanyNameMap.put(obj.ContactId, obj.AccountId);
				}
			}	
			
			system.debug('----CompanyNameMap-----'+CompanyNameMap);
			if(tempAccountContactRelation!=null && tempAccountContactRelation.size()>0){
				updatePrimaryContactOnAccount(tempAccountContactRelation);		
			}
			
			if(CompanyNameMap!=null && CompanyNameMap.size()>0){
				updateCompanyNameOnAccount(CompanyNameMap);		
			}
		}catch(exception ex){
			ATG_utility.LogException(ex);
		}
	}
	
	public void beforeUpdate(list<SObject> soNewList,map<id,SObject> soOldMap){
		try{
			list<AccountContactRelation> tempAccountContactRelation = new list<AccountContactRelation>();
			map<Id,AccountContactRelation> TriggerOldMap = (map<Id,AccountContactRelation>)soOldMap;
			
			for(AccountContactRelation obj : (list<AccountContactRelation>)soNewList){
				if(obj.AccountId!=null && obj.ContactId!=null && obj.IsActive==true &&
				   string.isNotBlank(obj.Roles) && obj.Roles.contains('Primary Contact') &&
				   obj.Roles!=TriggerOldMap.get(obj.id).Roles){
					tempAccountContactRelation.add(obj);	
					CorporateToPrimaryContact.put(obj.AccountId, obj.contactID);
				}	
			}	
			
			if(tempAccountContactRelation!=null && tempAccountContactRelation.size()>0) {
				updatePrimaryContactOnAccount(tempAccountContactRelation);		
			}	
		}catch(exception ex){
			system.debug('-------'+ex.getLineNumber());
			ATG_utility.LogException(ex);
		}
	}

	public void beforeDelete(list<SObject> soNewList){}

	public void afterInsert(list<SObject> soNewList){
	}

	public void afterUpdate(list<SObject> soNewList,map<id,SObject> soOldMap){
	}

	public void afterDelete(list<SObject> soNewList){}
	
	
	//------------------------------Method Definition-----------------------------------------//
    
	public void updatePrimaryContactOnAccount(list<AccountContactRelation> tempAccountContactRelation){
		
		system.debug('----tempAccountContactRelation---'+tempAccountContactRelation);
		system.debug('----CorporateToPrimaryContact---'+CorporateToPrimaryContact);
		
		map<id, Account> corporateAccounts = new map<id, Account>();
		map<id, Account> primaryContacts = new map<id, Account>();
		list<Account> accountToUpdate = new list<Account>();
		
		if(CorporateToPrimaryContact!=null && CorporateToPrimaryContact.size()>0){
			for(Account obj : [select id, IsPersonAccount,Phone,ATG_Primary_Phone__c,ATG_Email__c,personEmail, personContactID,ATG_Primary_Contact__c,personMobilePhone from Account where recordTypeID!=: ATG_ConstantsUtility.MASTER_PERSON_ACCOUNT_RECORDTYPEID and
							   recordTypeID!=: ATG_ConstantsUtility.MASTER_BUSINESS_ACCOUNT_RECORDTYPEID and 
							   (id IN : CorporateToPrimaryContact.keyset() or personContactID IN : CorporateToPrimaryContact.values())]){
				if(obj.isPersonAccount == false)
					corporateAccounts.put(obj.id,obj);
				else
					primaryContacts.put(obj.personContactID,obj);
			}	
		}	
		
		system.debug('----corporateAccounts---'+corporateAccounts);
		system.debug('----primaryContacts---'+primaryContacts);
		
		if(tempAccountContactRelation!=null && tempAccountContactRelation.size()>0 &&
		   corporateAccounts!=null && corporateAccounts.size()>0 && primaryContacts!=null && primaryContacts.size()>0){
			for(AccountContactRelation obj : tempAccountContactRelation){
				corporateAccounts.get(obj.AccountID).ATG_Email__c = 	primaryContacts.get(obj.contactID).personEmail;
				corporateAccounts.get(obj.AccountID).ATG_Primary_Phone__c = primaryContacts.get(obj.contactID).personMobilePhone;
				corporateAccounts.get(obj.AccountID).ATG_Primary_Contact__c = primaryContacts.get(obj.contactID).id;
				
				system.debug('----1---'+corporateAccounts.get(obj.AccountID));
			}	
			
			update corporateAccounts.values();	
		}
	}
	
	public void updateCompanyNameOnAccount(map<id,id> CompanyNameMap){
		map<id, Account> updatedAccounts = new map<id, Account>();
		
		for(Account obj : [select id,PersonContactId,ATG_Company_Name__c from Account where PersonContactId IN : CompanyNameMap.keyset() and isPersonAccount = true
						   and recordTypeID!=: ATG_ConstantsUtility.MASTER_PERSON_ACCOUNT_RECORDTYPEID and
							   recordTypeID!=: ATG_ConstantsUtility.MASTER_BUSINESS_ACCOUNT_RECORDTYPEID]){
			
			obj.ATG_Company_Name__c = CompanyNameMap.get(obj.PersonContactId);
			updatedAccounts.put(obj.id,obj);
		}	
		
		if(updatedAccounts!=null && updatedAccounts.size()>0){
			update updatedAccounts.values();
		}	
		
	}
}