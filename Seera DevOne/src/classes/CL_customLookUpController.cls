public class CL_customLookUpController {

class CL_getAttachment {
    @AuraEnabled
    public String name;
    @AuraEnabled
    public DateTime fileDate;
    @AuraEnabled
    public String fileId;
    @AuraEnabled
    public String fileType;
    }
    
    @AuraEnabled   
    public static List<sObject> fetchLookUpValues(String searchKeyWord){ 
       
        String searchKey = searchKeyWord + '%'; 
        
        List<sObject> returnList = new List<sObject>();
      
        // Create a Dynamic SOQL Query For Fetch Record List with LIMIT 5   
        String sQuery =  'select id, Name from CL_Vehicle__c where Name LIKE: searchKey order by createdDate DESC';
       // String sQuery =  'Select id,Name From CL_Vehicle__c where Name LIKE \'%'+String.escapeSingleQuotes(searchKeyWord)+'%\'';
       
        List<sObject> lstOfRecords = Database.query(sQuery);
        for (sObject obj: lstOfRecords) {
            returnList.add(obj);
       }
       
     
         return returnList;
    }
    @AuraEnabled
    public static List<CL_Vehicle__c> fetchVehicleValues(String selectedVehicle){ 
        
        List<sObject> returnList = new List<sObject>();
        String searchKey = '\''+selectedVehicle + '\''; 
        // Create a Dynamic SOQL Query For Fetch Record List with LIMIT 5   
        String sQuery =  'select id, Name from CL_Vehicle__c where Name =: selectedVehicle';
        List<CL_Vehicle__c> lstOfRecords = Database.query(sQuery);
        for (CL_Vehicle__c obj: lstOfRecords) {
            returnList.add(obj);
       }
       
     
         return returnList;
    }
    @AuraEnabled 
    public static String fetchUser(){
    List<Profile> PROF = [SELECT Id, Name FROM Profile WHERE Id=:userinfo.getProfileId() LIMIT 1];
    String MyProflieName = PROF[0].Name;
    system.debug('Profile Name :' +MyProflieName );
            return MyProflieName;
    }
    @AuraEnabled
    public static List<CL_customLookUpController.CL_getAttachment> getAllFiles(String getVehicleID) {
     List<Profile> PROF = [SELECT Id, Name FROM Profile WHERE Id=:userinfo.getProfileId() LIMIT 1];
     String MyProflieName = PROF[0].Name;
     system.debug('Profile Name :' +MyProflieName );
      List<CL_customLookUpController.CL_getAttachment> getAttacmentLst = new List<CL_customLookUpController.CL_getAttachment>();
     //if(MyProflieName == 'CL Fleet Manager')
     if(MyProflieName == 'System Administrator')
     {
         System.debug('vehcle id-->'+getVehicleID);
         String squery = 'SELECT Id, Name, CL_Driver__c FROM CL_Vehicle__c WHERE Id =\''+getVehicleID+'\'';
         List<CL_Vehicle__c> records = database.query(squery);
         List<CL_Vehicle__c> accountID = [Select Id, Name, CL_Driver__c FROM CL_Vehicle__c WHERE Id=:getVehicleID];
         system.debug('accountID --> '+accountID+'   --rec -->'+records);
         String accountDet = string.valueOf(accountID[0].CL_Driver__c);
         system.debug('accountDet --> '+accountDet);
         List<Attachment> lAtt = [Select Id, Name, ContentType, LastModifiedDate, BodyLength
                                  from Attachment 
                                  where ParentID=:accountDet];
         system.debug('Files:' + lAtt);
                                 if(lAtt !=null && lAtt.size()>0){
            for(Attachment att : lAtt){
                CL_customLookUpController.CL_getAttachment getAttacment = new CL_customLookUpController.CL_getAttachment();
                getAttacment.fileId = att.Id;
                getAttacment.name = att.Name;
                getAttacment.fileType = att.ContentType;
                getAttacment.fileDate = att.LastModifiedDate;
        
                getAttacmentLst.add(getAttacment);
            }
        }    
     } 
        
      else if(MyProflieName == 'CL Driver')
     {
      ID contactId = [Select contactid from User where id =: Userinfo.getUserid()].contactId;
      ID AccID  = [Select AccountID from Contact where id =: contactId].AccountId;
      
      List<Attachment> lAtt = [Select Id, Name, ContentType, LastModifiedDate, BodyLength
                            from Attachment 
                            where ParentID=:AccID];
            system.debug('Files:' + lAtt);
                                 if(lAtt !=null && lAtt.size()>0){
            for(Attachment att : lAtt){
                CL_customLookUpController.CL_getAttachment getAttacment = new CL_customLookUpController.CL_getAttachment();
                getAttacment.fileId = att.Id;
                getAttacment.name = att.Name;
                getAttacment.fileType = att.ContentType;
                getAttacment.fileDate = att.LastModifiedDate;
        
                getAttacmentLst.add(getAttacment);
            }
        }    
     } 
        return getAttacmentLst;
  }
   @AuraEnabled 
   public static List<CL_customLookUpController.CL_getAttachment> getAllContentFiles(String getVehicleID) {
     /* CL_PortalController.UserDetailCls usd=new CL_PortalController.UserDetailCls(Userinfo.getUserid());
      System.debug('usd.LumiRole-->'+usd.LumiRole+' ClientID-> '+usd.clientAccount.id+' DriverID->'+usd.driverAccount.id);
       if(usd.LumiRole == 'Admin'){  //client ID - Business Account ID 
          system.debug('usd.clientAccount.Id: ' +usd.clientAccount.Id);
          return getContentDocList(usd.clientAccount.Id);
           
       }
      else{
          system.debug('usd.clientAccount.Id: ' +usd.driverAccount.Id);
           return getContentDocList(usd.driverAccount.Id);
    }*/
      CL_Vehicle__c veh=[select CL_Driver__c from CL_Vehicle__c where ID=:getVehicleID][0];
       return getContentDocList(veh.CL_Driver__c);
  }
 @AuraEnabled       
 public static List<CL_customLookUpController.CL_getAttachment> getContentDocList(string id){
        List<CL_customLookUpController.CL_getAttachment> getDocLst = new List<CL_customLookUpController.CL_getAttachment>();
        List<ContentDocumentLink> lDocs = [SELECT Id, LinkedEntityId, ContentDocumentId, Visibility, IsDeleted, ShareType,
                                                   ContentDocument.Title, ContentDocument.createdDate, ContentDocument.FileType
                                                  FROM ContentDocumentLink Where LinkedEntityId=:id];
     system.debug('Files:' + lDocs);
      if(lDocs !=null && lDocs.size()>0){
        for(sObject fls : lDocs){
                CL_customLookUpController.CL_getAttachment getAttacment = new CL_customLookUpController.CL_getAttachment();
                getAttacment.fileId = (String)fls.get('ContentDocumentId');
                getAttacment.name = (String)fls.getSobject('ContentDocument').get('Title');
                getAttacment.fileType = (String) fls.getSobject('ContentDocument').get('FileType');
                getAttacment.fileDate =(Datetime)  fls.getSobject('ContentDocument').get('createdDate');
                getDocLst.add(getAttacment);
            System.debug('getAttacmentLst-->'+getDocLst);
             }
          }    
            return getDocLst;
       }
     }