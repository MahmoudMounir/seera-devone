/* Controller associated with pages rendering the survey.
 * Used by SurveyPage, ResultsPage, TakeSurvey
 */
global virtual without sharing class ViewSurveyController {
    
    public string qQuestion {get; set;}
    public string qChoices {get; set;}
    public string surveyName {get; set;}
    public string surveyHeader {get; set;} 
    public string surveyId  {get; set{this.surveyId = value;init();}}   
    public string renderSurveyPreview {get; set;}  
    public string questionName {get; set;}  
    public string questionType {get; set;}
    public string templateURL {get; set;}
    public string surveyThankYouText {get; set;}
    public string surveyContainerCss {get; set;}
    public string surveyThankYouURL {get; set;}
    public string caseId {get; set;}
    public string contactId {get;set;}
    public string accountID {get;set;}
    public String ownerName{set;get;}
    public string bookingID {get;set;}
    public string anonymousAnswer {get;set;}
    public string baseURL {get;set;}
    public string userId{get;set;}
    public string userName{get;set;}
    public string surveyTakerId {get;set;}
    public string chatKey {get;set;}
    public string livechatID {get;set;}
    
    public Integer allQuestionsSize {get; set;}
    
    public list<SelectOption> anonymousOrUser {get;set;}
    public list<SelectOption> singleOptions {get; set;} 
    public list<SFQuestion> allQuestions {get; set;}
    public list<string> responses {get; set;}
    public list<string> newOrder {get;set;}
    
    public Boolean questionRequired {get; set;}
    public Boolean isInternal {get;set;}
    public Boolean qRequired {get; set;}
    public Boolean thankYouRendered{get;set;}
    public String language{set;get;}
    
    public Map<String,String> thumbsUpQuestionMap{set;get;}
    
    public account customer;
    public boolean isSubmitted{get;set;}
    public boolean disableSubmitButton{set;get;}
    
    /* Retrieves the list of questions, the survey name, after retrieving the 
       necessary parameters from the url.
    */
 //------------------------------------------------------------------------------// 
    public ViewSurveyController(ApexPages.StandardController stdController){
        // Get url parameters
        surveyId = Apexpages.currentPage().getParameters().get('id');
        caseId   = Apexpages.currentPage().getParameters().get('caId');
        contactId = Apexpages.currentPage().getParameters().get('cId'); 
        accountID = Apexpages.currentPage().getParameters().get('aID'); 
        bookingID = Apexpages.currentPage().getParameters().get('bID'); 
        language = Apexpages.currentPage().getParameters().get('language');
        ownerName = Apexpages.currentPage().getParameters().get('oId');
        
        system.debug('******'+ownerName);
        
        if(language == null || language == ''){
            language = 'en';
        }
        disableSubmitButton = false;
        system.debug('*****surveyId'+surveyId);
        
        /*if(caseId ==null || caseId.length()<15)
            caseId = 'none';
        if(contactId ==null || contactId.length()<15)
            contactId = 'none';
        */
        
        // By default the preview is not showing up
        renderSurveyPreview = 'false';
        init();
    } 
    
    public ViewSurveyController(viewShareSurveyComponentController controller){
        surveyId = Apexpages.currentPage().getParameters().get('id');
        caseId   = Apexpages.currentPage().getParameters().get('caId');
        contactId = Apexpages.currentPage().getParameters().get('cId'); 
        accountID = Apexpages.currentPage().getParameters().get('aID'); 
        bookingID = Apexpages.currentPage().getParameters().get('bID'); 
        chatKey = Apexpages.currentPage().getParameters().get('chatKey');
         
        system.debug('-----chatKey------'+chatKey);
        system.debug('*****surveyId');
        /*if(caseId ==null || caseId.length()<15)
            caseId = 'none';
        if(contactId ==null || contactId.length()<15)
            contactId = 'none';
		*/
        // By default the preview is not showing up
        renderSurveyPreview = 'false';
        init();
    }
    
    private Boolean isSurveySubmitted(String caseId,String bookingId,String liveChatId){
        Boolean isSubmitted = false;
        
        if(null != caseId){
            for(SurveyTaker__c surveyTaken:[SELECT Id FROM SurveyTaker__c WHERE 
                                            Case__c=:caseId ]){
                isSubmitted = true;
            }
        }else if(null != bookingId){
            for(SurveyTaker__c surveyTaken:[SELECT Id FROM SurveyTaker__c WHERE 
                                            ATG_Booking__c=:bookingId ]){
                isSubmitted = true;
            }
        }else if(null != livechatId){
            for(SurveyTaker__c surveyTaken:[SELECT Id FROM SurveyTaker__c WHERE 
                                            ATG_Live_Chat_Transcript__c=:livechatID ]){
                isSubmitted = true;
            }
        }
        
        return isSubmitted;
    }
    
    
    public void init()  {
        
       disableSubmitButton = false;
         chatKey = Apexpages.currentPage().getParameters().get('chatKey');
                
        system.debug('-----chatKey------'+chatKey);   
        system.debug('-----chatKey------'+chatKey);
        
        if (surveyId != null){ 
            // Retrieve all necessary information to be displayed on the page
            allQuestions = new list<SFQuestion>();
            setupQuestionList();
            setSurveyNameAndThankYou(surveyId);
            anonymousOrUser = new list<SelectOption>();
            anonymousOrUser.add(new SelectOption('Anonymous',System.Label.LABS_SF_Anonymous));
            anonymousOrUser.add(new SelectOption('User','User ' + UserInfo.getFirstName() + ' ' + UserInfo.getLastName()));
            anonymousAnswer = 'Anonymous';
            isInternal =true;
            newOrder = new list<string>();
            string urlBase = URL.getSalesforceBaseUrl().toExternalForm();
            baseURL = urlBase;
            
            userId = UserInfo.getUserId();
            userName = UserInfo.getName();
            
            string profileId = UserInfo.getProfileId();
            thankYouRendered=false;
            try{
                Profile p = [select Id, UserType from Profile where Id=:profileId];
                if (p.UserType == 'Guest')
                    isInternal = false;
                else
                    isInternal = true;
                
                if(string.isNotBlank(AccountID) && AccountID!=null && 
                   string.isNotBlank(bookingID) && bookingID!=null){
                    
                    checkExistingSurvey();  
                }
                
                
                if(string.isNotBlank(AccountID) && AccountID!=null){
                    customer = new Account();
                    list<Account> tempAccountList = [select id, ATG_NPS_Recent_Score__c from Account where id =: AccountID];
                    if(tempAccountList!=null && tempAccountList.size()>0){
                        customer =  tempAccountList[0];
                    }
                }
                
                
                if(string.isNotBlank(chatKey)){
                    
                    list<LiveChatTranscript> tempList = [select id,ChatKey__c,accountID,CaseId from LiveChatTranscript where ChatKey__c =: chatKey];
                    if(tempList!=null && tempList.size()>0){
                        system.debug('-----1------'+tempList[0].accountID);
                        
                        livechatID = tempList[0].id;
                        
                        if(tempList[0].caseId != null){
                            caseId = tempList[0].CaseId;
                        }
                        
                        list<Account> tempAccountList = [select id, ATG_NPS_Recent_Score__c from Account where id =: tempList[0].accountID];
                        if(tempAccountList!=null && tempAccountList.size()>0){
                            customer =  tempAccountList[0];
                            accountID = customer.id;
                        }   
                    }
                    
                    system.debug('-----accountID------'+accountID);
                }
                 isSubmitted = isSurveySubmitted(caseId, bookingId,liveChatID);
            }
            catch (Exception e){
                isInternal = false;
            }
            
        }       
    }

//----------------------------------------------------------------------------//    
    
    /* Called during the setup of the page. Retrieve questions and responses from DB and inserts them in 2 lists. */
    public Integer setupQuestionList(){
        getAQuestion();
        return allQuestions.size();
    }
    
   /** Sets the survey's name variable   *  param: sID   The survey ID as specified in the DB */
    public void setSurveyNameAndThankYou(string sId){
        Survey__c s = [SELECT Name, Id, URL__c, Thank_You_Text__c, thankYouText__c, thankYouLink__c, Survey_Header__c, Survey_Container_CSS__c FROM Survey__c WHERE Id =:sId];
        surveyName = s.Name;
        surveyHeader = s.Survey_Header__c;
        templateURL = s.URL__c+'id='+sId;//+'&cId={!Contact.Id}'+'&caId='+'{!Case.id}';
        surveyThankYouText = s.Thank_You_Text__c;
        if (surveyThankYouText == null)
            surveyThankYouText = System.Label.LABS_SF_Survey_Submitted_Thank_you;
        surveyThankYouURL = s.thankYouLink__c;
        surveyContainerCss = s.Survey_Container_CSS__c;
    }
    
//------------------------------------------------------------------------------//   
  public Pagereference updateSurveyName(){
      try{
        Survey__c s = [SELECT Name, Id, URL__c, thankYouText__c, thankYouLink__c FROM Survey__c WHERE Id =:surveyId];
        s.Name = surveyName;
        update s;
      }catch (Exception e){
        Apexpages.addMessages(e);
      }
      return null;
  } 

//------------------------------------------------------------------------------//      
    public Pagereference updateSurveyThankYouAndLink(){
        try{
            Survey__c s = [SELECT Name, Id, URL__c, thankYouText__c, thankYouLink__c FROM Survey__c WHERE Id =:surveyId];
            s.thankYouText__c = surveyThankYouText;
            s.thankYouLink__c = surveyThankYouURL;
            update s;
        }catch(Exception e){
            Apexpages.addMessages(e);
        }
        return null;
    }

//------------------------------------------------------------------------------//    
  /** When requested from the page - when the user clicks on 'Update Order' - this function will reorganize the list so that it is displayed in the new order   */
    public Pagereference refreshQuestionList(){
        setupQuestionList();
        return null;
    }
//------------------------------------------------------------------------------//      

//------------------------------------------------------------------------------//    
    public static boolean checkRequired(string response, Survey_Question__c question){
        if(question.Required__c == true){
            if(response == null || response =='NO RESPONSE')
                return false;
        }
        return true;
    } 

    /** Redirects the page that displays the detailed results of the survey, 
       from all users who took the survey.
    */
    public PageReference resultPage() {
        return new PageReference('/apex/ResultsPage?id='+surveyId);
    }

 
//------------------------------------------------------------------------------//  

//------------------------------------------------------------------------------//  

   /** 
    */
    public list<string> getResponses() {
        list<SurveyQuestionResponse__c> qr = [Select Survey_Question__c, SurveyTaker__c, Response__c, Name From SurveyQuestionResponse__c limit 100];
        list<string> resp = new list<string>();
        for (SurveyQuestionResponse__c r : qr) {
            resp.add(r.Response__c);
        }
        return resp;
    }  


    
  /** Fills up the List of questions to be displayed on the Visualforce page
   */   
    public list<SFQuestion> getAQuestion() {
            qQuestion = '';
            qChoices ='';
        
        list<Survey_Question__c> allQuestionsObject = 
                                        [Select s.Type__c, s.Id, s.Survey__c, s.Required__c, s.Question__c, 
                                        s.OrderNumber__c, s.Name, s.Choices__c , s.hascomments__c
                                        From Survey_Question__c s 
                                        WHERE s.Survey__c =: surveyId ORDER BY s.OrderNumber__c];
        System.debug(allQuestionsObject);
        allQuestions = new list<SFQuestion>();
        
        Double old_OrderNumber = 0;
        Double new_OrderNumber;
        Double difference = 0;
        /* Make sure that the order number follow each other (after deleting a question, orders might not do so) */
        for (Survey_Question__c q : allQuestionsObject){ 
            new_OrderNumber = q.OrderNumber__c;
            difference = new_OrderNumber - old_OrderNumber - 1;
            if (difference > 0) {
                Double dd = double.valueOf(difference);
                Integer newOrderInt = dd.intValue();
                q.OrderNumber__c -= Integer.valueOf(newOrderInt); 
            }
            old_OrderNumber = q.OrderNumber__c;
            SFQuestion theQ = new SFQuestion(q);
            theQ.hasComments = q.hascomments__c;
            allQuestions.add(theQ);
        }
        allQuestionsSize = allQuestions.size();
        return allQuestions;
    }   
    
    public void submitResults() {
        try {
            string comments = '';
            list<SurveyQuestionResponse__c> sqrList = new list<SurveyQuestionResponse__c>();
            
            string rating = '';
            for (SFQuestion q : allQuestions) {
                
                SurveyQuestionResponse__c sqr = new SurveyQuestionResponse__c();
                if (q.renderSelectRadio == 'true') {
                    if (q.required && (q.selectedOption == null || q.selectedOption == '')) {
                        Apexpages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Please fill out all required fields'));
                        return;
                    }
                    if (q.selectedOption == null || q.selectedOption == '') {
                        sqr.Response__c = '';
                    } else {
                        sqr.Response__c = q.singleOptions.get(Integer.valueOf(q.selectedOption)).getLabel();
                    }
                    sqr.Survey_Question__c = q.Id;
                    
                } else if (q.renderFreeText == 'true') {
                    if (q.required && q.choices == '') {
                        Apexpages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Please fill out all required fields'));
                        return;
                    }
                    sqr.Response__c = q.choices;
                    sqr.Survey_Question__c = q.Id;
                } else if (q.renderSelectCheckboxes == 'true') {
                    if (q.required && (q.selectedOptions == null || q.selectedOptions.size() == 0)) {
                        Apexpages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Please fill out all required fields'));
                        return;
                    }
                    for (string opt : q.selectedOptions) {
                        sqr = new SurveyQuestionResponse__c();
                        if (opt == '' || opt == null) {
                            sqr.Response__c = '';
                        } else {
                            sqr.Response__c = q.multiOptions.get(Integer.valueOf(opt)).getLabel();
                        }
                        sqr.Survey_Question__c = q.Id;
                    }
                } else if (q.renderSelectRow == 'true') {
                    if (q.required && (q.selectedOption == null || q.selectedOption == '')) {
                        Apexpages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Please fill out all required fields'));
                        return;
                    }
                    if (q.selectedOption == null || q.selectedOption == '') {
                        sqr.Response__c = '';
                    } else {
                        sqr.Response__c = q.rowOptions.get(Integer.valueOf(q.selectedOption)).getLabel();
                    }
                    sqr.Survey_Question__c = q.Id;
                }/*else if(q.renderThumbsUp == 'true'){
                    if(q.required && (q.selectedThumbsValue == null || q.selectedThumbsValue == '')){
                     Apexpages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Please fill out all required fields'));
                        return;
                    }
                    if (q.selectedThumbsValue == null || q.selectedThumbsValue == '') {
                        sqr.Response__c = '';
                    } else {
                        sqr.Response__c = q.selectedThumbsValue;
                    }
                    sqr.Survey_Question__c = q.Id;
                }*/
                
                system.debug('---q.hasComments----'+q.hasComments);
                system.debug('---c----'+sqr.Response__c);
                if(q.hasComments == true)
                    comments = sqr.Response__c;
                else
                    rating = sqr.Response__c;
                    
                sqrList.add(sqr);
            }
           disableSubmitButton = true;
            system.debug('---rating----'+rating);
            if(AddSurveyTaker()){
                System.debug('Here 3');
                list<case> tempLowCase = new list<case>();
                
                
                
                for (SurveyQuestionResponse__c sqr : sqrList){
                    sqr.SurveyTaker__c = surveyTakerId;
                    
                }
                
                /*
                ATG_Survey_Configuration__c customSettings = ATG_Survey_Configuration__c.getValues('Create Case');
                if(rating.contains('Dissatisfied') && customSettings!=null &&
                   customSettings.Owner__c!=null &&
                   customSettings.Owner__c!=''){
                    case objCase = new case();
                    objCase.ATG_Survey_Taken__c = surveyTakerId;
                    objCase.AccountId = accountID;
                    objCase.ATG_Booking__c = bookingID;
                    objCase.Status = 'New';
                    objCase.Origin = 'Survey';
                    objCase.Subject = 'Low Customer Rating: ' +rating;
                    objCase.description = string.isNotBlank(comments) ?comments : '';
                    objCase.recordTypeID = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Complaint').getRecordTypeId();
                    objCase.ownerID = customSettings.Owner__c;
                    System.debug('Pwner Id: ' + objCase.ownerID);
                    
                    tempLowCase.add(objCase);     
                } 
                
                
                
                if(tempLowCase!=null && tempLowCase.size()>0)
                    insert tempLowCase; 
                
                customer.ATG_NPS_Recent_Score__c = Rating;
                update customer;*/
               
                /* Update the CSAT Score based on the Rating*/
                system.debug('*****');
                system.debug(rating);
                system.debug(customer);
                if(rating != null && customer != null){
                   
                    for(ATG_Survey_Rating__c surveyRatingCS:ATG_Survey_Rating__c.getAll().Values()){
                        system.debug(surveyRatingCS.ATG_Survey_Response__c);
                        system.debug(rating);
                        system.debug(surveyRatingCS.ATG_Survey_Response__c.trim().equalsIgnoreCase(rating.trim()));
                        if(surveyRatingCS.ATG_Survey_Response__c != null &&
                           surveyRatingCS.ATG_Survey_Response__c.trim().equalsIgnoreCase(rating.trim())){
                               system.debug('****entered');
                               if(bookingId == null || string.isBlank(bookingId)){
                                    customer.ATG_CSAT_Recent_Score__c = surveyRatingCS.ATG_CSAT_Score__c; //for all the otehr surveys 
                                                                                                         // except booking and social media add CSAT score
                               }
                               
                               
                           }
                    }
                    
                    if(bookingId != null && !string.isEmpty(bookingId)){
                        customer.ATG_NPS_Recent_Score__c = rating;
                    }
                    
                    system.debug('****' + customer);
                    update customer;
                }
                
                
                
                
                insert sqrList;
                thankYouRendered=true;
            }
        }catch(Exception e){
            System.debug('Exception: ' + e.getMessage());
                System.debug('line: ' + e.getLineNumber());
            if(isInternal) {
                Apexpages.addMessages(e);
            }else{
                System.debug('Exception: ' + e.getMessage());
                System.debug('line: ' + e.getLineNumber());
                Apexpages.addMessage(new ApexPages.Message(ApexPages.Severity.Error, 'Some error occured while saving response'));
            }
        }
    }
    
    
    private Boolean AddSurveyTaker() {
        string userId;
        if (surveyId == null){
            return false;
        }
        
        system.debug('***caseId'+caseId);
        
        if(caseId!=null && (caseId.toUpperCase() =='NONE'|| caseId.length()<5))
          caseId = null;    
        if(contactId!=null && (contactId.toUpperCase() =='NONE'|| contactId.length()<5))
          contactId = null;         
        if (anonymousAnswer != 'Anonymous')
            userId = UserInfo.getUserId();
        else
            userId = null;
        
        if(anonymousAnswer != 'Anonymous' && (contactId != null || caseId != null)){
            list<SurveyTaker__c> check = [Select Contact__c, Survey__c, Case__c, User__c From SurveyTaker__c Where Contact__c=:contactId and Survey__c=:surveyId and Case__c = :caseId and User__c=:UserId];
            if(check != null && check.size()>0){
                Apexpages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, System.Label.LABS_SF_You_have_already_taken_this_survey));
                return false;       
            }
        }
        SurveyTaker__c st = new SurveyTaker__c();
        st.Contact__c = contactId;
        st.Survey__c = surveyId;
        st.Taken__c = 'false';
        st.Case__c = caseId;
        st.User__c = userId;
        if(string.isNotBlank(accountId)){
            st.atg_customer__c = accountId;
        }
        if(string.isNotBlank(bookingID)){
            st.atg_booking__c = bookingID;
        }
        if(string.isNotBlank(livechatID))
            st.ATG_Live_Chat_Transcript__c = livechatID;
        String decryptedOwnerName = '';
        if(string.isNotBlank(ownerName)){
            try{
                if(ownerName.contains(' ')){
                    ownerName = ownerName.replace(' ', '+');
                }
                decryptedOwnerName = ATG_utility.DecryptPassword(ownerName);
            }catch(Exception ex){
                ATG_utility.LogException(ex);
            }
        }
        
        st.ATG_Current_Case_Owner_Name__c = decryptedOwnerName;
        
        String businessUnit = null;
        if(null != bookingId){
        for(ATG_Booking__c objBooking:[SELECT ATG_Business_Unit__c FROM ATG_Booking__c WHERE
                                       Id=:bookingId LIMIT 1]){
                                           businessUnit = objBooking.ATG_Business_Unit__c;                                   
                                       }
        }
        
        
        if(null != caseId){
            for(Case objCase:[SELECT ATG_Bu_FROM_Case__c FROM Case WHERE Id=:caseId Limit 1]){
                businessUnit = objCase.ATG_Bu_FROM_Case__c;
            }
        }
        
        
        system.debug('****');
        if(null != liveChatID){
            for(LiveChatTranscript lctObj:[SELECT Business_Unit__c FROM LiveChatTranscript Where Id=:liveChatID Limit 1]){
                businessUnit = lctObj.Business_Unit__c;
            }
        }
        
        
        system.debug('***'+liveChatID);
        system.debug('***'+businessUnit);
        st.Business_Unit__c = businessUnit;    
        insert st;  
        surveyTakerId = st.Id;
        return true;    
    }
    
    public void checkExistingSurvey(){
        list<SurveyTaker__c> tempList = new list<SurveyTaker__c>();
        tempList = [select id from SurveyTaker__c where Survey__c=:surveyId and atg_customer__c=:accountId and atg_booking__c=:bookingId];
        if(tempList!=null && tempList.size()>0){
            thankYouRendered = true;    
        }else{
            thankYouRendered = false;
        }
    }
}