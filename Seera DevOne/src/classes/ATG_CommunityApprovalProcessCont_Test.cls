/**
 * Created by mnazir001 on 11/26/18.
 */

@IsTest
private class ATG_CommunityApprovalProcessCont_Test {
    @IsTest
    static void testSubmitETOForApproval() {
	
        Account acc = ATG_TestDataFactory.createEmployeeAccount();
        System.debug('Account ==>' + acc);
        insert acc;


        System.assert(acc != null, 'Unable to create account');


        Account PersonAccount = [SELECT id, PersonContactId FROM Account WHERE id=:acc.Id];
        System.debug('Account Contact Id==>' + PersonAccount);

        User accUser = ATG_TestDataFactory.createUser(acc, 'MoH Approver Community Login User');
        accUser.ContactId = PersonAccount.PersonContactId;
        accUser.User_Type__c = ATG_CommunityApprovalProcessController.USERTYPE_APPROVER;
        insert accUser;

        System.debug('user ==>' + accUser);
        System.assert(accUser != null, 'Unable to create user');


        // Requester User
        Account acc1 = ATG_TestDataFactory.createEmployeeAccount();
       acc1.ATG_Employee_Id__c = '4614839';
        acc1.ATG_Employee_Id__c = '00000000';
        acc1.PersonEmail = 'adfasdfasf@moh.gov.sa.com';
        acc1.PersonMobilePhone = '+971 507301000';
        acc1.ATG_Passport_Number__c = 'GMD93293432';
        acc1.ATG_National_ID__c='0000000000';
        acc1.ATG_Mobile_Number_Value__c = '0521293949';
        System.debug('Account ==>' + acc1);
        insert acc1;


        System.assert(acc1 != null, 'Unable to create account');


        Account PersonAccount1 = [SELECT id, PersonContactId FROM Account WHERE id=:acc1.Id];
        System.debug('Account Contact Id==>' + PersonAccount1);
        User accUser1 = ATG_TestDataFactory.createUser(acc1, 'MoH Requester Community Login User');

        accUser1.ContactId = PersonAccount1.PersonContactId;
        accUser1.User_Type__c = 'Requestor';
        insert accUser1;

        System.debug('user ==>' + accUser1);
        System.assert(accUser1 != null, 'Unable to create user');



        Test.startTest();

        List<Case> cases = ATG_TestDataFactory.CreateETOCase(5);
        for(Case c: cases){
            c.status = 'Approval Pending';
        }

        insert cases;






        List<ATG_Master__c> masterPrograms = ATG_TestDataFactory.createMaster(1, 'p');
        upsert masterPrograms;

        List<ATG_Master__c> masterDepartments = ATG_TestDataFactory.createMaster(1, 'd');
        upsert masterDepartments;

        List<ATG_Master__c> masterCCGR = ATG_TestDataFactory.createMaster(1, 'c');
        upsert masterCCGR;

        List<ATG_Department_Relationship__c> departmentRelationships = new List<ATG_Department_Relationship__c>();


        List<Case> casesList = ATG_TestDataFactory.CreateETOCase(1);
        for(Integer i = 0; i < casesList.size(); i++){

            casesList[i].ATG_Program__c = masterPrograms[i].Id;
            casesList[i].ATG_Department__c = masterDepartments[i].Id;
            casesList[i].ATG_CC_CCGR_Number__c = masterCCGR[i].Id;
            ATG_Department_Relationship__c departmentRelationship = new ATG_Department_Relationship__c( Department__c= masterDepartments[i].Id,Program__c=masterPrograms[i].Id );
            departmentRelationships.add(departmentRelationship);
            casesList[i].Status = ATG_CommunityListViewController.STATUS_APPROVAL_PENDING;
            casesList[i].ATG_ProgramName__c = masterPrograms[i].Name;
        }

        upsert departmentRelationships;
        upsert casesList;



        String flightCityRT = Schema.SObjectType.ATG_Master__c.getRecordTypeInfosByDeveloperName().get('Cities').getRecordTypeId();
        ATG_Master__c city1 = new ATG_Master__c(Name='city1', RecordTypeId=flightCityRT);
        ATG_Master__c city2 = new ATG_Master__c(Name='city2', RecordTypeId=flightCityRT);
        List<ATG_Master__c> cities = new List<ATG_Master__c>();
        cities.add(city1);
        cities.add(city2);
        insert cities;

        ATG_Case_Details__c cd1 = new ATG_Case_Details__c(ATG_Case_Number__c=casesList[0].Id, ATG_From_Airport__c=city1.id, ATG_To_Airport__c=city2.id);
        ATG_Case_Details__c cd2 = new ATG_Case_Details__c(ATG_Case_Number__c=casesList[0].Id, ATG_From_Airport__c=city1.id, ATG_To_Airport__c=city2.id);
        ATG_Case_Details__c cd3 = new ATG_Case_Details__c(ATG_Case_Number__c=casesList[0].Id, ATG_From_Airport__c=city1.id, ATG_To_Airport__c=city2.id);
        List<ATG_Case_Details__c> cds = new List<ATG_Case_Details__c>();
        cds.add(cd1);
        cds.add(cd2);
        cds.add(cd3);
        insert cds;

        List<ATG_Requested_Traveller__c> travellersList = ATG_TestDataFactory.CreateRTraveller(4);
        for(ATG_Requested_Traveller__c traveller: travellersList){
            traveller.Request_Number__c = casesList[0].Id;
            traveller.ATG_Employee__c = PersonAccount.Id;
        }
        travellersList[1].RecordTypeId = ATG_ConstantsUtility.RequestedTraveller_Visitor_RT;
        travellersList[2].RecordTypeId = ATG_ConstantsUtility.RequestedTraveller_Dependant_RT;
        try{
        upsert travellersList;}catch(exception e){}


        List<User_Department__c> userDepartments = new List<User_Department__c>();
        for(Integer i = 0; i < casesList.size(); i++) {

            User_Department__c ud = new User_Department__c(User__c = accUser.Id, ATG_Account__c = acc.ATG_Company_Name__c,
                    CCGR_CC__c = masterCCGR[i].Id, Department__c = masterDepartments[i].Id, Program__c = masterPrograms[i].Id,
                    Department_Relationship__c = departmentRelationships[i].Id);
            System.debug('UD ==>' + ud);

            userDepartments.add(ud);
        }
        upsert userDepartments;

        casesList[0].ATG_Primary_Traveller__c = PersonAccount.Id;
        update casesList;


        // Add documents details
        ATG_Document_Details__c d1 = new ATG_Document_Details__c(ATG_Document_Name__c='passport', ATG_Employee__c=PersonAccount.Id, ATG_Request_Number__c=cases[0].Id);
        ATG_Document_Details__c d2 = new ATG_Document_Details__c(ATG_Document_Name__c='iqama', ATG_Employee__c=PersonAccount.Id, ATG_Request_Number__c=cases[0].Id);
        ATG_Document_Details__c d3 = new ATG_Document_Details__c(ATG_Document_Name__c='other', ATG_Employee__c=PersonAccount.Id, ATG_Request_Number__c=cases[0].Id);
        List<ATG_Document_Details__c>docs = new List<ATG_Document_Details__c>();
        docs.add(d1);
        docs.add(d2);
        docs.add(d3);
        insert docs;

        Test.stopTest();

        System.runAs(accUser1){
            ATG_CommunityApprovalProcessController.SubmitETOForApproval(casesList[0].Id, accUser.Id,'Test');
        }


    }
 

    @IsTest
    static void testProcessApproval() {

        Account acc = ATG_TestDataFactory.createEmployeeAccount();
        System.debug('Account ==>' + acc);
        insert acc;


        System.assert(acc != null, 'Unable to create account');


        Account PersonAccount = [SELECT id, PersonContactId FROM Account WHERE id=:acc.Id];
        System.debug('Account Contact Id==>' + PersonAccount);

        User accUser = ATG_TestDataFactory.createUser(acc, 'MoH Approver Community Login User');
        accUser.ContactId = PersonAccount.PersonContactId;
        accUser.User_Type__c = ATG_CommunityApprovalProcessController.USERTYPE_APPROVER;
        insert accUser;

        System.debug('user ==>' + accUser);
        System.assert(accUser != null, 'Unable to create user');


        List<Case> cases = ATG_TestDataFactory.CreateETOCase(2);
        for(Case c: cases){
            c.ATG_Primary_Traveller__c = PersonAccount.Id;
            c.status = 'Approval Pending';
        }


        upsert cases;




        list<ATG_Requested_Traveller__c> rtList = ATG_TestDataFactory.CreateRTraveller(2);
        rtList[0].Request_Number__c= cases[0].Id;
        rtList[0].RecordTypeId = ATG_ConstantsUtility.RequestedTraveller_Employee_RT;
        rtList[1].Request_Number__c = cases[0].Id;
        rtList[1].RecordTypeId = ATG_ConstantsUtility.RequestedTraveller_Employee_RT;
        
        insert rtList[0];
        insert rtList[1];




        Test.startTest();

        List<String> caseIds = new List<String>();
        for(Case c: cases)
            caseIds.add(c.Id);

        for(String caseId : caseIds){
            ATG_CommunityApprovalProcessController.SubmitETOForApproval(caseId, accUser.Id,'');
        }

        // Add documents details
        ATG_Document_Details__c d1 = new ATG_Document_Details__c(ATG_Document_Name__c='passport', ATG_Employee__c=PersonAccount.Id, ATG_Request_Number__c=cases[0].Id);
        ATG_Document_Details__c d2 = new ATG_Document_Details__c(ATG_Document_Name__c='iqama', ATG_Employee__c=PersonAccount.Id, ATG_Request_Number__c=cases[0].Id);
        ATG_Document_Details__c d3 = new ATG_Document_Details__c(ATG_Document_Name__c='other', ATG_Employee__c=PersonAccount.Id, ATG_Request_Number__c=cases[0].Id);
        List<ATG_Document_Details__c>docs = new List<ATG_Document_Details__c>();
        docs.add(d1);
        docs.add(d2);
        docs.add(d3);
        insert docs;

        try{
            ATG_CommunityApprovalProcessController.SubmitETOForApproval(caseIds[0], accUser.Id,'');
            ATG_CommunityApprovalProcessController.ProcessApproval(caseIds, 'Test', 'Approve');
        }catch (Exception ex){
            system.debug('Approved Error' + ex.getStackTraceString());
        }

        try{
           ATG_CommunityApprovalProcessController.SubmitETOForApproval(caseIds[0], accUser.Id,'test');
            ATG_CommunityApprovalProcessController.ProcessApproval(caseIds, 'Test', 'Reject');
        }catch (Exception ex){
            system.debug('Reject Error' + ex.getStackTraceString());
        }


        try{
           ATG_CommunityApprovalProcessController.SubmitETOForApproval(caseIds[0], accUser.Id,'test');
            ATG_CommunityApprovalProcessController.ProcessApproval(caseIds, 'Test', 'Return');
        }catch (Exception ex){
            system.debug('Return Error' + ex.getStackTraceString());
        }
        Test.stopTest();

    }

  


    @IsTest
    static void testGetCaseDetails() {

        List<Case> cases = ATG_TestDataFactory.CreateETOCase(5);
        for(Case c: cases){
            c.status = 'Approval Pending';
        }

        upsert cases;

        ATG_CommunityApprovalProcessController.GetCaseDetails(cases[0].Id);
    }

}