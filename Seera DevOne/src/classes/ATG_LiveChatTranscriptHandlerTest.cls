/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class ATG_LiveChatTranscriptHandlerTest {

    static testMethod void ATG_LiveChatTranscriptHandle1() {
        LiveChatVisitor lcv = new LiveChatVisitor();
        insert lcv;
        
        liveChatTranscript obj = new liveChatTranscript();
        obj.LiveChatVisitorId =  lcv.id;
        obj.Last_Name__c = 'dsd';
        obj.Mobile_Country_Code__c = 'United Arab Emirates (+971)';
        obj.Mobile_Number__c = '0554507894';
        insert obj;
        
        LiveChatTranscriptEvent le = new LiveChatTranscriptEvent();
        le.LiveChatTranscriptId = obj.id;
        le.type = 'ChatRequest';
        le.time = system.now();
        
        list<liveChatTranscript> soNewList = new list<liveChatTranscript>();
        soNewList.add(obj);
        
        map<id,liveChatTranscript> soOldMap = new map<id,liveChatTranscript>();
        soOldMap.put(obj.id,obj);
        
        ATG_LiveChatTranscriptHandler objClass = new ATG_LiveChatTranscriptHandler();
        objClass.beforeInsert (soNewList);
        objClass.beforeUpdate (soNewList,soOldMap);
        objClass.beforeDelete (soNewList);
        objClass.afterInsert (soNewList);
        objClass.afterUpdate (soNewList,soOldMap);
        objClass.afterDelete (soNewList);
        
    }
    
    static testMethod void ATG_LiveChatTranscriptHandle2() {
        LiveChatVisitor lcv = new LiveChatVisitor();
        insert lcv;
        
        liveChatTranscript obj = new liveChatTranscript();
        obj.LiveChatVisitorId =  lcv.id;
        obj.Last_Name__c = 'dsd';
        obj.Business_Unit__c = 'Tajawal';
        obj.ATG_Product__c = 'Flights';
        insert obj;
        
        LiveChatTranscriptEvent le = new LiveChatTranscriptEvent();
        le.LiveChatTranscriptId = obj.id;
        le.type = 'ChatRequest';
        le.time = system.now();
        
        list<liveChatTranscript> soNewList = new list<liveChatTranscript>();
        soNewList.add(obj);
        
        map<id,liveChatTranscript> soOldMap = new map<id,liveChatTranscript>();
        soOldMap.put(obj.id,obj);
        
        ATG_LiveChatTranscriptHandler objClass = new ATG_LiveChatTranscriptHandler();
        objClass.beforeInsert (soNewList);
        objClass.beforeUpdate (soNewList,soOldMap);
        objClass.beforeDelete (soNewList);
        objClass.afterInsert (soNewList);
        objClass.afterUpdate (soNewList,soOldMap);
        objClass.afterDelete (soNewList);
        
    }
}