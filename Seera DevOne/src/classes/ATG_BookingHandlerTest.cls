/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class ATG_BookingHandlerTest {

    static testMethod void ATG_BookingHandler1() {
        
        Account obj = new Account();
        obj.firstName = 'Test';
        obj.lastName = 'Guest';
        obj.personemail = 'testing@test.com';
        obj.recordTypeID = ATG_ConstantsUtility.PERSON_ACCOUNT_RECORDTYPEID;
        obj.ATG_Business_Unit_Picklist__c = 'Al Tayyar';
        obj.PersonMobilePhone = '+966 555555555';
        obj.Type = 'Leisure';
        insert obj;
        
        Account obj1 = new Account();
        obj1.firstName = 'Test';
        obj1.lastName = 'Guest';
        obj1.personemail = 'check@check.com';
        obj1.recordTypeID = ATG_ConstantsUtility.AMH_PERSON_ACCOUNT_RECORDTYPEID;
        obj1.ATG_Business_Unit_Picklist__c = 'Al Mosafer';
        obj1.PersonMobilePhone = '';
        obj1.Type = 'Leisure';
        insert obj1;
        
        Account obj2 = new Account();
        obj2.firstName = 'Test';
        obj2.lastName = 'Guest';
        obj2.personemail = 'duck@duck.com';
        obj2.recordTypeID = ATG_ConstantsUtility.TJW_PERSON_ACCOUNT_RECORDTYPEID;
        obj2.ATG_Business_Unit_Picklist__c = 'Tajawal';
        obj2.PersonMobilePhone = '+966 525252525';
        obj2.Type = 'Leisure';
        insert obj2;
        
     	Case objCase = new Case();
        objCase.AccountID = obj.id;
        objCase.recordTypeID = ATG_ConstantsUtility.Case_booking_RECORDTYPEID;
        objCase.ATG_Travel_Order_Number__c = 'adad';
        insert objCase;
        
        CaseComment objCaseC = new CaseComment();
        objCaseC.ParentId = objCase.Id;
        insert objCaseC;
        
        ATG_Master__c objCCGR = new ATG_Master__c();
        objCCGR.recordTypeID = ATG_ConstantsUtility.ATG_MASTER_CCGR_RT;
        objCCGR.name = '12345';
        objCCGR.ATG_Type__c = 'CCGR';
        insert objCCGR;
        
        ATG_Internal_Email_Domain__c internalDomain = new ATG_Internal_Email_Domain__c();
        internalDomain.Name = 'test.com';
        insert internalDomain;
                
        ATG_Booking__c book = new ATG_Booking__c(Name = 'TestBook',
                              ATG_Product_Type__c='Flight',
                              ATG_Email__c ='testing@test.com',//'test@test.com',
                              ATG_Mobile__c = '+966 578555555',
                              ATG_Collection_Mode__c = 'Revenue',
                              ATG_Customer_Name1__c = 'zxc',
                              ATG_Business_Unit__c ='Al Tayyar',
                              ATG_Travel_End_Date__c = system.today(),
                              ATG_Travel_Order_Number__c = 'adad');
        
       // insert book;
                             
        ATG_Booking__c book2 = new ATG_Booking__c(Name = 'TestBook',
                              ATG_Product_Type__c='Flight',
                              ATG_Email__c ='testing@test.com',//'test@test.com',
                              ATG_Mobile__c = '+966578555555',
                              ATG_Collection_Mode__c = 'Revenue',
                              ATG_Customer_Name1__c = 'zxc',
                              ATG_Business_Unit__c ='Al Tayyar',
                              ATG_Travel_End_Date__c = system.today(),
                              ATG_Case__c = objCase.id);
        
      //  insert book2;
        
        ATG_Booking__c book3 = new ATG_Booking__c(Name = 'TestBook',
                              ATG_Product_Type__c='Flight',
                              ATG_Email__c ='rest@test.com',//'test@test.com',
                              ATG_Mobile__c = '+966 578455555',
                              ATG_Collection_Mode__c = 'Revenue',
                              ATG_Customer_Name1__c = 'zxc',
                              ATG_Business_Unit__c ='Al Tayyar',
                              ATG_Travel_End_Date__c = system.today());
                                             
       // insert book3;
        
        ATG_Booking__c book4 = new ATG_Booking__c(Name = 'TestBook',
                              ATG_Product_Type__c='Flight',
                              ATG_Email__c ='asdasd@asdasd.com',//'test@test.com',
                              ATG_Mobile__c = '+966 565656565',
                              ATG_Collection_Mode__c = 'Revenue',
                              ATG_Customer_Name1__c = 'zxc',
                              ATG_Travel_End_Date__c = system.today(),
                              ATG_Track_ID__c = 'ass',
                              ATG_Business_Unit__c ='Tajawal',
                              ATG_Booking_Reference_Number__c = 'asd',
                              ATG_Contact_Email__c ='asdasd@asdasd.com');
        
      //  insert book4; 
                            
        ATG_Booking__c book5 = new ATG_Booking__c(Name = 'TestBook',
                              ATG_Product_Type__c='Flight',
                              ATG_Email__c ='uiuiui@uiuiui.com',//'test@test.com',
                              ATG_Mobile__c = '+966 585858585',
                              ATG_Collection_Mode__c = 'Revenue',
                              ATG_Customer_Name1__c = 'zxc',
                              ATG_Travel_End_Date__c = system.today(),
                              ATG_Track_ID__c = 'dsd',
                              ATG_Business_Unit__c ='Al Mosafer',
                              ATG_Booking_Reference_Number__c = 'sdfs',
                              ATG_Contact_Email__c = 'sd@sd.com');
      //  insert book5;
        
        ATG_Booking__c book6 = new ATG_Booking__c(Name = 'TestBook',
                              ATG_Product_Type__c='Flight',
                              ATG_Email__c ='duck@duck.com',//'test@test.com',
                              ATG_Collection_Mode__c = 'Revenue',
                              ATG_Customer_Name1__c = 'zxc',
                              ATG_Travel_End_Date__c = system.today(),
                              ATG_Track_ID__c = 'ass',
                              ATG_Business_Unit__c ='Tajawal',
                              ATG_Booking_Reference_Number__c = 'asasx');
        
      //  insert book6;
                             
        ATG_Booking__c book7 = new ATG_Booking__c(Name = 'TestBook',
                              ATG_Product_Type__c='Flight',
                              ATG_Email__c ='check@check.com',//'test@test.com',
                              ATG_Collection_Mode__c = 'Revenue',
                              ATG_Customer_Name1__c = 'zxc',
                              ATG_Travel_End_Date__c = system.today(),
                              ATG_Track_ID__c = 'dsd',
                              ATG_Business_Unit__c ='Al Mosafer',
                              ATG_Booking_Reference_Number__c = 'zcsd');
        
       // insert book7;       
       
       ATG_Booking__c book8 = new ATG_Booking__c(Name = 'TestBook',
                              ATG_Product_Type__c='Flight',
                              ATG_Email__c ='sa@sa.com',//'test@test.com',
                              ATG_Mobile__c = '+966 585858585',
                              ATG_Collection_Mode__c = 'Revenue',
                              ATG_Customer_Name1__c = 'as',
                              ATG_Travel_End_Date__c = system.today(),
                              ATG_Track_ID__c = 'as',
                              ATG_Business_Unit__c ='Tajawal',
                              ATG_Booking_Reference_Number__c = 'as',
                              ATG_Contact_Email__c = 'as@as.com');               
        
        list<ATG_booking__c> soNewList = new list<ATG_booking__c>();
        soNewList.add(book);
        soNewList.add(book2);
        soNewList.add(book3);
        soNewList.add(book4);
        soNewList.add(book5);
        soNewList.add(book6);
        soNewList.add(book7);
        soNewList.add(book8);
        insert soNewList;
        
        book.ATG_Mobile__c = '+966578555555';
        book.ATG_Contact_Email__c = 'testing@test.com';
        update book;
        
        book2.ATG_Contact_Email__c = 'testing@test.com';
        update book2;
        
        delete book2;
    }
}