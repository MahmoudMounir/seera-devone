/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class ATG_utilityTest {
    
    @testSetup
    public static void testDataSetup(){
        CequensConfigurations__c objCC = new CequensConfigurations__c();
        objCC.Name = '';
        objCC.Active__c = true;
        objCC.Cequens_SMS_API_Key__c = 'mFecwc8Jc40E6rRe23IT4zIqtIMXTQrlPeMwxMbHhai5l0ttJC09z/IrRRUS3wRy3t18tLrANYNJO7FA9E3kWQ==    ';
        objCC.Message_Type__c = '';
        objCC.Endpoint__c = 'http://api.cequens.com';
        objCC.Sender_Name__c = 'Altayyar';
        objCC.SMS_SignIn_Endpoint__c = 'http://api.cequens.com/signIn';
        objCC.User_Name__c = 'Altayyar';
        insert objCC;
    }
    
    static testMethod void ATG_utility1() {
              
        ATG_utility.LogException('',''); 
        
        Schema.sObjectType sobject_type = Account.getSObjectType();
        //grab the sobject that was passed
        Schema.DescribeSObjectResult sobject_describe = sobject_type.getDescribe(); //describe the sobject
        ATG_utility.getAllFields(sobject_describe); 
        
        ATG_utility.getRecordTypeId('Account'); 
        ATG_utility.getallAPINames('Account'); 
        
        Set<String> lst = new Set<String>();
        lst.add('abc;pqr;lmn');
        ATG_utility.getValuesInQuotes(lst); 
        
        ATG_utility.integrationUser(); 
        ATG_utility.companyID('123'); 
        ATG_utility.GenerateHexString(1); 
        ATG_utility.GetAccessibleFields('Account'); 
        
        ATG_utility objUtil = new ATG_utility();
        
        set<string> queueName = new set<string>();
        queueName.add('ATG Leisure Agent');
        ATG_utility.getQueueIdFromName(queueName);
        objUtil.getPickListValuesInfo('Case', 'Status');
        
        ATG_utility.queueMembers(queueName);
        ATG_utility.validateIqama('3333333');
        
        set<string> SetUserIDs = new set<string>();
        queueName.add(userinfo.getuserID());
        ATG_utility.getUserPresenceStatus(queueName);
        
        ATG_utility.DecryptPassword(ATG_utility.EncryptPassword('test'));
        
        try{
            Integer i = 1/0;
        }catch(Exception e){
            ATG_utility.LogException(e);
        }
        
        ATG_utility.getKeyPrefix('Account');
        ATG_utility.getDailingCodes();
        ATG_utility.GetUserInfo();
        
        test.startTest();
            MockHttpResponseGenerator.signInResponse = '{"replyCode":0,"replyMessage":"[Request handled successfully] Access Token is created sucessfully.","requestId":"aa0dda80-4bb1-11e9-86a0-9f29889fffc4","clientRequestId":0,"requestTime":"2019-03-21T10:16:44.584","data":{"access_token":"eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ0b2tlbiI6ImVhMjU5MDYxYjJhNWFmYWY0MmVjNmZiYzE1Y2QwMDAwNmRkNWQ3N2FlNjJjZmQ4NmZhMGM0MTBhY2I3Zjg5ZGI1NzM2NjdiZWI4YTlmMmYyMzA3ZDBhOWM4OWQzMjliM2JlN2ZmMDgyMGQ1Nzc1OGQzNDA1NzYwNDcxNzhiOTY3OWFiMmUxMTQ4NDExY2IwNGI1MzEzNWI0M2ZmMzhmZTYiLCJpYXQiOjE1MzE5MjM3MTh9.KJH46BaaXYaVOnoMfxCt2xomg39lIalc1MhoclSJYn0"}}';
            MockHttpResponseGenerator.response = '{"replyCode":-1,"replyMessage":"[Request failed] Bad Request","requestId":"db877b40-4baf-11e9-b284-e560c00b8458","clientRequestId":0,"requestTime":"2019-03-21T10:03:48.596","error":{"status":400,"description":"Parameters missing or invalid for requested resource or validations failed.","internalErrors":[{"code":-6,"details":"At least one valid recipient required."}]}}';       
            MockHttpResponseGenerator mock = new MockHttpResponseGenerator();
            Test.setMock(HttpCalloutMock.class, mock);
            ATG_EmailToSMSHandler.RecordId = UserInfo.getUserId();
            ATG_Utility.SMSRequestClass smsReqObj = new  ATG_Utility.SMSRequestClass();
            smsReqObj.messageText = 'Test Body';
            smsReqObj.recipients = '971526515765';//email.Subject;
            ATG_utility.SendSMS(smsReqObj);
        
            //ATG_utility.getCequensAccessToken(UserInfo.getProfileId(), ATG_ConstantsUtility.ALTAYYAR_BusinessUnit);  
            //ATG_utility.getCequensAccessToken(null, ATG_ConstantsUtility.ALTAYYAR_BusinessUnit);
        test.stopTest();
        
    }
	
    static testMethod void CoETestCase1() {
    	ATG_utility.createErrorLog('Test', 'Test', 'Test', null, null);
    	Account objAccount = new Account();
        objAccount.Name = 'Test Vendor';
        objAccount.RecordTypeId = ATG_ConstantsUtility.Account_CoE_Air_Vendor_RecordType;
        objAccount.Type = 'Contracted';
        insert objAccount;
        
        list<Case> lstCases = new list<Case>();
        
        Case objCase = new case(Status = 'New',
                            recordtypeid = ATG_ConstantsUtility.SOURCING_CONTRACT_REQUEST_RECORDTYPE,
                            Origin = 'Phone',
                            AccountId = objAccount.Id,
                            ATG_Function__c = 'Air',
                            ATG_Stakeholders_Team__c = 'Finance',
                            ownerID = userinfo.getuserID());
        lstCases.add(objCase);
		insert lstCases;
                        
        lstCases[0].Status = ATG_ConstantsUtility.STR_Update_Relavent_Teams;
        try{
        	update lstCases[0];
        }catch(Exception ex){}
    }
    
}