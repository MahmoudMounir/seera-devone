@istest
global class MockHttpResponseGenerator implements HttpCalloutMock{
   
    public static string signInResponse;
    public static string response;
    
    global HTTPResponse respond(HTTPRequest req) {
        HttpResponse res = new HttpResponse();
        res.setHeader('Content-Type', 'application/json');
        res.setBody('{"foo":"bar"}');
        res.setStatusCode(200);
        if(req.getEndpoint().indexOf('signIn') != -1)
            res.setBody(MockHttpResponseGenerator.signInResponse);
        else if (response != null)
            res.setBody(MockHttpResponseGenerator.response);
        return res;
    }
}