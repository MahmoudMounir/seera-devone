@isTest
private class ATG_SocialMediaSurveyControllerTest {
	
    static testmethod void test_SocialMediaSurvey(){
        Case objCase = new Case();
        objCase.Origin = 'Twitter';
        objCase.OwnerId = UserInfo.getUserId();
        objCase.ATG_BU_From_Case__c = 'Tajawal';
        objCase.RecordTypeId = ATG_ConstantsUtility.TJW_CASE_RECORDTYPE;
        insert objCase;
        
        ATG_SocialMediaSurveyController.generateSurveyURL(objCase.Id);
    }
    
     static testmethod void test_SocialMediaSurvey1(){
        Case objCase = new Case();
        objCase.Origin = 'Twitter';
        objCase.OwnerId = UserInfo.getUserId();
        objCase.ATG_BU_From_Case__c = 'Al Mosafer';
         objCase.RecordTypeId = ATG_ConstantsUtility.AMH_CASE_RECORDTYPE;
        insert objCase;
        
        ATG_SocialMediaSurveyController.generateSurveyURL(objCase.Id);
    }
}