/**************************************************************************************************
* Name               : ATG_CaseCommentTriggerHandler                                                               
* Description        : Trigger Handler for the CaseComment SObject. This class implements the ITrigger
             interface to help ensure the trigger code is bulkified and all in one place.                                       
* Created Date       : 19th Feb 2019                                                                
* Created By         : Diana Correa (PWC Digital)                                              
* -------------------------------------------------------------------------------------------------
* Version    Author    Date           Comment                                                              
*/
public class ATG_CaseCommentTriggerHandler implements ATG_TriggerInterface{
    
    /*
     * Method Name : beforeInsert
     * @input : CaseComment List
     */
    public void beforeInsert(list<SObject> soNewList){
        updateBookingNo(soNewList);
    }
    
    public void beforeUpdate(list<sobject> soNewList, map<id,sobject> soOldMap){}
    
    public void beforeDelete(list<SObject> soNewList){}
    
    public void afterInsert(list<SObject> soNewList){}
    
    public void afterUpdate(list<SObject> soNewList,map<id,SObject> soOldMap){}
    
    public void afterDelete(list<SObject> soNewList){}
    
   /*
     * Method Name : updateBookingNo
     * @input : CaseComment List
     * Description :  For Al Mosafer and Tajawal Business Units, 
     * When the Case comment is added on case which has booking linked or where the case is linked to booking,
     * add the booking reference number in the beginning of the case comment as BookingId-(BookingReferenceNo):
     * This is used for syncing notes/comments to hub. In Hub, Parent Id for Notes/Comments is booking which is 
     * mandatory.
     */
    private void updateBookingNo(list<SObject> soNewList){
        
        Set<Id> caseIdSet = new Set<Id>();
        
         String authorName = null;
        for(User loggedInUser:[SELECT Email FROM User Where Id=:UserInfo.getUserId()]){
            if(loggedInUser.Email != null){
                String[] emailArray = loggedInUser.Email.split('@');
                authorName = emailArray[0];
            }
        }
       
        Map<Id,String> CaseCommentBookingRefMap = new Map<Id,String>();
        Map<Id,Id> CaseAndCaseCommentMap = new Map<Id,Id>();
        Set<Id> caseCommentIdSet = new Set<Id>();
        for(CaseComment objCaseComment:(List<CaseComment>)soNewList){
            caseIdSet.add(objCaseComment.ParentId);
            CaseAndCaseCommentMap.put(objCaseComment.ParentId,objCaseComment.Id);
             
        }
        
       
        Set<ID> updateAuthorCaseSet = new Set<Id>();
        
        for(Case objCase:[SELECT ATG_Booking__r.ATG_Booking_Reference_Number__c,Id,ATG_BU_From_Case__c FROM Case 
                          WHERE Id in:caseIdSet ]){
            if(objCase.ATG_BU_From_Case__c != null &&
              (objCase.ATG_BU_From_Case__c.equalsIgnoreCase(ATG_ConstantsUtility.TAJAWAL_BusinessUnit) || 
                objCase.ATG_BU_From_Case__c.equalsIgnoreCase(ATG_ConstantsUtility.ALMOSAFER_BusinessUnit))
                && objCase.ATG_Booking__c != null && objCase.ATG_Booking__r.ATG_Booking_Reference_Number__c!= null){
                    CaseCommentBookingRefMap.put(CaseAndCaseCommentMap.get(objCase.Id),objCase.ATG_Booking__r.ATG_Booking_Reference_Number__c);
        
            }
            updateAuthorCaseSet.add(objCase.Id);
                
         }
        
       
        
        for(ATG_Booking__c objBooking:[SELECT ATG_Booking_Reference_Number__c,ATG_Case__c,ATG_Business_Unit__c 
                                FROM ATG_Booking__c 
                                WHERE ATG_Case__c IN: caseIdSet AND ATG_Booking_Reference_Number__c != null]){
              if(objBooking.ATG_Business_Unit__c != null &&
              (objBooking.ATG_Business_Unit__c.equalsIgnoreCase(ATG_ConstantsUtility.TAJAWAL_BusinessUnit) || 
                objBooking.ATG_Business_Unit__c.equalsIgnoreCase(ATG_ConstantsUtility.ALMOSAFER_BusinessUnit))){
                    CaseCommentBookingRefMap.put(CaseAndCaseCommentMap.get(objBooking.ATG_Case__c),objBooking.ATG_Booking_Reference_Number__c);
                }
         }
        
        if(!CaseCommentBookingRefMap.isEmpty()){
            for(CaseComment objCaseComment:(List<CaseComment>)soNewList){
                for(Id caseCommentId:CaseCommentBookingRefMap.keySet()){
                    if(caseCommentId == objCaseComment.Id){
                        objCaseComment.CommentBody = 'BookingNo-'+CaseCommentBookingRefMap.get(caseCommentId)+': '+objCaseComment.CommentBody;
                        system.debug('$$'+objCaseComment.CommentBody);
                    }
                }
               
            }
        }
        
        if(updateAuthorCaseSet.size()>0){
            for(CaseComment objCaseComment:(List<CaseComment>)soNewList){
                if(updateAuthorCaseSet.contains(objCaseComment.ParentId) &&
                null != authorName){
                 objCaseComment.CommentBody =  objCaseComment.CommentBody+' *Added By @'+authorName;
                }
            }
        }
        
    }

}