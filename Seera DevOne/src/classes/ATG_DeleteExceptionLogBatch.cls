/**************************************************************************************************
* Name               : ATG_DeleteExceptionLogBatch                                                               
* Description        : batch class to delete exceptions once in a week.                                        
* Created Date       : 10th June 2018                                                                *
* Created By         : Swati Sehrawat (PWC Digital)                                              
* -------------------------------------------------------------------------------------------------
* Version		Author		Date		Comment                                                               *

**************************************************************************************************/

global without sharing class ATG_DeleteExceptionLogBatch implements Database.Batchable<sObject> {
	
	String query = '';
	
	global ATG_DeleteExceptionLogBatch(String queryStr) {
		query = queryStr; 
	}
	
	global Database.QueryLocator start(Database.BatchableContext BC) {
		return Database.getQueryLocator(query);
	}

   	global void execute(Database.BatchableContext BC, List<ATG_Exception_Logs__c> scope) {
        try{ 
            if(scope!=null && scope.size()>0) 
                delete scope;
        }catch(Exception e){ 

        } 
	}
	
	global void finish(Database.BatchableContext BC) {
		
	}
	 
	
}