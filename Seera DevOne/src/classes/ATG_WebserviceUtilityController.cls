/**************************************************************************************************
* Name               : ATG_WebserviceUtilityController                                             *                                                                        
* Description        : Webservice utility class for callouts                                      *
* Created Date       : 12/17/2018                                                                 * 
* Created By         : PWC                                                                        *
* ------------------------------------------------------------------------------------------------*
* VERSION  AUTHOR    DATE            COMMENTS                                                     *
* 1.0      Diana    12/17/2018      Initial Draft.                                                *         
**************************************************************************************************/
public class ATG_WebserviceUtilityController {
    
    /****************************************************************************************
    @Description - This method is used invoke webservice for creditlimitcheck
    @Param - Account Record Id
    @Return - Message 
    ****************************************************************************************/
    public static ATG_FinCreditlimitcheck.Record_element creditLimitCheck(String customerId){
        
        ATG_FinCreditlimitcheck.Record_element response= new ATG_FinCreditlimitcheck.Record_element();
        try{
            Map<String, MOH_Fetch_PNR__c> mpSettings = MOH_Fetch_PNR__c.getAll();
             
            //get the auth settings for the API call
            MOH_Fetch_PNR__c iSettings= mpSettings.containsKey('SAPCreditCheckLimit') ? mpSettings.get('SAPCreditCheckLimit'): new MOH_Fetch_PNR__c();
            
            String userName = ATG_Utility.DecryptPassword(iSettings.ATG_UserName__c);
            String password = ATG_Utility.DecryptPassword(iSettings.ATG_Password__c);
            String endPointURL = iSettings.ATG_Endpoint_URL__c;
            system.debug('Creds**'+userName+'**'+password);
            Blob headerValue = Blob.valueOf(userName +':' +password);
            String authorizationHeader = 'Basic ' + EncodingUtil.base64Encode(headerValue);
            system.debug(authorizationHeader);
            system.debug(customerId);
            
            ATG_FinCreditlimitcheck.HTTPS_Port stub = new ATG_FinCreditlimitcheck.HTTPS_Port();
            
            stub.inputHttpHeaders_x = new Map<String,String>();
            stub.inputHttpHeaders_x.put('Authorization',authorizationHeader);
            stub.endpoint_x = endPointURL;
            stub.timeout_x = 120000;
            
            ATG_FinCreditlimitcheck.RecordInput_element requestParam = new ATG_FinCreditlimitcheck.RecordInput_element();
            requestParam.CustomerID = customerId; //customer id from account
            requestParam.TransactionID = 'SF';//'0'; // default value
            requestParam.BookingValule = '0'; //defalut value
            requestParam.BookingCurrency = Label.ATG_Credit_Limit_Check_Booking_Currency; // default currency value "SAR"
            system.debug('ATG_WebserviceUtilityController - creditLimitCheck '+requestParam);
            system.debug(stub);
            response = stub.CreditLimitCheck_Out(requestParam);
            system.debug('ATG_WebserviceUtilityController - creditLimitCheck '+response);
        }catch(Exception ex){
            ATG_utility.LogException(ex);
        }
        
        return response;
        
    }
    
}