/**************************************************************************************************
* Name               : ATG_DeleteExceptionLogScheduler                                                               
* Description        : scheduler to run ATG_DeleteExceptionLogBatch                                       
* Created Date       : 10th June 2018                                                                *
* Created By         : Swati Sehrawat (PWC Digital)                                              
* -------------------------------------------------------------------------------------------------
* Version		Author		Date		Comment                                                               *

**************************************************************************************************/

global class ATG_DeleteExceptionLogScheduler implements Schedulable{
    
    global void execute(SchedulableContext sc) {
		String ELQueryStr =' SELECT Id,CreatedDate FROM Exception_Log__c ';
		ATG_DeleteExceptionLogBatch b = new ATG_DeleteExceptionLogBatch(ELQueryStr);  
		database.executebatch(b);
	}  
	
}