/**************************************************************************************************
* Name               : ATG_contentDocumentLinkHandler 
* Description        : Helper class that is used to handle various trigger events on ContentDocumentLink Object                                    
* Created Date       : 30 Jul 2018                                                              *
* Created By         : PWC Digital                                              
* -------------------------------------------------------------------------------------------------
* Version       Author      Date        Comment                                                               *

**************************************************************************************************/

public class ATG_contentDocumentLinkHandler implements ATG_TriggerInterface  {
    
    
    public  ATG_contentDocumentLinkHandler(){
      
    }

    public void beforeInsert(list<SObject> soNewList){
       ATG_contentDocumentLinkHandler.updateShareType(soNewList);
    }
  
    public void beforeUpdate(list<sobject> soNewList, map<id,sobject> soOldMap){
    }

    public void beforeDelete(list<SObject> soNewList){}

    public void afterInsert(list<SObject> soNewList){
        ATG_contentDocumentLinkHandler.updateDocumentId(soNewList);       
    }

    public void afterUpdate(list<SObject> soNewList,map<id,SObject> soOldMap){}
    
    public void afterDelete(list<SObject> soNewList){
        
        
    }
    
    
    public static void updateDocumentId(list<SObject> soNewList){
        list<ATG_Document_Details__c> documentDetailsToUpdate = new list<ATG_Document_Details__c>();
        list<ContentDocumentLink> contentList =  (list<ContentDocumentLink>)soNewList;        
        for(ContentDocumentLink con : contentList){            
            if(Schema.ATG_Document_Details__c.SObjectType == con.LinkedEntityId.getSobjectType())
                documentDetailsToUpdate.add(new ATG_Document_Details__c(Id=con.LinkedEntityId,ATG_Document_ID__c=con.contentDocumentId));
        }
        
        if(documentDetailsToUpdate !=null && !documentDetailsToUpdate.isEmpty())
            update documentDetailsToUpdate;    
        
    }
    
    
    public static void updateShareType(list<SObject> soNewList){        
        string MOHCaseRecordType = ATG_ConstantsUtility.ATG_CASE_ETO_RECORDTYPE ;       
        list<string> communityProfileIds = new list<string>();
        
        for(user usr : [select Id,User_Type__c from user where id = : userInfo.getUserId()])
        for(ContentDocumentLink cdl : (list<ContentDocumentLink>)soNewList){
            if(cdl.LinkedEntityId.getSobjectType() == schema.ATG_Document_Details__c.SobjectType && (usr.User_Type__c == 'Requestor' || usr.User_Type__c == 'Requestor & Approver')){
                cdl.shareType = 'I';
                cdl.Visibility = 'AllUsers';
            }
        }
    
    }
}