/**************************************************************************************************
* Name               : ATG_PortalHomePageUtility                                                               
* Description        : Utility class for all Portal Home page components                                   
* Created Date       : 18 Jul 2018                                                              *
* Created By         : PWC Digital                                              
* -------------------------------------------------------------------------------------------------
* Version       Author      Date        Comment                                                               *

**************************************************************************************************/

public with sharing class ATG_PortalHomePageUtility {


    // Request Status
    public static String STATUS_NEW = Label.New;
    public static String STATUS_APPROVED = Label.Approved;
    public static String STATUS_SENT_BACK = Label.Sent_Back;
    public static String STATUS_REJECTED = Label.Rejected;
    public static String STATUS_COMPLETED = Label.Completed;
    public static String STATUS_APPROVAL_PENDING = Label.Approval_Pending;
    public static String STATUS_Returned = Label.Returned;
    public static String STATUS_ASSIGNED = Label.Assigned;
    public static string STATUS_INPROGRESS = Label.In_Progress;

    // User Type
    public static String USER_ADMIN = 'Admin';
    public static String USER_REQUESTER = 'Requestor';
    public static String USER_APPROVER = 'Approver';
    public static String USER_REQUESTER_APPROVER = 'Requestor & Approver';

    /**
     * @description Aura enabled method to calculate record count for homepage dashboard
     * components of portal
     *
     * @param loginUserId
     *
     * @return Dashboard stats structure based on the logged in user profile
     */
    @AuraEnabled
    public static DashboardCount getDashboardRecordsCount(string loginUserId) {

        DashboardCount DashboardCountDetails = new DashboardCount();

//        DateTime StartYear = Datetime.newInstance(Date.Today().Year(), 1, 1, 0, 0, 0);
        DateTime StartMonth = Datetime.newInstance(Date.Today().Year(),Date.Today().Month(), 1, 0, 0, 0);


        map<string, ATG_Case_Status_Configs_PortalChart__c> stageConfigs =  ATG_PortalHomePageUtility.GetStageConfigs();



        system.debug(stageConfigs.keySet());

        AggregateResult[] groupedResults = [SELECT Count(id) allRequests FROM Case where RecordTypeId =: ATG_ConstantsUtility.ATG_CASE_ETO_RECORDTYPE and ATG_Status_External__c in : stageConfigs.keySet() and IsDeleted = false];
        for (AggregateResult ar : groupedResults)  {
            //System.debug('Count' + ar.get('allRequests'));
            DashboardCountDetails.allRequestsCount = Integer.valueOf(ar.get('allRequests'));
        }



        for (Case cas : [
                select id,ATG_Status_External__c,status,createdDate,ATG_Approved_Date__c,ClosedDate,ATG_Assigned_Date__c,ATG_Rejected_Date__c,ATG_Sent_Back__c
                from case
                where ATG_Status_External__c in : stageConfigs.keySet() and  RecordTypeId = : ATG_ConstantsUtility.ATG_CASE_ETO_RECORDTYPE and createdDate <= :DateTime.Now()
                and IsDeleted = false
//                and ((createdDate <= :DateTime.Now() and createdDate >= :StartMonth)
//                or (ATG_Rejected_Date__c <= :DateTime.Now() and ATG_Rejected_Date__c >= :StartMonth)
//                or (ATG_Approved_Date__c <= :DateTime.Now() and ATG_Approved_Date__c >= :StartMonth)
//                or (ClosedDate <= :DateTime.Now() and ClosedDate >= :StartMonth)
//                or (ATG_Sent_Back__c <= :DateTime.Now() and ATG_Sent_Back__c >= :StartMonth))
        ]) {

            if (cas.createdDate.Date() > StartMonth)
                DashboardCountDetails.allRequestsMonth += 1;

            if (cas.ATG_Status_External__c == STATUS_NEW) { //Label.New
                DashboardCountDetails.newRequestsAll += 1;
            } else if (cas.ATG_Status_External__c == STATUS_APPROVED || cas.ATG_Status_External__c == STATUS_INPROGRESS) {
                DashboardCountDetails.approvedRequestsAll += 1;
            } else if (cas.ATG_Status_External__c == STATUS_SENT_BACK) {
                DashboardCountDetails.sentBackRequests += 1;
            } else if (cas.ATG_Status_External__c == STATUS_REJECTED ) {

                if (cas.ATG_Rejected_Date__c != null && cas.ClosedDate.Date() > StartMonth)
                    DashboardCountDetails.cancelledRequestsMonth += 1;

                DashboardCountDetails.cancelledRequests += 1;
            } else if (cas.ATG_Status_External__c == STATUS_COMPLETED) {

                if (cas.ClosedDate != null && cas.ClosedDate.Date() > StartMonth)
                    DashboardCountDetails.completedRequestsMonth += 1;

                DashboardCountDetails.completedRequestsAll += 1;
            } else if (cas.ATG_Status_External__c == STATUS_APPROVAL_PENDING)
                DashboardCountDetails.pendingApprovalRequests += 1;
            else if (cas.ATG_Status_External__c  == STATUS_Returned)
                DashboardCountDetails.ReturnedRequests += 1;
        }


        return DashboardCountDetails;
    }

    public class DashboardCount {

        @AuraEnabled
        public integer allRequestsCount, approvedRequestsAll, approvedRequestsToday, assignedRequestsAll,
                assignedRequestsToday, cancelledRequests, completedRequestsAll, completedRequestsToday,
                newRequestsAll, newRequestsToday, sentBackRequests, pendingApprovalRequests,ReturnedRequests ;

        @AuraEnabled
        public integer cancelledRequestsMonth, completedRequestsMonth, allRequestsMonth;

        public DashboardCount() {
            allRequestsCount = 0;
            approvedRequestsToday = 0;
            approvedRequestsAll = 0;
            assignedRequestsAll = 0;
            assignedRequestsToday = 0;
            completedRequestsAll = 0;
            completedRequestsToday = 0;
            newRequestsAll = 0;
            newRequestsToday = 0;
            sentBackRequests = 0;
            ReturnedRequests = 0;
            cancelledRequests = 0;
            pendingApprovalRequests = 0;


            cancelledRequestsMonth = 0;
//            assignedRequestsMonth = 0;
            completedRequestsMonth = 0;
            allRequestsMonth = 0;

        }

    }

    /**
     * @description Retrieves the most recent requests to show on the home page dashboard
     *
     * @return list of most recent cases based for current user
     */
    @AuraEnabled
    public static list<Case> GetMostRecentRequests() {

        User currentUser = ATG_PortalHomePageUtility.GetUserDetails('');
        if (currentUser.User_type__c == USER_APPROVER || currentUser.User_type__c == USER_REQUESTER_APPROVER)
            return [
                    select Id,MOH_Request_Number__c,ATG_CC_CCGR_Number_Value__c,CreatedDate,Status,
                            ATG_ETO_Requestor__r.Name,ATG_ETO_Request_Detaillink__c,ATG_Status_External__c,ATG_Submission_Date_Time__c
                    from Case
                    where ATG_Status_External__c = :STATUS_APPROVAL_PENDING
                    order by ATG_Submission_Date_Time__c  Desc
                    limit 5
            ];
        else
            return [
                    select Id,MOH_Request_Number__c,ATG_CC_CCGR_Number_Value__c,CreatedDate,Status,
                        ATG_ETO_Requestor__r.Name,ATG_ETO_Request_Detaillink__c,ATG_Status_External__c
                        from Case
                        order by createdDate Desc
                        limit 5
                    ];


    }


    /**
     * @description A user settings wrapper object of current User information
     * and help and training user links
     */
    public class UserSettings {
        @AuraEnabled
        public User userInfo;

        @AuraEnabled
        public ATG_HelpTraining__c helpTrainingLink;
    }

    /**
     * @description Fetches users settings of the current user
     *
     * @return UserSettings
     */
    @AuraEnabled
    public static UserSettings GetUserSettings() {
        UserSettings userSettings = new UserSettings();

        userSettings.userInfo = ATG_PortalHomePageUtility.GetUserDetails(null);
        userSettings.helpTrainingLink = ATG_HelpTraining__c.getInstance();

        return userSettings;
    }

    /**
     * @description An helper method to get current user details, and update the language
     *
     * @param changeLanguageTo
     *
     * @return User details
     */
    @AuraEnabled
    public static user GetUserDetails(string changeLanguageTo) {

        user usrObj = [
                select id,LanguageLocaleKey,Name,firstName,middleName,lastName, ContactId, User_Type__c,UserRole.Name,AccountId,ProfileId,Profile.Name
                from user
                where id = :userInfo.getUserId()
        ];


        savepoint sp;
        try {
            Database.setSavepoint();
            if (usrObj != null && !string.IsEmpty(changeLanguageTo)) {
                usrObj.LanguageLocaleKey = changeLanguageTo;
                
                if(changeLanguageTo == 'ar')
                    usrObj.Preferred_Language__c = 'Arabic';
                else
                    usrObj.Preferred_Language__c = 'English';                
                
                    
                update usrObj;
            }
        } catch (exception e) {
            ATG_utility.LogException(e);
            Database.RollBack(sp);
        }

        return usrObj;
    }

    /**
     * @description structure with list of user assigned departments to show on user profile page
     *
     * @return structure of departments & programs keys to list of values
     */
    @AuraEnabled
    public static Map<String, List<Map<string, string>>> GetUserDepartments() {

        Map<String, List<Map<string, string>>> userDepartmentsMap = new Map<String, List<Map<string, string>>>();
        userDepartmentsMap.put('programs', new List<Map<string, string>>());
        userDepartmentsMap.put('departments', new List<Map<string, string>>());


        List<Map<string, string>> programSet = new List<Map<string, string>>();
        List<Map<string, string>> departmentSet = new List<Map<string, string>>();

        for (User_Department__c ud : [SELECT Id, Program__r.Name, Program__r.Name_in_Arabic__c, Program__r.ATG_Active__c,Department__r.Name, Department__r.Name_in_Arabic__c, Department__r.ATG_Active__c FROM User_Department__c WHERE User__c = :UserInfo.getUserId()]) {
            if (ud.Department__r.ATG_Active__c) {
                Map<string, string> dep_relationship = new Map<string, string>{
                        'name' => ud.Department__r.Name, 'name_ar' => ud.Department__r.Name_in_Arabic__c
                };
                departmentSet.add(dep_relationship);
            }

            if (ud.Program__r.ATG_Active__c) {
                Map<string, string> prog_relationship = new Map<string, string>{
                        'name' => ud.Program__r.Name, 'name_ar' => ud.Program__r.Name_in_Arabic__c
                };
                programSet.add(prog_relationship);
            }
        }

        userDepartmentsMap.get('programs').addAll(programSet);
        userDepartmentsMap.get('departments').addAll(departmentSet);

        return userDepartmentsMap;
    }


    /**
     * @description fetches the home donut chart data based on the type
     *
     * @param type - name of chart type
     *
     * @return structure with map name to count
     */
    @AuraEnabled
    public static map<string,integer> GetChartData(string type) {

        map<string, ATG_Case_Status_Configs_PortalChart__c> portalDashboardConfig = ATG_PortalHomePageUtility.GetStageConfigs();
        
        
        
        
        
        map<string, integer> ChartDataMap = new map<string,integer>();       
        if (type == '1'){
            for(case cs : [select  ATG_Status_External__c from case where ATG_Status_External__c in :portalDashboardConfig.keySet() and  RecordTypeId =: ATG_ConstantsUtility.ATG_CASE_ETO_RECORDTYPE Order By ATG_Status_External__c]){
                if(!ChartDataMap.containsKey(cs.ATG_Status_External__c))
                    ChartDataMap.put(cs.ATG_Status_External__c,1);
                else
                    ChartDataMap.put(cs.ATG_Status_External__c,ChartDataMap.get(cs.ATG_Status_External__c)+1);          
            }        
        }            
        return ChartDataMap;
    }


    /**
     * @description Fetches all configurations for the stage
     *
     * @return structure of portal chart custom setting data
     */
    @AuraEnabled
    public static map<string, ATG_Case_Status_Configs_PortalChart__c> GetStageConfigs() {
        map<string, ATG_Case_Status_Configs_PortalChart__c> configMap = new map<string, ATG_Case_Status_Configs_PortalChart__c>();
        User usr = [select id,accountId,User_Type__c from User where id = :userInfo.getUserId()];
        for (ATG_Case_Status_Configs_PortalChart__c dc : ATG_Case_Status_Configs_PortalChart__c.getAll().values()) {
            if((usr.user_type__c == USER_ADMIN && dc.Visible_to_Admin__c) || ((usr.user_type__c == USER_APPROVER || usr.user_type__c == USER_REQUESTER_APPROVER) && dc.Visible_to_Approver__c) || (usr.user_type__c == USER_REQUESTER && dc.Visible_to_Requestor__c)){
                configMap.put(dc.Name, dc);
                if(string.isnotBlank(dc.Status_in_Arabic__c))
                    configMap.put(dc.Status_in_Arabic__c,dc);
                
                
            }else if(test.isRunningTest())   
                configMap.put(dc.Name, dc);
        }      

        return configMap;
        
    }

    /**
     * @description Fetches all home donut type data based on the profile
     *
     * @return donut chart data
     */
    @AuraEnabled
    public static map<string, string> getAccessibleDashboard() {
        User usr = [select id,accountId,User_Type__c,LanguageLocaleKey from User where id = :userInfo.getUserId()];
        map<string, string> ChartConfigs = new map<string, string>();

        for (Portal_Dashboard_Config__c dc : Portal_Dashboard_Config__c.getAll().values()) {
            if (((usr.user_type__c == USER_ADMIN && dc.Visible_to_Admin__c) || ((usr.user_type__c == USER_APPROVER || usr.user_type__c == USER_REQUESTER_APPROVER) && dc.Visible_to_Approver__c) || (usr.user_type__c == USER_REQUESTER && dc.Visible_to_Requestor__c)) && dc.Active__c) {
                if (usr.LanguageLocaleKey.containsAny('Arabic'))
                    ChartConfigs.put(dc.Arabic_Name__c, dc.type__c); else
                        ChartConfigs.put(dc.Report_Name__c, dc.type__c);
            }

        }
       
        return ChartConfigs;


    }

    /**
     * @description fetches account profile fields for the user from the defined field set
     *
     * @return list of field set fields
     */
    @AuraEnabled
    public static List<String> getProfileFields() {
 
        List<String> fnames = new List<String>();
        Schema.SObjectType targetType = Schema.getGlobalDescribe().get('Account');
        Schema.DescribeSObjectResult describe = targetType.getDescribe();
        Map<String, Schema.FieldSet> fsMap = describe.fieldSets.getMap();
        Schema.FieldSet fs = fsMap.get('MoH_User_Profile_Page');

        List<FieldSetMember> fset = new List<FieldSetMember>();
        List<Schema.FieldSetMember> fieldSet;
        if(fs != null && fs.getFields() != null){
            fieldSet = fs.getFields();
            
            for (Schema.FieldSetMember f: fieldSet) {
                fnames.add(f.getFieldPath());
            }            
        }

 
        System.debug('Account Fields ' + fnames);
 
        return fnames;
 
 
    }

}