/**************************************************************************************************
* Name               : ATG_BookingToCustomerLinking                                                               
* Description        : Batch job on booking to link booking records with corporate customers at night
					   once they are inserted in SF through informatica                                       
* Created Date       : 31st July 2018                                                                *
* Created By         : Swati Sehrawat (PWC Digital)                                              
* -------------------------------------------------------------------------------------------------
* Version    Author    Date    Comment                                                               *

**************************************************************************************************/

global without sharing class ATG_Batch_BookingToCustomerLink implements Database.Batchable<sObject>,Database.AllowsCallouts{ 
     
	global Database.QueryLocator start(Database.BatchableContext BC){ 
    	string query = 'select id, ATG_Customer__c, ATG_Customer_Id__c,ATG_Linked_Through_Batch__c from ATG_Booking__c where ATG_Customer_Id__c!=NULL and ATG_Customer__c=NULL' ;
        return Database.getQueryLocator(query); 
	} 
	
	global void execute(Database.BatchableContext BC,list<ATG_Booking__c> bookingList){ 
    	try{ 
    		map<string,ATG_Booking__c> mapOfBookings = new map<string,ATG_Booking__c>();
    		system.debug('---bookingList---'+bookingList);
    		if(bookingList!=null && bookingList.size()>0){
    			for(ATG_Booking__c obj : bookingList)
    				if(obj.ATG_Customer_Id__c!=null)
    					mapOfBookings.put(obj.ATG_Customer_Id__c, obj);	
    		}
    		
    		system.debug('---mapOfBookings---'+mapOfBookings);
    		map<string,Account> existingCustomers = new map<string,Account>();
    		if(mapOfBookings!=null && mapOfBookings.size()>0){
    			for(Account acc : [select id, ATG_Tapro_ID__c from Account where isPersonAccount = false 
    							   and ATG_Tapro_ID__c IN : mapOfBookings.keyset() and recordTypeID!=:ATG_ConstantsUtility.MASTER_PERSON_ACCOUNT_RECORDTYPEID
    							   and recordTypeID!=:ATG_ConstantsUtility.MASTER_BUSINESS_ACCOUNT_RECORDTYPEID]){
    				existingCustomers.put(acc.ATG_Tapro_ID__c,acc);	
    			}	
    		}
    		
    		system.debug('---existingCustomers---'+existingCustomers);
    		if(existingCustomers!=null && existingCustomers.size()>0){
    			for(ATG_Booking__c obj : bookingList){
    				if(existingCustomers.get(obj.ATG_Customer_Id__c)!=null){
	    				obj.ATG_Customer__c = existingCustomers.get(obj.ATG_Customer_Id__c).id;	
	    				obj.ATG_Linked_Through_Batch__c = true;
    				}
    			}	
    			update bookingList;
    		}
    		
    	}catch(Exception ex){ 
			ATG_utility.LogException(ex);
        } 
    } 
    
    global void finish(Database.BatchableContext BC){ 
    }   
}