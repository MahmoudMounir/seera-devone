/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class ATG_Retrieve_PNRTest {

    static testMethod void ATG_Retrieve_PNRTest1() {
        
        
        ATG_Retrieve_PNR objClass = new ATG_Retrieve_PNR();
        
        PNR__c pnr = new PNR__c();
        pnr.Name = 'abc';
        pnr.ATG_IATA_Number__c = '2345';
        insert pnr;
        
        PNR_Passenger__c p = new PNR_Passenger__c();
        p.PNR__c = pnr.id;
        p.Status__c = 'Original';
        insert p;
        
        ATG_Retrieve_PNR.FetchPNRInformation(pnr.Name);
        
        ATG_Retrieve_PNR.ATG_FetchPNR(pnr.Name,null,'0.0.0.0','Mozilla');
        
    }
}