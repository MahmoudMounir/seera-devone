/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class ATG_CustomLookUpControllerTest {

    /*static testMethod void myUnitTest() {
    	ATG_Master__c airlineCode = new ATG_Master__c();
        airlineCode.recordTypeID = Schema.SObjectType.ATG_Master__c.getRecordTypeInfosByName().get('Airline').getRecordTypeId();
        airlineCode.name = 'Emirates';
        airlineCode.Airline_Code__c = 'EK';
		insert airlineCode;
		
        List <sObject> tempList= ATG_CustomLookUpController.fetchLookUpValues('Emirates', 'ATG_Master__c', 'Airline');
        
    }*/
    
     static testMethod void fetchLookUpValuesTest() {
       Test.startTest();
        ATG_Master__c masterObj = new ATG_Master__c();
        masterObj.ATG_Active__c = true;
        masterObj.Name = 'Dubai';
        masterObj.RecordTypeId = ATG_ConstantsUtility.MASTER_City_RECORDTYPEID;
        insert masterObj;
        ATG_CustomLookUpController.fetchLookUpValues('Dubai', 'ATG_Master__c', 'Cities');
        
       Test.stopTest(); 
    }
}