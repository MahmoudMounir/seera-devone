/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
public with sharing class ATG_PopulateUniqueNumberBatchTest {
    @testSetUp
    static void ATG_AccountHandler_TestData(){
        Account Acc = ATG_TestDataFactory.createEmployeeAccount();        
        insert acc;
        
        list<Account> lstAccounts = new list<Account>();
        
        Account obj = new Account();
        obj.firstName = 'Test';
        obj.lastName = 'Test';
        obj.personemail = 'test@test.com';
        obj.personMobilePhone = '+971 555553456';
        obj.recordTypeID = ATG_ConstantsUtility.PERSON_ACCOUNT_RECORDTYPEID;
        obj.ATG_Business_Unit_Picklist__c = 'Tajawal';
        obj.ATG_Master_Account__c = acc.id;
        lstAccounts.add(obj);//insert obj;
        
        obj = new Account();
        obj.firstName = 'Test';
        obj.lastName = 'Test 2';
        obj.personemail = 'test@test.ae';
        obj.personMobilePhone = '+971 555553123';
        obj.recordTypeID = ATG_ConstantsUtility.PERSON_ACCOUNT_RECORDTYPEID;
        obj.ATG_Business_Unit_Picklist__c = 'Tajawal';
        //obj.ATG_Master_Account__c = acc.id;
        obj.ATG_Unique_ID__c = 'TJW-00002345';
        lstAccounts.add(obj);//insert obj;
        
        Account obj2 = new Account();
        obj2.firstName = 'Test';
        obj2.lastName = 'Test';
        obj2.personemail = 'test@test.com';
        obj2.recordTypeID = ATG_ConstantsUtility.PERSON_ACCOUNT_RECORDTYPEID;
        obj2.ATG_Business_Unit_Picklist__c = 'Al Mosafer';
        obj2.ATG_Master_Account__c = acc.id;
        lstAccounts.add(obj2);//insert obj2;
        
        obj2 = new Account();
        obj2.firstName = 'Test';
        obj2.lastName = 'Test 2';
        obj2.personemail = 'test@test.ae';
        obj2.recordTypeID = ATG_ConstantsUtility.PERSON_ACCOUNT_RECORDTYPEID;
        obj2.ATG_Business_Unit_Picklist__c = 'Al Mosafer';
        //obj2.ATG_Master_Account__c = acc.id;
        obj.ATG_Unique_ID__c = 'ALM-00002345';
        lstAccounts.add(obj2);//insert obj2;
        
        insert lstAccounts;
        
        Case objCase = new Case();
        objCase.subject = 'test';
        objCase.AccountId = lstAccounts[0].Id;
        insert objCase;
        
        ATG_Booking__c objBooking = new ATG_Booking__c();
        objBooking.ATG_Customer__c = lstAccounts[0].Id;
        objBooking.ATG_Contact_Person__c = lstAccounts[0].Id;
        //insert objBooking;
        
        Note objNote = new Note();
        objNote.Title = 'Test';
        objNote.ParentId = obj.Id;
        objNote.Body = 'yt';
        //insert objNote;
        
    }
    @isTest
    static void myTest() {
        ATG_PopulateUniqueNumberBatch objBatch = new ATG_PopulateUniqueNumberBatch();
        test.startTest();
            database.executeBatch(objBatch );
        	string query = 'select Id,RecordTypeId,ATG_Unique_ID__c,ATG_Golden_ID__c,ATG_Business_Unit_Picklist__c from Account where RecordTypeId != null order by CreatedDate' ;
            list<Account> lstAccounts = Database.query(query);
        	objBatch.populateUniqueIds(lstAccounts);
        test.stopTest();
        objBatch.execute(null);
        //objBatch.dummy();        
        
    }
}