/**************************************************************************************************
* Name               : ATG_CommunityUserManagementController                                                               
* Description        : Controller to manage user creation screen                                   
* Created Date       : 30 Jul 2018                                                              *
* Created By         : PWC Digital                                              
* -------------------------------------------------------------------------------------------------
* Version       Author      Date        Comment                                                               *

**************************************************************************************************/

public without sharing class ATG_CommunityUserManagementController {


   
    
   @AuraEnabled
    public static CommunityMessages CreateCommunityUser(Id accountId,Boolean Isactive){    


            Account accObj = [select Id,firstName,PersonContactId,LastName,MiddleName,PersonEmail,PersonMobilePhone,ATG_User_Type__c,ATG_Community_User__c,ATG_Community_User__r.IsActive from Account where id=:accountId];
            
            
            // Validation Check 
            
            string validationMessage;
            system.debug('IsActive++'+Isactive);
            validationMessage = ATG_CommunityUserManagementController.CheckUserManagementValidationRules(accObj,Boolean.ValueOf(Isactive));
            if(validationMessage != 'Success'){
                    return new CommunityMessages('Error',validationMessage);
            }   
                
                
            //Get Profile Ids      
            map<string,UserType_Profile_Mapping__c> utProfile = UserType_Profile_Mapping__c.getAll();       
            
            string profId ;
             User usrObj;
            // Create user Only if user is not existing 
            Savepoint sp;
            try{
                Database.setSavepoint();
                if(accObj.ATG_Community_User__c ==null && isActive){
                    if(utProfile !=null && utProfile.containsKey(accObj.ATG_User_Type__c)){         
                        for(Profile profObj :[select id,Name from profile where Name= : utProfile.get(accObj.ATG_User_Type__c).Profile_Name__c])
                            profId =  profObj.Id;
                        
                    }
                     if(!string.isBlank(profId)){
                         usrObj = new User(ProfileId=profId,firstName=accObj.firstName,LastName=accObj.LastName,MiddleName = accObj.MiddleName,MobilePhone=accObj.PersonMobilePhone,Email=accObj.PersonEmail,ContactId=accObj.PersonContactId);
                         usrObj.UserName = ATG_CommunityUserManagementController.GenerateUserName(usrObj.Email);        
                         usrObj.timezonesidkey = 'Asia/Riyadh';          
                         usrObj.IsActive = Isactive;
                         usrObj.languagelocalekey = 'en_US'; 
                         usrObj.localesidkey = 'en_US';
                         usrObj.Alias  = usrObj.UserName.split('@')[0].substring(0,2);
                         usrObj.EmailEncodingKey = 'ISO-8859-1'; 
                         usrObj.profileId = profId ;
                         usrObj.User_Type__c = accObj.ATG_User_Type__c;
                         system.debug('&&&&&'+profId);
                       
                        Database.DMLOptions dmo = new Database.DMLOptions();
                        dmo.EmailHeader.triggerUserEmail = true;       
                        dmo.EmailHeader.triggerOtherEmail = false;
                        dmo.EmailHeader.triggerAutoResponseEmail = false;       
                        dmo.optAllOrNone = false;
                        
                        usrObj.setOptions(dmo);
                        
                        insert usrObj;                 
                        
                        ATG_CommunityUserManagementController.updateCommunityUseronAccount(accountId,usrObj.Id);
                        
                        return new CommunityMessages('Success',Label.UserActivationSuccessMessage);
                    }else
                        return new CommunityMessages('Error','Error');
                    
                }else if(accObj.ATG_Community_User__c != null && accObj.ATG_Community_User__r.IsActive && !isActive){
                    user userObjtoDeactivate = new user(id =  accObj.ATG_Community_User__c,IsActive = false);
                    update userObjtoDeactivate;                    
                    return new CommunityMessages('Success',Label.UserDeActivationSuccessMessage);               
                }else if(accObj.ATG_Community_User__c != null && !accObj.ATG_Community_User__r.IsActive && isActive){
                    user userObjtoDeactivate = new user(id =  accObj.ATG_Community_User__c,IsActive = true);
                    update userObjtoDeactivate;                     
                   return new CommunityMessages('Success',Label.UserActivationSuccessMessage);                 
                   
                }
                 
                 return new CommunityMessages('Success','Success');                 
            }catch(exception e){               
                system.debug('JJJJJ*'+e.getStackTraceString()+usrObj);  
                Database.Rollback(sp); 
                ATG_utility.LogException(e);             
                return new CommunityMessages('Error',e.getMessage());                     
            }     
            
               
    
    }
    
    
    // Method to validate user creation Quickaction validations
    
    public static string CheckUserManagementValidationRules(Account acc,Boolean isActive){        
        if(acc.ATG_Community_User__c !=null && (acc.ATG_Community_User__r.IsActive == isActive) && isActive)
            return Label.User_is_Already_Active;
        else if(acc.ATG_Community_User__c !=null && (acc.ATG_Community_User__r.IsActive == isActive) && !isActive)
            return Label.User_is_Already_InActive;        
        return 'Success';        
    }
    
    // Method to Update the Community User on Account
    public static void updateCommunityUseronAccount(string accountId,string userId){
        Account acc = new Account(id=accountId,ATG_Community_User__c = userId,ownerId=Label.ATG_Admin_User);
        try{
            update acc;
        }catch(exception e){ATG_utility.LogException(e);}
    }   
    
    
    public static string GenerateUserName(string emailString){
        string userName;
        string userNameSrchString;
        if(!String.IsEmpty(emailString) && emailString.containsAny('@')){
            userName = emailString.split('@')[0];
            userNameSrchString =  '%'+userName +'%';
            integer usrsCnt =  [select count() from user where username like : userNameSrchString];
            
            if(usrsCnt !=null && usrsCnt >0){ usrsCnt++;      
                userName +=string.ValueOf(usrsCnt)+'@'+emailString.split('@')[1]; //Label.CommunityDomainName;
                return   userName ;
            }
        }
        
        system.debug('## User Name'+userName);
        
        return emailString;
    }
    
    
    @AuraEnabled
    public static CommunityMessages RestPassword(Id accountId){
        try{
            Account acc = [select Id,ATG_Community_User__c,ATG_User_Type__c,ATG_CommunityUser_IsActive__c from Account where id = : accountId and ATG_Community_User__r.isActive = true];
            if(acc.ATG_CommunityUser_IsActive__c && acc.ATG_Community_User__c  !=null && (acc.ATG_User_Type__c== 'Approver' || acc.ATG_User_Type__c == 'Requestor' ||  acc.ATG_User_Type__c == 'Program Coordinator')){
                system.resetPassword(acc.ATG_Community_User__c,true);                
                return new CommunityMessages('Success',Label.Password_Reset_Success);
             }else if((acc.ATG_Community_User__c  == null || !acc.ATG_CommunityUser_IsActive__c) && (acc.ATG_User_Type__c== 'Approver' || acc.ATG_User_Type__c== 'Requestor' ||  acc.ATG_User_Type__c == 'Program Coordinator'))         
                return new CommunityMessages('Error',Label.Password_Reset_Error); 
             else
                 return new CommunityMessages('Error',Label.ManageUsers_Error) ;  
        
        }catch(exception e){
            return new CommunityMessages('Error',Label.Password_Reset_Error);      
        }   
    }   
       
      
    //Method to update user departments to user
    @AuraEnabled
    public static string updateUserDepartments(string accountId,string userId,list<string> programsToAssociate,list<string> departmentsToAssociate){
        

        // Delete User Relationships
        list<User_Department__c> udListToRemove = new list<User_Department__c>();  
        list<User_Department__c> useDepartmentListToUpsert = new list<User_Department__c>();
        
        if((accountId !=null && userId !=null))
            for(User_Department__c ud : [select id from user_Department__c where ATG_Account__c =:accountId and User__c=:userId])
                udListToRemove.add(ud);
        
        Delete udListToRemove;
        
        // Upsert the selected      
                     
        if(programsToAssociate !=null && !departmentsToAssociate.isEmpty()){                                       
            for(ATG_Department_Relationship__c  dr : [Select id,Department__c,Program__c from ATG_Department_Relationship__c where Department__c in : departmentsToAssociate and Program__c in : programsToAssociate]){                        
                string uniqueKey = userId+dr.program__c+dr.Department__c; // UserId+Program+Department Relationship ID(18 digit Ids)
                useDepartmentListToUpsert.add(new User_Department__c(User__c = userId,Program__c = dr.program__c, Department__c = dr.Department__c,Department_Relationship__c = dr.id,Unique_Identifier__c = uniqueKey));
             }    
             if(useDepartmentListToUpsert !=null && !useDepartmentListToUpsert .isEmpty())
                upsert useDepartmentListToUpsert Unique_Identifier__c;   
        
            return 'Success'; 
        }
        
              return accountId+'UsrID : '+userId+'progsId'+programsToAssociate;
            
    }   
     
    
    
    // Method to fetch multiple Programs.
    
    public static list<ATG_Master__c> FetchPrograms(){
        list<ATG_Master__c> masterList = new list<ATG_Master__c>();
        string recordType = ATG_ConstantsUtility.ATG_MASTER_Program_RT;
        string masterQueryString = 'select id,name,RecordType.developerName,Name_in_Arabic__c  from ATG_Master__c';
        string filterCondition = ' where RecordTypeId =:recordType and ATG_Active__c=true';  
        
        list<string> programsToView = new list<string>();
        User currentUser = [select id,User_Type__c,contactId,accountId from user where id = : userInfo.getUserId() limit 1];
        if(currentUser.User_Type__c == 'Approver'){             
            
            for(User_Department__c udd  : [select Id,Program__c from  User_Department__c where User__c =:currentUser.Id and ATG_Account__c = : currentUser.accountId])             
                programsToView.add(udd.Program__c);        
            
            filterCondition += ' and id in : programsToView';
        } 
        
        masterQueryString +=filterCondition ;
        
        return database.Query(masterQueryString);
    }
    
    // Fetch Departments related to selected Program.
    @AuraEnabled
    public static list<ATG_Master__c> FetchDepartments(list<id> selectedPrograms){        
        /*return [select id,Department__r.id,Department__r.Name,Program__c,Program__r.Name from ATG_Department_Relationship__c
                                                                  where Program__c in : selectedPrograms];     */
         
         map<id,ATG_Master__c> depMap = new map<id,ATG_Master__c>();
         for(ATG_Department_Relationship__c dr : [select id,Department__r.id,Department__r.Name,Program__c,Program__r.Name,Program__r.Name_in_Arabic__c ,Department__r.Name_in_Arabic__c from ATG_Department_Relationship__c
                                                                   where Program__c in : selectedPrograms]){
                depMap.put(dr.Department__c,dr.Department__r);                                                       
         } 
         
         return depMap.values();                                                                                                                 
                                                                                                          
    }


    public class CommunityMessages{
        @AuraEnabled
        public string communityMessageTyp,communitymsg; 
        
        public  CommunityMessages(string communityMessageTyp,string communitymsg){
            this.communityMessageTyp = communityMessageTyp;
            this.communitymsg = communitymsg;
        }       
    }
    
    
    @AuraEnabled
    public static map<string,list<string>> GetUserDepartments(){
        map<string,list<string>> userDepartmentsMap = new map<string,list<string>>();
        userDepartmentsMap.put('programs',new list<string>());
        userDepartmentsMap.put('departments',new list<string>());
        set<string> programsSet = new set<string>();
        set<string> departmentsSet = new set<string>();
        for(User_Department__c ud : [select id,program__r.Name,program__r.ATG_Active__c,Department__r.Name,Department__r.ATG_Active__c from User_Department__c where User__c =:userInfo.getUserId()]){
        
            if(ud.Department__r.ATG_Active__c)
               departmentsSet.add(ud.Department__r.Name);
            if(ud.program__r.ATG_Active__c)
               programsSet.add(ud.Program__r.Name) ;              
            
        }
         userDepartmentsMap.get('programs').addAll(programsSet);  
        userDepartmentsMap.get('departments').addAll(departmentsSet);
        system.debug('*****'+userDepartmentsMap);
        return userDepartmentsMap;
    
    }
    
    
    @AuraEnabled
    public static UserDetailCls  GetUserDetails(string accountId){
        UserDetailCls  userDetailObj = new UserDetailCls(accountId);      
        return userDetailObj;
    }   
    
    // User Details structure using on the usermanagement community screen. 
    public class UserDetailCls {
        @AuraEnabled
        public user userObj;
        @AuraEnabled
        public  list<id> availableDepartments,selectedPrograms,selectedDepartments,
                                    programsToRemove,departmentsToRemove; 
        @AuraEnabled
        public list<ATG_Master__c>  availablePrograms;  
        @AuraEnabled
        public Account accDetail;    
        
                                 
        
        public UserDetailCls(string accId){
            
            selectedPrograms = new list<id>();
            selectedDepartments = new list<id>();   
            userObj = new User();           
            set<id> selectedProgSet = new set<id>();
            set<id> selectedDepSet = new set<id>();
            accDetail  = new Account();
            
            set<id> approverProgSet = new set<id>();
            
            // fetch User details,Selected Programs, selected Departments 
            if(String.isNotEmpty(accId) && String.isNotBlank(accId)){
                
                for(Account acc : [select id,ATG_User_Type__c from Account where id = : accId])
                    accDetail = acc;
                    
                 User currentUser = [select id,User_Type__c,contactId,accountId from user where id = : userInfo.getUserId() limit 1];
                if(currentUser.User_Type__c == 'Approver'){      
                    
                    for(User_Department__c udd  : [select Id,Program__c from  User_Department__c where User__c =:currentUser.Id and ATG_Account__c = : currentUser.accountId])             
                        approverProgSet.add(udd.Program__c);            
                    
                }     
            
            
                for(User usr : [select id,profile.Name,(select id,name,program__r.Name,program__r.id,
                                       Department__r.Name,Department__r.id from User_Departments__r) from user where accountId =: accId]){
                    userObj =   usr;
                    for(User_Department__c ud : usr.User_Departments__r){
                       
                        if(currentUser.User_Type__c == 'Approver' && approverProgSet.contains(ud.program__c))
                            selectedProgSet.add(ud.program__c);
                         else if(currentUser.User_Type__c != 'Approver')
                            selectedProgSet.add(ud.program__c);   
                            
                        selectedDepSet.add(ud.Department__c);                       
                    }   
                
                }
            }
            
            selectedPrograms.addAll(selectedProgSet);
            selectedDepartments.addAll(selectedDepSet);
            // Get list of Programs         
            availablePrograms = ATG_CommunityUserManagementController.FetchPrograms();
                        
        }
        
    }
    
     @AuraEnabled
    public static CommunityMessages UpdateUserRelationships(string accountID,string UserId,list<string> programsList,list<string> departmentsList,string userType){
        list<User_Department__c> userDepartmentListToInsert = new list<User_Department__c>();
        list<User_Department__c> userDepartmentListToDelete = new list<User_Department__c>();
        list<string> progsToIgnore = new list<string>();
        list<string> depToIgnore= new list<string>();
        savepoint sp;
        try{
            database.setSavepoint();           
            
            User currentUser = [select id,User_Type__c,contactId,accountId from user where id = : userInfo.getUserId() limit 1];
            

            if(currentUser.User_Type__c == 'Approver'){     
                for(User_Department__c udd  : [select Id,Program__c,Department__c from  User_Department__c where ATG_Account__c = : currentUser.accountId and  User__c =: currentUser.Id]) {            
                    progsToIgnore.add(udd.Program__c); 
                    depToIgnore.add(udd.Department__c);
                }     
                userDepartmentListToDelete = [select id from User_Department__c where ATG_Account__c =: accountID and Program__c  in : progsToIgnore];
            }else
                userDepartmentListToDelete = [select id from User_Department__c where ATG_Account__c =: accountID];
            if(userDepartmentListToDelete  !=null && userDepartmentListToDelete .size()>0)
                delete userDepartmentListToDelete;  
            
            
            if(userType == 'Requestor'){
                for(ATG_Department_Relationship__c depRel : [select id,Department__c,Program__c from ATG_Department_Relationship__c where Department__c in : departmentsList and Program__c in : programsList and Program__r.ATG_Active__c= true and Department__r.ATG_Active__c = true ]){ //and(program__c not in : progsToIgnore and Department__c not in : depToIgnore)
                    string uniqueKey = accountID+depRel.program__c+depRel.Department__c;
                    userDepartmentListToInsert.add(new User_Department__c(ATG_Account__c=accountID,User__c=UserId,Department__c=depRel.Department__c,Program__c=depRel.Program__c,Unique_Identifier__c=uniqueKey));
                }
            }else if(userType == 'Approver'){
                for(ATG_Master__c prog : [select id from ATG_master__c  where id in : programsList and ATG_Active__c= true]){                   
                    userDepartmentListToInsert.add(new User_Department__c(ATG_Account__c=accountID,User__c=UserId,Program__c=prog.id));
                }       
            } 
            
            insert userDepartmentListToInsert ;
            
            return new CommunityMessages('Success',Label.Manage_Users_Success);  
        
        }catch(exception e){
            
            Database.RollBack(sp);
            return new CommunityMessages('Error',Label.Manage_Users_Error);  
        }
    
    
    
    }
    
    
    
    
    // Method to create a community user upon accountInsert 
    @InvocableMethod(label='Create Community User' description='Create community user upon accountInsert')
    public static void CreateCommunityUser(List<ID> ids) {  
        ATG_CommunityUserManagementController.CreateCommunityUser(ids[0],true);
    }  
    
    
    @future
    public static void updateCommunityUserAsync(list<ID> accountIds){           
        list<User> userList = new list<User>();        
          //Get Profile Ids      
        map<string,string> utProfile = new map<string,string>();
        map<string,string> profileIdNameMap = new map<string,string>();
        
        for(UserType_Profile_Mapping__c utype : UserType_Profile_Mapping__c.getAll().Values())
            utProfile.put(utype.Name,utype.Profile_Name__c);     
        
        if(utProfile !=null){         
            for(Profile profObj :[select id,Name from profile where Name= : utProfile.values()])
              profileIdNameMap.put(profObj.Name,profObj.Id);                       
        }      
        
        for(Account acc : [select id,LastName,FirstName,MiddleName,PersonEmail,ATG_User_Type__c,PersonMobilePhone,ATG_Community_User__c from Account  where Id in : accountIds and ATG_Community_User__c !=null])
            userList.add(new user(id=acc.ATG_Community_User__c,Email =acc.PersonEmail,FirstName=acc.FirstName,MiddleName=acc.MiddleName,User_Type__c = acc.ATG_User_Type__c,profileId=profileIdNameMap.get(utProfile.get(acc.ATG_User_Type__c))));   
       try{     
           update userList;
       }catch(exception e){ATG_utility.LogException(e);}
    }  
    
    
    
    
       
    
    
  
    
    
    
    
    
}