/**************************************************************************************************
* Name               : ATG_SocialPostTriggerHandler
* Description        : Trigger Handler for the SocialPost SObject. This class implements the ATG_TriggerInterface
                      to help ensure the trigger code is bulkified and all in one place.                                       
* Created Date       : 22nd October 2018                                                                *
* Created By         : Paramasivan (PWC Digital)                                              
* -------------------------------------------------------------------------------------------------
* Version       Author      Date            Comment                                                               *
1.1             Leeba       12/12/2018      Logic for the case to go back to queue when a post comes from the client if
                                            the agent is offline. ITS Tickte # 51321
v1.2            Ravi        03/Feb/2018     Create/update the case and link it the Social Post which is getting created from Informatica
**************************************************************************************************/

public without sharing class ATG_SocialPostTriggerHandler implements ATG_TriggerInterface{
	
	public static map<string, string> mapMatrix = new map<string,string>();
	    
    public void beforeInsert(list<SObject> soNewList){
        assignOwners(soNewList);
        linkToParentPost(soNewList);        
    }
	
	public static set<string> setCaseStatus = new set<string>{ // if case status is any one of these then Case Owner will be changed to Queue
		'Pending - Third Party',
		'Pending Internally'
	};
	    
    public void beforeUpdate(list<SObject> soNewList,map<id,SObject> soOldMap){}

    public void beforeDelete(list<SObject> soNewList){}

    public void afterInsert(list<SObject> soNewList){
      try{
            UpdateCaseOwner(soNewList);//1.1
            populatefields(soNewList);
        }catch(exception ex){ 
            ATG_utility.LogException(ex);
        }
    }

    public void afterUpdate(list<SObject> soNewList,map<id,SObject> soOldMap){
      
    }

    public void afterDelete(list<SObject> soNewList){}
    
    // This method is added for changing the status of the case based on the posting user
    public void populatefields(list<SObject> soNewList){
        list<case> caseList = new List<case>();
        try{
            List<Socialpost> soPostList = (List<SocialPost>)soNewList;
            
            //v1.1
            map<Id,SocialPost> mapSocialPostsToLink = new map<Id,SocialPost>();
            
            if(soPostList!=null && soPostList.size()>0) {
                for(Socialpost soPostref : soPostList){
                    string recordId = soPostref.parentId;
                    
                    if(string.isNotBlank(recordId) && recordId.startswith('500')){
                        case caseref = new case(id = soPostref.parentId);
                        if(userinfo.getName().contains('Integration') && caseref.status != 'closed'){
                            caseref.status = 'In Progress';
                            caseList.add(caseref);
                        }
                        /* Commented by Ravi - As Rana don't want to change the status to Pending - Customer
                        if(!userinfo.getName().contains('Integration') && caseref.status != 'closed'){
                            caseref.status = 'Pending - Customer';
                        }*/
                    }
                    
                    if(soPostref.ATG_Is_From_ICRT__c){ //v1.2 - added the else block - if case is blank create or link it from exiting open cases
                        mapSocialPostsToLink.put(soPostref.Id,soPostref);
                    }else{
                        if((soPostref.Provider == 'Instagram' || soPostref.Provider == 'Facebook') && soPostref.IsOutbound == false && soPostref.ParentId == null && 
                        	(soPostref.ATG_Business_Unit__c == ATG_ConstantsUtility.TAJAWAL_BusinessUnit || soPostref.ATG_Business_Unit__c == ATG_ConstantsUtility.ALMOSAFER_BusinessUnit ) ){
                            mapSocialPostsToLink.put(soPostref.Id,soPostref);
                        }
                    }
                }
                
                if(caseList.size() > 0){
                  update caseList;
                }
                if(!mapSocialPostsToLink.isEmpty()){
                    linkCasesToPosts(mapSocialPostsToLink);
                }
                
            }
        }catch(Exception ex){
            system.debug('---Exception is-----'+ex.getLineNumber());
            ATG_utility.LogException(ex);
        }
    }
    
    //1.1 - This method is to change the caase owner to queue when a post comes from a user.
    public static void UpdateCaseOwner(List<SocialPost> lstNew){
        set<string> setCaseOwnerIds = new set<string>();
        map<string,boolean> MapUserOnlineStatus = new map<string,boolean>();
        map<string,Case> MapCaseTBU = new map<string,Case>();
        for(SocialPost post:lstNew){
            if(post.ATG_Is_From_ICRT__c == false){ //v1.2 - this logic should not fire when case getting created/assigned for MVP2
                if(post.IsOutbound==false && post.Case_Owner_Id__c != null && post.Case_Owner_Id__c.startswith('005') && post.BU_From_case__c=='Al Tayyar'){
                    setCaseOwnerIds.add(post.Case_Owner_Id__c);
                }
            }
        }
        if(setCaseOwnerIds.size()>0)
            MapUserOnlineStatus = ATG_utility.getUserPresenceStatus(setCaseOwnerIds);
            
        for(SocialPost post:lstNew){
            if(post.ATG_Is_From_ICRT__c == false){ //v1.2 - this logic should not fire when case getting created/assigned for MVP2
                if(post.IsOutbound==false && post.Case_Queue_Id__c!=null && (post.Case_Owner_Id__c.startswith('005') || system.test.isrunningtest()) && MapUserOnlineStatus.get(post.Case_Owner_Id__c)==null)
                    MapCaseTBU.put(post.ParentId,new Case(Id=post.ParentId,OwnerId=post.Case_Queue_Id__c));
            }
        }
        
        if(MapCaseTBU.size()>0)
            update MapCaseTBU.values();
    }
    
    //v1.1
    
    public static void assignOwners(list<SocialPost> lstSocialPosts){
        map<string, string> mapMatrixBUs = new map<string,string>();
        map<Id, SocialPost> mapOutboundParents = new map<Id, SocialPost>();
		map<string, string> mapBUOwners = new map<string,string>();
        
        for(SocialAccountMatrix__c objCS : SocialAccountMatrix__c.getAll().values()){
            if(objCS.FacebookAccountName__c != null){
                mapMatrix.put(objCS.FacebookAccountName__c, objCS.Facebook_Queue_Id__c);
                mapMatrixBUs.put(objCS.FacebookAccountName__c, objCS.ATG_Business_Unit__c);
            }
            if(objCS.InstagramAccountName__c != null){
                mapMatrix.put(objCS.InstagramAccountName__c, objCS.Instagram_Queue_Id__c);
                mapMatrixBUs.put(objCS.InstagramAccountName__c, objCS.ATG_Business_Unit__c);
            }
            if(objCS.TwitterAccountName__c != null){
                mapMatrix.put(objCS.TwitterAccountName__c, objCS.Twitter_Queue_Id__c);
                mapMatrixBUs.put(objCS.TwitterAccountName__c, objCS.ATG_Business_Unit__c);
            	mapBUOwners.put(objCS.ATG_Business_Unit__c, objCS.Twitter_Queue_Id__c);
            }
            
        }
        
        for(SocialPost objPS : lstSocialPosts){
            if(objPS.IsOutbound && objPS.ReplyToId != null){
                mapOutboundParents.put(objPS.ReplyToId, null);
            }
        }

        if(!mapOutboundParents.isEmpty()){
            mapOutboundParents = new map<Id, SocialPost>([select Id,OutboundSocialAccountId,TopicProfileName,ATG_Business_Unit__c from SocialPost where Id IN : mapOutboundParents.keySet()]);
        }

        for(SocialPost objPS : lstSocialPosts){
            if(objPS.IsOutbound && objPS.ReplyToId != null && mapOutboundParents.containsKey(objPS.ReplyToId) ){
                SocialPost objParent = mapOutboundParents.get(objPS.ReplyToId);
                objPS.TopicProfileName = objParent.TopicProfileName;
                objPS.ATG_Business_Unit__c = objParent.ATG_Business_Unit__c;
            }
            
            string profilename = objPS.TopicProfileName;
            if(profilename != null ){
                profilename = profilename.indexOf('SCS ') != -1 ? profilename.replace('SCS ','') : profilename;
                if(objPS.Provider == 'Twitter' && profilename.indexOf('@') != 0)
                    profilename = '@'+profilename;
                if(mapMatrix.get(profilename) != null) //  && objPS.IsOutbound == false
                    objPS.OwnerId = mapMatrix.get(profilename);
                if(mapMatrixBUs.containsKey(profilename))
                    objPS.ATG_Business_Unit__c = mapMatrixBUs.get(profilename);
            }
            if(objPS.OwnerId != null && string.valueOf(objPs.OwnerId).indexOf('005') == 0 && mapBUOwners.get(objPS.ATG_Business_Unit__c) != null)
            	objPS.OwnerId = mapBUOwners.get(objPS.ATG_Business_Unit__c);
        }
    }
    
    public static void linkToParentPost(list<SocialPost> lstSocialPosts){
        map<string,string> mapParentPosts = new map<string,string>();
        map<string,string> mapPersonaIds = new map<string,string>();
        
        for(SocialPost objPS : lstSocialPosts){
            if(objPS.ATG_Is_From_ICRT__c){
                if(objPS.ATG_Parent_Post_Id__c != null){
                    mapParentPosts.put(objPS.ATG_Parent_Post_Id__c,null);
                }
                if(objPS.ATG_Persona_UserId__c != null && objPS.PersonaId == null){
                	mapPersonaIds.put(objPS.ATG_Persona_UserId__c, null);
                }
                if(objPS.Provider == 'Twitter' && objPS.MessageType == 'Tweet' && objPS.Name.indexOf('TWEET FROM:') != 0){
                    objPS.Name = 'TWEET FROM: '+objPS.Name;
                }
            }
        }
        
        if(!mapPersonaIds.isEmpty()){
        	for(SocialPersona objPersona : [select Id,ExternalId from SocialPersona where ExternalId IN : mapPersonaIds.keySet()]){
        		mapPersonaIds.put(objPersona.ExternalId, objPersona.Id);
        	}
        	for(SocialPost objPS : lstSocialPosts){
        		if(objPS.ATG_Persona_UserId__c != null && objPS.PersonaId == null && mapPersonaIds.get(objPS.ATG_Persona_UserId__c) != null){
        			objPS.PersonaId = mapPersonaIds.get(objPS.ATG_Persona_UserId__c);
        		}
        	}
        }
        
        if(!mapParentPosts.isEmpty()){
            for(SocialPost objPost : [select Id,ExternalPostId,ParentId,PersonaId from SocialPost where ExternalPostId IN : mapParentPosts.keySet()]){
                for(SocialPost objPS : lstSocialPosts){
                    if(objPS.ATG_Parent_Post_Id__c != null && objPS.ATG_Parent_Post_Id__c == objPost.ExternalPostId){
                        objPS.ReplyToId = objPost.Id;
                        if(objPost.PersonaId != null && objPS.ATG_Persona_UserId__c == string.valueOf(objPost.PersonaId)){
                            objPS.PersonaId = objPost.PersonaId;
                        	objPS.ParentId = objPost.ParentId;
                        }
                    }
                }
            }
        }       
    }
    
    public static void linkCasesToPosts(map<Id,SocialPost> mapSocialPosts){
        // check for social personn, if not found create a new one
        map<string, SocialPersona> mapSocialPersonas = new map<string, SocialPersona>();
        set<string> setPersonnaExtIds = new set<string>();
        list<SocialPersona> lstSocialPersonas = new list<SocialPersona>();
        map<string, Case> mapCases = new map<string, Case>();
        list<SocialPost> lstSocialPostToUpdate = new list<SocialPost>();
        map<Id,Case> mapCaseStatus = new map<Id, Case>();
        set<string> setSocialProviders = new set<string>();
        set<string> setProviders = new set<string>();
        set<string> MessageTypes = new set<string>();
        set<string> PersonaIds = new set<string>();
        map<Id, SocialPost> mapDMPosts = new map<Id, SocialPost>();
        map<Id, SocialPost> mapNormalPosts = new map<Id, SocialPost>();
        
        for(Socialpost objSP : mapSocialPosts.values()){
                if(objSP.ATG_Persona_UserId__c != null){
                    setPersonnaExtIds.add(objSP.ATG_Persona_UserId__c);
                }
                if(objSP.ATG_Persona_Name__c != null){
                    setPersonnaExtIds.add(objSP.ATG_Persona_Name__c);
                }
             
            if(objSP.ParentId == null && objSP.ExternalPostId != null){
                mapCases.put(objSP.ExternalPostId, null);
            }
            
            if(objSP.ATG_Business_Unit__c != null){
                setSocialProviders.add(objSP.ATG_Business_Unit__c);
            }
            
            if(objSP.Provider != null){
                setProviders.add(objSP.Provider);
            }
            
            if(objSP.MessageType != null){
                MessageTypes.add(objSP.MessageType);
            }
        }
        
        system.debug('setPersonnaExtIds is : '+setPersonnaExtIds);
        
        if(!setPersonnaExtIds.isEmpty()){
            for(SocialPersona objSP : [select Id,Name,ExternalId,Provider,MediaType, Account__c, Account__r.ATG_Business_Unit_Picklist__c, Account__r.RecordType.DeveloperName, Account__r.RecordTypeId//,
                                        //(select Id,ParentId,Provider,MessageType,PersonaId from Posts where ATG_Business_Unit__c IN : setSocialProviders AND ParentId != null AND Provider IN : setProviders order by CreatedDate desc) 
                                        from SocialPersona where (ExternalId IN : setPersonnaExtIds OR Name IN : setPersonnaExtIds)]){
                mapSocialPersonas.put(objSP.ExternalId, objSP);
                if(setPersonnaExtIds.contains(objSP.ExternalId))
                    setPersonnaExtIds.remove(objSP.ExternalId);
                PersonaIds.add(objSP.Id);
            }
            
            if(!setPersonnaExtIds.isEmpty()){
                //Create the Personna
                for(Socialpost objSP : mapSocialPosts.values()){
                    if(objSP.ATG_Persona_UserId__c != null && setPersonnaExtIds.contains(objSP.ATG_Persona_UserId__c)){
                        lstSocialPersonas.add(
                            new SocialPersona(
                                ParentId = objSP.Id,
                                Name = objSP.ATG_Persona_Name__c,
                                ProfileUrl = '',
                                RealName = objSP.ATG_Persona_Display_Name__c,
                                ExternalId = objSP.ATG_Persona_UserId__c,
                                ExternalPictureURL = '',
                                Provider = objSP.Provider
                            )
                        );
                        setPersonnaExtIds.remove(objSP.ATG_Persona_UserId__c);
                    }
                }
                
                if(!lstSocialPersonas.isEmpty()){
                    insert lstSocialPersonas;
                    for(SocialPersona objP : lstSocialPersonas){
                        mapSocialPersonas.put(objP.ExternalId, objP);
                    }
                }
            }
        }
        
        if(!PersonaIds.isEmpty()){
        	for(SocialPost obj :[select Id,ParentId,Provider,MessageType,PersonaId from SocialPost 
        		where PersonaId IN : PersonaIds AND ATG_Business_Unit__c IN : setSocialProviders AND
        		MessageType != 'Private'  AND MessageType != 'Direct' AND 
        		ParentId != null AND Provider IN : setProviders order by CreatedDate desc limit 1]){
        		
        		mapNormalPosts.put(obj.PersonaId, obj);
        		mapCaseStatus.put(obj.ParentId,null);
        	}
        	
        	for(SocialPost obj :[select Id,ParentId,Provider,MessageType,PersonaId from SocialPost 
        		where PersonaId IN : PersonaIds AND ATG_Business_Unit__c IN : setSocialProviders AND
        		(MessageType = 'Private' OR MessageType = 'Direct') AND 
        		ParentId != null AND Provider IN : setProviders order by CreatedDate desc limit 1]){
        		
        		mapDMPosts.put(obj.PersonaId, obj);
        		mapCaseStatus.put(obj.ParentId,null);
        	}
        }
        
        if(!mapCases.isEmpty()){
            map<string, string> queueMap = new map<string, string>();
            map<string, Boolean> mapUserOnlineStatus = new map<string, boolean>();
            
            
            if(!mapCaseStatus.isEmpty()){
                mapCaseStatus = new map<Id,Case>([select Id,IsClosed,Status,ATG_BU_From_Case__c,ATG_Current_Queue_Id__c,OwnerId,ClosedDate from Case where Id IN : mapCaseStatus.keySet()]);
            	
            	set<string> queueName = new set<string>();
	            queueName.add('AMH_CS_Social_Live_Chat');
	            queueMap = ATG_utility.getQueueIdFromName(queueName);
            	
            	for(Case objCase : mapCaseStatus.values()){
            		if(string.valueOf(objCase.OwnerId).indexOf('005') == 0){
            			mapUserOnlineStatus.put(objCase.OwnerId, false);
            		}
            	}
            }
            
            AssignmentRule[] rules = [select id from AssignmentRule where SobjectType = 'Case' and Active = true limit 1];
            
            InboundSocialPostHandlerImpl objISPH = new InboundSocialPostHandlerImpl();
            map<string, Id> mapMasterAs = new map<string, Id>();
            map<Id,Account> mapAccounts = new map<Id,Account>();
            
            if(mapUserOnlineStatus.size()>0)
            	mapUserOnlineStatus = ATG_utility.getUserPresenceStatus(mapUserOnlineStatus.keySet());
            
            system.debug('mapSocialPersonas is : '+mapSocialPersonas);
            
	        for(Socialpost objSP : mapSocialPosts.values()){
	        	if(objSP.Provider != 'Twitter' || (!mapMatrix.containsKey('@'+objSP.ATG_Persona_Name__c)) ){
	            	//find the case from previous post which is linked with the personna
	                SocialPersona objPersona = mapSocialPersonas.get(objSP.ATG_Persona_UserId__c);
	                system.debug('objPersona is : '+objPersona);
	                if( mapSocialPersonas.get(objSP.ATG_Persona_UserId__c) != null && (mapDMPosts.get(objPersona.Id) != null || mapNormalPosts.get(objPersona.Id) != null )){//!mapSocialPersonas.get(objSP.ATG_Persona_UserId__c).Posts.isEmpty() )
	                    string caseId;
	                    if(objSP.messageType != 'Private' && objSP.messageType != 'Direct' && mapNormalPosts.get(objPersona.Id) != null){
	                    	caseId = mapNormalPosts.get(objPersona.Id).ParentId;
	                    }else if(mapDMPosts.get(objPersona.Id) != null && (objSP.messageType == 'Private' || objSP.messageType == 'Direct') ){
	                    	caseId = mapDMPosts.get(objPersona.Id).ParentId;
	                    } 
	                    
	                    //check if Case to be reopen
	                    //caseId = mapSocialPersonas.get(objSP.ATG_Persona_UserId__c).Posts[0].ParentId;
	                    system.debug('caseId is : '+caseId);
	                    system.debug('objSP is : '+objSP);
	                    if(caseId != null && mapCaseStatus.get(caseId) != null && mapCaseStatus.get(caseId).ATG_BU_From_Case__c == objSP.ATG_Business_Unit__c){
	                        Case objCase = mapCaseStatus.get(caseId);
	                        if(objCase.IsClosed){
	                            Boolean isReOpen = objISPH.caseShouldBeReopened( objCase ) ;
	                            if(isReOpen){
	                                objCase.Status = 'In Progress';
	                                if( string.valueOf(objCase.OwnerId).indexOf('005') == 0 && mapUserOnlineStatus.get(objCase.OwnerId) != true){
		                        		objCase.OwnerId = (objCase.ATG_Current_Queue_Id__c != null ? objCase.ATG_Current_Queue_Id__c : queueMap.get('AMH_CS_Social_Live_Chat') );
		                        	}
	                                mapCases.put(objSP.ExternalPostId, objCase );
	                            }
	                        }else if( objPersona != null && (objPersona.ExternalId == objSP.ATG_Persona_UserId__c || objPersona.Id == objSP.PersonaId)){
	                        	if(setCaseStatus.contains(objCase.Status)){
	                        		objCase.OwnerId = (objCase.ATG_Current_Queue_Id__c != null ? objCase.ATG_Current_Queue_Id__c : queueMap.get('AMH_CS_Social_Live_Chat') );
	                        		objCase.Status = 'In Progress';
	                        	}
	                        	if( string.valueOf(objCase.OwnerId).indexOf('005') == 0 && mapUserOnlineStatus.get(objCase.OwnerId) != true){
	                        		objCase.OwnerId = (objCase.ATG_Current_Queue_Id__c != null ? objCase.ATG_Current_Queue_Id__c : queueMap.get('AMH_CS_Social_Live_Chat') );
	                        	}
	                            mapCases.put(objSP.ExternalPostId, objCase );
	                        }
	                    }
	                }
	                system.debug('mapCases  '+mapCases);
	                
	                if(mapCases.get(objSP.ExternalPostId) == null ){
	                    Case objCase = new Case(Subject = objSP.Name);
	                    objCase.Description = objSP.Content;
	                    if (objSP.Provider != null) {
	                        objCase.Origin = objSP.Provider;
	                    }
	                    if(objSP.ATG_Business_Unit__c != null){
	                        if(objSP.ATG_Business_Unit__c == ATG_ConstantsUtility.TAJAWAL_BusinessUnit)
	                            objCase.RecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByDeveloperName().get('TJW_Generic').getRecordTypeId();
	                        else if(objSP.ATG_Business_Unit__c == ATG_ConstantsUtility.ALMOSAFER_BusinessUnit)
	                            objCase.RecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByDeveloperName().get('AMH_Generic').getRecordTypeId();
	                    }
	                    
	                    //SocialPersona objPersona = mapSocialPersonas.get(objSP.ATG_Persona_UserId__c);                  
	                    if( objPersona != null && objPersona.Account__c != null){
	                        if(objPersona.Account__r.ATG_Business_Unit_Picklist__c == objSP.ATG_Business_Unit__c && 
	                            (objPersona.Account__r.RecordTypeId == ATG_ConstantsUtility.PERSON_ACCOUNT_RECORDTYPEID || 
	                            objPersona.Account__r.RecordTypeId == ATG_ConstantsUtility.AMH_PERSON_ACCOUNT_RECORDTYPEID || 
	                            objPersona.Account__r.RecordTypeId == ATG_ConstantsUtility.TJW_PERSON_ACCOUNT_RECORDTYPEID) )
	                        {
	                            objCase.AccountId = objPersona.Account__c;
	                        }else if(objPersona.Account__r.RecordTypeId == ATG_ConstantsUtility.MASTER_PERSON_ACCOUNT_RECORDTYPEID){
	                            mapMasterAs.put(objSP.ExternalPostId, objPersona.Account__c );
	                        }
	                    }
	                    
	                    if (rules.size() > 0){
	                        //Creating the DMLOptions for "Assign using active assignment rules" checkbox
	                        Database.DMLOptions dmlOpts = new Database.DMLOptions();
	                        dmlOpts.assignmentRuleHeader.assignmentRuleId= rules[0].id;
	        
	                        //Setting the DMLOption on Case instance
	                        objCase.setOptions(dmlOpts);
	                    }
	                    mapCases.put(objSP.ExternalPostId,objCase);
	                }
	            }
	        }
            //query on master account to populate the Account on Case based on the Business Unit
            if(!mapMasterAs.isEmpty()){
                mapAccounts = new map<Id,Account>([select Id,
                                                        (select Id,ATG_Business_Unit_Picklist__c,LastModifiedDate from Accounts__r where ATG_Business_Unit_Picklist__c != null) 
                                                        from Account where Id IN : mapMasterAs.values()] );
                for(Socialpost objSP : mapSocialPosts.values()){
                    if(mapCases.get(objSP.ExternalPostId) != null && mapCases.get(objSP.ExternalPostId).AccountId == null ){
                        SocialPersona objPersona = mapSocialPersonas.get(objSP.ATG_Persona_UserId__c);
                        if(objPersona != null && mapAccounts.get(objPersona.Account__c) != null && !mapAccounts.get(objPersona.Account__c).Accounts__r.isEmpty()){
                            for(Account objA : mapAccounts.get(objPersona.Account__c).Accounts__r){
                                if(objA.ATG_Business_Unit_Picklist__c == objSP.ATG_Business_Unit__c){
                                    mapCases.get(objSP.ExternalPostId).AccountId = objA.Id;
                                    break;
                                }
                            }
                        }
                    }
                }
            
            }
            
            set<string> caseIds = mapCases.keySet();
            for(string cId : caseIds){
            	if(mapCases.get(cId) == null){
            		mapCases.remove(cId);
            	}
            }
            if(!mapCases.isEmpty())
            	upsert mapCases.values();
        }
        
        for(Socialpost objSP : mapSocialPosts.values()){
            Socialpost objTemp;
            if(objSP.PersonaId == null && mapSocialPersonas.get(objSP.ATG_Persona_UserId__c) != null && mapSocialPersonas.get(objSP.ATG_Persona_UserId__c).Id != null ){
                objTemp = new Socialpost(Id=objSP.Id);
                objTemp.PersonaId = mapSocialPersonas.get(objSP.ATG_Persona_UserId__c).Id;
            }
            if(objSP.ParentId == null && mapCases.get(objSP.ExternalPostId) != null && mapCases.get(objSP.ExternalPostId).Id != null ){
                objTemp = objTemp != null ? objTemp : new Socialpost(Id=objSP.Id);
                objTemp.ParentId = mapCases.get(objSP.ExternalPostId).Id;
            }
            if(objTemp != null)                 
                lstSocialPostToUpdate.add(objTemp);
        }
        if(!lstSocialPostToUpdate.isEmpty()){
            update lstSocialPostToUpdate;
            //string jSONString = JSON.serialize(lstSocialPostToUpdate);
            //updatePost(jSONString);
            deleteDuplicateFeeds(mapSocialPosts.keySet());
        }   
    }
        
    @future
    public static void deleteDuplicateFeeds(set<Id> lstParents){
        map<Id,FeedItem> mapFeeds = new map<Id,FeedItem>();
        list<FeedItem> toDel = new list<FeedItem>();
        for(FeedItem obj : [select Id,Body,ParentId from FeedItem where ParentId IN : lstParents order by CreatedDate]){
            if(mapFeeds.containsKey(obj.ParentId)){
                if(mapFeeds.get(obj.ParentId).Body == obj.Body){
                    toDel.add(obj);
                }
            }else{
                mapFeeds.put(obj.ParentId,obj);
            }
        }
        if(!toDel.isEmpty())
            delete toDel;
    }   
    
}