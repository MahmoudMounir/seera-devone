/**************************************************************************************************
* Name               : ATG_TriggerFactory                                                               
* Description        : Used to instantiate and execute Trigger Handlers associated with sObjects.                                    
* Created Date       : 10th June 2018                                                                *
* Created By         : Swati Sehrawat (PWC Digital)                                              
* -------------------------------------------------------------------------------------------------
* Version       Author      Date        Comment                                                               *

**************************************************************************************************/

public with sharing class ATG_TriggerFactory {
    
    public static void createHandler(Schema.sObjectType soType) {
        // Get a handler appropriate to the object being processed
        ATG_TriggerInterface handler = getHandler(soType);
        
        // Execute the handler to fulfil the trigger
        execute(handler);
    }
    
    private static void execute(ATG_TriggerInterface handler){
        // Before Trigger
        if (Trigger.isBefore){
            
            // Iterate through the records to be deleted passing them to the handler.
            if (Trigger.isDelete)
                handler.beforeDelete(Trigger.old);
                
            // Iterate through the records to be inserted passing them to the handler.
            else if (Trigger.isInsert)
                handler.beforeInsert(Trigger.new);
                
            // Iterate through the records to be updated passing them to the handler.
            else if (Trigger.isUpdate)
                handler.beforeUpdate(Trigger.new, Trigger.oldMap);
        }
        else{
            
            // Iterate through the records deleted passing them to the handler.
            if (Trigger.isDelete)
                handler.afterDelete(Trigger.old);
                
            // Iterate through the records inserted passing them to the handler.
            else if (Trigger.isInsert)
                handler.afterInsert(Trigger.new);
            // Iterate through the records updated passing them to the handler.
            else if (Trigger.isUpdate)
                handler.afterUpdate(Trigger.new, Trigger.oldMap);
        }
        
    }
    
    // private static method to get the appropriate handler for the object type.
    // Modify this method to add any additional handlers.
    private static ATG_TriggerInterface getHandler(Schema.sObjectType soType) {
        if (soType == Account.sObjectType)
            return new ATG_AccountHandler();
        if (soType == Case.sObjectType)
            return new ATG_CaseHandler(); 
        if (soType == LiveChatTranscript.sObjectType)
            return new ATG_LiveChatTranscriptHandler(); 
        if (soType == ATG_Booking__c.sObjectType)
            return new ATG_BookingHandler();
        if (soType == AccountContactRelation.sObjectType)
            return new ATG_AccountContactRelationHandler(); 
        //if (soType == Attachment.sObjectType)
          //  return new ATG_AttachmentHandler(); 
        if (soType == ATG_Traveller__c.sObjectType)
            return new ATG_TravellerHandler();
        if (soType == contentDocumentLink.sObjectType)
            return new ATG_contentDocumentLinkHandler();    
        if (soType == Payment_Details__c.sObjectType)
            return new ATG_PaymentDetailHandler();
        if (soType == ATG_Booking_Detail__c.sObjectType)
            return new ATG_BookingDetailHandler();
        if (soType == ATG_Requested_Traveller__c.sObjectType)
           return new ATG_RequestTravellerHandler();    
        if (soType == EmailMessage.sObjectType)
            return new ATG_EmailMessageHandler();
        if (soType == socialPost.sObjectType)
            return new ATG_SocialPostTriggerHandler();
        if(soType == CaseComment.sObjectType)
            return new ATG_CaseCommentTriggerHandler();
         if(soType == QA_Monitoring__c.sObjectType)
             return new ATG_QAMonitorHandler();
         if(soType == Contract.sObjectType)
             return new ATG_ContractHandler();
                        
        return null;
    }
    
}