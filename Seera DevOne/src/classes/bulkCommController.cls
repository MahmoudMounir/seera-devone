public class bulkCommController {
   
    @AuraEnabled   
    public static List<sObject> fetchLookUpValues(String searchKeyWord){ 
       
        String searchKey = '%' + searchKeyWord + '%' ; 
        String searchCities = 'Airline';
        system.debug('Search Key' + searchKey);
        List<sObject> returnList = new List<sObject>();
      
        // Create a Dynamic SOQL Query For Fetch Record List with LIMIT 5   
        String sQuery =  'select id,Name from ATG_Master__c where Name LIKE: searchKey and RecordTypeId in (SELECT Id FROM RecordType WHERE Name =: searchCities)';
        List<sObject> lstOfRecords = Database.query(sQuery);
        system.debug(' List' + lstOfRecords);
        for (sObject obj: lstOfRecords) {
            returnList.add(obj);
       }
       
            system.debug('Return List' + returnList);
         return returnList;
        
    }
    @AuraEnabled
    public static ContactDataTableWrapper fetchSegmentDetails(String masterName,String startDate,String endDate,Decimal pageNumber, Decimal pageSize){ 
          system.debug('String1' + masterName);
          system.debug('String2' + startDate);
          system.debug('String3' + endDate);
          String getName = masterName.substringAfter(') - ');
          system.debug('Master' + getName);
            Id profileId=userinfo.getProfileId();
            String profileName=[Select Id,Name from Profile where Id=:profileId].Name;
            system.debug('ProfileName'+profileName);
          String sDate = startDate + ' 00:00:00';
          String eDate = endDate + ' 00:00:00';
          Date sDate1 = Date.valueOf(startDate);
          Date eDate1 = Date.valueOf(endDate);
          system.debug('sDate'+ sDate1);
          system.debug('eDate'+ eDate1);
          //List<sObject> returnList = new List<sObject>();
          Integer pSize = (Integer)pageSize;
          Integer pNumber = (Integer)pageNumber;
          //Offset for SOQL
          Integer offset = (pNumber - 1) * pSize;
        String businessUnit;
        if(profileName.contains('ATG'))
        {
             businessUnit = 'Al Tayyar';
        }
        else if(profileName.contains('ALM') )
        {
             businessUnit = 'Al Mosafer';
        }
        else if(profileName.contains('TJW') || profileName.contains('System Administrator'))
        {
             businessUnit = 'Tajawal';
        }
      system.debug('businessUnit' + businessUnit);
        String sQuery =  'select id, Name,Contact_Name__c,Contact_Number__c,ATG_Airline__c,Booking_No__c,Email_Address__c,ATG_Origin__c,ATG_Arrival_Date_Time__c,ATG_Destination__c,ATG_Departure_Date_Time__c from Segment_Detail__c where ( Business_Unit__c = : businessUnit) AND (ATG_Arrival_Date_Time__c >=: sDate1 AND ATG_Arrival_Date_Time__c <= :eDate1) AND (ATG_Departure_Date_Time__c >=: sDate1 AND ATG_Departure_Date_Time__c <= :eDate1) AND (ATG_Airline__c =: getName)';
        system.debug('sQuery' + sQuery);
        List<Segment_Detail__c> lstOfRecords = Database.query(sQuery);
        //Total Records
        Integer totalRecords = [SELECT COUNT() FROM Segment_Detail__c where ( Business_Unit__c = : businessUnit) AND (ATG_Arrival_Date_Time__c >=: sDate1 AND ATG_Arrival_Date_Time__c <= :eDate1) AND (ATG_Departure_Date_Time__c >=: sDate1 AND ATG_Departure_Date_Time__c <= :eDate1) AND (ATG_Airline__c =: getName)];
        Integer recordEnd = pSize * pNumber;
        
        ContactDataTableWrapper returnList =  new ContactDataTableWrapper();  
        returnList.pageSize = pSize;
        returnList.pageNumber = pNumber;
        returnList.recordStart = offset + 1;
        returnList.recordEnd = totalRecords >= recordEnd ? recordEnd : totalRecords;
        returnList.totalRecords = totalRecords;
        returnList.contactList = [select id, Name,Contact_Name__c,ATG_Airline__c,Contact_Number__c,Booking_No__c,Email_Address__c,ATG_Origin__c,ATG_Destination__c,ATG_Arrival_Date_Time__c,ATG_Departure_Date_Time__c from Segment_Detail__c where ( Business_Unit__c = : businessUnit) AND (ATG_Arrival_Date_Time__c >=: sDate1 AND ATG_Arrival_Date_Time__c <= :eDate1) AND (ATG_Departure_Date_Time__c >=: sDate1 AND ATG_Departure_Date_Time__c <= :eDate1) AND  (ATG_Airline__c =: getName) LIMIT :pSize OFFSET :offset];
       
        
        /*
        for (Segment_Detail__c obj: lstOfRecords) {
            returnList.add(obj);
       }
       */
       system.debug('Segment List' + returnList);
        
         return returnList;
    }
    //Wrapper Class For Segemnt DataTable  
    public class ContactDataTableWrapper {
        @AuraEnabled
        public Integer pageSize {get;set;}
        @AuraEnabled
        public Integer pageNumber {get;set;}
        @AuraEnabled
        public Integer totalRecords {get;set;}
        @AuraEnabled
        public Integer recordStart {get;set;}
        @AuraEnabled
        public Integer recordEnd {get;set;}
        @AuraEnabled
        public List<Segment_Detail__c> contactList {get;set;}
    }
    
    @AuraEnabled
  public static void sendemail(List<string> ids,String templates,String txtLong,String bu,String language,String mode){
      system.debug('Email BU:' + bu);
      List<Messaging.SingleEmailMessage> mails =  new List<Messaging.SingleEmailMessage>();     
      for(Segment_Detail__c cc : [select id,name,Email_Address__c,ATG_Booking_Detail__r.ATG_Booking__c from Segment_Detail__c  where ID IN : ids]){
          ATG_Booking__c getEmail = [Select Id,ATG_Contact_Email__c,ATG_Contact_Person__c from ATG_Booking__c where Id =: cc.ATG_Booking_Detail__r.ATG_Booking__c ];          
          String langTemplate = templates + ' ' + language + ' ' + mode + ' ' + bu;
          EmailTemplate template = [Select id,body from EmailTemplate where name =: langTemplate ];
          String templateId = template.Id;
          Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
          List<String> sendTo = new List<String>();
          sendTo.add(getEmail.ATG_Contact_Email__c);
          mail.setToAddresses(sendTo); 
          String email;
          if( bu == 'Seera')
          {
              email = 'noreply@tajawal.com';
          }
          else if (bu == 'Almosafer')
          {
              email = 'noreply@almosafer.com';
          }
          else if (bu == 'Tajawal')
          {
              email = 'noreply@tajawal.com';
          }
          OrgWideEmailAddress owa = [select id, Address, DisplayName from OrgWideEmailAddress where Address =: email];
          mail.setOrgWideEmailAddressId(owa.id);        
          mail.setSubject(templates + ' Notification');
          mail.setHtmlBody(txtLong);          
          mails.add(mail); 

      }
     
     Messaging.sendEmail(mails);
     system.debug('Email Free Text:' + mails);
     
          
   }  
    
   @AuraEnabled
  public static void sendMessage(List<string> ids,String templates,String txtLong){
      
         
      for(Segment_Detail__c cc : [select id,name,Email_Address__c,ATG_Booking_Detail__r.ATG_Booking__c from Segment_Detail__c where ID IN : ids]){
         
          ATG_Booking__c getEmail = [Select Id,ATG_Contact_Email__c,ATG_Contact_Person__c,ATG_Mobile__c from ATG_Booking__c where Id =: cc.ATG_Booking_Detail__r.ATG_Booking__c ];
          String mobNum = getEmail.ATG_Mobile__c;
           
          ATG_Utility.SMSRequestClass smsReqObj = new  ATG_Utility.SMSRequestClass();
            if(mobNum.contains('+'))
            {
                mobNum = mobNum.replaceAll('\\+','').trim();  
            }
             if(!string.isEmpty(mobNum) && !string.isEmpty(txtLong)){
                         smsReqObj.messageText = txtLong;
                         smsReqObj.recipients = mobNum;                       
                         ATG_utility.SendSMS(smsReqObj);                         
             }
          
                    
    
      }
         
}
 
    @AuraEnabled
  public static EmailTemplate getEmailTemplates(String template,String language,String mode,String senderID){
     String langTemplate = template + ' ' + language + ' ' + mode + ' ' + senderID;
     EmailTemplate body = [Select body from EmailTemplate where name =: langTemplate ];    
     return body;
     
}

 @AuraEnabled
    public static void updateSegment(List<String> ids,String mode){

        for(Segment_Detail__c cc : [select id,Email__c,SMS__c from Segment_Detail__c  where ID IN : ids]){
            if(mode == 'Email'){
                cc.Email__c = true;
            }
            else if (mode == 'SMS'){
                cc.SMS__c = true;
            }
            update cc;
         }
           
        
    }
    
 
}