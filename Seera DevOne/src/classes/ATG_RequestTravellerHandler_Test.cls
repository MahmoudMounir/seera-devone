/**
 * Created by mnazir001 on 11/26/18.
 */

@IsTest
private class ATG_RequestTravellerHandler_Test {
    
    static testmethod void validateDuplicateEmployeeTest(){
        
         list<Case> caseList = ATG_TestDataFactory.CreateETOCase(1);
         insert caseList;
         
         list<ATG_Requested_Traveller__c> rtList = ATG_TestDataFactory.CreateRTraveller(3);
         rtList[1].ATG_Date_of_Birth__c = Date.newInstance(2010,2, 1);
         rtList[2].ATG_Date_of_Birth__c = Date.newInstance(2005,2, 1);
         rtList[1].Request_Number__c = caseList[0].Id;
         rtList[2].Request_Number__c = caseList[0].Id;
         rtList[0].Request_Number__c = caseList[0].Id;
         rtList[0].recordTypeID = ATG_ConstantsUtility.RequestedTraveller_Employee_RT;
         rtList[1].recordTypeID = ATG_ConstantsUtility.RequestedTraveller_Dependant_RT ;
         rtList[2].recordTypeID = ATG_ConstantsUtility.RequestedTraveller_Dependant_RT;

         
         insert rtList ;
         
         Delete rtList[0];
         
         
         
    
    }

}