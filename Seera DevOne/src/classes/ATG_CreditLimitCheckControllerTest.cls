@isTest
public class ATG_CreditLimitCheckControllerTest {

    @testsetup
    static void setupData(){
        
       Account accTest = ATG_TestDataFactory.createEmployeeAccount();
        accTest.ATG_Tapro_ID__c = '49086457';
        accTest.PersonEmail ='test@altayyar.com';
       insert accTest;
        
        
    }
    
    static testmethod void test_creditlimitcheck(){
        Test.startTest();
        String accountId = [SELECT Id from Account where PersonEmail ='test@altayyar.com'].Id;
       
        Test.setMock(WebServiceMock.class, new ATG_CreditLimitCheckWebserviceMockImpl('A'));
        String message = ATG_CreditLimitCheckController.CreditLimitCheckForCustomer(accountId);
            system.assertEquals('Credit Limit Available :17606', message);
        Test.stopTest();
         ATG_CreditLimitCheckController.CheckAccessibility();
    }
    
    static testmethod void test_creditlimitcheck_nocustomer(){
        Test.startTest();
        String accountId = [SELECT Id from Account where PersonEmail ='test@altayyar.com'].Id;
        Test.setMock(WebServiceMock.class, new ATG_CreditLimitCheckWebserviceMockImpl('B'));
        String message = ATG_CreditLimitCheckController.CreditLimitCheckForCustomer(accountId);
            system.assertEquals('Customer does not exist.', message);
        Test.stopTest();
    }
    
    static testmethod void test_creditlimitcheck_invalidCustomer(){
        Test.startTest();
        String accountId = [SELECT Id from Account where PersonEmail ='test@altayyar.com'].Id;
        Account acc = new Account(Id=accountId);
        acc.ATG_Tapro_ID__c = null;
        update acc;
        Test.setMock(WebServiceMock.class, new ATG_CreditLimitCheckWebserviceMockImpl('B'));
        String message = ATG_CreditLimitCheckController.CreditLimitCheckForCustomer(accountId);
            system.assertEquals('Invalid Customer ID', message);
        Test.stopTest();
    }
}