public without sharing class ATG_TravellerHandler implements ATG_TriggerInterface {
 
    // Constructor
    public void ATG_TravellerHandler(){
      
    } 
    
    public void beforeInsert(list<SObject> soNewList){
        try{
            searchExistingCustomerOnBooking(soNewList);
            searchExistingCustomer(soNewList);
        }catch(exception ex){
            system.debug('-------'+ex.getLineNumber());
            ATG_utility.LogException(ex);
        }
    }
  
    public void beforeUpdate(list<sobject> soNewList, map<id,sobject> soOldMap){
    }
 
    public void beforeDelete(list<SObject> soNewList){}
 
    public void afterInsert(list<SObject> soNewList){
        ATG_TravellerHandler.populateTravelleronRequestTraveller((list<ATG_Traveller__c>)soNewList);
    }
 
    public void afterUpdate(list<SObject> soNewList,map<id,SObject> soOldMap){
        ATG_TravellerHandler.ProcessVoidTravelers_ETO((list<ATG_Traveller__c>)soNewList,(map<id,ATG_Traveller__c>)soOldMap);
    }
    
    public void afterDelete(list<SObject> soNewList){}
  
    //------------------------------Method Definition-----------------------------------------// 
    
    
    //method to populate customer from booking
    public void searchExistingCustomerOnBooking(list<ATG_Traveller__c> travellerNewList){
        set<id> existingBookings = new set<id>();
        for(ATG_Traveller__c obj : travellerNewList){
            if(string.isBlank(obj.ATG_Business_Unit__c))
                obj.ATG_Business_Unit__c = 'Al Tayyar';
                
            if(obj.ATG_Booking__c!=null)
                existingBookings.add(obj.ATG_Booking__c);
        }
        
        
        map<id,id> existingAccountOnBooking = new map<id,id>();
        if(existingBookings!=null && existingBookings.size()>0){
            for(ATG_Booking__c obj : [select id, ATG_Customer__c from ATG_Booking__c where id IN : existingBookings
                                      and ATG_Customer__c!=null]){
                existingAccountOnBooking.put(obj.id,obj.ATG_Customer__c);   
            }       
        }
        
        system.debug('---existingAccountOnBooking----'+existingAccountOnBooking);
        if(existingAccountOnBooking!=null && existingAccountOnBooking.size()>0){
            for(ATG_Traveller__c obj : travellerNewList){
                if(obj.ATG_Booking__c!=null)
                    obj.ATG_Customer__c = existingAccountOnBooking.get(obj.ATG_Booking__c);
            }
        }
    }
    
    //method to populate traveller from the email address available on traveller record and BU
    public void searchExistingCustomer(list<ATG_Traveller__c> travellerNewList){
        map<string,ATG_Traveller__c> travellerToUpdate = new map<string,ATG_Traveller__c>();
        map<string,Account> existingAccount = new map<string,Account>();
        
        for(ATG_Traveller__c obj : travellerNewList){
            if(string.isNotBlank(obj.ATG_Email__c) && obj.ATG_Email__c!=null && string.isNotBlank(obj.ATG_Business_Unit__c))    
                travellerToUpdate.put(obj.ATG_Email__c,obj);
            
        }
 
        if(travellerToUpdate!=null && travellerToUpdate.size()>0){
            for(Account obj : [select id,PersonEmail,ATG_Business_Unit_Picklist__c,isPersonAccount from Account where PersonEmail IN : travellerToUpdate.keyset()
                               and recordTypeID !=: ATG_ConstantsUtility.MASTER_PERSON_ACCOUNT_RECORDTYPEID 
                               and recordTypeID !=: ATG_ConstantsUtility.MASTER_BUSINESS_ACCOUNT_RECORDTYPEID
                               and isPersonAccount = true]){
                    existingAccount.put(obj.PersonEmail, obj);                          
            }   
        }
        
        map<id,ATG_Traveller__c> travellerWithNoCustomer = new map<id,ATG_Traveller__c>();
        if(existingAccount!=null && existingAccount.size()>0){ 
            for(ATG_Traveller__c obj : travellerToUpdate.values()){
                if(string.isNotBlank(obj.ATG_Email__c) && obj.ATG_Business_Unit__c == existingAccount.get(obj.ATG_Email__c).ATG_Business_Unit_Picklist__c)
                    obj.Traveller__c = existingAccount.get(obj.ATG_Email__c).id;
            }
        }
        
        if(travellerToUpdate!=null && travellerToUpdate.size()>0){
            for(ATG_Traveller__c obj : travellerToUpdate.values()){
                if(obj.Traveller__c!=null){}
                else{
                    travellerWithNoCustomer.put(obj.id,obj);    
                }
            }
        }
 
        map<string,Account> accountToInsert = new map<string,Account>();
        if(travellerWithNoCustomer!=null && travellerWithNoCustomer.size()>0){ 
            User integrationUser = new User();
            integrationUser = ATG_Utility.integrationUser();
            
            for(ATG_Traveller__c obj : travellerWithNoCustomer.values()){
                if(string.isNotBlank(obj.ATG_Email__c)){
                    Account objAcc = new Account();
                    objAcc.recordTypeID = ATG_ConstantsUtility.PERSON_ACCOUNT_RECORDTYPEID;
                    objAcc.lastName = obj.ATG_Last_Name__c;
                    objAcc.personEmail = obj.ATG_Email__c;
                    objAcc.ATG_Business_Unit_Picklist__c = obj.ATG_Business_Unit__c;
                    objAcc.ownerID = integrationUser.id;
                    accountToInsert.put(obj.ATG_Email__c,objAcc);
                }
            }       
        }
        
        if(accountToInsert!=null && accountToInsert.size()>0){
            upsert accountToInsert.values();
            for(ATG_Traveller__c obj : travellerWithNoCustomer.values()){
                obj.Traveller__c = accountToInsert.get(obj.ATG_Email__c).Id;
            }   
         }
    }
    
    
    // Method to Populate Traveller Info on the Request Traveller 
    
    private static void populateTravelleronRequestTraveller(list<ATG_Traveller__c> travellerNewList){
         // Get Traveller Details 
        map<string,map<string,ATG_Traveller__c>> travelOrderTravMap = new map<string,map<string,ATG_Traveller__c>>();
        list<ATG_Requested_Traveller__c> rtListToUpdate = new list<ATG_Requested_Traveller__c>();
        set<string> ticketNumbers = new set<string>();
        for(ATG_Traveller__c travObj : [select id,Name,ATG_Booking__r.ATG_Travel_Order_Number__c,ATG_Ticket_Number__c,ATG_Booking__r.Name,ATG_Selling_Amount__c,ATG_Ticket_Issue_Date__c,ATG_VAT_IN_Amount__c ,ATG_AirLine_Fee__c,ATG_Booking__r.ATG_Case__c  from ATG_Traveller__c where Id  in : travellerNewList and ATG_Booking__r.ATG_Case__c !=null]){
            if(!travelOrderTravMap.containsKey(travObj.ATG_Booking__r.ATG_Case__c)){
                travelOrderTravMap.put(travObj.ATG_Booking__r.ATG_Case__c,new map<string,ATG_Traveller__c>());
                travelOrderTravMap.get(travObj.ATG_Booking__r.ATG_Case__c).put(travObj.ATG_Ticket_Number__c,travObj);
            }else if(travelOrderTravMap.containsKey(travObj.ATG_Booking__r.ATG_Case__c)  && !travelOrderTravMap.get(travObj.ATG_Booking__r.ATG_Case__c).containsKey(travObj.ATG_Ticket_Number__c)){
                travelOrderTravMap.get(travObj.ATG_Booking__r.ATG_Case__c).put(travObj.ATG_Ticket_Number__c,travObj);                
            }
            ticketNumbers.add(travObj.ATG_Ticket_Number__c);    
        }
         system.debug('*********123'+travelOrderTravMap);

        if(travelOrderTravMap !=null && travelOrderTravMap.size()>0){
            for(ATG_Requested_Traveller__c rt : [select Id,ATG_Ticket_Number__c,Request_Number__r.ATG_Reference_Number__c,Request_Number__r.PNR_Number__c from ATG_Requested_Traveller__c where Request_Number__c in : travelOrderTravMap.keySet() and ATG_PNR_Downloaded__c = true and ATG_Ticket_Number__c in : ticketNumbers]){   
                if(travelOrderTravMap.containsKey(rt.Request_Number__c) && travelOrderTravMap.get(rt.Request_Number__c).containsKey(rt.ATG_Ticket_Number__c)){
                    rt.ATG_Ticket_Amount__c = travelOrderTravMap.get(rt.Request_Number__c).get(rt.ATG_Ticket_Number__c).ATG_Selling_Amount__c;
                    rt.ATG_Date_of_Issue__c  = travelOrderTravMap.get(rt.Request_Number__c).get(rt.ATG_Ticket_Number__c).ATG_Ticket_Issue_Date__c;
                    rt.ATG_VAT_IN_Amount__c = travelOrderTravMap.get(rt.Request_Number__c).get(rt.ATG_Ticket_Number__c).ATG_VAT_IN_Amount__c ;
                    rt.ATG_AirLine_Fee__c = travelOrderTravMap.get(rt.Request_Number__c).get(rt.ATG_Ticket_Number__c).ATG_AirLine_Fee__c;
                    rtListToUpdate.add(rt);                 
                }   
            }
            
            try{
                if(rtListToUpdate !=null && rtListToUpdate.size()>0)
                    update rtListToUpdate;  
            }catch(exception ex){
                 ATG_utility.LogException(ex);
            }   
        }   
        
            
        
        }


        private static void ProcessVoidTravelers_ETO(list<ATG_Traveller__c> travNewList,map<id,ATG_Traveller__c> travellerMap){
            
            map<string,set<string>> caseTicketNumberMap = new map<string,set<string>>();
            set<string> travelersSet = new set<string>();
            set<string> ticketNumbers = new set<string>();
            list<ATG_Requested_Traveller__c> rtListToUpdate = new list<ATG_Requested_Traveller__c>();
            // Check Void scenarios
            system.debug('Traveller Update Trig');
            for(ATG_Traveller__c tr : travNewList){
                if(tr.ATG_Void_Flag__c  && tr.ATG_Void_Flag__c != travellerMap.get(tr.Id).ATG_Void_Flag__c)
                    travelersSet.add(tr.Id);        
            }   
            
            
            for(ATG_Traveller__c trav : [select Id,ATG_Booking__c,ATG_Ticket_Number__c,ATG_Booking__r.Name,ATG_Booking__r.ATG_Case__c from ATG_Traveller__c where id in : travelersSet and ATG_Booking__r.ATG_Case__c !=null]){
                if(!caseTicketNumberMap.containsKey(trav.ATG_Booking__r.ATG_Case__c)){
                    caseTicketNumberMap.put(trav.ATG_Booking__r.ATG_Case__c,new set<string>());
                    caseTicketNumberMap.get(trav.ATG_Booking__r.ATG_Case__c).add(trav.ATG_Ticket_Number__c);
                }else if(caseTicketNumberMap.containsKey(trav.ATG_Booking__r.ATG_Case__c))  
                    caseTicketNumberMap.get(trav.ATG_Booking__r.ATG_Case__c).add(trav.ATG_Ticket_Number__c);
                
                ticketNumbers.add(trav.ATG_Ticket_Number__c);
            }   
            
            
            // Update Requested Traveler Details 
            
            for(ATG_Requested_Traveller__c rt : [select Id,ATG_Ticket_Number__c,ATG_Ticket_Amount__c,ATG_VAT_IN_Amount__c,ATG_AirLine_Fee__c from ATG_Requested_Traveller__c where ATG_Ticket_Number__c in : ticketNumbers and Request_Number__c in : caseTicketNumberMap.keySet() and ATG_PNR_Downloaded__c = true]){
                rt.ATG_Ticket_Number__c = '';
                rt.ATG_Ticket_Amount__c = '';
                rt.ATG_VAT_IN_Amount__c = null;
                rt.ATG_AirLine_Fee__c = null;
                rt.ATG_PNR_Downloaded__c = false;
                rt.ATG_Date_of_Issue__c = null;
                rtListToUpdate.add(rt); 
            }

            try{
            if(rtListToUpdate !=null && rtListToUpdate.size()>0)
                Update rtListToUpdate;    
            }catch(exception ex){
                ATG_utility.LogException(ex);
            }  
          
          
           system.debug('Traveller Update Trig'+rtListToUpdate);    
            
        }       
        
    }