/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest(seeAlldata=false)
private class ATG_SocialPostTriggerHandlerTest {

    static Map<String, Object> sampleSocialData = getSampleSocialData('1');
	public static map<string, string> mapQueueDeveloperNames = new map<string, string>();
	
	@testSetup
	static void createSocialMatrix(){
		
		list<SocialAccountMatrix__c> lstMatrix = new list<SocialAccountMatrix__c>();
		lstMatrix.add(
			new SocialAccountMatrix__c(Name = ATG_ConstantsUtility.ALTAYYAR_BusinessUnit, ATG_Business_Unit__c=ATG_ConstantsUtility.ALTAYYAR_BusinessUnit, 
										FacebookAccountName__c = 'ALTFB', Facebook_Queue_Id__c = mapQueueDeveloperNames.get('ATG_Leisure_Email_Social'), 
										TwitterAccountName__c = 'ALTTweet', Twitter_Queue_Id__c = mapQueueDeveloperNames.get('ATG_Leisure_Email_Social'), 
										InstagramAccountName__c = 'ALTInsta',Instagram_Queue_Id__c = mapQueueDeveloperNames.get('ATG_Leisure_Email_Social'))
		);
		lstMatrix.add(
			new SocialAccountMatrix__c(Name = ATG_ConstantsUtility.ALMOSAFER_BusinessUnit, ATG_Business_Unit__c = ATG_ConstantsUtility.ALMOSAFER_BusinessUnit, 
										FacebookAccountName__c = 'ALMFB', Facebook_Queue_Id__c = mapQueueDeveloperNames.get('AMH_CS_Social_Live_Chat'), 
										TwitterAccountName__c = 'ALMTweet', Twitter_Queue_Id__c = mapQueueDeveloperNames.get('AMH_CS_Social_Live_Chat'), 
										InstagramAccountName__c = 'ALMInsta',Instagram_Queue_Id__c = mapQueueDeveloperNames.get('AMH_CS_Social_Live_Chat'))
		);
		lstMatrix.add(
			new SocialAccountMatrix__c(Name = ATG_ConstantsUtility.TAJAWAL_BusinessUnit, ATG_Business_Unit__c= ATG_ConstantsUtility.TAJAWAL_BusinessUnit, 
										FacebookAccountName__c = 'TJWFB', Facebook_Queue_Id__c = mapQueueDeveloperNames.get('TJW_CS_Social_Media'), 
										TwitterAccountName__c = 'TJWTweet', Twitter_Queue_Id__c = mapQueueDeveloperNames.get('TJW_CS_Social_Media'), 
										InstagramAccountName__c = 'TJWInsta',Instagram_Queue_Id__c = mapQueueDeveloperNames.get('TJW_CS_Social_Media'))
		);
		insert lstMatrix;
	}
	
	public static void getQueues(){
		for(Group objG : [SELECT DeveloperName,Id,Name,Type FROM Group WHERE Type = 'Queue' AND Id IN (select QueueId from QueuesObject) ]){
			mapQueueDeveloperNames.put(objG.DeveloperName, objG.Id);
		}
	}
	
    static testMethod void myUnitTest() {
       	ATG_TestUtility objTest = new ATG_TestUtility();
       	
       	Account corp = objTest.createLeisureAccount();
       	Case caseID = objTest.createbookingCase(corp.id);
       	
       	SocialPost post = getSocialPost(sampleSocialData);
       	post.parentId = caseID.id;
       	insert post;
       	
        ATG_SocialPostTriggerHandler objClass = new ATG_SocialPostTriggerHandler();
        list<SocialPost> soNewList = new list<SocialPost>();
        soNewList.add(post);
        
        map<id,SocialPost> soOldMap = new map<id,SocialPost>();
        
        objClass.beforeInsert (soNewList);
        objClass.beforeUpdate (soNewList,soOldMap);
        objClass.beforeDelete (soNewList);
        objClass.afterInsert (soNewList);
        objClass.afterUpdate (soNewList,soOldMap);
        objClass.afterDelete (soNewList);
    }
    
    static SocialPost getSocialPost(Map<String, Object> socialData) {
        SocialPost post = new SocialPost();
       	post.Name = String.valueOf(socialData.get('source'));
        post.Content = String.valueOf(socialData.get('content'));
        post.Posted = Date.valueOf(String.valueOf(socialData.get('postDate')));
        post.PostUrl = String.valueOf(socialData.get('postUrl'));
        post.Provider = String.valueOf(socialData.get('mediaProvider'));
        post.MessageType = String.valueOf(socialData.get('messageType'));
        post.ExternalPostId = String.valueOf(socialData.get('externalPostId'));
        post.R6PostId = String.valueOf(socialData.get('r6PostId'));
        post.TopicProfileName = '@kav_test';
        return post;
    }
    
    static SocialPersona getSocialPersona(Map<String, Object> socialData) {
        SocialPersona persona = new SocialPersona();
        persona.Name = String.valueOf(socialData.get('author'));
        persona.RealName = String.valueOf(socialData.get('realName'));
        persona.Provider = String.valueOf(socialData.get('mediaProvider'));
        persona.MediaProvider = String.valueOf(socialData.get('mediaProvider'));
        persona.ExternalId = String.valueOf(socialData.get('externalUserId'));        
        return persona;
    }
    
    static SocialPost getICRTSocialPost() {
        SocialPost post = new SocialPost();
       	post.Name = 'Testclass';
        post.PostUrl = 'https:\\twitter.com/12345678';
        post.Provider = 'Twitter';
        post.MessageType = 'Tweet';
        post.TopicProfileName = 'TJWTweet';
        post.ATG_Is_From_ICRT__c = true;
        post.ATG_Parent_Post_Id__c = null;
        post.ATG_Persona_Display_Name__c = 'Salesforce Test Class';
        post.ATG_Persona_Name__c = 'testclass';
        post.ATG_Persona_UserId__c = '123456';
        post.ExternalPostId = '123456789';
        post.ATG_Business_Unit__c = ATG_ConstantsUtility.TAJAWAL_BusinessUnit;
        return post;
    }
    
    static Map<String, Object> getSampleSocialData(String suffix) {
        Map<String, Object> socialData = new Map<String, Object>();
        socialData.put('r6PostId', 'R6PostId' + suffix);
        socialData.put('r6SourceId', 'R6SourceId' + suffix);
        socialData.put('postTags', null);
        socialData.put('externalPostId', 'ExternalPostId' + suffix);
        socialData.put('content', 'Content' + suffix);
        socialData.put('postDate', '2015-01-12T12:12:12Z');
        socialData.put('mediaType', 'Twitter');
        socialData.put('author', 'Author');
        socialData.put('skipCreateCase', false);
        socialData.put('mediaProvider', 'TWITTER');
        socialData.put('externalUserId', 'ExternalUserId');
        socialData.put('postUrl', 'PostUrl' + suffix);
        socialData.put('messageType', 'Tweet');
        socialData.put('source', 'Source' + suffix);
        socialData.put('replyToExternalPostId', null);
        socialData.put('realName', 'Real Name');
        return socialData;
    }
    
    static testMethod void myTestForMVP2() {
        //createSocialMatrix();
       	SocialPost objPostParent = getICRTSocialPost();
       	insert objPostParent;
       	
       	SocialPost objPost = getICRTSocialPost();
       	objPost.ATG_Parent_Post_Id__c = objPostParent.ExternalPostId;
       	objPost.ExternalPostId = objPost.ExternalPostId+'1';
       	insert objPost;
       	
       	SocialPost objPost1 = getICRTSocialPost();
       	objPost1.ExternalPostId = objPost1.ExternalPostId+'2';
       	insert objPost1;
       	
    }
    
}