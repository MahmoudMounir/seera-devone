@isTest
private class CL_FleetManagementController_Test{
  @testSetup
  static void setupTestData(){
    test.startTest();
      Account businessAccount=new Account(Name='Lumi BusinessAccount');
      insert businessAccount;
      Account DriverAccount=new Account(Name='Lumi Driver Account');
      insert DriverAccount;
      CL_Vehicle__c cl_vehicle_Obj1 = new CL_Vehicle__c(CL_Business_Account__c=businessAccount.id,CL_Driver__c=DriverAccount.id,Name='23233');
            Insert cl_vehicle_Obj1; 
         CL_Vehicle__c cl_vehicle_Obj2 = new CL_Vehicle__c(CL_Business_Account__c=DriverAccount.id,CL_Driver__c=businessAccount.id,Name='232334');
            Insert cl_vehicle_Obj2; 
      
      Id p = [select id from profile where name LIKE '%Fleet Manager%'].id;
       Account ac = new Account(name ='Test Account') ;
        insert ac;

		Contact con = new Contact(LastName ='testCon',AccountId = ac.Id);
		insert con;

User user = new User(alias = 'test123', email='test123@noemail.com',
emailencodingkey='UTF-8', lastname='Testing', languagelocalekey='en_US',
localesidkey='en_US', profileid = p, country='United States',IsActive =true,
ContactId = con.Id,
timezonesidkey='America/Los_Angeles', username='tester@noemail.com'
                    );

insert user;
test.stopTest();
  }
  static testMethod void test_getUserDetails_UseCase1(){
      User usr=[select id from User LIMIT 1][0];
      String usrID=(String)usr.id;
      
      //****Creating a new user****//
      

JSON.serialize( new User(email='test123@noemail.com',
lastname='Testing', firstname='Testing FirstName',
username='tester@noemail.com'));
      
      //****Creating a new User ends here***//
      String UserobjType = String.valueOf(usr);
      User Users=[select id from User][0];
      CL_FleetManagementController.getUserDetails(Users.id);
      List<CL_Vehicle__c> vehList =[select id from CL_Vehicle__c where Name='23233'];
     
       List<CL_Vehicle__c> vehList2 =[select id from CL_Vehicle__c where Name='232334'];
     
     // CL_FleetManagementController.getReassigntUserDetails(vehID);
     Account accnt=[select id from Account][0];
      String accntID=(String)accnt.id;
      String userID='';
      CL_PortalController.UserDetailCls userDtl=CL_FleetManagementController.getUserDetails(userID);
      userDtl.userObj.Email='abc@hh.com';
      CL_FleetManagementController.reassignVehicle(vehList[0],vehList2[0],accntID);
      CL_FleetManagementController.createCommunityUser(vehList[0],JSON.serialize( userDtl));
     // CL_FleetManagementController.fetchVehicles('Search Keyword');
     // CL_FleetManagementController.CommunityMessages cmessage =new CL_FleetManagementController.CommunityMessages('TEST',true);
    //  CL_FleetManagementController.updateUserAsync('Test User');
      
    
  }
  
}