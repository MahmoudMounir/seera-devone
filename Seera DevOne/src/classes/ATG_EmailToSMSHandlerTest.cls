/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class ATG_EmailToSMSHandlerTest {

    static testMethod void ATG_EmailToSMSHandler1() {
        Contact Con = new Contact();
        con.LastName = 'sample';
        con.Email = 'someaddress@email.com';
        Insert con;
        
       
        Messaging.InboundEnvelope envelope = new Messaging.InboundEnvelope();
        
        ATG_EmailToSMSHandler obj = new ATG_EmailToSMSHandler(); 
        
    	Messaging.InboundEmail email = new Messaging.InboundEmail() ;
        email.subject = 'Sandbox:+971557044444';
        email.fromAddress = 'someaddress@email.com';
        email.plainTextBody = 'email body';
        obj.handleInboundEmail(email, envelope);
        
        Messaging.InboundEmail email2 = new Messaging.InboundEmail() ;
        email2.subject = '971557044444';
        email2.fromAddress = 'someaddress@email.com';
        email2.plainTextBody = 'email body';
        obj.handleInboundEmail(email2, envelope);
        
        ATG_TestUtility objTest = new ATG_TestUtility();
        Account corp = objTest.createLeisureAccount();
        corp.ATG_Business_Unit_Picklist__c = 'Tajawal';
        update corp;
        
        Messaging.InboundEmail email3 = new Messaging.InboundEmail() ;
        email3.subject = '+971557044444 Id-'+corp.id;
        email3.fromAddress = 'someaddress@email.com';
        email3.plainTextBody = 'email body';
        obj.handleInboundEmail(email3, envelope);
        
        Messaging.InboundEmail email4 = new Messaging.InboundEmail() ;
        email4.subject = '+971557044444 ProfileId'+userinfo.getProfileId();
        email4.fromAddress = 'someaddress@email.com';
        email4.plainTextBody = 'email body';
        obj.handleInboundEmail(email4, envelope);
        
        ATG_Booking__c book = new ATG_Booking__c(Name = 'TestBook',
                              ATG_Product_Type__c='Flight',
                              ATG_Email__c ='test@test.com',
                              ATG_Travel_End_Date__c = system.today(), ATG_Business_Unit__c = 'Tajawal');
                              
        insert book;
        
        Messaging.InboundEmail email5 = new Messaging.InboundEmail() ;
        email5.subject = ' +971557044444 Id-'+book.id;
        email5.fromAddress = 'someaddress@email.com';
        email5.plainTextBody = 'email body';
        obj.handleInboundEmail(email5, envelope);
    }
   
}