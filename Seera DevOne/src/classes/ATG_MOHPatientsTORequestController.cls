/**************************************************************************************************
* Name               : ATG_MOHPatientsTORequestController                                                               
* Description        : Controller to manage the MOH Patients TO Requests                                   
* Created Date       : 12 Apr 2019                                                              *
* Created By         : Accenture                                              
**************************************************************************************************/
public without sharing class  ATG_MOHPatientsTORequestController {

    static final Integer MAX_TO_NUMBER = 100;    
    public static final Id ATG_CASE_PATIENTS_MTO_RECORDTYPE = Schema.SObjectType.Case.getRecordTypeInfosByDeveloperName().get('MOH_Patients_Manual_TO').getRecordTypeId();

 
    // initialize booking case
    @AuraEnabled
    public static case getMasterCaseDetails(String caseId){
        if(caseId != null && caseId.length() >= 15){
            Case masterCase = ([SELECT Id,ATG_Reference_Number__c,ATG_Reference_Date__c,ATG_CC_CCGR_Number__c,ATG_CC_CCGR_Number__r.Name,ATG_Department__c,ATG_Department__r.Name, ATG_Program__c,ATG_Program__r.Name, Cabin_Class__c FROM Case WHERE Id =: caseId]);
            return masterCase;
        }
        return new case();
    }
 
    // initialize booking case
    @AuraEnabled
    public static MOHPatientTravelDetails getBooking(String caseId){
        if(caseId != null && caseId.length() >= 15){
            return ATG_MOHPatientsTORequestController.GetListPatientTODetails(caseId);
        }

        return new MOHPatientTravelDetails();
    }
    
    
    // delete sub-case
    @AuraEnabled
    public static boolean deleteSubCase(String subCaseId){
        
        case subCase = new case(id=subCaseId);

        Savepoint sp = Database.setSavepoint();
        boolean result = false;
        try{  
            delete subcase;

            result =  true;
        } catch (Exception e){
            if(sp != null)
                Database.RollBack(sp);
                throw new AuraHandledException(e.getMessage());             
            }
        return result;  
    }   
    
    // initialize booking case
    @AuraEnabled
    public static sObject createNewSubCaseandReqTraveller(case masterCase,string eventFields ){
        system.debug('masterCase ' + masterCase);
        case subcase = masterCase.clone(false);
        subcase.recordTypeId = ATG_CASE_PATIENTS_MTO_RECORDTYPE;
        subcase.parentId = masterCase.Id;
       
      
        Map<String, Object> fieldMap = (Map<String, Object>)JSON.deserializeUntyped(eventFields); 
        system.debug('fieldMap: '+fieldMap);
        Schema.SObjectType targetType = Schema.getGlobalDescribe().get('ATG_Requested_Traveller__c');
        SObject newSobject = targetType.newSObject();
        Map<String, Schema.sObjectField> targetFields = targetType.getDescribe().fields.getMap();
        for (String key : fieldMap.keySet())
        {
            Object value = fieldMap.get(key);
            Schema.DisplayType valueType = targetFields.get(key).getDescribe().getType();
            if (value instanceof String && valueType != Schema.DisplayType.String)
            {
                String svalue = (String)value;
                if (valueType == Schema.DisplayType.Date)
                    newSobject.put(key, Date.valueOf(svalue));
                else if(valueType == Schema.DisplayType.DateTime)
                    newSobject.put(key, DateTime.valueOfGmt(svalue));
                else if (valueType == Schema.DisplayType.Percent || valueType == Schema.DisplayType.Currency)
                    newSobject.put(key, svalue == '' ? null : Decimal.valueOf(svalue));
                else if (valueType == Schema.DisplayType.Double)
                    newSobject.put(key, svalue == '' ? null : Double.valueOf(svalue));
                else if (valueType == Schema.DisplayType.Integer)
                    newSobject.put(key, Integer.valueOf(svalue));
                else if (valueType == Schema.DisplayType.Base64)
                    newSobject.put(key, Blob.valueOf(svalue));
                else
                    newSobject.put(key, svalue);
            }
            else if(value instanceof String){
                newSobject.put(key, value);
            }
                
        }
        
      Savepoint sp = Database.setSavepoint();
      sObject resultObject = null;
      try{  
        insert subcase;
        
        newSobject.put('Request_Number__c',subcase.id);
        
        insert newSobject;
        
        resultObject =  newSobject;
       } catch (Exception e){
        if(sp != null)
            Database.RollBack(sp);
            throw new AuraHandledException(e.getMessage());             
        }
        return resultObject;    
    }   
    
 // Method to fetch the CCGR Requested.
 @AuraEnabled
 public static list < ATG_CCGR_CC_Relationship__c > FetchCCGRDetails() {
  set < id > departAndProgIdSet = new set < id > ();
  list < string > ccListType = new list < string > ();
  for (CCGR_CC_Requestor_Settings__c ccreq: CCGR_CC_Requestor_Settings__c.getAll().Values()) {

   if (ccreq.ATG_Searchable__c) {
    if (ccreq.Name == 'CCGR')
     ccListType.add('CCGR');
    if (ccreq.Name == 'CC No')
     ccListType.add('CC No');
   }
  }

  return [Select id, CC_CCGR_Number__c, CC_CCGR_Number__r.Name, program__r.Name, program__r.Name_in_Arabic__c, program__c, Department__r.Name, Department__c, Department__r.Name_in_Arabic__c, CC_CCGR_Number__r.Name_in_Arabic__c,
   CC_CCGR_Number__r.ATG_Type__c from ATG_CCGR_CC_Relationship__c];
 }

 
  // Method to fetch the CCGR Requested.
 @AuraEnabled
 public static list < ATG_CCGR_CC_Relationship__c > FetchCCGR(string searchQuery) {
  set < id > departAndProgIdSet = new set < id > ();
  list < string > ccListType = new list < string > ();
  for (CCGR_CC_Requestor_Settings__c ccreq: CCGR_CC_Requestor_Settings__c.getAll().Values()) {

   if (ccreq.ATG_Searchable__c) {
    if (ccreq.Name == 'CCGR')
     ccListType.add('CCGR');
    if (ccreq.Name == 'CC No')
     ccListType.add('CC No');
   }
  }
 searchQuery = '%' + searchQuery + '%';
  return [Select id, CC_CCGR_Number__c, CC_CCGR_Number__r.Name, program__r.Name, program__r.Name_in_Arabic__c, program__c, Department__r.Name, Department__c, Department__r.Name_in_Arabic__c, CC_CCGR_Number__r.Name_in_Arabic__c,
   CC_CCGR_Number__r.ATG_Type__c from ATG_CCGR_CC_Relationship__c WHERE  CC_CCGR_Number__r.Name LIKE : searchQuery LIMIT 50];
 }
 
   // Method to fetch the Deapartment Requested.
 @AuraEnabled
 public static list < ATG_CCGR_CC_Relationship__c > FetchDepartement(string searchQuery, string CCGRId) {
  set < id > departAndProgIdSet = new set < id > ();
  list < string > ccListType = new list < string > ();
  for (CCGR_CC_Requestor_Settings__c ccreq: CCGR_CC_Requestor_Settings__c.getAll().Values()) {

   if (ccreq.ATG_Searchable__c) {
    if (ccreq.Name == 'CCGR')
     ccListType.add('CCGR');
    if (ccreq.Name == 'CC No')
     ccListType.add('CC No');
   }
  }
 searchQuery = '%' + searchQuery + '%';
  return [Select id, CC_CCGR_Number__c, CC_CCGR_Number__r.Name, program__r.Name, program__r.Name_in_Arabic__c, program__c, Department__r.Name, Department__c, Department__r.Name_in_Arabic__c, CC_CCGR_Number__r.Name_in_Arabic__c,
   CC_CCGR_Number__r.ATG_Type__c from ATG_CCGR_CC_Relationship__c  WHERE CC_CCGR_Number__c=:CCGRId AND  Department__r.Name LIKE : searchQuery LIMIT 50];
 }
 
 
    // Method to fetch the Deapartment Requested.
 @AuraEnabled
 public static list < ATG_CCGR_CC_Relationship__c > FetchProgram(string CCGRId, string DepartmentId) {
  set < id > departAndProgIdSet = new set < id > ();
  list < string > ccListType = new list < string > ();
  for (CCGR_CC_Requestor_Settings__c ccreq: CCGR_CC_Requestor_Settings__c.getAll().Values()) {

   if (ccreq.ATG_Searchable__c) {
    if (ccreq.Name == 'CCGR')
     ccListType.add('CCGR');
    if (ccreq.Name == 'CC No')
     ccListType.add('CC No');
   }
  }
  return [Select id, CC_CCGR_Number__c, CC_CCGR_Number__r.Name, program__r.Name, program__r.Name_in_Arabic__c, program__c, Department__r.Name, Department__c, Department__r.Name_in_Arabic__c, CC_CCGR_Number__r.Name_in_Arabic__c,
   CC_CCGR_Number__r.ATG_Type__c from ATG_CCGR_CC_Relationship__c  WHERE CC_CCGR_Number__c=:CCGRId AND Department__c=:DepartmentId LIMIT 1];
 }

 @AuraEnabled
 public static MOHPatientTravelDetail updateMasterCase(Case caseObj) {
// caseObj.ATG_Purpose_of_Visit__c='Ministerial Delegation';
   update caseObj;


  return ATG_MOHPatientsTORequestController.GetPatientTODetails(caseObj.Id);
 }

 // Method to save Case and Traveller Info.

 @AuraEnabled
 public static MOHPatientTravelDetail SaveTravellerInfo(string caseId, ATG_Requested_Traveller__c travellerDetail, string type) {
  savepoint sp;
  map < string, RT_Required_Docs_Community__c > requestedTravellerDocs = RT_Required_Docs_Community__c.getAll();
  //list < ATG_Document_Details__c > docsToInsert = new list < ATG_Document_Details__c > ();
  try {
   Database.setSavepoint();
   if (travellerDetail != null && !string.IsBlank(caseId) && !string.isBlank(type)) {
    travellerDetail.Request_Number__c = caseId;
    if (type == 'Patient')
     travellerDetail.RecordTypeId = Schema.SObjectType.ATG_Requested_Traveller__c.getRecordTypeInfosByDeveloperName().get('Patient').getRecordTypeId();
    else if (type == 'Dependant')
     travellerDetail.RecordTypeId = Schema.SObjectType.ATG_Requested_Traveller__c.getRecordTypeInfosByDeveloperName().get('Dependant').getRecordTypeId();

    upsert travellerDetail;

/*

    map < string, ATG_Document_Details__c > docDetailsMap = ATG_MOHPatientsTORequestController.GetDocumentMasterDocs(travellerDetail.ATG_employee__c);

    for (RT_Required_Docs_Community__c rtDocs: RT_Required_Docs_Community__c.getAll().Values()) {
     if (rtDocs.Is_Default__c && docDetailsMap.containskey(rtDocs.Name))
      docsToInsert.add(new ATG_Document_Details__c(ATG_Document_Name__c = rtDocs.Name, ATG_Document_Description__c = rtDocs.Document_Discription__c, Requested_Travelers__c = travellerDetail.id, ATG_Document_ID__c = docDetailsMap.get(rtDocs.Name).ATG_Document_ID__c));
    }

    insert docsToInsert;
*/
   }

  }
  Catch(exception e) {
   Database.rollback(sp);
   return null;

  }
  return ATG_MOHPatientsTORequestController.GetPatientTODetails(caseId);
 }


 public static map < string, ATG_Document_Details__c > GetDocumentMasterDocs(id AccountID) {
  map < string, ATG_Document_Details__c > docDetailsMap = new map < string, ATG_Document_Details__c > ();
 /* 
 for (ATG_Document_Details__c doc: [select id, ATG_Document_Description__c, ATG_Document_Name__c, ATG_Document_ID__c from ATG_Document_Details__c where ATG_employee__c =: AccountID]) {
   docDetailsMap.put(doc.ATG_Document_Name__c, doc);
  }
  */
  return docDetailsMap;
  
 }


 // method to save Case and Case Details
 @AuraEnabled
 public static MOHPatientTravelDetails UpdateCaseDetails(Case masterCase, list < ATG_Case_Details__c > caseDetailList) {
  savepoint sp;
  //  try{
  Database.SetSavepoint();
  system.debug('masterCase Id: ' + masterCase.Id);
  if (masterCase != null && (caseDetailList != null && !caseDetailList.isEmpty())) {
      List <ATG_Case_Details__c> oldCaseDetailsToDelete = new List < ATG_Case_Details__c >();
      List <case> subCasesToUpdate = new List <case>();
      List <ATG_Case_Details__c> caseDetailsToInsert = new List <ATG_Case_Details__c>();
      
      MOHPatientTravelDetails MOHPatientTOs = ATG_MOHPatientsTORequestController.GetListPatientTODetails(masterCase.Id);
      MOHPatientTOs.masterCase = masterCase;

       if (masterCase.ATG_Trip_Type__c == 'RoundTrip' && caseDetailList.size() == 2) {
        caseDetailList[0].ATG_To_Airport__c = caseDetailList[1].ATG_To_Airport__c;
        caseDetailList[1].ATG_From_Airport__c = caseDetailList[0].ATG_To_Airport__c;
        caseDetailList[1].ATG_To_Airport__c = caseDetailList[0].ATG_From_Airport__c;
        caseDetailList[1].ATG_Date__c = caseDetailList[1].ATG_Return_Date__c;
        caseDetailList[0].ATG_Return_Date__c = caseDetailList[1].ATG_Return_Date__c;
       }      
      
      for(MOHPatientTravelDetail MOHPatientTO: MOHPatientTOs.MOHPatientTravelDetailList){
          system.debug('MOHPatientTO.bookingCase.Id: ' + MOHPatientTO.bookingCase.Id);
          Case subCaseToUpdate = new
          case (id = MOHPatientTO.bookingCase.Id, Cabin_Class__c = masterCase.Cabin_Class__c, ATG_Purpose_of_Visit__c = 'Ministerial Delegation'/*masterCase.ATG_Purpose_of_Visit__c*/, ATG_Trip_Type__c = masterCase.ATG_Trip_Type__c);
    
           // 1 - Delete existing CaseDetails
          if (MOHPatientTO.caseDetails != null && MOHPatientTO.caseDetails.size()>0){
             oldCaseDetailsToDelete.addAll(MOHPatientTO.caseDetails); 
          }         
        
           if (caseDetailList.size() > 0) {
            if (caseDetailList[0].ATG_Date__c != null && caseDetailList[0].ATG_Date__c > Date.Today() && Date.Today().daysBetween(caseDetailList[0].ATG_Date__c) <= integer.ValueOf(Label.Urgent_Priority_Days))
             subCaseToUpdate.Priority = 'Urgent';
            else
             subCaseToUpdate.Priority = 'Regular';
           
           
           }
            subCasesToUpdate.add(subCaseToUpdate);
          
           Integer flightOrder = 1;
           for (ATG_Case_Details__c caseDetail: caseDetailList) {
            caseDetail.id = null;
        
            caseDetail.ATG_Flight_Order__c = flightOrder;
            flightOrder = flightOrder + 1;
            
            ATG_Case_Details__c tempCaseDetail = new ATG_Case_Details__c();
            tempCaseDetail = caseDetail.clone(false);
            tempCaseDetail.ATG_Case_Number__c = MOHPatientTO.bookingCase.id;
            caseDetailsToInsert.add(tempCaseDetail);
           }          
                         
          }
            system.debug('oldCaseDetailsToDelete: ' + oldCaseDetailsToDelete);
           if (oldCaseDetailsToDelete!=null && oldCaseDetailsToDelete.size() >0) {
               delete oldCaseDetailsToDelete;
           }
            system.debug('caseDetailsToInsert: ' + caseDetailsToInsert);
            if (caseDetailsToInsert!=null && caseDetailsToInsert.size() > 0){      
                insert caseDetailsToInsert;
            }
            system.debug('subCasesToUpdate: ' + subCasesToUpdate);
           if (subCasesToUpdate!=null && subCasesToUpdate.size() >0) {
               update subCasesToUpdate;
           }
           
           update masterCase;
      }
      
  /*  }catch(exception e){
       if(sp != null)
           Database.RollBack(sp);
      
       return null;
  }  */

  return ATG_MOHPatientsTORequestController.GetListPatientTODetails(masterCase.id);
 }

 @AuraEnabled
 public static MOHPatientTravelDetails GetListPatientTODetails(string caseId) {
    MOHPatientTravelDetails  listPatientTOReqObj = new MOHPatientTravelDetails();
    case masterCase = new case();
    masterCase = getMasterCaseDetails(caseId);
    listPatientTOReqObj.masterCase = masterCase; 
    //Get ids of sub-cases
    Set<Id> subCaseIds = new Map<Id, case>([SELECT Id FROM Case WHERE ParentId =: caseId LIMIT: MAX_TO_NUMBER]).keySet();

    if(subCaseIds.size()>0){
     for(Id subCaseId:subCaseIds){       
        listPatientTOReqObj.MOHPatientTravelDetailList.add(ATG_MOHPatientsTORequestController.GetPatientTODetails(subCaseId));
     }  
    }     
    return listPatientTOReqObj;
} 
 
 @AuraEnabled
 public static MOHPatientTravelDetail GetPatientTODetails(string caseId) {

  MOHPatientTravelDetail patientTOReqObj = new MOHPatientTravelDetail();
  // Query All the Details 
  List < string > fieldsList = new List < string > (GetAccessibleFields('Case'));
  fieldsList.add('ATG_CC_CCGR_Number__r.Name');
  fieldsList.add('ATG_CC_CCGR_Number__r.Name_in_Arabic__c');
  fieldsList.add('ATG_Department__r.Name');
  fieldsList.add('ATG_Department__r.Name_in_Arabic__c');
  fieldsList.add('ATG_Program__r.Name');
  fieldsList.add('ATG_Program__r.Name_in_Arabic__c');
  string CaseQueryString = 'Select ';
  CaseQueryString += string.join(fieldsList, ',');


  // Query Case detail fields
  string CaseDetailsQueryString = 'Select ';
  fieldsList = new List < string > (GetAccessibleFields('ATG_Case_Details__c'));
  CaseDetailsQueryString += string.join(fieldsList, ',') + ' from Case_Details__r ORDER BY ATG_Flight_Order__c ASC';

  // Query Requested Traveler Info
  string requestedTravellersQueryString = 'Select ';
  fieldsList = new List < string > (GetAccessibleFields('ATG_Requested_Traveller__c'));
  requestedTravellersQueryString += string.join(fieldsList, ',') + ', ATG_employee__r.ATG_Job_Title__c, RecordType.DeveloperName from Requested_Travelers__r order by CreatedDate ASC';

  CaseQueryString += ',(' + CaseDetailsQueryString + '),' + '(' + requestedTravellersQueryString + ') from case where id=\'' + caseId + '\'';



  for (Case caseObj: Database.Query(CaseQuerySTring)) {
   patientTOReqObj.bookingCase = caseObj;
   patientTOReqObj.requestedTravelerInfo = caseObj.Requested_Travelers__r;
   patientTOReqObj.caseDetails = caseObj.Case_Details__r;
  }

  fieldsList = new List < string > (GetAccessibleFields('ATG_Document_Details__c'));

  list < ATG_Requested_Traveller__c > rtList = patientTOReqObj.requestedTravelerInfo;
  String DocumentDetailQuerySTring = 'Select ' + string.join(fieldsList, ',') + ' from ATG_Document_Details__c where Requested_Travelers__c in : rtList ';

  for (ATG_Document_Details__c docObj: Database.Query(DocumentDetailQuerySTring)) { //[Select id,Requested_Travelers__c from ATG_Document_Details__c where Requested_Travelers__c in :patientTOReqObj.requestedTravelerInfo]){         
   if (patientTOReqObj.TravellerDocuments.containsKey(docObj.Requested_Travelers__c))
    patientTOReqObj.TravellerDocuments.get(docObj.Requested_Travelers__c).add(docObj);
   else {
    patientTOReqObj.TravellerDocuments.put(docObj.Requested_Travelers__c, new list < ATG_Document_Details__c > ());
    patientTOReqObj.TravellerDocuments.get(docObj.Requested_Travelers__c).add(docObj);
   }

  patientTOReqObj.ContentIdsForDocs = ATG_MOHPatientsTORequestController.GetDocumentDetails(patientTOReqObj.bookingCase.Id, new list < string > ());

  }

  return ATG_MOHPatientsTORequestController.validateCase(patientTOReqObj);

  //return  patientTOReqObj;      
 }


 // patientTO Validations

 private static MOHPatientTravelDetail validateCase(MOHPatientTravelDetail patientTOdetail) {

  map < string, object > validationMap = new map < string, object > ();
  set < string > dependantValidationPurposeffVisit = new set < string > ();
  dependantValidationPurposeffVisit.addAll(Label.Max_Dependant_Purposeofvisit_Val.split(','));
  string messageString = '';
  if (patientTOdetail != null) {
   if (patientTOdetail.requestedTravelerInfo.size() > 0) {
    if (string.isBlank(messageString) && dependantValidationPurposeffVisit.contains(patientTOdetail.bookingCase.ATG_Purpose_of_Visit__c)) {
     string patientRT = Schema.SObjectType.ATG_Requested_Traveller__c.getRecordTypeInfosByDeveloperName().get('Patient').getRecordTypeId();
     map < string, integer > ptDependantsCountMap = new map < string, Integer > ();
     map < string, ATG_Requested_Traveller__c > ptDetailsMap = new map < string, ATG_Requested_Traveller__c > ();
     for (ATG_Requested_Traveller__c rt: patientTOdetail.requestedTravelerInfo) {
      if (rt.RecordTypeId == patientRT) {
       ptDetailsMap.put(rt.Id, rt);
       if (!ptDependantsCountMap.containsKey(rt.Id))
        ptDependantsCountMap.put(rt.Id, 0);
      } 
      
      else if (rt.ATG_Patient_Traveller__c != null) {
       if (!ptDependantsCountMap.containsKey(rt.ATG_Patient_Traveller__c))
        ptDependantsCountMap.put(rt.ATG_Patient_Traveller__c, 1);
       else if (ptDependantsCountMap.containsKey(rt.ATG_Patient_Traveller__c)) {
        ptDependantsCountMap.put(rt.ATG_Patient_Traveller__c, ptDependantsCountMap.get(rt.ATG_Patient_Traveller__c) + 1);
       }
      }
      
     }

     for (string ptId: ptDependantsCountMap.keySet()) {
      if (ptDependantsCountMap.get(ptId) > integer.valueOf(Label.Max_Dependants)) {
       if (string.IsBlank(messageString))
        messageString += Label.Max_Dependants_Validation + ' ' + Label.Max_Dependants + '.' + '\r\n';
       messageString += ptDetailsMap.get(ptId).ATG_Traveller_Full_Name__c + '\r\n';
      }
     }

     validationMap.put('Error', messageString);
    }

    if (string.isBlank(messageString)) {
     for (ATG_Requested_Traveller__c rt: patientTOdetail.requestedTravelerInfo) {
      if (rt.ATG_ID_Type__c != 'Passport' && patientTOdetail.bookingCase.ATG_Outside_GCC_Travel_Identifier__c > 0) {
       if (string.IsBlank(messageString))
        messageString += Label.Passport_Mandatory_Validation + '\r\n';
       messageString += rt.ATG_Traveller_Full_Name__c + '\r\n';
       validationMap.put('Error', messageString);
      } else if (rt.ATG_ID_Type__c != 'Passport' && rt.ATG_Nationality__c != 'SA' && patientTOdetail.bookingCase.ATG_Outside_GCC_Travel_Identifier__c == 0 && patientTOdetail.bookingCase.ATG_Domestic_Trip_Count__c == 0) {
       if (string.IsBlank(messageString))
        messageString += Label.Passport_Mandatory_Validation + '\r\n';
       messageString += rt.ATG_Traveller_Full_Name__c + '\r\n';
       validationMap.put('Error', messageString);
      }

     }
    }

   }
  }

  patientTOdetail.messageMap = validationMap;

  return patientTOdetail;
 }

 // Case detail inner class that contains all 

 public class MOHPatientTravelDetails {
  @AuraEnabled
  public Map < String, String > travellerRecordTypes;
    @AuraEnabled
  public case masterCase;
  @AuraEnabled
  public list <MOHPatientTravelDetail > MOHPatientTravelDetailList;


  public MOHPatientTravelDetails() {

   // load record types
   travellerRecordTypes = travellerRecordTypes = new Map < String, String > ();   
   Id RequestedTraveller_patient_RT = Schema.SObjectType.ATG_Requested_Traveller__c.getRecordTypeInfosByDeveloperName().get('Patient').getRecordTypeId();
   Id RequestedTraveller_Dependant_RT = Schema.SObjectType.ATG_Requested_Traveller__c.getRecordTypeInfosByDeveloperName().get('Dependant').getRecordTypeId();
   travellerRecordTypes.put('Patient', RequestedTraveller_patient_RT);
   travellerRecordTypes.put('Dependant', RequestedTraveller_Dependant_RT);
   masterCase = new case();
   MOHPatientTravelDetailList = new list <MOHPatientTravelDetail >();
  }
 }


 // Case detail inner class that contains all 

 public class MOHPatientTravelDetail {
  @AuraEnabled
  public Case bookingCase;
  @AuraEnabled
  public list < ATG_Requested_Traveller__c > requestedTravelerInfo;
  @AuraEnabled
  public list < ATG_Case_Details__c > caseDetails;
  @AuraEnabled
  public map < id, list < ATG_Document_Details__c >> TravellerDocuments;
  @AuraEnabled
  public map < string, list < string >> ContentIdsForDocs;
  @AuraEnabled
  public map < string, object > messageMap;

  public MOHPatientTravelDetail() {
   bookingCase = new Case(Subject = 'New MoH Patient Booking', ATG_CC_CCGR_Number__c = null, ATG_Department__c = null, ATG_Program__c = null);
   requestedTravelerInfo = new list < ATG_Requested_Traveller__c > ();
   caseDetails = new list < ATG_Case_Details__c > ();
   TravellerDocuments = new map < id, list < ATG_Document_Details__c >> ();
   ContentIdsForDocs = new map < string, list < string >> ();
   messageMap = new map < string, object > ();
  }
 }

 // Method to Link patient and Dependant Requested Traveller
 @AuraEnabled
 public static string UpdateRequestedTravelerRelationship(string patientId, string dependentId) {

    
  ATG_Requested_Traveller__c rtObj = new ATG_Requested_Traveller__c(id = dependentId, ATG_Patient_Traveller__c = patientId);
  try {
   if (patientId != null && patientId != null)
    update rtObj;
   else
    return 'Error updating relationship';

   return 'Success';
  } catch (exception e) {
   return 'Error';
  }
 }


 @InvocableMethod(label = 'Create Documents'
  description = 'Creates a Document detail records on Requested Traveller creation')
 public static void CreateDocuments(list < id > RTIds) {


  map < id, ATG_Requested_Traveller__c > RT_AccountMap = new map < id, ATG_Requested_Traveller__c > ();
  map < id, ATG_Document_Details__c > documentDetailMap = new map < id, ATG_Document_Details__c > ();
  list < Attachment > attachmentsList = new list < Attachment > ();
  map < string, RT_Required_Docs_Community__c > docNames = new map < string, RT_Required_Docs_Community__c > ();
  map < id, set < string >> docsForEachEmp = new map < id, set < string >> ();
  list < id > contentDocsList = new list < id > ();

  list < ATG_Requested_Traveller__c > RTDependantList = new List < ATG_Requested_Traveller__c > ();

  list < ATG_Document_Details__c > documentDetailsToInsert = new list < ATG_Document_Details__c > ();

  Savepoint sp;

  try {
   for (RT_Required_Docs_Community__c rtDocs: RT_Required_Docs_Community__c.getAll().Values()) {
    docNames.put(rtDocs.Name, rtDocs);
   }



   for (ATG_Requested_Traveller__c rt: [select id, ATG_employee__c, RecordTypeId, Request_Number__c from ATG_Requested_Traveller__c where id in: RTIds]) {
    if (rt.RecordTypeId == Schema.SObjectType.ATG_Requested_Traveller__c.getRecordTypeInfosByDeveloperName().get('Patient').getRecordTypeId()) {
     RT_AccountMap.put(rt.ATG_employee__c, rt);
     docsForEachEmp.put(rt.ATG_employee__c, docNames.keySet());
    } else {
     RTDependantList.add(rt);
    }
   }



   // Get the documents        

   if (RT_AccountMap != null && RT_AccountMap.size() > 0) {
    for (ATG_Document_Details__c docdetails: [select id, ATG_Document_Name__c, ATG_Document_Description__c, ATG_employee__c, ATG_Document_ID__c, ATG_Request_Number__c from ATG_Document_Details__c where ATG_employee__c in: RT_AccountMap.KeySet() and ATG_Document_Name__c in: docNames.keySet() and ATG_employee__c != null]) {
     ATG_Document_Details__c doc = docdetails.clone(false, false, false, false);
     doc.ATG_employee__c = null;
     //doc.ATG_Document_ID__c = '';   
     doc.Requested_Travelers__c = RT_AccountMap.get(docdetails.ATG_employee__c).Id;
     doc.ATG_Request_Number__c = RT_AccountMap.get(docdetails.ATG_employee__c).Request_Number__c;

     documentDetailMap.put(docdetails.Id, doc);
     if (!STRING.ISBLANK(docdetails.ATG_Document_ID__c) && docdetails.ATG_Document_ID__c.containsAny(','))
      contentDocsList.addAll(docdetails.ATG_Document_ID__c.split(','));
     else if (!STRING.ISBLANK(docdetails.ATG_Document_ID__c))
      contentDocsList.add(docdetails.ATG_Document_ID__c);

     // Remove the docs from the map to filter the missing documents per emp              
     if (docsForEachEmp.get(docdetails.ATG_employee__c).contains(docdetails.ATG_Document_Name__c)) {
      if (docsForEachEmp.get(docdetails.ATG_employee__c).size() == 1)
       docsForEachEmp.remove(docdetails.ATG_employee__c);
      else
       docsForEachEmp.get(docdetails.ATG_employee__c).remove(docdetails.ATG_Document_Name__c);
     }




    }



    if (documentDetailMap.values().size() > 0) {
     insert documentDetailMap.values();

     // Insert ContentDocs for New Doc Detail Recs Created                
     if (contentDocsList != null && !contentDocsList.isEmpty() && documentDetailMap != null && documentDetailMap.size() > 0)
      ATG_MOHPatientsTORequestController.CreateContentDocuments(documentDetailMap, contentDocsList);

    }



    if (docsForEachEmp.keySet().size() > 0) {

     for (id empId: docsForEachEmp.KeySet()) {
      if (!docsForEachEmp.get(empId).isEmpty()) {
       list < string > docNamesList = new list < string > (docsForEachEmp.get(empId));
       for (string docName: docNamesList) {
        ATG_Document_Details__c doc = new ATG_Document_Details__c(ATG_Document_Name__c = docName, Requested_Travelers__c = RT_AccountMap.get(empId).Id, ATG_Document_Description__c = docNames.get(docName).Document_Discription__c);
        doc.ATG_Request_Number__c = RT_AccountMap.get(empId).Request_Number__c;
        documentDetailsToInsert.add(doc);
       }
      }
     }



    }
   }

   if (RTDependantList != null && RTDependantList.size() > 0) {
    for (ATG_Requested_Traveller__c rt: RTDependantList) {
     for (RT_Required_Docs_Community__c docNam: docNames.Values()) {
      ATG_Document_Details__c doc = new ATG_Document_Details__c(ATG_Document_Name__c = docNam.Name, Requested_Travelers__c = rt.Id, ATG_Document_Description__c = docNam.Document_Discription__c, ATG_Request_Number__c = rt.Request_Number__c);
      documentDetailsToInsert.add(doc);
     }
    }
   }

   if (documentDetailsToInsert != null && documentDetailsToInsert.size() > 0)
    insert documentDetailsToInsert;

  } catch (exception e) {

   //ATG_utility.LogException(e);
  }

 }

 /*   
   @AuraEnabled
   public static string ProcessApproval(string caseId,string comments,string priority,string approvalEvent){  // 1 --> Approval 2 --> Send Back 3--> Reject
       
        if(!string.isEmpty(caseId)){
             if(approvalEvent == '1')
                 return ATG_CommunityApprovalProcessController.ProcessApproval(new list<string>{caseId},comments,'Approve');
              else if(approvalEvent == '2')
                 return ATG_CommunityApprovalProcessController.ProcessApproval(new list<string>{caseId},comments,'Send Back'); 
               else if(approvalEvent == '3')
                 return ATG_CommunityApprovalProcessController.ProcessApproval(new list<string>{caseId},comments,'Reject');                         
        }
        
        return 'Error';   
      
       
   }
  */


 @AuraEnabled
 public static map < string, list < string >> GetDocumentDetails(string caseId, list < string > documentIdList) {
  map < string, list < string >> documentContentMap = new map < string, list < string >> ();

  if ((documentIdList == null || documentIdList.isEmpty()) && caseId != null) {
   for (ATG_Document_Details__c dd: [select Id from ATG_Document_Details__c where ATG_Request_Number__c =: caseId]) {
    documentIdList.add(dd.Id);
    documentContentMap.put(dd.Id, new list < string > ());
   }
  }



  if (documentIdList != null && !documentIdList.isEmpty()) {
   for (ContentDocumentLink cl: [select LinkedEntityId, contentDocumentId, contentDocument.Createddate from ContentDocumentLink where LinkedEntityId in: documentIdList order by contentDocument.Createddate Desc])
    documentContentMap.get(cl.LinkedEntityId).add(cl.contentDocumentId);
  }

  return documentContentMap;

 }



 public static void CreateContentDocuments(map < id, ATG_Document_Details__c > oldDoc_NewDocDetails, list < Id > contentDocsList) {

  // Get All the Content Type Ids


  set < id > DocIdSet = oldDoc_NewDocDetails.keySet();
  map < id, List < ContentVersion >> contentVersionMap = new map < id, List < ContentVersion >> ();
  map < string, ContentVersion > Doc_ContentDocversion = new map < string, ContentVersion > ();
  map < string, string > Doc_ContentDocMap = new map < string, string > ();
  list < Id > contentDocList = new list < Id > ();
  map < id, id > versionId_OldDocId = new map < id, Id > ();
  map < id, id > versionId_NewDocId = new map < id, Id > ();

  string ContentDocumentLinkQuery = 'select ID,ContentDocumentId,LinkedEntityId from ContentDocumentLink where LinkedEntityId in : DocIdSet   and  ContentDocumentId IN  (';
  string docIdStrings = '';
  for (string docI: contentDocsList)
   docIdStrings = '\'' + docI + '\',';

  docIdStrings += docIdStrings.removeEnd(',');

  ContentDocumentLinkQuery += docIdStrings + ')';



  for (ContentDocumentLink cd: Database.Query(ContentDocumentLinkQuery)) // select ID,ContentDocumentId,LinkedEntityId from ContentDocumentLink where LinkedEntityId IN : DocIdSet and ContentDocumentId in : contentDocsList
   //  for(ContentDocumentLink cd : [select ID,ContentDocumentId,LinkedEntityId from ContentDocumentLink where LinkedEntityId IN : DocIdSet and ContentDocumentId in : contentDocsList and LinkedEntityId  !=null and ContentDocumentId !=null])
   Doc_ContentDocMap.put(cd.ContentDocumentId, cd.LinkedEntityId);



  // Clone Content Version 

  for (ContentVersion cv: [select id, Description, ContentDocumentId, FileExtension, FileType, VersionData, ContentLocation, PathOnClient,
    Title from contentVersion where IsLatest = true and contentDocumentId in: Doc_ContentDocMap.keySet() and contentDocumentId != null
   ]) {
   ContentVersion cvObj = new ContentVersion(ContentLocation = cv.ContentLocation, PathOnClient = cv.PathOnClient, Title = cv.Title, VersionData = cv.VersionData);
   Doc_ContentDocversion.put(cv.ContentDocumentId, cvObj);

  }




  if (Doc_ContentDocversion != null && Doc_ContentDocversion.size() > 0)
   insert Doc_ContentDocversion.values();



  for (Id oldDocId: Doc_ContentDocversion.keySet())
   versionId_OldDocId.put(Doc_ContentDocversion.get(oldDocId).Id, oldDocId);

  for (ContentVersion cvNew: [select id, Description, ContentDocumentId, FileExtension, FileType, VersionData, ContentLocation, PathOnClient,
    Title from contentVersion where IsLatest = true and Id in: versionId_OldDocId.keySet()
   ]) {

   versionId_NewDocId.put(cvNew.Id, cvNew.ContentDocumentId);
  }

  // Create Content Document Links
  List < ContentDocumentLink > cdLinkToInsert = new list < ContentDocumentLink > ();
  for (Id versionID: versionId_NewDocId.keySet()) {
   ContentDocumentLink cDe = new ContentDocumentLink();
   cDe.ContentDocumentId = versionId_NewDocId.get(versionID);
   cDe.LinkedEntityId = oldDoc_NewDocDetails.get(Doc_ContentDocMap.get(versionId_OldDocId.get(versionID))).Id;
   cDe.ShareType = 'I';
   cDe.Visibility = 'AllUsers';
   cdLinkToInsert.add(cDe);
  }

  if (cdLinkToInsert != null)
   insert cdLinkToInsert;


  // Get the ContentDocument for New Versions



 }


 @AuraEnabled
 public static Map < String, List < Portal_Traveller_Field_Setting__mdt >> getTravellerFieldMetadata() {


  List < Portal_Traveller_Field_Setting__mdt > patientFields = [
   SELECT Field_Api_Name__c, Portal_Account__c, TravellerType__c, Readonly__c, Required__c, Order__c, ATG_ID_Type_Field__c
   FROM Portal_Traveller_Field_Setting__mdt
   WHERE Active__c = true AND TravellerType__c in (SELECT id FROM Portal_Traveller_Type__mdt WHERE DeveloperName = 'Visitor')
   ORDER BY Order__c ASC
  ];

  List < Portal_Traveller_Field_Setting__mdt > dependentFields = [
   SELECT Field_Api_Name__c, Portal_Account__c, TravellerType__c, Readonly__c, Required__c, Order__c, ATG_ID_Type_Field__c
   FROM Portal_Traveller_Field_Setting__mdt
   WHERE Active__c = true AND TravellerType__c in (SELECT id FROM Portal_Traveller_Type__mdt WHERE DeveloperName = 'Dependent')
   ORDER BY Order__c ASC
  ];

  /*
  List < Portal_Traveller_Field_Setting__mdt > newpatientFields = [
   SELECT Field_Api_Name__c, Portal_Account__c, TravellerType__c, Readonly__c, Required__c, Order__c, ATG_ID_Type_Field__c
   FROM Portal_Traveller_Field_Setting__mdt
   WHERE Active__c = true AND TravellerType__c in (SELECT id FROM Portal_Traveller_Type__mdt WHERE DeveloperName = 'New_patient')
   ORDER BY Order__c ASC
  ];

  List < Portal_Traveller_Field_Setting__mdt > visitorFields = [
   SELECT Field_Api_Name__c, Portal_Account__c, TravellerType__c, Readonly__c, Required__c, Order__c, ATG_ID_Type_Field__c
   FROM Portal_Traveller_Field_Setting__mdt
   WHERE Active__c = true AND TravellerType__c in (SELECT id FROM Portal_Traveller_Type__mdt WHERE DeveloperName = 'Visitor')
   ORDER BY Order__c ASC
  ];
*/


  Map < string, List < Portal_Traveller_Field_Setting__mdt >> fieldsMetadata = new Map < string, List < Portal_Traveller_Field_Setting__mdt >> ();

  fieldsMetadata.put('patientFieldSet', patientFields);
  fieldsMetadata.put('DependentFieldSet', dependentFields);

  return fieldsMetadata;
 }
 
    //get all accessible fields
    public static set<string> GetAccessibleFields(string ObjName) {
        set<string> accessibleFields = new set<String>();
        Map<String, Schema.SobjectField> fields = Schema.getGlobalDescribe().get(ObjName).getDescribe().fields.getMap();
        for (String s : fields.keySet()) {
            if ((s != 'Name') && (fields.get(s).getDescribe().isAccessible())) {
                accessibleFields.add(s);
            }
        }
        return accessibleFields;
    } 
    
     @AuraEnabled
    public static user getUserDetails(){    
        return ATG_PortalHomePageUtility.GetUserDetails('');
    }
}