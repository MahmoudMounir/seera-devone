/**************************************************************************************************
* Name               : ATG_Utility                                                               
* Description        : Utility class with generic funtions                                        
* Created Date       : 10th June 2018                                                                *
* Created By         : Swati Sehrawat (PWC Digital)                                              
* -------------------------------------------------------------------------------------------------
Version     Author      Date            Comment                                                               *
v1.1        Ravindra    24/12/2018      Added the logic to send the SMS from Task - Sender name should be Tajawal - Passing the Task Id in the Email Subject
v1.2        Ravindra    20/Feb/2019     Added the method to return the Dailing codes of countries
v1.3        Diana       06/03/2019      Added the logic to set the sender name based on business unit if ProfileId is not passed 
                                        and create/update a task with type as log a call
**************************************************************************************************/

public without sharing class ATG_utility {
     
    // Method to insert exception in the custom object Exception Logs
    public static void LogException(Exception e) {
        ATG_Exception_Logs__c EL =  new ATG_Exception_Logs__c();
        EL.Cause__c = String.valueOf(e.getCause());
        EL.Line_Number__c = e.getLineNumber();
        EL.Message__c = e.getMessage(); 
        EL.Stack_Trace__c = (e.getStackTraceString() != null && e.getStackTraceString().length() > 1000 ? e.getStackTraceString().substring(0,999) : e.getStackTraceString());
        // EL.Stack_Trace__c = e.getStackTraceString();
        EL.Type_Name__c = e.getTypeName();
        insert EL;
    }
    
    //Method to insert exception in the custom object Exception Logs
    
     public static void createErrorLog(String methodName, String errorMessage,string className,String PNR,ID CaseID){
        ATG_Exception_Logs__c errorLog = new ATG_Exception_Logs__c();
        errorLog.ATG_Method_Name__c = methodName;
        errorLog.ATG_Description__c = errorMessage;
        errorLog.ATG_Class_Name__c=className;
        errorLog.ATG_PNR__c=PNR;
        errorLog.ATG_Case_ID__c=CaseID;
        errorLog.Case_ID__c=CaseID;
        insert errorLog;
    }
    
   // Method for Generic Logs  
    public static void LogException(string Message , string type) {
        ATG_Exception_Logs__c EL =  new ATG_Exception_Logs__c();       
        EL.Message__c = Message;       
        EL.Type_Name__c = type;
        insert EL;
    } 
    
    
    // method to create query and get all fields depending on the object.
    public static String getAllFields(DescribeSObjectResult describeResult) {
        string sObjectName = describeResult.getName();
        string query = ' SELECT ';
        list<String> fieldNames = new List<String> ( describeResult.fields.getMap().keySet() );
        list<RecordType> rts = [select Id from RecordType where SObjectType = :sObjectName];
        query += (rts.size() > 0) ? 'RecordType.DeveloperName ,RecordType.Name , ' : '';
        query +=  String.join( fieldNames, ',' ) + ' FROM ' + describeResult.getName();
        return query;
    }
    
    // Method to get record types Info.
    public static Map<String, Schema.RecordTypeInfo> getRecordTypeId(String objectAPIName){
        Map<String, Schema.SObjectType> schemaMap = Schema.getGlobalDescribe();
        DescribeSObjectResult result = schemaMap.get(objectAPIName).getDescribe();
        return result.getRecordTypeInfosByName();
    }
    
    //Method returns Field API Names Set.
    public static set<string> getallAPINames(string ObjName){
        set<string> lstAPINames = new set<String>();
        Map<String, Schema.SObjectField> objectFields = Schema.getGlobalDescribe().get(ObjName).getDescribe().fields.getMap();
        lstAPINames = new set<String>(objectFields.keySet());
        return lstAPINames;
    }
    
    //this method is used to return the string from list of array values
    //ex: abc;pqr;lmn = ('abc','pqr','lmn')
    public static String getValuesInQuotes(Set<String> lst){ 
        string condition ='(';
        for(String strid : lst){
            condition += '\''+strid+'\',';
        }
        condition = condition.substring(0,condition.length()-1);
        condition += ')';
        return condition;
    }
    
    //Method to get integration user for case/PA owners when created through code.
    public static user integrationUser(){
        User integrationUser = new User();
        list<User> tempUserList = [select id from user where name =: Label.integration_User LIMIT 1];
        if(tempUserList!=null && tempUserList.size()>0)
            integrationUser = tempUserList[0];
        
        return integrationUser;
    }
    
    //Method to return employee's company.
    //either pass account or account ID to get company recordID
    public static string companyID(string employeeRecordID){
        if(string.isNotBlank(employeeRecordID)){
            list<contact> tempAccountList = new list<contact>();
            tempAccountList = [select id , (select id, AccountId from AccountContactRelations) from contact where id=:employeeRecordID];       
            if(tempAccountList!=null && tempAccountList.size()>0){
                if(tempAccountList[0].AccountContactRelations!=null && tempAccountList[0].AccountContactRelations.size()>0)
                    return tempAccountList[0].AccountContactRelations[0].AccountId;
            }
        }
        return '';
    } 
    
    //Method to generate alphanumeric string. Generally used for passwords
    
    public static string GenerateHexString(integer len){
        string key = EncodingUtil.convertToHex(crypto.generateAesKey(128));
        string passWrd = key.substring(0,len);
        return  passWrd;
    }
    
    //get all accessible fields
    public static set<string> GetAccessibleFields(string ObjName) {
        set<string> accessibleFields = new set<String>();
        Map<String, Schema.SobjectField> fields = Schema.getGlobalDescribe().get(ObjName).getDescribe().fields.getMap();
        for (String s : fields.keySet()) {
            if ((s != 'Name') && (fields.get(s).getDescribe().isAccessible())) {
                accessibleFields.add(s);
            }
        }
        return accessibleFields;
    }
    
    
   //Start :  Methods to Send SMS.
    
    public static string SendSMS(SMSRequestClass smsStructure){
        
        string requestBody;
        string accessToken = 'Bearer '+ATG_utility.getCequensAccessToken(ATG_EmailToSMSHandler.AgentProfileId,ATG_EmailToSMSHandler.BusinessUnit);
         CequensConfigurations__c cconfigs = CequensConfigurations__c.getInstance();
         
        if(ATG_EmailToSMSHandler.AgentProfileId != null){
            cconfigs = CequensConfigurations__c.getInstance(ATG_EmailToSMSHandler.AgentProfileId);
            //smsStructure.senderName = 'Seera';
        }else if(ATG_EmailToSMSHandler.BusinessUnit != null){
            for(CequensConfigurations__c cconfigsCS:[SELECT Cequens_SMS_API_Key__c,Message_Type__c,Endpoint__c,Sender_Name__c,
                                                    SMS_SignIn_Endpoint__c,User_Name__c,Active__c FROM CequensConfigurations__c
                                                    WHERE Sender_Name__c=:ATG_EmailToSMSHandler.BusinessUnit.replace(' ','') LIMIT 1]){
                cconfigs = cconfigsCS;
            }
            //smsStructure.senderName = 'Seera';
        }
        if(smsStructure !=null && !String.isBlank(smsStructure.recipients) && !String.isBlank(smsStructure.messageText))
            smsStructure.senderName = cconfigs.Sender_Name__c;
            //smsStructure.senderName = 'Seera';
            smsStructure.messageType = cconfigs.Message_Type__c;
            requestBody = JSON.serialize(smsStructure);            
        if(!string.isEmpty(requestBody) && cconfigs.Active__c){
            smsStructure.senderName = 'Seera';
             Http h = new Http();
             HttpRequest request = new HttpRequest();
             request.setEndpoint(cconfigs.Endpoint__c);
             request.setHeader('Content-Type','application/json');
             request.setHeader('Authorization',accessToken);
             request.setBody(requestBody);
             system.debug('Endpoint'+cconfigs.Endpoint__c);
             system.debug('***'+requestBody);
             request.setMethod('POST');
             HttpResponse res = h.send(request);
             
             system.debug('**res.getBody() is : '+res.getBody());
               
             ATG_Utility.SMSResponseHandler(res.getBody(),requestBody,smsStructure.recipients);           
             return res.getBody();
        }       
        return '';      
    }
    
    
    public static string getCequensAccessToken(string ProfileId,string BusinessUnit){
         CequensConfigurations__c cconfigs = CequensConfigurations__c.getInstance();
         
        if(ProfileId != null){
            cconfigs = CequensConfigurations__c.getInstance(ProfileId);
        }else if(BusinessUnit != null){
            for(CequensConfigurations__c cconfigsCS:[SELECT Cequens_SMS_API_Key__c,Message_Type__c,Endpoint__c,Sender_Name__c,
                                                    SMS_SignIn_Endpoint__c,User_Name__c,Active__c FROM CequensConfigurations__c
                                                    WHERE Sender_Name__c=:BusinessUnit.replace(' ','') LIMIT 1]){
                cconfigs = cconfigsCS;
            }
        }
            
         SMSSigninRequestClass signinrequestClass = new SMSSigninRequestClass();
         signinrequestClass.apiKey = ATG_utility.DecryptPassword(cconfigs.Cequens_SMS_API_Key__c);
         signinrequestClass.userName =  cconfigs.User_Name__c;
         string requestBody= JSON.serialize(signinrequestClass);
         
         if(!string.isEmpty(requestBody) && cconfigs.Active__c){
             string responseString;
             Http h = new Http();
             HttpRequest request = new HttpRequest();
             request.setEndpoint(cconfigs.SMS_SignIn_Endpoint__c);
             request.setHeader('Content-Type','application/json');
             request.setBody(requestBody);
             request.setMethod('POST');
             HttpResponse res = h.send(request);             
             responseString = res.getBody();
             system.debug(' responseString '+responseString);
             Map<String, Object> respMap = (Map<String, Object>)JSON.deserializeUntyped(responseString);
             
             if(respMap !=null && respMap.containsKey('data')){
                map<string,object> dataMap = (map<string,object>)respMap.get('data');
                if(dataMap !=null && dataMap.containsKey('access_token'))
                    return (string)dataMap.get('access_token');                 
             }            
        } 
        
        return '';       
         
    }
    

    public class SMSRequestClass{        
        public string messageText;
        public string senderName;
        public string messageType;
        public string recipients;
        //public string AgentProfileId; //v1.1  
    }
    
    public class SMSSigninRequestClass{
        public string apiKey;
        public string userName;
    }
    
      
    
     public static void SMSResponseHandler(string responseString,String requestBody,String recipients){
         try{ 
        Map<String, Object> respMap = (Map<String,Object>)JSON.deserializeUntyped(responseString);      
        if(respMap.containsKey('replyCode') && respMap.get('replyCode') !=null){
            integer repCode = (integer)respMap.get('replyCode');
            String repMessage = (respMap.containsKey('replyMessage') && respMap.get('replyMessage') !=null)?(String)respMap.get('replyMessage'):'';
            String reqId = (respMap.containsKey('requestId') && respMap.get('requestId') !=null)?(String)respMap.get('requestId'):'';
            if(repCode == -1){
                ATG_utility.LogException(('Response : '+responseString+'\nRequest Body :'+requestBody),'SMS');
                
            }//v1.2:START
            
            if(ATG_EmailToSMSHandler.RecordId != null && ATG_EmailToSMSHandler.RecordId.startsWith('00T')){
                Task smsTask = new Task(Id=ATG_EmailToSMSHandler.RecordId,ATG_SMS_Response__c='Response Message : '+repMessage+' Request Id :'+reqId);
                update smsTask;
            }else if(ATG_EmailToSMSHandler.RecordId != null){
                Task smsTaskNew = new Task();
                smsTaskNew.WhatId = ATG_EmailToSMSHandler.RecordId;
                smsTaskNew.Status = 'Completed';
                smsTaskNew.Subject = 'SMS Sent';
                smsTaskNew.Description = requestBody;
                smsTaskNew.Phone_Mobile_Number__c = recipients;
                smsTaskNew.OwnerId = system.label.Integration_User_Id;//UserInfo.getUserId();
                smsTaskNew.ATG_SMS_Response__c = 'Response Message : '+repMessage+' Request Id :'+reqId;
                smsTaskNew.Type = 'SMS';
                insert smsTaskNew;
            }//v1.2 End           
        }
         }catch(Exception ex){
          ATG_utility.LogException(ex);   
         }
            
        
    } 
    
    // End of SMS Related Methods
    
    public static map<string,id> getQueueIdFromName(set<string> queueName) {
        map<string,id> queueMap = new map<string,id>();
        
        system.debug('----queueName------'+queueName);
        for(Group obj : [Select Id, Name, DeveloperName from Group where type = 'Queue' and DeveloperName IN : queueName]){
            queueMap.put(obj.DeveloperName,obj.id);  
        }
        
        system.debug('----queueMap------'+queueMap);
        return queueMap;
    }
    
    
    // Method to fetch Pick list values
    public map<string,string> getPickListValuesInfo(string sobject_type,string FieldApiName){
    
        map<string,string> picklistValues = new map<string,string>();
        Map<String, Schema.SObjectType> global_describe = Schema.getGlobalDescribe();
        Map<String, Schema.SObjectField> object_fields_map = global_describe.get(sobject_type).getDescribe().fields.getMap();

        return picklistValues;
    }
    
    //method to get all members in a queue
    public static list<id> queueMembers(set<string> queueName){
        list<id> queueMembers = new list<id>();
        map<string,string> queueMap = getQueueIdFromName(queueName);
        
        for(GroupMember obj : [select id, UserOrGroupId from GroupMember where groupId IN :queueMap.values() and  Group.Type = 'Queue']){
            queueMembers.add(obj.UserOrGroupId);        
        }
        return queueMembers;        
    }
    
    
    public static user GetUserInfo(){
    
        return [select id,Name,User_Type__c,ProfileId,Profile.name,Email,UserName,UserRole.Name from User where id=: userInfo.getUserID() limit 1];
    
    }
    
    
    public static string validateIqama(string inputStr){
        if(!string.isBlank(inputStr)){
            integer lastDigit;
            map<integer,integer> numberMap = new map<integer,integer>();
        
            if(!string.isBlank(inputStr) && inputStr.length()>0 && inputStr.length() == 10){            
                for(integer i =1;i < 10 ; i++)              
                    numberMap.put(i,integer.ValueOf(inputStr.subString(i-1,i)));            
            }
            
            integer result =0;
            for(integer  i : numberMap.keySet()){
                if(Math.mod(i,2) ==1){            
                    integer tempIntVal =(numberMap.get(i) * 2);              
                    string  tempVar = string.valueOf(tempIntVal);              
                    if(tempvar.length() == 2)
                        tempIntVal = Integer.ValueOf(tempvar.substring(0,1)) + integer.valueOf(tempvar.substring(1,2));
                    numberMap.put(i,tempIntVal);
                }
                result +=numberMap.get(i);           
            }
        
            system.debug('After CHange'+numberMap);
                
            if(string.valueOf(result).length() == 2){
                if(string.valueOf(result).substring(1,2) == '0')
                    lastDigit = 0;
                else{
                    integer retValue = integer.ValueOf(string.valueOf(result).substring(1,2));
                    lastDigit = 10-retValue;
                }   
            }else if(string.valueOf(result).length() == 1)
                lastDigit =  result;
            return string.valueOf(lastDigit);
        }
        return '';
    }
    
     // Method to get the User Presence status of the users passed
    public static map<string,boolean> getUserPresenceStatus(set<string> SetUserIDs){
        map<string,boolean> MapUserPresenceStatus = new map<string,boolean>();
        for(UserServicePresence usp:[select Id,IsCurrentState,UserId from UserServicePresence where UserId IN:SetUserIDs and IsCurrentState=true]){
            string UserId = usp.UserId;
            UserId = UserId.substring(0,15);
            MapUserPresenceStatus.put(UserId,usp.IsCurrentState);
            MapUserPresenceStatus.put(usp.UserId,usp.IsCurrentState); // to get the 18 Digit Id
        }
        return MapUserPresenceStatus;
    }
    
    
     // Method to EncryptPassword
    public static string EncryptPassword(string password){
        if(string.isNotBlank(password)){
            blob EncryptedPasswordblob = Crypto.encryptWithManagedIV('AES256', ATG_ConstantsUtility.cryptoKey, blob.valueOf(password));
            string strEncryptedPasswordText = EncodingUtil.base64Encode(EncryptedPasswordblob); 
            return strEncryptedPasswordText;
         }
         return '';
    }
    
   
   
    // Method to DecryptPassword
    public static string DecryptPassword(string encryptedPassword){
        if(string.isNotBlank(encryptedPassword)){            
            string decryptedTxt;
            blob encodedEncryptedblob = EncodingUtil.base64Decode(encryptedPassword);
            blob decryptedblob = Crypto.decryptWithManagedIV('AES256',ATG_ConstantsUtility.cryptoKey,encodedEncryptedblob);
            string decryptedPasswordText = decryptedblob.toString();
            return decryptedPasswordText;
        }
        return '';
    }
    
    //v1.2
    public static set<string> getDailingCodes(){
        set<string> setCodes = new set<string>();
        system.debug('ATG_ConstantsUtility.DAILING_CODE_RECORDTYPEID '+ATG_ConstantsUtility.DAILING_CODE_RECORDTYPEID);
        for(ATG_Master__c objM : [select ATG_Dial_Code__c from ATG_Master__c where RecordTypeId=:ATG_ConstantsUtility.DAILING_CODE_RECORDTYPEID AND ATG_Dial_Code__c != null]){
            setCodes.add(objM.ATG_Dial_Code__c);
        }
        return setCodes;
    }
    //end of v1.2   
    //
    
    //v1.3 get key prefix
    public static string getKeyPrefix(String sObjectName){
        Map<String, Schema.SObjectType> m  = Schema.getGlobalDescribe() ;
        system.debug('==>m is==>'+m);
        Schema.SObjectType s = m.get(sObjectName) ;
        system.debug('==>Sobject Type is ==>'+s);
        Schema.DescribeSObjectResult r = s.getDescribe() ;
        String keyPrefix = r.getKeyPrefix();
        return keyPrefix;
    }
    
    public static string sendCoENotifications(list<Case> lstCases){
        list<Messaging.SingleEmailMessage> emails = new list<Messaging.SingleEmailMessage>();
        Messaging.SingleEmailMessage mail;
        set<string> teamNames = new set<string>();
        map<string, Email_Alert_Notification__mdt> mapTeamConfig = new map<string, Email_Alert_Notification__mdt>();
        map<string, string> mapTemplateIds = new map<string, string>();
        map<string, string> mapContactIds = new map<string, string>();//Email and Id
        map<string, OrgWideEmailAddress> mapOrgWideAddresses = new map<string, OrgWideEmailAddress>();//Email and Id
        //set<string> setTemp = new set<string>();
        list<Task> lstTasks = new list<Task>();
        
        for(Case objCase : lstCases){
            if(objCase.ATG_Stakeholders_Team__c != null){
                teamNames.addAll( objCase.ATG_Stakeholders_Team__c.split(';') );
            }
        }
        
        if(!teamNames.isEmpty()){
            for(Email_Alert_Notification__mdt obj : [select Email_Template_Developer_Name__c,Form_Address__c,Function__c,Team_Name__c,To_Addresses__c,Case_Record_Type_API_Name__c from Email_Alert_Notification__mdt where Team_Name__c IN : teamNames AND Team_Name__c != null AND To_Addresses__c != null ]){
                if(obj.Function__c != null && obj.Team_Name__c != null && obj.Case_Record_Type_API_Name__c != null){
                    mapTeamConfig.put( obj.Function__c.toLowerCase()+':'+obj.Team_Name__c.toLowerCase()+':'+obj.Case_Record_Type_API_Name__c.toLowerCase(), obj );
                    if(obj.Email_Template_Developer_Name__c != null)
                        mapTemplateIds.put(obj.Email_Template_Developer_Name__c, null);
                    if(obj.To_Addresses__c != null){
                        for(string email : obj.To_Addresses__c.split(',')){
                            if(String.isNotEmpty(email)){
                                mapContactIds.put(email.trim().toLowerCase(),null);
                                //setTemp.add(email);
                            }
                        }
                    }
                    if(obj.Form_Address__c != null)
                        mapOrgWideAddresses.put(obj.Form_Address__c, null);
                }
            }
            
            if(!mapTeamConfig.isEmpty()){
                for(EmailTemplate obj : [select DeveloperName,Id from EmailTemplate where DeveloperName IN : mapTemplateIds.keySet()]){
                    mapTemplateIds.put(obj.DeveloperName, obj.Id);
                }
                
                for(OrgWideEmailAddress obj : [select Address,DisplayName,Id from OrgWideEmailAddress where Address IN : mapOrgWideAddresses.keySet()]){
                    mapOrgWideAddresses.put(obj.Address, obj);    
                }
                
                for(Contact obj : [select Id,Email from Contact Where AccountId =: system.label.CoE_Account_Id_for_Team_Emails AND Email != null AND Email IN : mapContactIds.keySet()]){
                    mapContactIds.put(obj.Email, obj.Id);
                    //setTemp.remove(email);
                }
                /*if(!setTemp.isEmpty()){
                    //Create a Contact 
                }*/
                
                for(Case objCase : lstCases){
                    if(objCase.ATG_Function__c != null && objCase.ATG_Stakeholders_Team__c != null){
                        for(string team : objCase.ATG_Stakeholders_Team__c.split(';')){
                            if(team != null && team != ''){
                                string key = objCase.ATG_Function__c.toLowerCase()+':'+team.trim().toLowerCase()+':'+objCase.ATG_Record_Type_Name__c.toLowerCase();
                                if(mapTeamConfig.containsKey(key)){
                                    emails.add(prepareMessage(objCase, mapTemplateIds, mapTeamConfig.get(key), mapContactIds, mapOrgWideAddresses));
                                    lstTasks.add(prepareTasks(objCase, team));
                                }
                            }                            
                        }
                    }
                    
                }
                
                if(!emails.isEmpty()){
                    try{
                        Messaging.sendEmail(emails);
                    }catch(Exception ex){
                        
                    }
                }
                if(!lstTasks.isEmpty())
                    insert lstTasks;
            }            
        }
        
        return null;
    }
    
    public static Messaging.SingleEmailMessage prepareMessage(Case objCase, map<string, string> templateIds, Email_Alert_Notification__mdt objMD, map<string, string> mapContactIds, map<string, OrgWideEmailAddress> mapOrgWideAddresses){
        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
        mail.templateId = templateIds.get(objMD.Email_Template_Developer_Name__c);
        //mail.setInReplyTo(objMD.Form_Address__c);
        mail.setReplyTo(objMD.Form_Address__c);
        //mail.setSenderDisplayName('Sourcing - CoE');
        if(mapOrgWideAddresses.get(objMD.Form_Address__c) != null){
            mail.setOrgWideEmailAddressId( mapOrgWideAddresses.get(objMD.Form_Address__c).Id );
            //mail.setSenderDisplayName(mapOrgWideAddresses.get(objMD.Form_Address__c).DisplayName);
        }
        
        mail.whatid = objCase.Id;
        mail.targetobjectid = mapContactIds.get(objMD.To_Addresses__c.split(',')[0]);
        if(objMD.To_Addresses__c.split(',').size() > 1){
            list<string> lst = objMD.To_Addresses__c.split(',');
            lst.remove(0);
            mail.toAddresses = lst;   
        }
        mail.setSaveAsActivity(true);
        return mail;
    }
    
    public static Task prepareTasks(Case objCase, string teamName){
        Task objTask = new Task();
        objTask.OwnerId = objCase.OwnerId;
        objTask.WhatId = objCase.Id;
        objTask.Type = 'Other';
        objTask.Subject = 'Email notificaiton sent to '+teamName+' for confirmation';
        objTask.Description = 'Email sent to notify the '+teamName+' team';
        objTask.Priority = 'Normal';
        objTask.Status = 'Open';
        return objTask;
    }
   
}