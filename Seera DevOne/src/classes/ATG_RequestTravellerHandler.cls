/**************************************************************************************************
* Name               : ATG_RequestTravellerHandler
* Description        : Trigger Handler class for ATG_Requested_Traveller__c Object
* Created Date       : 10 Oct 2018                                                              *
* Created By         : PWC Digital                                              
* -------------------------------------------------------------------------------------------------
* Version       Author      Date        Comment                                                               *

**************************************************************************************************/


public class ATG_RequestTravellerHandler implements ATG_TriggerInterface{

   
  public static boolean UpdateTravellerData = false;  
   
    // Constructor
  public void ATG_RequestTravellerHandler (){}  

  
  public void beforeInsert(list<SObject> soNewList){ 
      
      ATG_RequestTravellerHandler.validateRequestTraveller((list<ATG_Requested_Traveller__c>)soNewList,null);
      ATG_RequestTravellerHandler.prepopulateFields((list<ATG_Requested_Traveller__c>)soNewList,null);
      
      ATG_RequestTravellerHandler.validateDuplicateEmployee((list<ATG_Requested_Traveller__c>)soNewList);
  }
  
  public void beforeUpdate(list<SObject> soNewList,map<id,SObject> soOldMap){
      ATG_RequestTravellerHandler.validateRequestTraveller((list<ATG_Requested_Traveller__c>)soNewList,(map<id,ATG_Requested_Traveller__c>)soOldMap);     
      ATG_RequestTravellerHandler.prepopulateFields((list<ATG_Requested_Traveller__c>)soNewList,(map<id,ATG_Requested_Traveller__c>)soOldMap);
      
      ATG_RequestTravellerHandler.validateDuplicateEmployee((list<ATG_Requested_Traveller__c>)soNewList);
  }

  public void beforeDelete(list<SObject> SOoldList){
      ATG_RequestTravellerHandler.DeleteDependants(SOoldList);
      ATG_RequestTravellerHandler.deleteRTDocuments(SOoldList);
  }

  public void afterInsert(list<SObject> soNewList){
      ATG_RequestTravellerHandler.UpdateTravellersCount((list<ATG_Requested_Traveller__c>)soNewList,null);
  }

  public void afterUpdate(list<SObject> soNewList,map<id,SObject> soOldMap){    
        ATG_RequestTravellerHandler.UpdateTravellersCount((list<ATG_Requested_Traveller__c>)soNewList,(map<id,ATG_Requested_Traveller__c>)soOldMap);
        ATG_RequestTravellerHandler.UpdatePrimaryTraveller(null,null,(list<ATG_Requested_Traveller__c>)soNewList,(map<id,ATG_Requested_Traveller__c>)soOldMap);  
   }

  public void afterDelete(list<SObject> SOoldList){
      ATG_RequestTravellerHandler.UpdateTravellersCount(SOoldList,null);     
  }
  
  
   /****************************************************************************************
    @Description - This method used to validate if the Traveller added as a New employee is 
                   already existing in the Database under Ministry of Health 
    @Param - Trigger.New   
    ****************************************************************************************/

    
    public static void validateDuplicateEmployee (list<ATG_Requested_Traveller__c> travellerNewList){
    
        string rtEmployee = ATG_ConstantsUtility.RequestedTraveller_Employee_RT;
    
        Set<String> employeeIds = new Set<String>();
        for(ATG_Requested_Traveller__c newTraveller : travellerNewList){
            if(newTraveller.RecordTypeId == rtEmployee && newTraveller.ATG_Employee__c == null && !string.isBlank(newTraveller.ATG_Employee_Id__c))
                employeeIds.add(newTraveller.ATG_Employee_Id__c);    
        }
        
        map<String,Account> existingEmployees = new Map<String, Account>();
        for(Account duplicateAccount: [SELECT ATG_Employee_Id__c FROM Account WHERE RecordType.DeveloperName = 'PersonAccount' AND ATG_Employee_Id__c IN :employeeIds ]){
            existingEmployees.put(duplicateAccount.ATG_Employee_Id__c, duplicateAccount);
        }
    
       
        
        for(ATG_Requested_Traveller__c newTraveller : travellerNewList){
            if( (newTraveller.RecordTypeId == rtEmployee
                 && (!string.isBlank(newTraveller.ATG_Employee_Id__c) 
                     && newTraveller.ATG_Employee__c == null 
                     && existingEmployees.containsKey(newTraveller.ATG_Employee_Id__c)
                    ) 
                )){              
                    newTraveller.adderror(Label.Duplicate_Employee_ID_Traveller);
                    newTraveller.ATG_Employee_Id__c.adderror(Label.Duplicate_Employee_Id);
                    
                 }    
        }
    
    }
    
    
  /****************************************************************************************
    @Description - This method contains validations on Request Traveler in Before Insert and 
                   Before Update trigger events 
    @Param       - Trigger.New,Trigger.OldMap,  
   ****************************************************************************************/   
        
  public static void validateRequestTraveller(list<ATG_Requested_Traveller__c> travellerNewList, map<id,ATG_Requested_Traveller__c> travellerOldMap){
      if(travellerNewList !=null){
          for(ATG_Requested_Traveller__c rt : travellerNewList){              
              if(rt.ATG_ID_Type__c == 'National ID / IQAMA' && !String.IsBlank(rt.ATG_National_ID__c) && rt.ATG_National_ID__c!=null && travellerOldMap == null ){
                  if(rt.ATG_National_ID__c.right(1) != ATG_Utility.validateIqama(rt.ATG_National_ID__c))
                      rt.ATG_National_ID__c.addError(Label.IQAMA_Validation_Message);                     
              }else if(rt.ATG_ID_Type__c == 'National ID / IQAMA' && rt.ATG_National_ID__c!=null && !String.IsBlank(rt.ATG_National_ID__c)  && travellerOldMap != null){
                  if(rt.ATG_National_ID__c.right(1) != ATG_Utility.validateIqama(rt.ATG_National_ID__c) && rt.ATG_National_ID__c.right(1) != ATG_Utility.validateIqama(rt.ATG_National_ID__c)){
                      rt.ATG_National_ID__c.addError(Label.IQAMA_Validation_Message);                     

                  }    
              }      
          }
      }
  }
  
  
  /****************************************************************************************
    @Description - This method is used to Prepopulate fields on the Request Traveler record 
                   in either of the before insert and before update trigger events
    @Param       - Trigger.New,Trigger.OldMap,  
   ****************************************************************************************/
  
  
    public static void prepopulateFields(list<ATG_Requested_Traveller__c> travellerNewList,map<id,ATG_Requested_Traveller__c> oldMap){        
        for(ATG_Requested_Traveller__c rt : travellerNewList){
            // Populate CaseID
            if(rt.Request_Number__c !=null && trigger.isInsert)
                rt.ATG_Case_ID__c = rt.Request_Number__c;    
            if(rt.Request_Number__c !=null && trigger.isUpdate && rt.Request_Number__c != oldMap.get(rt.id).Request_Number__c){
                rt.ATG_Case_ID__c = rt.Request_Number__c;    
            }    
                
                    
        }
    }
  
 /****************************************************************************************
    @Description - This method is used to Update the No of Adults,Children and 
                    Infants count on the Case
    @Param       - Trigger.New,Trigger.OldMap,  
   ****************************************************************************************/
  
  public static void UpdateTravellersCount(list<ATG_Requested_Traveller__c> travellerNewList,map<id,ATG_Requested_Traveller__c> oldMap){
      
    list<ATG_Requested_Traveller__c> rtList = new list<ATG_Requested_Traveller__c>();
    set<id> caseIdSet = new set<id>();
    map<id,case> caseMapToUpdate = new map<id,Case>();
    
    
    
    if(trigger.isAfter){
        if(trigger.isInsert)            
            rtList.addAll(travellerNewList);
        
        if(trigger.isUpdate){
            for(ATG_Requested_Traveller__c rt : travellerNewList){
                if(rt.ATG_Date_of_Birth__c != oldMap.get(rt.Id).ATG_Date_of_Birth__c)
                    rtList.add(rt);
                if(rt.Request_Number__c != oldMap.get(rt.Id).Request_Number__c){
                    caseIdSet.add(oldMap.get(rt.Id).Request_Number__c); 
                    caseIdSet.add(rt.Request_Number__c);                 
                }      
            }   
        }       
        
        if(trigger.isDelete)
            if(travellerNewList !=null && travellerNewList.size()>0)
                rtList.addAll(travellerNewList);
    }   
    
    // Collect Case IDs
    
    for(ATG_Requested_Traveller__c rt : rtList)
        caseIdSet.add(rt.Request_Number__c);
    system.debug('*****&*&'+caseIdSet);
    
    if(caseIdSet !=null && !caseIdSet.isEmpty()){
        case caseObj;
        for(ATG_Requested_Traveller__c rt : [select Request_Number__c,ATG_Adult_Child__c from ATG_Requested_Traveller__c where Request_Number__c in : caseIdSet]){
            if(!caseMapToUpdate.containsKey(rt.Request_Number__c))
                caseMapToUpdate.put(rt.Request_Number__c,new case(id = rt.Request_Number__c,ATG_No_Of_Adults__c =0,ATG_No_of_Children_Travelling__c =0,ATG_No_of_Infants_Travelling__c =0));
            
            if(rt.ATG_Adult_Child__c == 'Adult')
                caseMapToUpdate.get(rt.Request_Number__c).ATG_No_Of_Adults__c += 1;
            else if(rt.ATG_Adult_Child__c == 'Child')    
                caseMapToUpdate.get(rt.Request_Number__c).ATG_No_of_Children_Travelling__c +=1;
            else if(rt.ATG_Adult_Child__c == 'Infant')  
                 caseMapToUpdate.get(rt.Request_Number__c).ATG_No_of_Infants_Travelling__c +=1;  
            
            system.debug('*****&*&'+caseMapToUpdate);
                
        }
        
        if(!caseMapToUpdate.values().isEmpty())
            update caseMapToUpdate.values();
    }   
    
    
      
  }  
  
  
   /****************************************************************************************
    @Description - This method is used to Delete all the Dependant Travelers associated to 
                    the Employee or Visitor when they are deleted                   
    @Param       - Trigger.New
   ****************************************************************************************/
  
  
  public static void DeleteDependants(list<ATG_Requested_Traveller__c> travellerNewList){
       
       
       string rtEmployee = ATG_ConstantsUtility.RequestedTraveller_Employee_RT;
       string vistorRt = ATG_ConstantsUtility.RequestedTraveller_Visitor_RT;
       list<id> employeeIds = new list<id>();
        list<ATG_Requested_Traveller__c  > rtToDelete= new list<ATG_Requested_Traveller__c  >();
        
        map<string,ATG_Requested_Traveller__c> caseRtMap = new map<string,ATG_Requested_Traveller__c>();
       for(ATG_Requested_Traveller__c  rt : travellerNewList) {
           if(rtEmployee == rt.RecordTypeId || vistorRt == rt.RecordTypeId)
               employeeIds.add(rt.Id);
               
           if(rt.ATG_Is_Primary_Traveller__c)
                caseRtMap.put(rt.Request_Number__c,rt);    
       }
       
       if(caseRtMap !=null && !caseRtMap.isEmpty())
         ATG_RequestTravellerHandler.UpdatePrimaryTraveller(caseRtMap, new list<string>{rtEmployee,vistorRt},null,null);
       
       if(employeeIds !=null && employeeIds.size()>0){
           for(list<ATG_Requested_Traveller__c> dependantsList : [select Id from ATG_Requested_Traveller__c where ATG_Employee_Traveler__c in : employeeIds])
               rtToDelete.addAll(dependantsList);
           
           if(rtToDelete!=null && rtToDelete.size()>0)
               delete rtToDelete;    
       } 
  }
  
  
  /****************************************************************************************
    @Description - This method is used to Update the Primary Traveler details on the Case                  
    @Param       - map,list of RecordTypes,trigger.New,trigger.Old
   ****************************************************************************************/
  
  
   public static void UpdatePrimaryTraveller(map<string,ATG_Requested_Traveller__c> caseRtMap,list<string> recordTypeIDs,list<ATG_Requested_Traveller__c> travellerNewList,map<id,ATG_Requested_Traveller__c> tavellerOldMap){
    system.debug('UPD**');
    list<Case> caseListToUpdate = new list<Case>();
    map<id,Case> caseMap = new map<id,Case>();
    if(!ATG_RequestTravellerHandler.UpdateTravellerData){
        if(trigger.isDelete){
            system.debug('UPD***');
            map<string,ATG_Requested_Traveller__c> casePTMap = new map<string,ATG_Requested_Traveller__c  >();
            if(caseRtMap!=null && caseRtMap.size() >  0){
                for(ATG_Requested_Traveller__c  RT : [select id,ATG_Employee__c,Request_Number__c,ATG_Traveller_Full_Name__c,ATG_Mobile_Number__c,ATG_Email_Address__c from ATG_Requested_Traveller__c where Request_Number__c in : caseRtMap.keySet() and Id not in : caseRtMap.values() and RecordTypeID in : recordTypeIDs order by createdDate ASC ]){
                    if(!casePTMap.containsKey(RT.Request_Number__c))
                        casePTMap.put(rt.Request_Number__c,rt);
                }
            }
            system.debug('UPD***'+casePTMap);
            for(string casId : caseRtMap.keySet()){
                if(casePTMap.containsKey(casId))
                    caseMap.put(casId,new case(id=casId,ATG_Primary_Traveller__c = casePTMap.get(casId).ATG_Employee__c,ATG_Primary_Traveller_Nam__c=casePTMap.get(casId).ATG_Traveller_Full_Name__c,ATG_Primary_Traveller_Mobile_No__c = casePTMap.get(casId).ATG_Mobile_Number__c,ATG_Primary_Traveller_Email__c=casePTMap.get(casId).ATG_Email_Address__c));             
                else if(!casePTMap.containsKey(casId))
                    caseMap.put(casId,new case(id=casId,ATG_Primary_Traveller__c = null,ATG_Primary_Traveller_Nam__c='',ATG_Primary_Traveller_Mobile_No__c = '',ATG_Primary_Traveller_Email__c=''));                
                    //caseListToUpdate.add(new case(id=casId,ATG_Primary_Traveller__c = null,ATG_Primary_Traveller_Nam__c='',ATG_Primary_Traveller_Mobile_No__c = '',ATG_Primary_Traveller_Email__c=''));
            }  
            
                 
        }else if(trigger.isUpdate && Trigger.isAfter){
             for(ATG_Requested_Traveller__c  rt : travellerNewList){
                 if(rt.ATG_Traveller_Full_Name__c != tavellerOldMap.get(rt.Id).ATG_Traveller_Full_Name__c || rt.ATG_Mobile_Number__c!= tavellerOldMap.get(rt.Id).ATG_Mobile_Number__c|| rt.ATG_Email_Address__c != tavellerOldMap.get(rt.Id).ATG_Email_Address__c)         
                    caseMap.put(rt.Request_Number__c,new case(id=rt.Request_Number__c,ATG_Primary_Traveller__c = null,ATG_Primary_Traveller_Nam__c=rt.ATG_Traveller_Full_Name__c,ATG_Primary_Traveller_Mobile_No__c =rt.ATG_Mobile_Number__c,ATG_Primary_Traveller_Email__c=rt.ATG_Email_Address__c));             
                    //caseListToUpdate.add(new case(id=rt.Request_Number__c,ATG_Primary_Traveller__c = null,ATG_Primary_Traveller_Nam__c=rt.ATG_Traveller_Full_Name__c,ATG_Primary_Traveller_Mobile_No__c =rt.ATG_Mobile_Number__c,ATG_Primary_Traveller_Email__c=rt.ATG_Email_Address__c));
             }     
        }
        ATG_RequestTravellerHandler.UpdateTravellerData = true;
        /*if(caseListToUpdate !=null && !caseListToUpdate.isEmpty())
           update caseListToUpdate;*/
       
        if(caseMap !=null && caseMap.size()>0)
            update caseMap.values();
    } 

 }
 
 
 /****************************************************************************************
    @Description - This method is used to Delete RT Docs when the Traveller is Deleted               
    @Param       - map,list of RecordTypes,trigger.New,trigger.Old
   ****************************************************************************************/  
  
  public static void deleteRTDocuments(list<ATG_Requested_Traveller__c> travellerNewList){
        list<ATG_Document_Details__c> docstoDelete = new list<ATG_Document_Details__c>();
        for(list<ATG_Document_Details__c> doc : [select id from ATG_Document_Details__c where Requested_Travelers__c in : travellerNewList])
            docstoDelete.addAll(doc);   
        if(docstoDelete !=null && !docstoDelete.isEmpty())
            delete docstoDelete;      
  } 
  
  
  
  
  

}