/**************************************************************************************************
* Name               : ATG_Retrieve_PNR                                                               
* Description        : Controller for the button Retrieve GDS button
                                                          
* Created Date       : 9th Oct 2018                                                                *
* Created By         : Leeba Shibu(PWC Digital)                                              
* -------------------------------------------------------------------------------------------------
* Version    Author    Date    Comment                                                               */


public without sharing class ATG_Retrieve_PNR{

    @AuraEnabled public static PNRResults FetchPNRInformation(string PNRNumber){
        PNRResults objPNR = new PNRResults();
        for(PNR__c pnr:[select Id,Name,ATG_IATA_Number__c,ATG_Number_of_Adults__c,ATG_Number_Of_Children__c,ATG_Number_Of_Infants__c,ATG_Number_Of_Passengers__c,ATG_PNR_Type__c,
        (select Id,Name,ATG_AirLine_Code__c,ATG_Passenger_Full_Name__c,ATG_First_Name__c,ATG_Last_Name__c,ATG_Passenger_Type__c,PNR__c,ATG_Route__c,ATG_Ticket_Number__c,ATG_SYS_CCGR__c,ATG_Travel_Order__c,Status__c,Status_Length__c from PNR_Passengers__r where Status__c = 'Original' AND (Status_Length__c = 6 OR ATG_SYS_CCGR__c = 'INV')) from PNR__c where Name=:PNRNumber]){
            objPNR.pnr = pnr;
            for(PNR_Passenger__c psg:pnr.PNR_Passengers__r){
                objPNR.PNRPassengers.add(psg);
            }
        }
        return objPNR;
    }

    public class PNRResults{
        @AuraEnabled public PNR__c pnr;
        @AuraEnabled public list<PNR_Passenger__c> PNRPassengers;
        public PNRResults(){
            pnr = new PNR__c();
            PNRPassengers = new list<PNR_Passenger__c>();
        }
    }
    
    @AuraEnabled
    public static map<string,string> ATG_FetchPNR(string PNRNumber,string CaseId,string ipAddress,string browserName){    
        return ATG_FetchPNR.getCredentials(PNRNumber,CaseId,ipAddress,browserName);   
    } 
}