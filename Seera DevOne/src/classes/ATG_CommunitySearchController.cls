/**
 * Created by mnazir001 on 7/12/18.
 */
/**************************************************************************************************
* Name               : ATG_CommunitySearchController
* Description        : Controller to search employees for community users
* Created Date       : 7/12/18
* Created By         : PWC Digital
* ------------------------------------------------------------------------------------------------
* Version       Author      Date        Comment

**************************************************************************************************/



public with sharing class ATG_CommunitySearchController {
    @AuraEnabled
    public static Map<string, Object> searchForIds(String searchText) {

        Map<String, Object> response = new Map<String, Object>();

        String escapedSearchString = String.escapeSingleQuotes(searchText).toLowerCase();

        List<Account> accounts = new List<Account>();
        String accountName = 'Ministry of Health';
        string requestedTravellerEmployee ='Select ';

        try{

//            String soql = 'SELECT id, Name, firstName, middleName, lastName, PersonEmail, ATG_Grade__c, ATG_Level__c, ATG_Age__c, ATG_Gender__c, ' +
//                    'PersonMobilePhone, PersonBirthdate, ATG_Passport_Number__c, ATG_Passport_Issue_Date__c, ATG_Passport_Expiry_Date__c, ' +
//                    'ATG_Passport_Issuing_Country__c, ATG_National_ID__c, ATG_Nationality__c, ATG_National_ID_Expiry_Date__c, ' +
//                    'ATG_Employee_Id__c ' +
//                    'from Account WHERE ';




            if (!searchText.equals('')){

                // Query Requested Traveler Info
                List<String> fieldsList = new List<String>(ATG_utility.GetAccessibleFields('Account'));
                requestedTravellerEmployee += string.join(fieldsList,',') +', ATG_Company_Name__r.Name, RecordType.DeveloperName from Account WHERE ' +
                        ' (ATG_Employee_ID__c != null AND ATG_Company_Name__r.Name =\'' + accountName + '\' AND RecordType.DeveloperName=\'PersonAccount\') ';


                requestedTravellerEmployee += ' AND (' +
                        'ATG_Employee_ID__c LIKE \'%'+ escapedSearchString
                        +'%\' OR name LIKE \'%'+ escapedSearchString
                        +'%\' OR ATG_Employee_ID__c LIKE \'%'+ escapedSearchString
                        +'%\' OR ATG_National_ID__c LIKE \'%'+ escapedSearchString
                        +'%\' OR ATG_Passport_Number__c LIKE \'%'+ escapedSearchString
                        +'%\' OR ATG_Primary_Phone__c LIKE \'%'+ escapedSearchString
                        +'%\')';



                requestedTravellerEmployee = requestedTravellerEmployee + ' LIMIT 100';

                system.debug('SOQL search query' + requestedTravellerEmployee);
                accounts = Database.query(requestedTravellerEmployee);

                response.put('success', true);
                response.put('message', '');
                response.put('data', accounts);
            }
        }catch (Exception ex){
            System.debug('Employee Search error' + ex.getMessage());

            response.put('success', false);
            response.put('message', ex.getMessage() + '\n' + requestedTravellerEmployee);
            response.put('data', null);
        }

        return response;
    }
}