/**************************************************************************************************
* Name              : ATG_EmailToSMSHandler
* Description        : Inbound EmailServiceHandler that facilitates sending SMS.                                
* Created Date       : 30 Jul 2018                                                        *
* Created By        : PWC Digital                                        
* -------------------------------------------------------------------------------------------------
Version     Author      Date            Comment                                                     *
v1.1        Ravindra    24/12/2018      Added the logic to send the SMS from Task - Sender name should be Tajawal - Passing the ProfileId of the user in the Email Subject
v1.2        Diana       06/03/2019      Added the logic to have sender name as Tajawal/Al Mosafer for Automated SMS,and add SMS sent flag
**************************************************************************************************/



global with sharing class ATG_EmailToSMSHandler implements Messaging.InboundEmailHandler{
        
    public final string PROFILEID = 'ProfileId';
    public static string AgentProfileId;
    private final String ID = 'Id-';
    public static String RecordId;
    public static String BusinessUnit;
     global Messaging.InboundEmailResult handleInboundEmail(Messaging.inboundEmail email, 
                                                 Messaging.InboundEnvelope env){
    
    
        Messaging.InboundEmailResult result = new Messaging.InboundEmailResult();
        ATG_Utility.SMSRequestClass smsReqObj = new  ATG_Utility.SMSRequestClass();
        
        system.debug('email '+email);
        system.debug('email '+email.subject);
        
        string mobileNumber= email.subject;
        
        if(mobileNumber.contains('Sandbox:'))
            mobileNumber = mobileNumber.replaceAll('Sandbox:','').trim();
        if(mobileNumber.contains('+'))
            mobileNumber = mobileNumber.replaceAll('\\+','').trim();         
        
                                                     //v1.2
        if(string.isNotEmpty(mobileNumber) && mobileNumber.contains(ID)){
             RecordId = mobileNumber.split(ID)[1].trim(); 
             mobileNumber = mobileNumber.split(ID)[0].trim();
            
        }  
        //v1.1
        if(string.isNotEmpty(mobileNumber) && mobileNumber.contains(PROFILEID)){
            AgentProfileId = mobileNumber.split(PROFILEID)[1].trim();
            mobileNumber = mobileNumber.split(PROFILEID)[0].trim();
        }
        //End of v1.1   
        system.debug('Mobile Num'+mobileNumber+'@');  
                                                 
        if(string.isNotEmpty(RecordId) && RecordId.startsWith('001')){
              BusinessUnit = [SELECT ATG_Business_Unit_Picklist__c FROM Account where Id=:RecordId Limit 1].ATG_Business_Unit_Picklist__c;                                           
        }else if(string.isNotEmpty(RecordId) && RecordId.startsWith(ATG_Utility.getKeyPrefix('ATG_Booking__C'))){
            BusinessUnit = [SELECT ATG_Business_Unit__c FROM ATG_Booking__C Where Id=:RecordId LIMIT 1].ATG_Business_Unit__c;
        }else if(string.isNotEmpty(RecordId) && RecordId.startsWith(ATG_Utility.getKeyPrefix('Case'))){
            BusinessUnit = [SELECT ATG_BU_From_case__c FROM Case Where Id=:RecordId LIMIT 1].ATG_BU_From_case__c;
        }                                            
                                                     
        string smsBody = email.plainTextBody;
        
        if(!string.isEmpty(mobileNumber) && !string.isEmpty(smsBody)){
             smsReqObj.messageText = email.plainTextBody;
             smsReqObj.recipients = mobileNumber;//email.Subject;
             ATG_utility.SendSMS(smsReqObj);   
             result.success = true;
        }
                                                     
                                                     
        
        return result;
     }
    
}