/**************************************************************************************************
* Name               : ATG_AccountHandler                                                               
* Description        : Trigger Handler for the Account SObject. This class implements the ITrigger
                       interface to help ensure the trigger code is bulkified and all in one place.                                       
* Created Date       : 10th June 2018                                                                *
* Created By         : Swati Sehrawat (PWC Digital)                                              
* -------------------------------------------------------------------------------------------------
Version Author              Date            Comment                                                               *
v1.1    Ravindra            18/Feb/2019     Added the logic to update/link the Social Personnas to Master Account
v1.2    Ravindra/Swati      19/Feb/2019, 26/Mar/2019     Chnaged the condition to update Mobile Formula based on person mobile
v1.4    Ravindra            20/Feb/2019     Added the logic to format the mobile number and populate on the Person Mobile for the data coming form HUB
v1.5    Ravindra            26/Feb/2019     Added the logic to link the similar Accounts with AccountContact Relation - if same mobile number and different email based on BU
v1.6    Ravindra            27/Feb/2019     Added/updated the logic to populate the MasterId & Golden Id on the accounts
v1.7    Ravindra            28/Feb/2019     Added the logic to merge the customers if the Customer is alaredy exist in the salesforce without any hub id
v1.8    Ravindra            28/Feb/2019     Added the logic to format the mobile number coming from live chat 
**************************************************************************************************/

public without sharing class ATG_AccountHandler implements ATG_TriggerInterface{
    
    public static set<string> setDailingCodes = new set<string>();
    public static map<string, string> mapPickCountryCodes = new map<string, string>();
    public static map<string, long> mapMaxUniqueNumbers = new map<string, long>();
    
    public static Boolean ByPassUpdateTrigger = false;
    
    static list<Id> lstRecTypeIds = new list<string>{
        ATG_ConstantsUtility.TJW_PERSON_ACCOUNT_RECORDTYPEID,
        ATG_ConstantsUtility.AMH_PERSON_ACCOUNT_RECORDTYPEID
    };
    
    public void beforeInsert(list<SObject> soNewList){
        try{
            prepareMobileNumber(soNewList, null);//v1.8
            formatHubMobileNumber(soNewList, null);//v1.4
            //if(test.isRunningTest())
            	generateIDforCustomers(soNewList);
            checkEmailAddress(soNewList);
            ATG_AccountHandler.PrepopulateIsMoHAccountField((list<Account>)soNewList);
        }catch(exception ex){
            ATG_utility.LogException(ex);
        }
    }
    
    public void beforeUpdate(list<SObject> soNewList,map<id,SObject> soOldMap){
        if(!ATG_AccountHandler.ByPassUpdateTrigger){
	        if(ATG_ConstantsUtility.runOnceForAccount == false){
	            prepareMobileNumber(soNewList, (map<Id,Account>)soOldMap);//v1.8
	            formatHubMobileNumber(soNewList, (map<Id,Account>)soOldMap);//v1.4
	            //ATG_ConstantsUtility.runOnceForAccount = true;
	            map<Id,Account> TriggerOldMap = (map<Id,Account>)soOldMap;
	            updateMobile(soNewList,TriggerOldMap);
	            checkEmailAddress(soNewList);
	            ATG_AccountHandler.ValidateNationalID((list<Account>)soNewList);
	        }
        }
    }

    public void beforeDelete(list<SObject> soNewList){}

    public void afterInsert(list<SObject> soNewList){
        try{
            if(ATG_ConstantsUtility.runOnceForAccount == false){
                
                //v1.6
                checkForDuplicateCustomer(soNewList);
                
                //ATG_ConstantsUtility.runOnceForAccount = true;
                customer360ManagerAI(soNewList);
                            
                //v1.5
                linkSimilarAccounts(soNewList, null);
            }

        }catch(exception ex){
            system.debug('---1-----'+ex.getLineNumber());
            ATG_utility.LogException(ex);
        }
    }

    public void afterUpdate(list<SObject> soNewList,map<id,SObject> soOldMap){
        try{
        	if(!ATG_AccountHandler.ByPassUpdateTrigger){
	            if(ATG_ConstantsUtility.runOnceForAccount == false){
	                //ATG_ConstantsUtility.runOnceForAccount = true;
	                map<Id,Account> TriggerOldMap = (map<Id,Account>)soOldMap;
	                customer360ManagerAU(soNewList,TriggerOldMap);
	
	                //v1.1
	                updateSocialPersonna(soNewList, (map<Id,Account>)soOldMap);
	                //v1.5
	                linkSimilarAccounts(soNewList, (map<Id,Account>)soOldMap);
	            }
        	}
        }catch(exception ex){
            system.debug('----2----'+ex.getLineNumber());
            ATG_utility.LogException(ex);
        }
    }

    public void afterDelete(list<SObject> soNewList){}
    
    
    //------------------------------Method Definition-----------------------------------------//
    
    set<Account> AccountToOperate = new set<Account>();
    Map<string, ATG_Account_To_Account_Search__c> customSettings = ATG_Account_To_Account_Search__c.getAll();
    
    public void customer360ManagerAI(list<Account> AccountNewList){
        if(AccountNewList!=null && AccountNewList.size()>0){        
            for(Account acc : AccountNewList){
                if(acc.recordTypeID != ATG_ConstantsUtility.MASTER_PERSON_ACCOUNT_RECORDTYPEID && acc.recordTypeID != ATG_ConstantsUtility.MASTER_BUSINESS_ACCOUNT_RECORDTYPEID){
                    for(string key : customSettings.keyset()){
                        if(acc.get(Key)!=null)
                            AccountToOperate.add(acc);  
                    }
                }
            }
            
            if(AccountNewList!=null && AccountNewList.size()>0)
                customer360Manage(AccountToOperate);    
        }
    }
    
    
    public void customer360ManagerAU(list<Account> AccountNewList,map<id,Account> AccountOldMap){
        if(AccountNewList!=null && AccountNewList.size()>0 && AccountOldMap!=null && AccountOldMap.size()>0){           
            
            for(Account acc : AccountNewList){
                Account oldRecord = AccountOldMap.get(acc.Id);
                if(oldRecord!=null && acc.recordTypeID != ATG_ConstantsUtility.MASTER_PERSON_ACCOUNT_RECORDTYPEID && acc.recordTypeID != ATG_ConstantsUtility.MASTER_BUSINESS_ACCOUNT_RECORDTYPEID){
                    for(string key : customSettings.keyset()){
                        if(acc.get(Key)!=null && oldRecord.get(Key)!=null && acc.get(Key) != oldRecord.get(Key))
                            AccountToOperate.add(acc);  
                    }
                }
            }
            
            if(AccountNewList!=null && AccountNewList.size()>0)
                customer360Manage(AccountToOperate);    
        }
    }
    
    public void customer360Manage(set<Account> AccountToOperate){
        
        string query = 'Select Id, ATG_Record_Type_Name__c, firstName, lastName,Type,ATG_Passport_Number__c, ATG_National_ID__c, ATG_Master_Account__c,RecordTypeId, IsPersonAccount, ATG_Is_Master_Account__c,  ';
        map<String, set<String>> CustomSettingAccountMap = new map<String, set<String>>();
        map<String, Set<String>> accountConfiguredKeyMap = new Map<String, Set<String>>();

        for(ATG_Account_To_Account_Search__c thisConfiguration : ATG_Account_To_Account_Search__c.getAll().values()){
            string accountType = thisConfiguration.Is_Available_For_Person_Account__c ? 'Leisure' : 'Corporate';
            if(accountConfiguredKeyMap.containsKey(accountType)){
                accountConfiguredKeyMap.get(accountType).add(thisConfiguration.Name);   
            }else{
                accountConfiguredKeyMap.put(accountType, new Set<String>{thisConfiguration.Name});  
            }   
        }
        
        system.debug('-----accountConfiguredKeyMap-----'+accountConfiguredKeyMap);
        if(AccountToOperate!=null && AccountToOperate.size()>0){        
            for(Account acc : AccountToOperate){
                if(acc.recordTypeID != ATG_ConstantsUtility.MASTER_PERSON_ACCOUNT_RECORDTYPEID && acc.recordTypeID != ATG_ConstantsUtility.MASTER_BUSINESS_ACCOUNT_RECORDTYPEID){
                    for(String key : accountConfiguredKeyMap.get(acc.Type)){
                        set<String> tempSet = new set<String>();
                        if(acc.get(key)!=null){
                            tempSet.add((String)acc.get(key));
                            CustomSettingAccountMap.put(key, tempSet);
                            //query += key + ',';
                        }
                        if( query.contains( key + ',' ) == false )
                            query += key + ',';
                    }
                }
            }
        }
        system.debug('-----query-----'+query);
        
        boolean hasWhereCondition = false;
        if(CustomSettingAccountMap!=null && CustomSettingAccountMap.size()>0){  
            system.debug('-----CustomSettingAccountMap-----'+CustomSettingAccountMap);
            
            
            query = query.removeEnd(',') + ' From Account  WHERE ( ';
            for(String key : CustomSettingAccountMap.keySet()){
                if(CustomSettingAccountMap.get(key)!=null && CustomSettingAccountMap.get(key).size()>0){
                    query += key + ' IN ' + ATG_utility.getValuesInQuotes(CustomSettingAccountMap.get(key)) + ' OR '; 
                    hasWhereCondition = true;
                }
            }
            query = query.removeEnd(' OR ');
        }       
        
        system.debug('-----query-----'+query);
        list<Account> existingCustomer = new List<Account>();
        map<String, Account> uniqueKeyAccountMap = new Map<String, Account>();
        if(hasWhereCondition == true){
            string masterPerson = ATG_ConstantsUtility.MASTER_PERSON_ACCOUNT_RECORDTYPEID;
            string masterBusiness = ATG_ConstantsUtility.MASTER_BUSINESS_ACCOUNT_RECORDTYPEID;
            
            query = query+' ) AND recordTypeID !=: masterPerson AND recordTypeID !=: masterBusiness';
            
            system.debug('-----finalquery-----'+query);
            existingCustomer = Database.query(query);
        
        }
        
        system.debug('-----existingCustomer1-----'+existingCustomer);
        if(existingCustomer!=null && existingCustomer.size()>0){
            string oldKey = '';
            
            User integrationUser = new User();
            integrationUser = ATG_Utility.integrationUser();
            
            map<string, ATG_Master_Account_Fields__c> customSettingsForMaster = ATG_Master_Account_Fields__c.getAll();
            
            for(Account thisAccount : existingCustomer){
                string key = ATG_ConstantsUtility.KEY_SPLITTER;
                if(accountConfiguredKeyMap != null && thisAccount.Type != null && accountConfiguredKeyMap.containsKey(thisAccount.Type)){
	                for(String thisKey : accountConfiguredKeyMap.get(thisAccount.Type)){
	                    if(thisAccount.get(thisKey) != null)
	                        key = key + String.valueOf(thisAccount.get(thisKey)) + ATG_ConstantsUtility.KEY_SPLITTER;
	                }
                }
                system.debug('-----key-----'+key);
                if(!uniqueKeyAccountMap.containsKey(key)){
                    system.debug('-----oldKey-----'+oldKey);
                    if(string.isNotBlank(oldKey) && key.containsAny(oldKey)){
                    }else{
                        system.debug('-----1-----'+existingCustomer.size());
                        Account NewMaster = new Account();
                        NewMaster.ownerID = integrationUser.id;
                        if(existingCustomer.size()==1){
                            thisAccount.ATG_is_master_account__c = true;
                            thisAccount.ATG_master_account__c = null;
                            uniqueKeyAccountMap.put(key, thisAccount);
                        }else{
                            oldKey = key;
                            NewMaster = new Account();
                            system.debug('-----2-----'+thisAccount.ATG_master_account__c);
                            if(thisAccount.ATG_master_account__c!=null)
                                NewMaster.id = thisAccount.ATG_master_account__c;
                                
                            if(thisAccount.isPersonAccount == true)
                                NewMaster.recordTypeID = ATG_ConstantsUtility.MASTER_PERSON_ACCOUNT_RECORDTYPEID;
                            else
                                NewMaster.recordTypeID = ATG_ConstantsUtility.MASTER_BUSINESS_ACCOUNT_RECORDTYPEID;
                            
                            for(string str : customSettingsForMaster.keySet()){
                                if(thisAccount.get(str)!=null)
                                    NewMaster.put(str,thisAccount.get(str));
                            }
                            uniqueKeyAccountMap.put(key, NewMaster);    
                        }
                    }
                }
                system.debug('-----uniqueKeyAccountMap-----'+uniqueKeyAccountMap);
            }
        }   
        
        system.debug('----existingCustomer----'+existingCustomer);
        if(uniqueKeyAccountMap!=null && uniqueKeyAccountMap.size()>0){
            upsert uniqueKeyAccountMap.values();
            
            list<Account> updateAccountList = new List<Account>();
            if(existingCustomer.size()>1){
                map<String, Map<String, Account>> keyValueMasterAccountMap = new Map<String, Map<String, Account>>();
                map<String, List<Account>> masterAndChildAccountMap = new Map<String, List<Account>>();
                string oldKey = '';
                for(Account thisAccount : existingCustomer){
                    
                    for(string thisUniqueKey : uniqueKeyAccountMap.keySet()){
                        if(accountConfiguredKeyMap != null && thisAccount.Type != null && accountConfiguredKeyMap.containsKey(thisAccount.Type)){
	                        for(String thisConfiguredKey : accountConfiguredKeyMap.get(thisAccount.Type)){
	                            if(string.isNotBlank(oldKey) && thisUniqueKey.containsAny(oldKey)){
	                                if(thisAccount.get(thisConfiguredKey) != null && thisUniquekey.containsAny(String.valueOf(thisAccount.get(thisConfiguredKey)))){
	                                    thisAccount.ATG_Master_Account__c = uniqueKeyAccountMap.get(thisUniqueKey).Id;
	                                    thisAccount.ATG_Is_Master_Account__c = false;
	                                }
	                            }else{
	                                oldKey = thisUniqueKey;
	                                thisAccount.ATG_Is_Master_Account__c = true;
	                                thisAccount.ATG_Master_Account__c = null;
	                            }   
	                        }
	                        updateAccountList.add(thisAccount);
                        } 
                    }
                    system.debug('-----updateAccountList-----'+updateAccountList);
                
                }
            }else{
                existingCustomer[0].ATG_Is_Master_Account__c = true;
                existingCustomer[0].ATG_Master_Account__c = null;
                updateAccountList.add(existingCustomer[0]);
            }
            
            if(updateAccountList!=null && updateAccountList.size()>0){
                ATG_ConstantsUtility.runOnceForAccount = true;
                update updateAccountList;
                ATG_ConstantsUtility.runOnceForAccount = false;
            }
        }
    }
    
    public void generateIDforCustomers(list<Account> AccountNewList){
        map<string, ATG_BU_Configuration__c> customSettings = ATG_BU_Configuration__c.getAll();
        system.debug('-----customSettings-----'+customSettings);
        
        //v1.6
        string PreceedingId = ATG_ConstantsUtility.MASTER_GOLD_ID;
        string sTemp = '';
        
        for(Account acc : AccountNewList){
            if(acc.PersonMobilePhone!=null && string.isNotBlank(acc.PersonMobilePhone))
            	acc.ATG_Mobile_Formula__c = acc.PersonMobilePhone.right(9);
        }
        
        /*if(mapMaxUniqueNumbers.isEmpty()){
        	map<string, string> mapRecordTypes = new map<string, string>();
        	
        	for(Schema.RecordTypeInfo obj : Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().values()){
        		mapRecordTypes.put( obj.getRecordTypeId(), obj.getDeveloperName() );
        		mapMaxUniqueNumbers.put(obj.getRecordTypeId(), 0);
        	}

	        for(AggregateResult obj : [select MAX(ATG_Unique_ID__c) MaxUniqueId, MAX(ATG_Golden_ID__c) MaxGoldenId ,RecordTypeId from Account where (ATG_Unique_ID__c != null OR ATG_Golden_ID__c != null) Group by RecordTypeId ]){
			    system.debug(''+obj.get('RecordTypeId')+' '+obj.get('MaxUniqueId')+ ' '+obj.get('MaxGoldenId'));
			    long num = 0;
			    if(mapRecordTypes.get( obj.get('RecordTypeId')+'' ) == 'ATG_Master_Person_Account' || mapRecordTypes.get( obj.get('RecordTypeId')+'' ) == 'ATG_Master_Business_Account'){
				    if(obj.get('MaxGoldenId') != null){
				    	num = long.valueOf(obj.get('MaxGoldenId')+'');
				    }else{
				    	num = 0;
				    }
				    //num = 
				    mapMaxUniqueNumbers.put(''+obj.get('RecordTypeId'), num);
			    }else{
			    	if(mapRecordTypes.get( obj.get('RecordTypeId')+'' ) == 'AMH_Person_Account'){ // Al Mosafer
						num = getNumber( obj.get('MaxUniqueId')+'' );
			    	}else if( mapRecordTypes.get( obj.get('RecordTypeId')+'' ) == 'PersonAccount' ){ //Al Tayyar
			    		num = getNumber( obj.get('MaxUniqueId')+'' );
			    	}else if( mapRecordTypes.get( obj.get('RecordTypeId')+'' ) == 'TJW_Person_Account' ){//tajawal
			    		num = getNumber( obj.get('MaxUniqueId')+'' );
			    	}
			    	mapMaxUniqueNumbers.put(''+obj.get('RecordTypeId'), num);
			    }
			}
        }
        
        for(Account acc : AccountNewList){
        	if(mapMaxUniqueNumbers.containsKey(acc.RecordTypeId)){
        		long l;
        		if(acc.recordTypeID != ATG_ConstantsUtility.MASTER_PERSON_ACCOUNT_RECORDTYPEID && acc.recordTypeID != ATG_ConstantsUtility.MASTER_BUSINESS_ACCOUNT_RECORDTYPEID){
        			l = mapMaxUniqueNumbers.get(acc.RecordTypeId)+1;
	        		sTemp = string.valueOf(PreceedingId + l ).right(9);	
	        		acc.ATG_Unique_ID__c = ATG_ConstantsUtility.MAP_SHORT_BU.get(acc.ATG_Business_Unit_Picklist__c)+'-'+sTemp; //v1.6
	        		mapMaxUniqueNumbers.put(acc.RecordTypeId, l);
        		}else{
        			l = mapMaxUniqueNumbers.get(acc.RecordTypeId)+1;
                	acc.ATG_Golden_ID__c = string.valueOf(PreceedingId+ l ).right(9); //v1.6
        			mapMaxUniqueNumbers.put(acc.RecordTypeId, l);
        		}
        	}
        }*/
        
        /*for(Account acc : AccountNewList){
            
            if(acc.recordTypeID != ATG_ConstantsUtility.MASTER_PERSON_ACCOUNT_RECORDTYPEID && acc.recordTypeID != ATG_ConstantsUtility.MASTER_BUSINESS_ACCOUNT_RECORDTYPEID){
                if(customSettings.containsKey(acc.ATG_Business_Unit_Picklist__c)){
                    //string PreceedingId = '0';
                    Long newGenNumber = Long.valueOf(customSettings.get(acc.ATG_Business_Unit_Picklist__c).ATG_Last_Generated_Unique_ID__c)+1;
                    
                    sTemp = string.valueOf(PreceedingId+newGenNumber).right(9);
                    //sTemp = sTemp.right(9);
                    acc.ATG_Unique_ID__c = ATG_ConstantsUtility.MAP_SHORT_BU.get(acc.ATG_Business_Unit_Picklist__c)+'-'+sTemp; //v1.6
                    customSettings.get(acc.ATG_Business_Unit_Picklist__c).ATG_Last_Generated_Unique_ID__c = string.valueOf(newGenNumber);
                }
                
                if(acc.PersonMobilePhone!=null && string.isNotBlank(acc.PersonMobilePhone))
                    acc.ATG_Mobile_Formula__c = acc.PersonMobilePhone.right(9);
            }
            else{
                //v1.6
                Long newGenNumber = Long.valueOf(customSettings.get(ATG_ConstantsUtility.STR_MASTER).ATG_Last_Generated_Unique_ID__c)+1;
                acc.ATG_Golden_ID__c = string.valueOf(PreceedingId+newGenNumber).right(9); //v1.6
                customSettings.get(ATG_ConstantsUtility.STR_MASTER).ATG_Last_Generated_Unique_ID__c = string.valueOf(newGenNumber);
                
            }
                       
        }
        update customSettings.values();*/
    }
    
    public static long getNumber(string uniqueNo){
    	return uniqueNo.indexOf('-') != -1 ? ( long.valueOf(uniqueNo.split('-')[1] )) : 0;
    }
    
    public void updateMobile(list<Account> AccountNewList,map<id,Account> AccountOldMap){
        for(Account acc : AccountNewList){
            Account oldRecord = AccountOldMap.get(acc.Id);
            if(acc.PersonMobilePhone!=null && acc.recordTypeID != ATG_ConstantsUtility.MASTER_PERSON_ACCOUNT_RECORDTYPEID && acc.recordTypeID != ATG_ConstantsUtility.MASTER_BUSINESS_ACCOUNT_RECORDTYPEID){
                if ( string.isBlank(acc.ATG_Mobile_Formula__c) || (oldRecord != null && acc.PersonMobilePhone != oldRecord.PersonMobilePhone)) //v1.2 - changed the conditoin from Mobile Formula to Person Mobile
                    acc.ATG_Mobile_Formula__c = acc.PersonMobilePhone.right(9);
            }
        }
    }
    
    public void checkEmailAddress(list<Account> AccountNewList){
        Map<string, ATG_Emails_To_Exclude__c> customSettings = ATG_Emails_To_Exclude__c.getAll();
        
        system.debug('---customSettings---'+customSettings);
        if(customSettings!=null && customSettings.size()>0){
            for(Account acc : AccountNewList){
                system.debug('---acc.PersonEmail---'+acc.PersonEmail);
                
                if(acc.PersonEmail!=null && string.isNotBlank(acc.PersonEmail) && customSettings.containsKey(acc.PersonEmail))  
                     acc.addError('Please provide correct email address.');         
            }
        }
    }
    
    
    /****************************************************************************************
    @Description - Method to Validate National ID if the employee belongs to MOH
    @Param - Trigger.New   
    ****************************************************************************************/
    public static void PrepopulateIsMoHAccountField(list<Account> accList){
        if(!string.isBlank(Label.MoH_profiles_with_create_Emp)){
            list<string> profilesWithCreateUsers = Label.MoH_profiles_with_create_Emp.split(',');
            list<User> usr = [select id,profile.Name from User where id =: userInfo.getUserId() and profile.Name in : profilesWithCreateUsers];
            if(!usr.isEmpty()){ //usr !=null - By Ravi, changed the queery to assign to a list to avoid - List has no rows for assignment to SObject Error
                for(Account acc : accList)
                    acc.ATG_Is_MoH_Account__c = true; 
                 ATG_AccountHandler.ValidateNationalID(accList);                 
            }
        }     
           
    }
    
    
    /****************************************************************************************
    @Description - Method to Validate National ID if the employee belongs to MOH
    @Param - Trigger.New   
    ****************************************************************************************/
    
    public static void ValidateNationalID(list<Account> accNewList) {
         if(accNewList !=null){
            for(Account acc : accNewList){              
              if(acc.ATG_Is_MoH_Account__c && !String.IsBlank(acc.ATG_National_ID__c) && acc.ATG_National_ID__c!=null){
                  if(acc.ATG_National_ID__c.right(1) != ATG_Utility.validateIqama(acc.ATG_National_ID__c))
                      acc.ATG_National_ID__c.addError(Label.IQAMA_Validation_Message);                     
              }
           }
        }
    }
    
    //v1.1
    public static void updateSocialPersonna(list<Account> lstNewAccounts, map<Id, Account> mapOldAccounts){
        map<Id,Id> mapActIds = new map<Id,Id>();
        for(Account objA : lstNewAccounts){
            if(objA.ATG_Master_Account__c != null && objA.ATG_Master_Account__c != mapOldAccounts.get(objA.Id).ATG_Master_Account__c ){
                mapActIds.put(objA.Id,objA.ATG_Master_Account__c);
            }
        }
        if(!mapActIds.isEmpty()){
            list<SocialPersona> lstPersonas = new list<SocialPersona>();
            for(SocialPersona objSP : [select Id,Account__c from SocialPersona where Account__c IN : mapActIds.keySet()]){
                objSP.Account__c = mapActIds.get(objSP.Account__c);
                lstPersonas.add(objSP);
            }
            if(!lstPersonas.isEmpty()){
                update lstPersonas;
            }
        }
    }
    ///end of v1.1
        
    //v1.4
    public static void formatHubMobileNumber(list<Account> lstNewAccounts, map<Id,Account> mapOldAccounts){
        if(Userinfo.getUserName().contains(ATG_ConstantsUtility.INTEGRATION_USERNAME) || test.isRunningTest()){
            setDailingCodes = setDailingCodes.isEmpty() ? ATG_utility.getDailingCodes() : setDailingCodes;
            getCountryCodePickValues();
            
            for(Account objA : lstNewAccounts){
                if(objA.ATG_Mobile_Number_form_HUB__c != null && (mapOldAccounts == null || 
                        (mapOldAccounts != null && mapOldAccounts.containsKey(objA.Id) && mapOldAccounts.get(objA.Id).ATG_Mobile_Number_form_HUB__c != objA.ATG_Mobile_Number_form_HUB__c )) 
                ){
                    objA.PersonMobilePhone = formatPhoneNumber(objA.ATG_Mobile_Number_form_HUB__c);
                    
                    if(objA.PersonMobilePhone != null && objA.PersonMobilePhone.indexOf(' ') > -1 && objA.PersonMobilePhone.split(' ').size() > 1 && mapPickCountryCodes.containsKey( objA.PersonMobilePhone.split(' ')[0] ) ){
                        objA.Country_Code__pc = mapPickCountryCodes.get( objA.PersonMobilePhone.split(' ')[0] );
                        objA.Mobile_Number__pc = objA.PersonMobilePhone.split(' ')[1];
                    }
                }
            }
        }
    }

    public static string formatPhoneNumber(string mobNum){
        if(mobNum != null){
            if(mobNum.indexOf('+971') == 0){
                return mobNum.replace('+971','+971 ');
            }else if(mobNum.indexOf('+966') == 0){
                return mobNum.replace('+966','+966 ');
            }else if(mobNum.indexOf('+91') == 0){
                return mobNum.replace('+91','+91 ');
            }else if(mobNum.indexOf('+92') == 0){
                return mobNum.replace('+92','+92 ');
            }else if(mobNum.indexOf('+20') == 0){
                return mobNum.replace('+20','+20 ');
            }else{
                //setDailingCodes = setDailingCodes.isEmpty() ? ATG_utility.getDailingCodes() : setDailingCodes;
                for(string sCode : setDailingCodes){
                    if(mobNum.indexOf('+'+sCode) == 0){
                        if(mobNum.indexOf('+1') == 0 && mobNum.contains('-')){
                            if(mobNum.indexOf('+'+sCode) == 0 && sCode != '1'){
                                return mobNum.replace('+'+sCode,'+'+sCode+' ');
                            }
                        }else{
                            return mobNum.replace('+'+sCode,'+'+sCode+' ');
                        }
                    }
                }
            }
        }
        return null;
    }
    //end of v1.4

    //v1.5
    public static void linkSimilarAccounts(list<Account> lstNewAccounts, map<Id,Account> mapOldAccounts){
        
        set<Id> setIDs = new set<Id>();
        set<string> BU = new set<string>();
        set<string> mobileNumber = new set<string>();

        for(Account objA : lstNewAccounts){
            if(string.isNotBlank(objA.PersonEmail) && string.isNotBlank(objA.PersonMobilePhone)){
                if(objA.ATG_Mobile_Formula__c != null){

                    if( (mapOldAccounts == null) || 
                    	(mapOldAccounts!=null && mapOldAccounts.containsKey(objA.Id) && mapOldAccounts.get(objA.Id).PersonMobilePhone != objA.PersonMobilePhone) ){
                        //setIDs.add(objA.Id);
                        //setIDs.add(objA.personContactID);
                        
                        mobileNumber.add(objA.ATG_Mobile_Formula__c);
                        BU.add(objA.ATG_Business_Unit_Picklist__c);
                    }
                    /*else if(mapOldAccounts == null){
                        mobileNumber.add(objA.ATG_Mobile_Formula__c);
                        BU.add(objA.ATG_Business_Unit_Picklist__c);
                    }*/
                    system.debug('mobileNumber '+mobileNumber);
                }
            }   
        }
        
        //delete the Account Relations if the mobile number has changed
        /* Since Contact persons on Bookings also creating same relationship - by deleting this may cause the loss of relationship for Booking
        if(!setIDs.isEmpty()){
            list<AccountContactRelation> tempList = [select Id from AccountContactRelation where (AccountId IN : setIDs  or ContactId IN : setIDs) AND AccountContactUniqueKey__c != null] ;
            if(tempList!=null && tempList.size()>0)
                delete tempList;
            
        }*/
        
        if(!mobileNumber.isEmpty() && !BU.isEmpty()){
            map<string, AccountContactRelation> mapACRs = new map<string, AccountContactRelation>();
            map<Id,Account> mapAccounts = new map<Id, Account>( [select Id,Name,ATG_Business_Unit_Picklist__c,personContactID,ATG_Mobile_Formula__c from Account 
                                    where ATG_Mobile_Formula__c IN : mobileNumber AND recordTypeId IN : lstRecTypeIds AND ATG_Business_Unit_Picklist__c IN : BU]);
            if(!mapAccounts.isEmpty()){
                for(Account objP : mapAccounts.values()){
                    system.debug('');
                    for(Account objC : mapAccounts.values()){
                        if(objP.Id != objC.Id && objP.ATG_Business_Unit_Picklist__c == objC.ATG_Business_Unit_Picklist__c &&
                          objP.ATG_Mobile_Formula__c == objC.ATG_Mobile_Formula__c){
                            AccountContactRelation obj = new AccountContactRelation(
                                    AccountId = objP.Id,
                                    ContactId = objC.personContactID,
                                    AccountContactUniqueKey__c = objP.Id+'-'+objC.personContactID                           
                            );
                            obj.Roles = 'Linked Account';
                            mapACRs.put( (objP.Id+'-'+objC.personContactID), obj);
                        }
                    }
                    break;
                }
                if(!mapACRs.isEmpty()){
                    for(AccountContactRelation obj : [select Id,AccountContactUniqueKey__c from AccountContactRelation where AccountContactUniqueKey__c IN : mapACRs.keySet() AND AccountContactUniqueKey__c != null ])
                        mapACRs.remove(obj.AccountContactUniqueKey__c);
                    upsert mapACRs.values() AccountContactUniqueKey__c;
                }
            }
        }
    }
    
    //end of v1.5

    //v1.6
    public static void checkForDuplicateCustomer(list<Account> lstNewAccounts){
        if(Userinfo.getUserName().contains(ATG_ConstantsUtility.INTEGRATION_USERNAME) || test.isRunningTest()){
            map<string, string> mapDupAccounts = new map<string, string>();
            set<string> BU = new set<string>();
            map<string, list<Account>> mapAccounts = new map<string, list<Account>>();
            set<string> setEmails = new set<string>();
            
            
            for(Account objA : lstNewAccounts){
                if(objA.PersonEmail != null && objA.ATG_Tapro_ID__c != null && 
                    (objA.ATG_Business_Unit_Picklist__c==ATG_ConstantsUtility.ALMOSAFER_BusinessUnit || objA.ATG_Business_Unit_Picklist__c==ATG_ConstantsUtility.TAJAWAL_BusinessUnit )){
                    
                    BU.add(objA.ATG_Business_Unit_Picklist__c);
                    mapDupAccounts.put( objA.PersonEmail+'-'+objA.ATG_Business_Unit_Picklist__c, objA.Id);
                    setEmails.add(objA.PersonEmail);
                }
            }
            
            system.debug('mapDupAccounts is '+mapDupAccounts);
            if(!mapDupAccounts.isEmpty()){
                
                for(Account objA : [select Id,LastName,FirstName,Salutation,PersonEmail,ATG_Business_Unit_Picklist__c,ATG_Tapro_ID__c from Account where 
                                        PersonEmail IN : setEmails AND ATG_Business_Unit_Picklist__c IN : BU and //mapDupAccounts.keySet()
                                        recordTypeID IN : lstRecTypeIds
                                        order by createdDate ]){
                    if(string.isBlank(objA.ATG_Tapro_ID__c )){
                        string key = objA.PersonEmail+'-'+objA.ATG_Business_Unit_Picklist__c;
                        list<Account> lst = mapAccounts.containsKey(key) ? mapAccounts.get(key) : new list<Account>();
                        lst.add(objA);
                        mapAccounts.put(key, lst);
                        
                    }
                }
                System.debug('mapAccounts is '+mapAccounts);
                if(!mapAccounts.isEmpty()){
                    for(string key : mapAccounts.keySet()){
                        Id AccountId = mapDupAccounts.get(key) != null ? mapDupAccounts.get(key) : null;
                        if(AccountId != null && trigger.newMap != null && trigger.newMap.get(AccountId) != null ){
                            Account objMas = new Account(Id=AccountId);
                            Account objChild1 = (Account)mapAccounts.get(key)[0];
                            
                            if(trigger.newMap.get(AccountId).get('LastName') == ATG_ConstantsUtility.STR_Guest){
                            	objMas.LastName = objChild1.LastName;
                            	objMas.FirstName = objChild1.FirstName;
                            	objMas.Salutation = objChild1.Salutation;
                            }
                            merge objMas objChild1;
                        }
                    }
                }
            
            }

        }       
    }

    //v1.8
    public static void prepareMobileNumber(list<Account> lstNewAccounts, map<Id,Account> mapOldAccounts){
        for(Account objAcc : lstNewAccounts){
            if(mapOldAccounts!=null){
                if(objAcc.Country_Code__pc != null && objAcc.Mobile_Number__pc != null && mapOldAccounts.containsKey(objAcc.Id) && 
                    (mapOldAccounts.get(objAcc.Id).Country_Code__pc != objAcc.Country_Code__pc ||  mapOldAccounts.get(objAcc.Id).Mobile_Number__pc != objAcc.Mobile_Number__pc))
                    objAcc.PersonMobilePhone = getCountryCode(objAcc.Country_Code__pc)+' '+objAcc.Mobile_Number__pc;
            }else{
                if(objAcc.Country_Code__pc != null && objAcc.Mobile_Number__pc != null)
                    objAcc.PersonMobilePhone = getCountryCode(objAcc.Country_Code__pc)+' '+objAcc.Mobile_Number__pc;
            }
        }
    }

    public static string getCountryCode(string sCode){
        if(sCode != null && sCode.contains('(') && sCode.contains(')')){
            sCode = sCode.substring(sCode.indexOf('(')+1, sCode.indexOf(')')).trim();
            sCode = sCode.replace(' ', '-');
        }
        return sCode;
    }

    public static void getCountryCodePickValues(){
        if(mapPickCountryCodes.isEmpty()){
            Schema.DescribeFieldResult field = Contact.Country_Code__c.getDescribe();
            string sCode;
            for(Schema.PicklistEntry pl : field.getPicklistValues()){
                sCode = pl.getValue();
                if(sCode != null && sCode != 'null'){
                    mapPickCountryCodes.put( getCountryCode(sCode), sCode );
                }
            }   
        }
    }
    
}