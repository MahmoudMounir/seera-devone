/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class ATG_CaseHandlerTest {

    static testMethod void ATG_CaseHandler1() {
        

        ATG_TestUtility objTest = new ATG_TestUtility();
        
        Account corp = objTest.createLeisureAccount();
        Account corp2 = objTest.createCorporateAccount();
        
        Entitlement et = new Entitlement();
        et.Name = label.ATG_Entitlement_Name;
        et.AccountId = corp.Id;
        insert et;
        
        Case caseID = objTest.createbookingCase(corp.id);
        
        Case objcs = new Case();
        objcs.AccountId = corp.Id;
        for(QueueSobject obj : [select Id,QueueId from QueueSobject where SobjectType = 'Case' limit 1]){
            objcs.OwnerId = obj.QueueId;
        }
        objcs.RecordTypeId = ATG_ConstantsUtility.ATG_CASE_ETO_RECORDTYPE;
        objcs.EntitlementId = et.Id;
        objcs.Follow_up_Date__c = system.today().addDays(5);
        objcs.Mobile_Number__c = '123456';
        objcs.Country_Code__c = 'Afghanistan (+93)';
        insert objcs;
        
        SocialPersona persona = new SocialPersona();
        persona.Name = 'Author';
        persona.RealName = 'Real Name';
        persona.Provider = 'Facebook';
        persona.ParentId = corp.Id;
        persona.MediaProvider = 'Facebook';
        persona.ExternalId = '213123';
        insert persona;

        SocialPost SP = new SocialPost();
        SP.Name = 'Test Social Post';
        SP.ParentId = objcs.Id;
        SP.PersonaId = persona.Id;
        SP.Headline = 'Test SP';
        SP.ExternalPostId = '213123';
        insert SP;
        
        test.startTest();
        
            objcs.SourceId = SP.Id;
            objcs.AccountId = corp2.Id;
            objcs.Origin = 'Facebook';
            update objcs;
            
            Case caseIDNoCustomer = objTest.createbookingCase(null);
            
            /*caseID.Type_of_Enquiry__c = 'General Enquiry';
            update CaseID;
            caseID.Type_of_Enquiry__c = 'New Booking';
            update CaseID;
            caseID.Type_of_Enquiry__c = 'Cancellation';
            update CaseID;
            caseID.Type_of_Enquiry__c = 'Amendment';
            update CaseID;
            caseID.Type_of_Enquiry__c = 'Complaint';
            caseID.Origin = 'Web';
            update CaseID;*/
    
            list<Case> soNewList = new list<Case>();
            soNewList.add(caseID);
            soNewList.add(caseIDNoCustomer);
            
            objcs.ATG_First_Response_Sent__c = true;
            objcs.OwnerId = userinfo.getuserid();
            objcs.Status = 'Closed';
            update objcs;
       
        test.stopTest();
        
        /*map<id,Case> soOldMap = new map<id,Case>();
        
        ATG_CaseHandler objClass = new ATG_CaseHandler();
        objClass.beforeInsert (soNewList);
        objClass.beforeUpdate (soNewList,soOldMap);
        objClass.beforeDelete (soNewList);
        objClass.afterInsert (soNewList);
        objClass.afterUpdate (soNewList,soOldMap);
        objClass.afterDelete (soNewList);
        */
    }
    
    static testMethod void ATG_CaseHandler2(){
        
         ATG_TestUtility objTest = new ATG_TestUtility();
         Account corp = objTest.createLeisureAccount();
        Account corp2 = objTest.createCorporateAccount();
        
        Entitlement et = new Entitlement();
        et.Name = label.TJW_Entitlement_Name;
        et.AccountId = corp.Id;
        insert et;
        
         Entitlement et1 = new Entitlement();
        et1.Name = label.AMH_Entitlement_Name;
        et1.AccountId = corp.Id;
        insert et1;
        
        Case objCS = new Case();
        objCS.Subject='test';
        objCS.Description = 'test';
        objCS.ATG_BU_From_Case__c = 'Tajawal';
        objCS.Origin = 'Web Contact Us';
        objCS.EntitlementId = et.Id;
        insert objCS;
        list<Case> soNewList = new list<Case>();
        soNewList.add(objCS);
        
        
        Case objCS1 = new Case();
        objCS1.Subject='test';
        objCS1.Description = 'test';
        objCS1.ATG_BU_From_Case__c = 'Al Mosafer';
        objCS1.Origin = 'Web Contact Us';
        objCS1.EntitlementId = et1.Id;
        insert objCS1;
        soNewList.add(objCS1);
         ATG_CaseHandler objClass = new ATG_CaseHandler();
        objClass.beforeInsert (soNewList);
    }
    
   
    static testMethod void ATG_CaseHandler3(){
        
         
        
        Profile p = [SELECT Id FROM Profile WHERE Name='System Administrator']; 
        User u = new User(Alias = 'admint', Email='adminuser@testorg.com', 
            EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
            LocaleSidKey='en_US', ProfileId = p.Id, 
            TimeZoneSidKey='America/Los_Angeles', UserName='adminuser@testorg1.com');
        
       insert u;
        
        Group groupO= new Group();
        groupO.Type = 'Queue';
        groupO.DeveloperName = 'AMH_Risk_Test';
        groupO.Name = 'AMH - Risk';
        insert groupO;
        
        GroupMember groupMemberO = new GroupMember();
        groupMemberO.GroupId = groupO.Id;
        groupMemberO.UserOrGroupId = u.Id;
        insert groupMemberO;
        
        
        
        System.runAs(u){
            
            Contact c = new Contact();
            c.Email = 'admin.c@pwc.com';
            c.LastName = 'PwcAdmin';
            insert c;
            Case objCS = new Case();
            objCS.Subject='test';
            objCS.Description = 'test';
            objCS.ATG_BU_From_Case__c = 'Al Mosafer';
            objCS.Origin = 'Web Contact Us';
            objCS.ATG_Team__c = null;
            objCS.OwnerId = UserInfo.getUserId();
            objCS.ContactId = c.Id;
            objCS.ATG_Email_Message_From_Address__c = 'admin.c@pwc.com';
            insert objCS;
            
             ATG_Queue_Team_Configuration__c qTeamC = new ATG_Queue_Team_Configuration__c();
        qTeamC.Name = 'AMH - Risk';
        qTeamC.ATG_Team_Name__c = 'Risk Management';
        insert qTeamC;
            
            list<Case> soNewList = new list<Case>();
            soNewList.add(objCS);
            
            objCS.OwnerId = u.Id;
            update objCS;
            
            map<id,Case> soOldMap = new map<id,Case>();
            soOldMap.put(objCS.Id,objCS);
            
            ATG_CaseHandler objClass = new ATG_CaseHandler();
            objClass.beforeInsert (soNewList);
            objClass.beforeUpdate (soNewList,soOldMap);
            objClass.beforeDelete (soNewList);
            objClass.afterInsert (soNewList);
            objClass.afterUpdate (soNewList,soOldMap);
            objClass.afterDelete (soNewList);
        }
    }
    
    static testMethod void ATG_CaseHandler5(){
        
         
        
        Profile p = [SELECT Id FROM Profile WHERE Name='System Administrator']; 
        User u = new User(Alias = 'admint', Email='adminuser@testorg.com', 
            EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
            LocaleSidKey='en_US', ProfileId = p.Id, 
            TimeZoneSidKey='America/Los_Angeles', UserName='adminuser@testorg1.com');
        
       insert u;
        
        Group groupO= new Group();
        groupO.Type = 'Queue';
        groupO.DeveloperName = 'AMH_Risk_Test';
        groupO.Name = 'AMH - Risk';
        insert groupO;
        
        GroupMember groupMemberO = new GroupMember();
        groupMemberO.GroupId = groupO.Id;
        groupMemberO.UserOrGroupId = u.Id;
        insert groupMemberO;
        
        
        
        System.runAs(u){
            Case objCS = new Case();
            objCS.Subject='test';
            objCS.Description = 'test';
            objCS.ATG_BU_From_Case__c = 'Al Mosafer';
            objCS.Origin = 'Web Contact Us';
            objCS.ATG_Team__c = 'On Queue';
            objCS.OwnerId = UserInfo.getUserId();
            insert objCS;
            
             ATG_Queue_Team_Configuration__c qTeamC = new ATG_Queue_Team_Configuration__c();
        qTeamC.Name = 'AMH - Risk';
        qTeamC.ATG_Team_Name__c = 'Risk';
        insert qTeamC;
            
            list<Case> soNewList = new list<Case>();
            soNewList.add(objCS);
            
            objCS.OwnerId = u.Id;
            update objCS;
            
            map<id,Case> soOldMap = new map<id,Case>();
            soOldMap.put(objCS.Id,objCS);
            
            ATG_CaseHandler objClass = new ATG_CaseHandler();
            objClass.beforeInsert (soNewList);
            objClass.beforeUpdate (soNewList,soOldMap);
            objClass.beforeDelete (soNewList);
            objClass.afterInsert (soNewList);
            objClass.afterUpdate (soNewList,soOldMap);
            objClass.afterDelete (soNewList);
        }
    }
    
    static testMethod void searchCustomerTestCase() {
        

        ATG_TestUtility objTest = new ATG_TestUtility();
        
        Account objAccount = objTest.createLeisureAccount();
        list<Case> lstCases = new list<Case>();
        
        Case bookingCase = new case(Status = 'New',
                            recordtypeid = ATG_ConstantsUtility.Case_booking_RECORDTYPEID,
                            ATG_Product_Type__c = 'Flight',
                            Origin = 'Phone',
                            ATG_BU_From_Case__c = 'Al Tayyar',
                            SuppliedEmail = objAccount.PersonEmail,
                            Type_of_Enquiry__c = 'General Enquiry',
                            ownerID = userinfo.getuserID());
        insert bookingCase;
        lstCases.add(bookingCase);
        
        Case bookingCase2 = new case(Status = 'New',
                            recordtypeid = ATG_ConstantsUtility.Case_booking_RECORDTYPEID,
                            ATG_Product_Type__c = 'Flight',
                            Origin = 'Phone',
                            Type_of_Enquiry__c = 'New Booking',
                            ATG_BU_From_Case__c = 'Al Tayyar',
                            SuppliedPhone = objAccount.PersonMobilePhone,
                            ownerID = userinfo.getuserID());
        insert bookingCase2;
        lstCases.add(bookingCase2);
        
        test.startTest();
        Case bookingCase3 = new case(Status = 'New',
                            recordtypeid = ATG_ConstantsUtility.Case_booking_RECORDTYPEID,
                            ATG_Product_Type__c = 'Flight',
                            Origin = 'Phone',
                            ATG_BU_From_Case__c = 'Al Tayyar',
                            SuppliedEmail = 'altayyar@pwc.com',
                            SuppliedName = 'Test Case',
                            suppliedPhone = '+971123456789',
                            Type_of_Enquiry__c = 'Cancellation',
                            ownerID = userinfo.getuserID());
        insert bookingCase3;
        lstCases.add(bookingCase3);
        ATG_CaseHandler objCasehandler = new ATG_CaseHandler();
        objCasehandler.updateRecordTypeID(lstCases, true, null);
        
        lstCases[0].Type_of_Enquiry__c = 'Amendment';
        lstCases[1].Type_of_Enquiry__c = 'Complaint';
        objCasehandler.updateRecordTypeID(lstCases, true, null);
        
        test.stopTest();       
    }
    
    static testMethod void CoETestCase1() {
        

        ATG_TestUtility objTest = new ATG_TestUtility();
        
        Account objAccount = new Account();
        objAccount.Name = 'Test Vendor';
        objAccount.RecordTypeId = ATG_ConstantsUtility.Account_CoE_Air_Vendor_RecordType;
        objAccount.Type = 'Contracted';
        insert objAccount;
        
        list<Case> lstCases = new list<Case>();
        
        Case objCase = new case(Status = 'New',
                            recordtypeid = ATG_ConstantsUtility.SOURCING_VENDOR_ACTIVATION_RECORDTYPE,
                            Origin = 'Phone',
                            AccountId = objAccount.Id,
                            ownerID = userinfo.getuserID());
        lstCases.add(objCase);
		insert lstCases;
        
        Task objTask = new Task();
        objTask.Status = 'Open';
        objTask.Subject = 'Test';
        objTask.WhatId = lstCases[0].Id;
        objTask.ActivityDate = System.today().addDays(30);
        insert objTask;
        
        lstCases[0].Status = ATG_ConstantsUtility.STR_CLOSED;
        try{
        	update lstCases[0];
        }catch(Exception ex){}
        
        ContentVersion objCV = new Contentversion(); 
        objCV.Title = 'CZDSTOU'; 
        objCV.PathOnClient = 'test'; 
        objCV.VersionData = EncodingUtil.base64Decode('Unit Test Attachment Body'); 
        List<ContentVersion> lstCV = new List<ContentVersion>(); 
        lstCV.add(objCV); 
        insert lstCV;
        
        list<ContentDocumentLink> lstCDL = new list<ContentDocumentLink>();
        for(ContentVersion obj : [select ContentDocumentId from ContentVersion where Id IN : lstCV]){
            lstCDL.add(
            	new ContentDocumentLink(
                	ContentDocumentId = obj.ContentDocumentId,
                    LinkedEntityId = lstCases[0].Id,
                    ShareType = 'I'
                )
            );
        }
        insert lstCDL;
        
        lstCases[0].Status = ATG_ConstantsUtility.STR_CREATE_IN_SAP;
        try{
        	update lstCases[0];
        }catch(Exception ex){}
        
        lstCases[0].Status = ATG_ConstantsUtility.STR_CLOSED;
        try{
        	update lstCases[0];
        }catch(Exception ex){}
        
        lstCases[0].Status = ATG_ConstantsUtility.STR_CANCELLED;
        update lstCases[0];
        
        
        test.startTest();
        
        	ATG_CaseHandler objCasehandler = new ATG_CaseHandler();
                
        test.stopTest();       
    }
    
    static testMethod void CoETestCase2() {
        

        ATG_TestUtility objTest = new ATG_TestUtility();
        
        Account objAccount = new Account();
        objAccount.Name = 'Test Vendor';
        objAccount.RecordTypeId = ATG_ConstantsUtility.Account_CoE_Air_Vendor_RecordType;
        objAccount.Type = 'Contracted';
        insert objAccount;
        
        list<Case> lstCases = new list<Case>();
        
        Case objCase = new case(Status = 'New',
                            recordtypeid = ATG_ConstantsUtility.Case_Contract_Generic_RecordType,
                            Origin = 'Phone',
                            AccountId = objAccount.Id,
                            ownerID = userinfo.getuserID());
        lstCases.add(objCase);
		insert lstCases;
        
        ContentVersion objCV = new Contentversion(); 
        objCV.Title = 'CZDSTOU'; 
        objCV.PathOnClient = 'test'; 
        objCV.VersionData = EncodingUtil.base64Decode('Unit Test Attachment Body'); 
        List<ContentVersion> lstCV = new List<ContentVersion>(); 
        lstCV.add(objCV); 
        insert lstCV;
        
        list<ContentDocumentLink> lstCDL = new list<ContentDocumentLink>();
        for(ContentVersion obj : [select ContentDocumentId from ContentVersion where Id IN : lstCV]){
            lstCDL.add(
            	new ContentDocumentLink(
                	ContentDocumentId = obj.ContentDocumentId,
                    LinkedEntityId = lstCases[0].Id,
                    ShareType = 'I'
                )
            );
        }
        insert lstCDL;
        
        Case objChildCase = new case(Status = 'New',
                            recordtypeid = ATG_ConstantsUtility.Case_Contract_Generic_RecordType,
                            Origin = 'Phone',
                            AccountId = objAccount.Id,
                            ownerID = userinfo.getuserID(),
                            ParentId = lstCases[0].Id
             );
        insert objChildCase;
        
        lstCases[0].Status = ATG_ConstantsUtility.STR_CLOSED;
        try{
        	update lstCases[0];
        }catch(Exception ex){}
        
    }
    
}