/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class ATG_QAMonitorHandlerTest {

    static testMethod void myUnitTest() {
        ATG_QAMonitorHandler objClass = new ATG_QAMonitorHandler();
        QA_Monitoring__c objQA =  new QA_Monitoring__c();
        objQA.Agent_Name__c = userinfo.getuserId();
        objQA.ATG_Business_Unit__c = 'Al Tayyar';
        objQA.Status__c = 'Monitoring Completed';
        insert objQA;
        
        objQA.Status__c = 'Coaching Request Sent';
        objQA.Coaching_Time__c = system.now();
        update objQA;
        
        User integrationUser = new User();
        integrationUser = ATG_Utility.integrationUser();
        objQA.Agent_Name__c = integrationUser.id;
        update objQA;
        
        delete objQA;
        
        QA_Monitoring__c objQA1 =  new QA_Monitoring__c();
        objQA1.Agent_Name__c = userinfo.getuserId();
        objQA1.ATG_Business_Unit__c = 'Tajawal';
        objQA1.Status__c = 'Monitoring Completed';
        insert objQA1;
        
        objQA1.Status__c = 'Coaching Completed';
        objQA1.Coaching_Time__c = system.now();
        update objQA1;
        
        objQA1.Agent_Name__c = integrationUser.id;
        update objQA1;
        
        QA_Monitoring__c objQA2 =  new QA_Monitoring__c();
        objQA2.Agent_Name__c = userinfo.getuserId();
        objQA2.ATG_Business_Unit__c = 'Al Mosafer';
        objQA2.Status__c = 'Coaching Completed';
        insert objQA2;
        
        Case objCase = new case(Status = 'New',
                            recordtypeid = ATG_ConstantsUtility.Case_Contract_Generic_RecordType,
                            Origin = 'Phone',
                            ownerID = userinfo.getuserID());
        insert objCase;
        
        objQA2 =  new QA_Monitoring__c();
        objQA2.Agent_Name__c = userinfo.getuserId();
        objQA2.ATG_Business_Unit__c = 'Al Mosafer';
        objQA2.Status__c = 'Coaching Completed';
        objQA2.ATG_Mistakes__c = 'Data';
        objQA2.RecordTypeId = ATG_ConstantsUtility.CLU_Quality_Check_RecordType;
        objQA2.Case_Number__c = objCase.Id;
        insert objQA2;
        
        
    }
}