/**************************************************************************************************
* Name               : ATG_BookingDetailHandler                                                               
* Description        : Trigger Handler for the Booking details SObject. This class implements the ITrigger
                       interface to help ensure the trigger code is bulkified and all in one place.                                       
* Created Date       : 22th september 2018                                                                *
* Created By         : Swati Sehrawat (PWC Digital)                                              
* -------------------------------------------------------------------------------------------------
* Version    Author    Date         Comment                                                                *
  v1.1      Leeba      21/10/2018   Populate Travel order number on booking header
  v1.2      Diana      05/03/2019   Populate Trip Type on booking
  v1.3      Leeba      20/2/2019    Calling the class for calculation in MVP2 Prodcuts
  v1.4      Leeba      30/3/2019    Populating PNR for MVP2
**************************************************************************************************/

public without sharing class ATG_BookingDetailHandler implements ATG_TriggerInterface {

    public void beforeInsert(list<SObject> soNewList){

    }
  
    public void beforeUpdate(list<sobject> soNewList, map<id,sobject> soOldMap){
    }

    public void beforeDelete(list<SObject> soNewList){}

    public void afterInsert(list<SObject> soNewList){
        try{
            //v1.1
            updateBookingTravelOrderNumber(soNewList,null,true);
            updateBookingHeader(soNewList);
            //v1.3
            ATG_BookingDetailController.updateBookingDetails(soNewList);
            
            
            
        }catch(exception ex){ 
            ATG_utility.LogException(ex);
        }
    }
 
    public void afterUpdate(list<SObject> soNewList,map<id,SObject> soOldMap){
        try{
            //v1.1
            updateBookingTravelOrderNumber(soNewList,(map<Id,ATG_Booking_Detail__c>)soOldMap,false);
            //v1.3
             if(!ATG_BookingDetailController.IsRecursive)
                ATG_BookingDetailController.updateBookingDetails(soNewList);
        }catch(exception e){
            ATG_utility.LogException(e);
        }
    }
    
    public void afterDelete(list<SObject> soNewList){}
    
    //------------------------------Method Definition-----------------------------------------//    
    
    public static void updateBookingHeader(List<ATG_Booking_Detail__c> BDNewList){
       
        map<id, list<ATG_Booking_Detail__c>> HeaderToDetailMap = new map<id, list<ATG_Booking_Detail__c>>();
        for(ATG_Booking_Detail__c obj : BDNewList){
            system.debug('-----obj.ATG_Booking__c---'+obj.ATG_Booking__c);
            if(HeaderToDetailMap.containsKey(obj.ATG_Booking__c)){
                List<ATG_Booking_Detail__c> lstTemp = HeaderToDetailMap.get(obj.ATG_Booking__c);
                system.debug('-----1lstTemp---'+lstTemp);
                lstTemp.add(obj);
            }else{
                List<ATG_Booking_Detail__c> lstTemp = new List<ATG_Booking_Detail__c>();
                lstTemp.add(obj);
                HeaderToDetailMap.put(obj.ATG_Booking__c,lstTemp);
            }       
        }   
        
        system.debug('-----HeaderToDetailMap---'+HeaderToDetailMap);
        
        map<id,ATG_Booking__c> existingBooking = new map<id,ATG_Booking__c>();
        if(HeaderToDetailMap!=null && HeaderToDetailMap.size()>0){
            for(ATG_Booking__c obj : [select id, ATG_Travel_End_Date__c, ATG_Product_Type__c, ATG_PNR_Reference_No__c,ATG_Business_Unit__c
                                      from ATG_Booking__c where id IN : HeaderToDetailMap.keyset()]){
                existingBooking.put(obj.id,obj);    
            }
        }
        
        system.debug('-----existingBooking---'+existingBooking);
        if(existingBooking!=null && existingBooking.size()>0){
            for(ATG_Booking__c obj : existingBooking.values()){
                
                system.debug('-----obj1---'+obj);
                
                for(ATG_Booking_Detail__c objBD : HeaderToDetailMap.get(obj.id)){
                    
                    system.debug('-----objBD---'+objBD);
                    if(objBD.ATG_Record_Type_Name__c == 'ATG_Flight'){
                        if(string.isBlank(obj.ATG_PNR_Reference_No__c))
                            obj.ATG_PNR_Reference_No__c = objBD.ATG_PNR__c;
                        else if(string.isNotBlank(obj.ATG_PNR_Reference_No__c) && string.isNotBlank(objBD.ATG_PNR__c) && ! obj.ATG_PNR_Reference_No__c.contains(objBD.ATG_PNR__c))
                            obj.ATG_PNR_Reference_No__c = obj.ATG_PNR_Reference_No__c+ ' ,'+objBD.ATG_PNR__c;
                    }else{
                        if(string.isBlank(obj.ATG_PNR_Reference_No__c))
                            obj.ATG_PNR_Reference_No__c = objBD.ATG_Reference_No__c;
                            
                        else if(string.isNotBlank(obj.ATG_PNR_Reference_No__c) && string.isNotBlank(objBD.ATG_Reference_No__c)  && ! obj.ATG_PNR_Reference_No__c.contains(objBD.ATG_Reference_No__c))
                            obj.ATG_PNR_Reference_No__c = obj.ATG_PNR_Reference_No__c+ ' ,'+objBD.ATG_Reference_No__c;
                    }
                    
                    if(string.isBlank(obj.ATG_Product_Type__c) && obj.ATG_Business_Unit__c==ATG_ConstantsUtility.ALTAYYAR_BusinessUnit)
                        obj.ATG_Product_Type__c = objBD.ATG_Booking_Type__c;
                    else if(string.isNotBlank(obj.ATG_Product_Type__c) && string.isNotBlank(objBD.ATG_Booking_Type__c) && ! obj.ATG_Product_Type__c.contains(objBD.ATG_Booking_Type__c) && obj.ATG_Business_Unit__c==ATG_ConstantsUtility.ALTAYYAR_BusinessUnit)
                        obj.ATG_Product_Type__c = obj.ATG_Product_Type__c +';'+objBD.ATG_Booking_Type__c;
                        
                    
                    if(objBD.ATG_Record_Type_Name__c == 'ATG_Flight' && obj.ATG_Travel_End_Date__c < objBD.ATG_Departure_Date__c){
                        obj.ATG_Travel_End_Date__c = objBD.ATG_Departure_Date__c;
                    }else if(objBD.ATG_Record_Type_Name__c == 'ATG_Hotel' && obj.ATG_Travel_End_Date__c < objBD.ATG_Check_Out__c){
                        obj.ATG_Travel_End_Date__c = Date.valueOf(objBD.ATG_Check_Out__c);
                    }else if(objBD.ATG_Record_Type_Name__c == 'ATG_Sightseeing' && obj.ATG_Travel_End_Date__c < objBD.ATG_Tour_Date__c){
                        obj.ATG_Travel_End_Date__c = objBD.ATG_Tour_Date__c;
                    }else if(obj.ATG_Travel_End_Date__c < objBD.ATG_End_Date__c){
                        obj.ATG_Travel_End_Date__c = objBD.ATG_End_Date__c;
                    }
                    // v1.4 - populatin PNR for MVP2
                    if(objBD.ATG_Record_Type_Name__c == 'ATG_Flight' && (obj.ATG_Business_Unit__c==ATG_ConstantsUtility.ALMOSAFER_BusinessUnit || obj.ATG_Business_Unit__c==ATG_ConstantsUtility.TAJAWAL_BusinessUnit )){
                        obj.ATG_PNR__c = objBD.ATG_PNR__c;
                    }
                    
                     if(objBD.ATG_Booking__c != null && objBD.ATG_Is_Domestic__c){
                        obj.ATG_Is_Domestic__c = objBD.ATG_Is_Domestic__c;
                    }
                    if(objBD.ATG_Booking__c != null && objBD.ATG_Hotel_Name__c != null){
                        obj.ATG_Hotel_Name__c = objBD.ATG_Hotel_Name__c;                    
                    }
                    
                    if(objBD.ATG_Booking__c != null && objBD.ATG_Check_Out__c != null){
                        obj.ATG_Check_out_date__c = objBD.ATG_Check_Out__c;
                    }
                
                    if(objBD.ATG_Booking__c != null && objBD.ATG_Trip_Type__c != null){
                       obj.ATG_Trip_Type__c = objBD.ATG_Trip_Type__c;
                    }
                    
                    if(objBD.ATG_Booking__c != null && objBD.ATG_AHS_Group_Name__c != null){
                       obj.ATG_AHS_Group_Name__c = objBD.ATG_AHS_Group_Name__c;
                    }
                }   
                
                system.debug('-----obj2---'+obj);
            }   
            
            update existingBooking.values();
        }
    }
    
    // v1.1 - Method to Update Travel Order Number on Booking Header
    public static void updateBookingTravelOrderNumber(List<ATG_Booking_Detail__c> BDNewList,
                                                      map<Id,ATG_Booking_Detail__c> soOldMap,Boolean IsInsert){
        map<string,ATG_Booking__c> MapBookingTBU = new map<string,ATG_Booking__c>();
        for(ATG_Booking_Detail__c obj : BDNewList){
            system.debug('-----obj.ATG_Booking__c---'+obj.ATG_Booking__c);
            
            if((obj.ATG_Travel_Order_Number__c!=null && obj.ATG_Booking__c!=null) && 
               (IsInsert || (soOldMap.get(obj.Id).ATG_Travel_Order_Number__c != obj.ATG_Travel_Order_Number__c))){
                MapBookingTBU.put(obj.ATG_Booking__c,new ATG_Booking__c(Id=obj.ATG_Booking__c,ATG_Travel_Order_Number__c=obj.ATG_Travel_Order_Number__c));
            }
            if(obj.ATG_sys_Travel_End_Date__c != null && obj.ATG_Booking__c != null &&
               (IsInsert || (soOldMap.get(obj.Id).ATG_sys_Travel_End_Date__c != obj.ATG_sys_Travel_End_Date__c))){
                    MapBookingTBU.put(obj.ATG_Booking__c,new ATG_Booking__c(Id=obj.ATG_Booking__c,
                                                                            ATG_Travel_End_Date__C=obj.ATG_sys_Travel_End_Date__c.date()));
            
               }
        }   
        if(MapBookingTBU.size()>0)
            update MapBookingTBU.values();
    
    }
    
     
}