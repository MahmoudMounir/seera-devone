/***********************************************************************************************************************
* Name               : ATG_ConstantsUtility                                                                            *                                                               
* Description        : Class to handle any constant values/labels/variables to be used in apex                         *                                       
* Created Date       : 19th June 2018                                                                                  *
* Created By         : Swati Sehrawat (PWC Digital)                                                                    *                                              
* ---------------------------------------------------------------------------------------------------------------------*
* Version       Author         Date                 Comment                                                            *       
* 1.0           Paramasivan    October,15/2018      Added additional constants.                                        *  
* 1.1           Leeba          Dec 9 2018           Added Constants for Almosafer and Tajawal Person accounts.         *                                     
* 1.2           Diana          Jan 8 2019           Added Constants for Almosafer and Tajawal Case Record Type.        *
* MVP2 1.3      Sravan         Feb 6 2018           Added a Constant value to controll Account linkage on the Case.    * 
* 1.4           Diana          Feb 10 2019          Added a constant for App Contact Us Origin.                        *
***********************************************************************************************************************/

public with sharing class ATG_ConstantsUtility {
    
    
    public static final Id PERSON_ACCOUNT_RECORDTYPEID = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('PersonAccount').getRecordTypeId();
    //1.1
    public static final Id AMH_PERSON_ACCOUNT_RECORDTYPEID = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('AMH_Person_Account').getRecordTypeId();//Schema.SObjectType.Account.getRecordTypeInfosByName().get('AMH - Person Account').getRecordTypeId();
    public static final Id TJW_PERSON_ACCOUNT_RECORDTYPEID = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('TJW_Person_Account').getRecordTypeId();//Schema.SObjectType.Account.getRecordTypeInfosByName().get('TJW - Person Account').getRecordTypeId();
    public static final Id Master_Person_Account_RECORDTYPEID = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('ATG_Master_Person_Account').getRecordTypeId();
            
    public static final Id BUSINESS_ACCOUNT_RECORDTYPEID =  Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('Business_Account').getRecordTypeId();
    //public static final Id MASTER_PERSON_ACCOUNT_RECORDTYPEID = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Master Person Account').getRecordTypeId();
    public static final Id MASTER_BUSINESS_ACCOUNT_RECORDTYPEID =  Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('ATG_Master_Business_Account').getRecordTypeId();
    public static final Id MASTER_City_RECORDTYPEID =  Schema.SObjectType.ATG_Master__c.getRecordTypeInfosByDeveloperName().get('Cities').getRecordTypeId();
    public static final Id MASTER_AirportNames_RECORDTYPEID =  Schema.SObjectType.ATG_Master__c.getRecordTypeInfosByDeveloperName().get('Airport_Names').getRecordTypeId();
    public static final Id Case_Generic_RECORDTYPEID =  Schema.SObjectType.Case.getRecordTypeInfosByDeveloperName().get('ATG_Generic').getRecordTypeId();
    public static final Id Case_booking_RECORDTYPEID =  Schema.SObjectType.Case.getRecordTypeInfosByDeveloperName().get('ATG_Booking_Enquiry').getRecordTypeId();
    public static final Id Case_Amendment_RECORDTYPEID =  Schema.SObjectType.Case.getRecordTypeInfosByDeveloperName().get('ATG_Amendment').getRecordTypeId();
    public static final Id Case_Cancellation_RECORDTYPEID =  Schema.SObjectType.Case.getRecordTypeInfosByDeveloperName().get('ATG_Cancellation').getRecordTypeId();
    public static final Id Case_Complaint_RECORDTYPEID =  Schema.SObjectType.Case.getRecordTypeInfosByDeveloperName().get('ATG_Complaint').getRecordTypeId();    
    public static final String KEY_SPLITTER = ':';
    public static boolean runOnceForAccount = false;

    public static final Id ATG_CASE_ETO_RECORDTYPE = Schema.SObjectType.Case.getRecordTypeInfosByDeveloperName().get('ATG_Electronic_Travel_Order').getRecordTypeId();
    public static final Id ATG_MASTER_Program_RT = Schema.SObjectType.ATG_Master__c.getRecordTypeInfosByDeveloperName().get('ATG_Program').getRecordTypeId();
    public static final Id ATG_MASTER_Department_RT = Schema.SObjectType.ATG_Master__c.getRecordTypeInfosByDeveloperName().get('ATG_Department').getRecordTypeId();
    public static final Id ATG_MASTER_CCGR_RT = Schema.SObjectType.ATG_Master__c.getRecordTypeInfosByDeveloperName().get('CCGR_CC').getRecordTypeId();
    public static final Id RequestedTraveller_Employee_RT = Schema.SObjectType.ATG_Requested_Traveller__c.getRecordTypeInfosByDeveloperName().get('Employee').getRecordTypeId();
    public static final Id RequestedTraveller_Dependant_RT = Schema.SObjectType.ATG_Requested_Traveller__c.getRecordTypeInfosByDeveloperName().get('Dependant').getRecordTypeId();
    public static final Id RequestedTraveller_Visitor_RT = Schema.SObjectType.ATG_Requested_Traveller__c.getRecordTypeInfosByDeveloperName().get('Visitor').getRecordTypeId();
    
    public static final Id DAILING_CODE_RECORDTYPEID = Schema.SObjectType.ATG_Master__c.getRecordTypeInfosByDeveloperName().get('ATG_Dial_Code').getRecordTypeId();
    
    private static string strKey = 'U8907654HG0765835671KLM67BTSRA12';
    public static final Blob cryptoKey = Blob.valueOf(ATG_ConstantsUtility.strKey);
    
     public static final Id TJW_CASE_RECORDTYPE= Schema.SObjectType.Case.getRecordTypeInfosByDeveloperName().get('TJW_Generic').getRecordTypeId();
               
    public static final Id AMH_CASE_RECORDTYPE = Schema.SObjectType.Case.getRecordTypeInfosByDeveloperName().get('AMH_Generic').getRecordTypeId();
                
    /* Busines Units */
    public static final String ALTAYYAR_BusinessUnit = 'Al Tayyar';
    public static final String TAJAWAL_BusinessUnit = 'Tajawal';
    public static final String ALMOSAFER_BusinessUnit = 'Al Mosafer';
    
    public static  boolean CREATE_ACCOUNTONCASECREATE = true;     //MVP 2 V1.
    
    public static final String ORIGIN_WEB_CONTACT_US = 'Web Contact Us';
    public static final String ORIGIN_APP_CONTACT_US = 'App Contact Us';//1.4
    public static final String ORIGIN_EMAIL = 'Email';
    
    public static final String ATG_Genral_Enquiry_Record_Type = Schema.SObjectType.Case.getRecordTypeInfosByDeveloperName().get('ATG_Generic').getRecordTypeId();
    public static final String ATG_Booking_Enquiry_Record_Type = Schema.SObjectType.Case.getRecordTypeInfosByDeveloperName().get('ATG_Booking_Enquiry').getRecordTypeId();
    public static final String ATG_Cancellation_Record_Type = Schema.SObjectType.Case.getRecordTypeInfosByDeveloperName().get('ATG_Cancellation').getRecordTypeId();
    public static final String ATG_Amendment_Record_Type= Schema.SObjectType.Case.getRecordTypeInfosByDeveloperName().get('ATG_Amendment').getRecordTypeId();
    public static final String ATG_Complaint_Record_Type = Schema.SObjectType.Case.getRecordTypeInfosByDeveloperName().get('ATG_Complaint').getRecordTypeId();
        
    
    public static final string INTEGRATION_USERNAME = 'integration.user@altayyar';
    public static final string MASTER_GOLD_ID = '000000000';
    public static final map<string, string> MAP_SHORT_BU = new map<string, string>{
        'Al Tayyar' => 'ATS', 'Tajawal' => 'TJW', 'Al Mosafer' => 'ALM', 'Master'=>''
    };
    public static final map<string, string> MAP_MESSAGE_TYPES = new map<string, string>{
        'POST' => 'Post', 'COMMENT' => 'Comment', 'PRIVATE_MESSAGE' => 'Private Message', 
        'RETWEET'=>'Retweet', 'REPLY' => 'Reply', 'DIRECT' => 'Direct Message', 'TWEET' => 'Tweet'
    };
    public static final string STR_MASTER = 'Master';
    public static final string STR_ABANDONED_CART = 'Abandoned Cart';
    public static final string STR_IN_PROGRESS = 'In Progress';
    public static final string STR_Guest = 'Guest';
    public static final string STR_CLOSED = 'Closed';
    public static final string STR_CANCELLED = 'Canceled'; 
    public static final string STR_ACTIVE = 'Active'; 
    public static final string STR_INACTIVE = 'Inactive'; 

    public static final String AMH_LIVECHAT_RECORDTYPE = Schema.SObjectType.LiveChatTranscript.getRecordTypeInfosByDeveloperName().get('AMH_Live_Chat_Transcript').getRecordTypeId(); 
    public static final String TJW_LIVECHAT_RECORDTYPE = Schema.SObjectType.LiveChatTranscript.getRecordTypeInfosByDeveloperName().get('TJW_Live_Chat_Transcript').getRecordTypeId();
    public static final String ATG_LIVECHAT_RECORDTYPE = Schema.SObjectType.LiveChatTranscript.getRecordTypeInfosByDeveloperName().get('ATG_Live_Chat_Transcript').getRecordTypeId();

    /* CLU/Sourcing Related */
    public static final String CLU_Quality_Check_RecordType = Schema.SObjectType.QA_Monitoring__c.getRecordTypeInfosByDeveloperName().get('CLU_Quality_Check').getRecordTypeId();
    public static final String Case_Contract_Stop_Sale_RecordType = Schema.SObjectType.Case.getRecordTypeInfosByDeveloperName().get('CLU_Stop_Sale').getRecordTypeId();
    public static final String Account_CLU_Person_Account_RecordType = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('COE_Person_Account').getRecordTypeId();
    public static final String Account_CoE_Air_Vendor_RecordType = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('Air_Vendor').getRecordTypeId();
    public static final String Case_Contract_Generic_RecordType = Schema.SObjectType.Case.getRecordTypeInfosByDeveloperName().get('CLU_Generic').getRecordTypeId();
	public static final String SOURCING_VENDOR_ACTIVATION_RECORDTYPE = Schema.SObjectType.Case.getRecordTypeInfosByDeveloperName().get('Sourcing_Vendor_Activation').getRecordTypeId();
	public static final String SOURCING_VENDOR_DEACTIVATION_RECORDTYPE = Schema.SObjectType.Case.getRecordTypeInfosByDeveloperName().get('Sourcing_Vendor_Deactivation').getRecordTypeId();
    public static final String SOURCING_CONTRACT_REQUEST_RECORDTYPE = Schema.SObjectType.Case.getRecordTypeInfosByDeveloperName().get('Sourcing_Contract_Request').getRecordTypeId();
    public static final String SOURCING_REQUEST_RECORDTYPE = Schema.SObjectType.Case.getRecordTypeInfosByDeveloperName().get('Sourcing_Request').getRecordTypeId();
    public static final string STR_CREATE_IN_SAP = 'Create in SAP';
    public static final string STR_Contracted = 'Contracted';
    public static final string STR_Activated = 'Activated';
    public static final string STR_Live = 'Live';
    public static final string STR_Update_Relavent_Teams = 'Updating Relevant Teams';
    /*End of CLU/Sourcing Related */
}// End of class.