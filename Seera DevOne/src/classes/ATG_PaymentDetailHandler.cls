/**************************************************************************************************
* Name               : ATG_PaymentDetailHandler                                                               
* Description        : Trigger Handler for the Payment details SObject. This class implements the ITrigger
                       interface to help ensure the trigger code is bulkified and all in one place.                                       
* Created Date       : 20th september 2018                                                                *
* Created By         : Swati Sehrawat (PWC Digital)                                              
* -------------------------------------------------------------------------------------------------
* Version   Author      Date            Comment                                                                *
v1.1        Leeba       9-Oct-2018      Update the Booking record with case based on Tapro Reference numbers in Payment detail
MVP2 V1.2   Diana       27-Feb-2019     If Payment comes in and the payment mode is bank transfer with attahcment or cash
                                        then change the case status to in progress and assign the owner
MVP2 V1.3   Ravi        17-Mar-2019     Added the logic to link the customer for orphan payments
**************************************************************************************************/

public without sharing class ATG_PaymentDetailHandler implements ATG_TriggerInterface {

    public void beforeInsert(list<SObject> soNewList){
    	searchExistingCustomer( (list<Payment_Details__c>)soNewList );
    }
  
    public void beforeUpdate(list<sobject> soNewList, map<id,sobject> soOldMap){
        //v1.1
        updateBookingbasedOnPayment((List<Payment_Details__c>)soNewList,(map<Id,Payment_Details__c>)soOldMap,false);
    }

    public void beforeDelete(list<SObject> soNewList){}

    public void afterInsert(list<SObject> soNewList){
        try{
            
            updateCaseBasedOnPayment(soNewList,null,true);
            UpdateCaseBasedOnPaymentMode(soNewList, null, true);
        }catch(exception ex){ 
            ATG_utility.LogException(ex);
        }
    }

    public void afterUpdate(list<SObject> soNewList,map<id,SObject> soOldMap){
         try{
            list<Payment_Details__c> TriggerNew = (List<Payment_Details__c>)soNewList;
            map<Id,Payment_Details__c> TriggerOldMap = (map<Id,Payment_Details__c>)soOldMap;
            updateCaseBasedOnPayment(TriggerNew,TriggerOldMap,false);
            UpdateCaseBasedOnPaymentMode(TriggerNew, TriggerOldMap, false);
            
            
            
        }catch(exception ex){
            ATG_utility.LogException(ex);
        }
    }
    
    public void afterDelete(list<SObject> soNewList){}
  
    //------------------------------Method Definition-----------------------------------------//    
    
    public static void updateCaseBasedOnPayment(List<Payment_Details__c> paymentNewList, map<id,Payment_Details__c> paymentOldMap, boolean isInsert){
        map<id,Payment_Details__c> updatedPayments = new map<id,Payment_Details__c>();
        set<id> casesLinkedWithPayment = new set<id>();
        for(Payment_Details__c obj : paymentNewList){
            if(isInsert == true && obj.ATG_Status__c !=null && string.isNotBlank(obj.ATG_Status__c)){
                updatedPayments.put(obj.id,obj);
                casesLinkedWithPayment.add(obj.ATG_Case__c);
            }else if(isInsert == false && obj.ATG_Status__c !=null && string.isNotBlank(obj.ATG_Status__c) &&
                  obj.ATG_Status__c!=paymentOldMap.get(obj.id).ATG_Status__c){
                updatedPayments.put(obj.id,obj);
                casesLinkedWithPayment.add(obj.ATG_Case__c);
            }
        }   
        
        system.debug('---updatedPayments----'+updatedPayments);
        system.debug('---casesLinkedWithPayment----'+casesLinkedWithPayment);
        
        map<Case,list<Payment_Details__c>> caseToPaymentMap = new map<Case,list<Payment_Details__c>>();
        if(updatedPayments!=null && updatedPayments.size()>0 && casesLinkedWithPayment!=null && casesLinkedWithPayment.size()>0){
            for(Case objCase : [select id, ownerID, status,Reason, Follow_up_Date__c, ATG_Previous_Owner__c, 
                                Account.isPersonAccount,
                                (select id,ATG_Status__c from Payment_Details__r where id IN : updatedPayments.keyset()) 
                                from Case where id IN : casesLinkedWithPayment AND 
                                ATG_BU_FROM_Case__c =: ATG_ConstantsUtility.ALTAYYAR_BusinessUnit]){
                
                caseToPaymentMap.put(objCase,objCase.Payment_Details__r);   
            }       
        }
        
        system.debug('---caseToPaymentMap----'+caseToPaymentMap);
        
        set<Case> caseToUpdate = new set<Case>();       
        if(caseToPaymentMap!=null && caseToPaymentMap.size()>0){
            set<string> queueName = new set<string>();
            queueName.add('ATG_Validation_Team');
            
            map<string, string> queueMap = new map<string, string>();
            queueMap = ATG_utility.getQueueIdFromName(queueName);
            
            system.debug('---queueMap----'+queueMap);
            
            for(Case caseObj : caseToPaymentMap.keyset()){
                for(Payment_Details__c objPD : caseToPaymentMap.get(caseObj)){
                    system.debug('---objPD.ATG_Status__c----'+objPD.ATG_Status__c);
                    
                    if(objPD.ATG_Status__c == 'Payment Pending'){
                        caseObj.status = 'Pending - Customer';  
                        break;
                    }   
                    
                    if(objPD.ATG_Status__c == 'Payment Received'){
                        caseObj.OwnerID = queueMap.get('ATG_Validation_Team');  
                        caseObj.status = 'Awaiting Verification';
                    }
                    
                    if(objPD.ATG_Status__c == 'Completed'){
                        caseObj.status = 'Closed';
                        caseObj.Reason = 'Booking Confirmed';
                    }
                    
                    if(objPD.ATG_Status__c == 'Payment Verified'){
                        caseObj.status = 'Payment Verified';
                        
                        if(caseObj.Account.isPersonAccount == false && caseObj.ATG_Previous_Owner__c !=null)
                            caseObj.OwnerID = caseObj.ATG_Previous_Owner__c;
                    }   
                                        
                    if(objPD.ATG_Status__c == 'Canceled' || objPD.ATG_Status__c == 'Expired'|| objPD.ATG_Status__c == 'Rejected'){
                        //caseObj.Follow_up_Date__c = system.today();
                        caseObj.status = 'Unsuccessful Transaction';
                    }
                }
                caseToUpdate.add(caseObj);
            }
        }
        
        system.debug('---caseToUpdate----'+caseToUpdate);
        
        if(caseToUpdate!=null && caseToUpdate.size()>0){
            list<Case> caseList = new list<Case>(caseToUpdate);
            system.debug('---caseList----'+caseList);
            update caseList;
        }
        
        
    }
    
    /*
        v1.1
        Method to Populate Case on Booking Based on the Booking number in Payment
    */
    public static void updateBookingbasedOnPayment(List<Payment_Details__c> paymentNewList, map<Id,Payment_Details__c> paymentOldMap, boolean isInsert){
        map<string,string> MapCaseByReference = new map<string,string>();
        map<string,string> MapBookingIds = new map<string,string>();
        set<string> setReferences = new set<string>();
        for(Payment_Details__c pd:paymentNewList){
            if(pd.ATG_Tapro_Flight_Reference_Number__c!=null)
                setReferences.add(pd.ATG_Tapro_Flight_Reference_Number__c);
            if(pd.ATG_Tapro_Hotel_Reference_Number__c!=null)
                setReferences.add(pd.ATG_Tapro_Hotel_Reference_Number__c);
            if(pd.ATG_Other_Sales_Tapro_Reference_Number__c!=null)
                setReferences.add(pd.ATG_Other_Sales_Tapro_Reference_Number__c);
                
                if(pd.ATG_Case__c!=null && paymentOldMap!=null && paymentOldMap.size()>0 && paymentOldMap.get(pd.Id)!=null){
                    if(paymentOldMap.get(pd.Id).ATG_Tapro_Flight_Reference_Number__c!=null && paymentOldMap.get(pd.Id).ATG_Tapro_Flight_Reference_Number__c!=pd.ATG_Tapro_Flight_Reference_Number__c && pd.ATG_Tapro_Flight_Reference_Number__c!=null)
                        MapCaseByReference.put(pd.ATG_Tapro_Flight_Reference_Number__c,pd.ATG_Case__c);
                    If(paymentOldMap.get(pd.Id).ATG_Tapro_Hotel_Reference_Number__c!=null && paymentOldMap.get(pd.Id).ATG_Tapro_Hotel_Reference_Number__c!=pd.ATG_Tapro_Hotel_Reference_Number__c && pd.ATG_Tapro_Hotel_Reference_Number__c!=null)
                        MapCaseByReference.put(pd.ATG_Tapro_Hotel_Reference_Number__c,pd.ATG_Case__c);
                    if(paymentOldMap.get(pd.Id).ATG_Other_Sales_Tapro_Reference_Number__c!=null && paymentOldMap.get(pd.Id).ATG_Other_Sales_Tapro_Reference_Number__c!=pd.ATG_Other_Sales_Tapro_Reference_Number__c && pd.ATG_Other_Sales_Tapro_Reference_Number__c!=null)
                        MapCaseByReference.put(pd.ATG_Other_Sales_Tapro_Reference_Number__c,pd.ATG_Case__c);
                }
        }
        if(setReferences!=null && setReferences.size()>0){
            for(ATG_Booking__c Bk:[Select Id,Name,ATG_Case__c from ATG_Booking__c where Name IN:setReferences]){
                MapBookingIds.put(Bk.Name,Bk.Id);
            }
        }
        if(MapCaseByReference.size()>0){
            list<ATG_Booking__c> lstBookingTBU = new list<ATG_Booking__c>();
            for(ATG_Booking__c Bk:[Select Id,Name,ATG_Case__c from ATG_Booking__c where Name IN:MapCaseByReference.keyset()]){
                if(MapCaseByReference.get(Bk.Name)!=null){
                    Bk.ATG_Case__c = MapCaseByReference.get(Bk.Name);
                    lstBookingTBU.add(Bk);
                }
            }
            if(lstBookingTBU.size()>0)
                update lstBookingTBU;
        }
        for(Payment_Details__c pd:paymentNewList){
            if(pd.ATG_Tapro_Flight_Reference_Number__c!=null && MapBookingIds.get(pd.ATG_Tapro_Flight_Reference_Number__c)!=null)
                pd.ATG_Tapro_Flight_Reference_Lookup__c = MapBookingIds.get(pd.ATG_Tapro_Flight_Reference_Number__c);
            if(pd.ATG_Tapro_Hotel_Reference_Number__c!=null && MapBookingIds.get(pd.ATG_Tapro_Hotel_Reference_Number__c)!=null)
                pd.ATG_Tapro_Hotel_Reference_Lookup__c = MapBookingIds.get(pd.ATG_Tapro_Hotel_Reference_Number__c);
            if(pd.ATG_Other_Sales_Tapro_Reference_Number__c!=null && MapBookingIds.get(pd.ATG_Other_Sales_Tapro_Reference_Number__c)!=null)
                pd.ATG_Tapro_Other_Sales_Reference_Lookup__c = MapBookingIds.get(pd.ATG_Other_Sales_Tapro_Reference_Number__c);
        }
    }
    
     //MVP2 1.2 
    // Update the case status and case owner id based on payment mode 
    private static void UpdateCaseBasedOnPaymentMode(List<Payment_Details__c> paymentNewList, map<Id,Payment_Details__c> paymentOldMap,Boolean isInsert){

        //iterate over payment and get all the cases linked from the payment
        map<id,Payment_Details__c> updatedPayments = new map<id,Payment_Details__c>();
        set<id> casesLinkedWithPayment = new set<id>();
         for(Payment_Details__c obj : paymentNewList){
            if(isInsert == true && obj.ATG_Status__c !=null && string.isNotBlank(obj.ATG_Status__c)
            && (string.isNotBlank(obj.ATG_Payment_Mode__c) && (obj.ATG_Payment_Mode__c == 'Cash')
            )){
                updatedPayments.put(obj.id,obj);
                casesLinkedWithPayment.add(obj.ATG_Case__c);
            }else if(isInsert == false && obj.ATG_Status__c !=null &&
             string.isNotBlank(obj.ATG_Status__c) &&
                 (obj.ATG_Payment_Mode__c == 'Cash') 
                  && obj.ATG_Payment_Mode__c != paymentOldMap.get(obj.id).ATG_Payment_Mode__c){
                updatedPayments.put(obj.id,obj);
                casesLinkedWithPayment.add(obj.ATG_Case__c);
            }
        }   
        
        system.debug('---updatedPayments----'+updatedPayments);
        system.debug('---casesLinkedWithPayment----'+casesLinkedWithPayment);
        
        map<Case,list<Payment_Details__c>> caseToPaymentMap = new map<Case,list<Payment_Details__c>>();
        if(updatedPayments!=null && updatedPayments.size()>0 && casesLinkedWithPayment!=null && casesLinkedWithPayment.size()>0){
            for(Case objCase : [select id, ownerID, status,Reason, Follow_up_Date__c, ATG_Previous_Owner__c,ATG_should_notify_the_case_owner__c, 
                                Account.isPersonAccount,ATG_Current_Queue_Id__c,
                                (select id,ATG_Status__c,ATG_Payment_Mode__c,ATG_Attachment__c from Payment_Details__r where id IN : updatedPayments.keyset()) 
                                from Case where id IN : casesLinkedWithPayment
                                AND ATG_BU_From_Case__c=:ATG_ConstantsUtility.ALMOSAFER_BusinessUnit]){
                
                caseToPaymentMap.put(objCase,objCase.Payment_Details__r);   
            }       
        }
        
       

        system.debug('---caseToPaymentMap----'+caseToPaymentMap);
        
        set<Case> caseToUpdate = new set<Case>();       
        if(caseToPaymentMap!=null && caseToPaymentMap.size()>0){
           
             //MVP2
            Set<String> setCaseOwnerIds = new Set<String>(); 
            map<string,boolean> MapUserOnlineStatus = new map<string,boolean>();
            
            for(Case objCase:caseToPaymentMap.keySet()){
                setCaseOwnerIds.add(objCase.OwnerId);
                
            }

            if(setCaseOwnerIds.size()>0){
                MapUserOnlineStatus = ATG_utility.getUserPresenceStatus(setCaseOwnerIds);
            }

            Set<String> queueNameSet = new Set<String>();
            queueNameSet.add('AMH_Risk');
            
            Map<String,Id> queueNameMap = new Map<String,Id>();
            queueNameMap = ATG_utility.getQueueIdFromName(queueNameSet);

            for(Case caseObj : caseToPaymentMap.keyset()){
                for(Payment_Details__c objPD : caseToPaymentMap.get(caseObj)){
                   
                    //MVP 2
                    if(objPD.ATG_Payment_Mode__c == 'Cash'){
                        caseObj.Status = 'In Progress';
                        caseObj.ATG_should_notify_the_case_owner__c = true;
                        
                        system.debug('****'+MapUserOnlineStatus.get(caseObj.OwnerId));
                        system.debug('****'+caseObj.ATG_Current_Queue_Id__c);
                        if(string.valueof(caseObj.OwnerId).startswith('005') &&
                         MapUserOnlineStatus.get(caseObj.OwnerId) == null &&
                        caseObj.ATG_Current_Queue_Id__c!= null){
                           caseObj.OwnerId = caseObj.ATG_Current_Queue_Id__c;
                        }
                    }

                }
                caseToUpdate.add(caseObj);
            }
        }
        
        system.debug('---caseToUpdate----'+caseToUpdate);
        
        if(caseToUpdate!=null && caseToUpdate.size()>0){
            list<Case> caseList = new list<Case>(caseToUpdate);
            system.debug('---caseList----'+caseList);
            update caseList;
        }
    }
    
    //v.3
    
    public void searchExistingCustomer(list<Payment_Details__c> lstPaymentDetails){        
        
        map<string,Account> existingAccount = new map<string,Account>();
        set<id> existingBooking = new set<id>();
        set<id> existingCase = new set<id>();
        
		set<string> emailSetTJW = new set<string>();
		set<string> phoneSetTJW = new set<string>();
		
		set<string> emailSetALM = new set<string>();
		set<string> phoneSetALM = new set<string>();
		
		boolean isALMBooking = false;
		boolean isTJWBooking = false;
		
        for(Payment_Details__c objPay : lstPaymentDetails){
        	if(objPay.ATG_Business_Unit__c != ATG_ConstantsUtility.ALTAYYAR_BusinessUnit){
	            if(objPay.ATG_Booking__c != null && objPay.ATG_Customer__c == null)
	                 existingBooking.add(objPay.ATG_Booking__c);
	            
	            if(objPay.ATG_Case__c != null && objPay.ATG_Customer__c == null)
	                existingCase.add(objPay.ATG_Case__c); 
	            
	            if(objPay.ATG_Booking__c == null && objPay.ATG_Case__c == null && objPay.ATG_Customer__c == null){
	            	
	            	if(objPay.ATG_Business_Unit__c == ATG_ConstantsUtility.ALMOSAFER_BusinessUnit){
						isALMBooking = true;
						if(string.isNotBlank(objPay.ATG_Customer_Mobile__c))
							phoneSetALM.add(objPay.ATG_Customer_Mobile__c.right(9));
														
						if(string.isNotBlank(objPay.ATG_Customer_Email__c))
							emailSetALM.add(objPay.ATG_Customer_Email__c);
					}	
					
					if(objPay.ATG_Business_Unit__c == ATG_ConstantsUtility.TAJAWAL_BusinessUnit){
						isTJWBooking = true;
						if(string.isNotBlank(objPay.ATG_Customer_Mobile__c))
							phoneSetTJW.add(objPay.ATG_Customer_Mobile__c.right(9));
													
						if(string.isNotBlank(objPay.ATG_Customer_Email__c))
							emailSetTJW.add(objPay.ATG_Customer_Email__c);
					}
	            }
        	}
        }
        
        if(!existingBooking.isEmpty()){
        	map<id,id> existingCustomerFromBooking = new map<id,id>();
            for(ATG_Booking__c obj : [select id, atg_customer__c from  ATG_Booking__c where id IN : existingBooking]){
            	existingCustomerFromBooking.put(obj.id, obj.atg_customer__c);
            }
            
        	if(existingCustomerFromBooking!=null && existingCustomerFromBooking.size()>0){
        		for(Payment_Details__c objPay : lstPaymentDetails){
        			if(existingCustomerFromBooking.containsKey(objPay.ATG_Booking__c))
        				objPay.atg_customer__c = existingCustomerFromBooking.get(objPay.ATG_Booking__c);
        		}
        	}
        }
        
        
        if(!existingCase.isEmpty()){
        	map<id,id> existingCustomerFromCase = new map<id,id>();
            for(case obj : [select id, AccountID from  case where id IN : existingCase]){
            	existingCustomerFromCase.put(obj.id, obj.AccountID);
            }
            
        	if(existingCustomerFromCase!=null && existingCustomerFromCase.size()>0){
        		for(Payment_Details__c objPay : lstPaymentDetails){
        			if(existingCustomerFromCase.containsKey(objPay.ATG_Booking__c))
        				objPay.atg_customer__c = existingCustomerFromCase.get(objPay.ATG_Booking__c);
        		}
        	}
        }
        
        if(isTJWBooking == true){
			for(Account obj : [select id,PersonEmail,ATG_Business_Unit_Picklist__c,ATG_Mobile_Formula__c from Account 
                               where (PersonEmail IN : emailSetTJW or ATG_Mobile_Formula__c IN : phoneSetTJW)
                       		   and recordTypeID =: ATG_ConstantsUtility.TJW_PERSON_ACCOUNT_RECORDTYPEID ]){
                                
				if(obj.ATG_Mobile_Formula__c!=null) 
					existingAccount.put(obj.ATG_Mobile_Formula__c+'-'+obj.ATG_Business_Unit_Picklist__c, obj); 
					
				if(obj.PersonEmail!=null)
					existingAccount.put(obj.PersonEmail+'-'+obj.ATG_Business_Unit_Picklist__c, obj);
			}   
		}
		
		if(isALMBooking == true){
			for(Account obj : [select id,PersonEmail,ATG_Business_Unit_Picklist__c,ATG_Mobile_Formula__c from Account 
                               where (PersonEmail IN : emailSetALM or ATG_Mobile_Formula__c IN : phoneSetALM)
                       		   and recordTypeID =: ATG_ConstantsUtility.AMH_PERSON_ACCOUNT_RECORDTYPEID ]){
                                
				if(obj.ATG_Mobile_Formula__c!=null) 
					existingAccount.put(obj.ATG_Mobile_Formula__c+'-'+obj.ATG_Business_Unit_Picklist__c, obj); 
					
				if(obj.PersonEmail!=null)
					existingAccount.put(obj.PersonEmail+'-'+obj.ATG_Business_Unit_Picklist__c, obj);
            }   
		}

        if(existingAccount!=null && existingAccount.size()>0){
            for(Payment_Details__c objPay : lstPaymentDetails){ 
                if(objPay.ATG_Booking__c == null && objPay.ATG_Case__c == null && objPay.ATG_Customer__c == null){
                	if(string.isNotBlank(objPay.ATG_Customer_Email__c) && existingAccount.get(objPay.ATG_Customer_Email__c+'-'+objPay.ATG_Business_Unit__c)!=null)
                        objPay.ATG_Customer__c = existingAccount.get(objPay.ATG_Customer_Email__c+'-'+objPay.ATG_Business_Unit__c).id; 
                    
                    else if(string.isNotBlank(objPay.ATG_Customer_Mobile__c) && existingAccount.get(objPay.ATG_Customer_Mobile__c.right(9)+'-'+objPay.ATG_Business_Unit__c)!=null)
                        objPay.ATG_Customer__c = existingAccount.get(objPay.ATG_Customer_Mobile__c.right(9)+'-'+objPay.ATG_Business_Unit__c).id;
                
                }
            }
        }    
    } // end of v1.3
}