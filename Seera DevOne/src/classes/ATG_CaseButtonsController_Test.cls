/**
 * Created by mnazir001 on 11/26/18.
 */

@IsTest
private class ATG_CaseButtonsController_Test {
    @IsTest
    static void testGlobalCaseButtons() {
        List<Case> casesList = ATG_TestDataFactory.CreateETOCase(10);
        casesList[0].ATG_KAM__c = userInfo.getUserID();
        upsert casesList;

        string ccgrid = '341412',
                comment = 'Test';
        ATG_CaseButtonsController.UpdateCaseComments(true, comment, casesList[0].Id, 'updatecc', ccgrid );
        ATG_CaseButtonsController.UpdateCaseComments(true, comment, casesList[0].Id, '3', ccgrid);
        ATG_CaseButtonsController.UpdateCaseComments(true, comment, casesList[0].Id, '4', ccgrid);
        ATG_CaseButtonsController.UpdateCaseComments(true, comment, casesList[0].Id, '5', ccgrid);
        ATG_CaseButtonsController.UpdateCaseComments(true, comment, casesList[0].Id, 'assigntosv', ccgrid);
        ATG_CaseButtonsController.UpdateCaseComments(true, comment, casesList[0].Id, 'assigntoreq', ccgrid);
        ATG_CaseButtonsController.UpdateCaseComments(true, comment, casesList[0].Id, 'assigntokam', ccgrid);
    }

    @IsTest
    static void testGetRelevantCCGR_CC_Account(){
        List<ATG_Master__c> masterPrograms = ATG_TestDataFactory.createMaster(10, 'p');
        upsert masterPrograms;

        List<ATG_Master__c> masterDepartments = ATG_TestDataFactory.createMaster(10, 'd');
        upsert masterDepartments;

        List<ATG_Master__c> masterCCGR = ATG_TestDataFactory.createMaster(10, 'c');
        upsert masterCCGR;

        List<ATG_CCGR_CC_Relationship__c> cAtgccgrccRelationships = new List<ATG_CCGR_CC_Relationship__c>();

        List<Case> casesList = ATG_TestDataFactory.CreateETOCase(10);
        for(Integer i = 0; i < casesList.size(); i++){

            casesList[i].ATG_Program__c = masterPrograms[i].Id;
            casesList[i].ATG_Department__c = masterDepartments[i].Id;
            casesList[i].ATG_CC_CCGR_Number__c = masterCCGR[i].Id;
            cAtgccgrccRelationships.add(new ATG_CCGR_CC_Relationship__c(CC_CCGR_Number__c =masterCCGR[i].Id, Department__c= masterDepartments[i].Id,Program__c=masterPrograms[i].Id ));

            casesList[i].Status = ATG_CommunityListViewController.STATUS_APPROVAL_PENDING;
            casesList[i].ATG_ProgramName__c = masterPrograms[i].Name;
        }
        upsert cAtgccgrccRelationships;

        upsert casesList;

        ATG_CaseButtonsController.GetRelevantCCGR_CC_Account(casesList[0].Id);
    }

    @IsTest
    static void testGetAccessibleButtons(){

        Account acc = ATG_TestDataFactory.createEmployeeAccount();
        insert acc;
        User userObj = ATG_TestDataFactory.createUser(acc, 'Test');

        List<Case> casesList = ATG_TestDataFactory.CreateETOCase(1);
        casesList[0].Status = ATG_CommunityListViewController.STATUS_APPROVAL_PENDING;
        upsert casesList;

        if(userObj != null){
            insert userObj;

            ATG_CaseButtons_Visibility__c atc = new ATG_CaseButtons_Visibility__c(Profile_Name__c=userObj.Profile.Name, ATG_User_Type__c=userObj.User_Type__c);
            insert atc;
        }

        ATG_CaseButtonsController.GetAccessibleButtons(casesList[0].Id);

    }

    @IsTest
    static void testMiscellaneousFunc(){


        ATG_CaseButtonsController.getUserDetails();

        List<Case> casesList = ATG_TestDataFactory.CreateETOCase(1);
        upsert casesList;

       // ATG_CaseButtonsController.ATG_FetchPNR('2344', casesList[0].Id, 'Chrome');

        ATG_CaseButtonsController.GetCaseDetails(casesList[0].Id);
        ATG_CaseButtonsController.CreateDownloadLog(casesList[0].Id);
//        ATG_CaseButtonsController.ProcessApproval(casesList[0].Id, 'Test', 'New');
    }
}