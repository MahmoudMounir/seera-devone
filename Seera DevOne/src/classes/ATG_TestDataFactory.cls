@isTest
public class ATG_TestDataFactory {
    
    public static list<case> CreateETOCase(integer count){       
        list<case> caseToInsert = new list<case>();
        string etoRType = ATG_ConstantsUtility.ATG_CASE_ETO_RECORDTYPE;
        list<ATG_master__c> programs = ATG_TestDataFactory.createMaster(count,'p');
        list<ATG_master__c> departments = ATG_TestDataFactory.createMaster(count,'d');
        list<ATG_master__c> ccgrs = ATG_TestDataFactory.createMaster(count,'c');
        
        for(integer i=0;i < count ; i++){
            case casObj = new case();
            casObj.RecordTypeID = etoRType;
            casObj.subject = 'ETO Request'+i;
            casObj.ATG_Reference_Number__c = '65776'+i;
            casObj.ATG_Rejected_Date__c = DateTime.now();
            casObj.ATG_CC_CCGR_Number__c =programs[i].Id; 
            casObj.ATG_Program__c =departments[0].Id;
            casObj.ATG_Department__c = ccgrs[0].Id;
            casObj.Status= 'New';
            caseToInsert.add(casObj);
            
            
        }
        
        //insert caseToInsert;
        
       return caseToInsert; 
    }
    
    
    public static list<ATG_Master__c> createMaster(integer count,string type){
        list<ATG_Master__c> MasterList = new list<ATG_Master__c>();
        string progRecType;
        if(type == 'p')
            progRecType = Schema.SObjectType.ATG_Master__c.getRecordTypeInfosByDeveloperName().get('ATG_Program').getRecordTypeId();
        else if(type == 'd')
            progRecType = Schema.SObjectType.ATG_Master__c.getRecordTypeInfosByDeveloperName().get('ATG_Department').getRecordTypeId();
        else if(type == 'c')
            progRecType = Schema.SObjectType.ATG_Master__c.getRecordTypeInfosByDeveloperName().get('CCGR_CC').getRecordTypeId();    
        
        for(integer i=0;i < count ; i++){
            ATG_Master__c masterObj = new ATG_Master__c();
            masterObj.Name = 'Test Prog'+i;
            masterObj.Name_in_Arabic__c  = 'أميره';
            masterObj.ATG_Active__c = true;
            masterObj.RecordTypeId = progRecType;
            masterObj.ATG_Type__c = 'CCGR';
            MasterList.add(masterObj); 
        }
        
        insert MasterList; 
        return MasterList;  
    }

    public static Account createEmployeeAccount(){

        RecordType personAccountRecordType =  [SELECT Id FROM RecordType WHERE DeveloperName = 'PersonAccount' and SObjectType = 'Account'];
        RecordType businessAccountRecordType =  [SELECT Id FROM RecordType WHERE DeveloperName = 'Business_Account' and SObjectType = 'Account'];

        Account newEmployeeAccount = null;

        try {
            Account companyAccount = new Account(Name='Ministry of Health', RecordTypeId=businessAccountRecordType.Id);
            insert companyAccount;

            newEmployeeAccount = new Account(
                    ATG_Company_Name__c=companyAccount.Id,
                    RecordTypeId = personAccountRecordType.Id,
                    AccountSource = 'MOH',
                    FirstName = 'أميره',
                    MiddleName='صالح',
                    LastName='الجهنى',
                    ATG_Employee_Id__c = '12345',
                    ATG_Grade__c = '79',
                    ATG_Level__c='3',
                    PersonEmail='testuser@moh.gov.sa.com',
                    PersonBirthdate= Date.parse('05/05/1991'),
                    ATG_Gender__c='Male',
                    PersonMobilePhone='+966 582141113',
                    ATG_Nationality__c='SA',
                    ATG_Passport_Number__c='GMT3243242',  
                    ATG_Passport_Issue_Date__c  = Date.Today() -1,                 
                    ATG_Passport_Issuing_Country__c='SA',
                    ATG_Passport_Expiry_Date__c= Date.today() + 1,
                    ATG_National_ID__c='1074112309',
                    ATG_National_ID_Expiry_Date__c=Date.today() + 1
            );

        }catch(Exception ex){
            System.debug('Reason Stacktrace ' + ex.getStackTraceString());
        }

        return newEmployeeAccount;

    }


    public static User createUser(Account account, String userProfile) {

        String defaultProfile = (userProfile != null ? userProfile : 'MoH Requester Community Login User');
        List<Profile> communityProfiles = [SELECT Id, Name, UserLicenseId,UserType FROM Profile WHERE Name=:defaultProfile];

        if(communityProfiles.size() <= 0)
            return  null;

        Profile profile = communityProfiles[0];

        String profileId = profile.Id; // To be filled in by customer.
        String roleEnum = null; // To be filled in by customer.
        String accountId = String.valueOf(account.Id); // To be filled in by customer.

        String userName = account.PersonEmail;


        List<String> errors = new List<String>();

        User u = new User();
        u.Username = userName;
        u.Email = account.PersonEmail;
        u.FirstName = account.FirstName;
        u.LastName = account.LastName;
        u.CommunityNickname = account.FirstName + account.ATG_Employee_Id__c;
        u.ProfileId = profileId;
        u.Alias = account.FirstName + account.LastName.substring(1);
        u.timezonesidkey = 'Asia/Riyadh';
        u.IsActive = true;
        u.languagelocalekey = 'en_US';
        u.localesidkey = 'en_US';
        u.Alias = account.PersonEmail.split('@')[0].substring(0,2);
        u.EmailEncodingKey = 'ISO-8859-1';

        String userId;

        try {

            userId = Site.createExternalUser(u, accountId);

        } catch(Site.ExternalUserCreateException ex) {
            errors = ex.getDisplayMessages();

            System.debug('Unable to create user ===>' + ex.getStackTraceString());
            System.debug('user ===>' + u);
        }

        return errors.size() > 0 ? null : u;
    }
    
     public static list<ATG_Requested_Traveller__c> CreateRTraveller(integer count){       
        
        
        list<ATG_Requested_Traveller__c> rtListtoInsert = new list<ATG_Requested_Traveller__c>();       
        for(integer i=0 ; i < count ; i++){
                ATG_Requested_Traveller__c rtObj = new ATG_Requested_Traveller__c();              
                // rtObj.ATG_Country_Dial_Code__c = 'Angola (244)';
                rtObj.ATG_Date_of_Birth__c = Date.Today()-10000;
                rtObj.ATG_Email_Address__c = 'test@moh.gov.sa';
               // rtObj.ATG_Employee__c = empAcc.Id;
                rtObj.ATG_Employee_Id__c = String.valueOf(i);
                rtObj.ATG_First_Name__c = 'أميره'+i;
                rtObj.ATG_Last_Name__c = 'أميره'+i;
                rtObj.ATG_Middle_Name__c = 'أميره'+i;               
                rtObj.ATG_Level__c = '1';
                rtObj.ATG_Grade__c = '1';
                rtObj.ATG_ID_Number__c = '123213213'+i;
                rtObj.ATG_ID_Type__c = 'National ID / IQAMA';
                rtObj.ATG_Job_Title__c = 'test';
                rtObj.ATG_Mobile_Number__c = '+971 507301993'+i;
                rtObj.ATG_National_ID__c = '2110765415';
                rtObj.ATG_National_ID_Passport_Expiry_Date__c = Date.Today()+100;
                rtObj.ATG_Gender__c = 'Male';
                rtObj.ATG_Nationality__c = 'SA';
                rtObj.Relationship__c = 'Son';
                rtObj.ATG_Mobile_Number_Value__c = '0521293949';
                rtListtoInsert.add(rtObj);
        }

        //upsert rtListtoInsert;

        return rtListtoInsert; 
    }

//    public static void createListViewConfigurations(){
//        ATG_CommunityTabName__mdt tab_mdtName = new ATG_CommunityTabName__mdt(Label='Pending Approvals', DeveloperName='Pending_Approvals',
//                ATG_Object_API_Name__c='Case', Actions__c='Approve,Return,Reject');
//
//        insert tab_mdtName;
//
//        ATG_Community_Tab_Related_Fields__mdt relatedFields = new ATG_Community_Tab_Related_Fields__mdt(Label='Departments-Active ?', DeveloperName='dp_name',
//                ATG_Community_Tab_Name__c=tab_mdtName.Id, ATG_Field_API_Name__c='ATG_Active__c', ATG_Field_API_Name_Arabic__c='ATG_Active__c', Is_Hidden__c=true,
//                Lex_Column_Type__c='text', Order__c=1, Sortable__c=true, URL_Label_Attribute__c='');
//
//        insert relatedFields;
//    }



   public static list<ATG_Case_Details__c> createCaseDetails(){            
        
        list<ATG_master__c> masterList = new list<ATG_master__c>();
        ATG_master__c atg_master = new ATG_master__c();
        atg_master.Name = 'Jeddah - Saudi Arabia';
        atg_master.ATG_City_Code__c = 'JS';
        atg_master.ATG_Region__c = 'GCC';
        atg_master.ATG_Country__c = 'Saudi Arabia';
        atg_master.RecordTypeID = Schema.SObjectType.ATG_Master__c.getRecordTypeInfosByDeveloperName().get('Cities').getRecordTypeId();
        atg_master.ATG_Active__c  = true;
        masterList.add(atg_master);
        
        atg_master = new ATG_master__c();
        atg_master.Name = 'Riyadh - Saudi Arabia';
        atg_master.ATG_City_Code__c = 'RI';
        atg_master.ATG_Region__c = 'GCC';
        atg_master.ATG_Country__c = 'Saudi Arabia';
        atg_master.ATG_Active__c  = true;
        atg_master.RecordTypeID = Schema.SObjectType.ATG_Master__c.getRecordTypeInfosByDeveloperName().get('Cities').getRecordTypeId();
        masterList.add(atg_master);
        
        insert masterList;
        
        
        list<ATG_Case_Details__c> cdObjList = new list<ATG_Case_Details__c>();
        ATG_Case_Details__c caseDetails = new ATG_Case_Details__c();
        
         caseDetails.ATG_From_Airport__c = masterList[0].Id; 
        caseDetails.ATG_Date__c = Date.Today()+2;
        cdObjList.add(caseDetails);
        caseDetails = new ATG_Case_Details__c();
        caseDetails.ATG_Return_Date__c = Date.Today()+10;
        caseDetails.ATG_To_Airport__c =  masterList[1].Id;
        
        return cdObjList;   
    }
    
    
    public static void createContentDocument(string LinkEntityID){
         
             ContentVersion contentVersionInsert = new ContentVersion(
                Title = 'Passport',
                PathOnClient = 'Passport.jpg',
                VersionData = Blob.valueOf('Test Content Data'),
                IsMajorVersion = true
            );
            insert contentVersionInsert;   
        
        
            // Insert Content Version
        
            ContentVersion contentVersionSelect = [SELECT Id, Title, ContentDocumentId FROM ContentVersion WHERE Id = :contentVersionInsert.Id LIMIT 1];
                    
           //Insert Content Document Link
        
            ContentDocumentLink cDe = new ContentDocumentLink();
            cDe.ContentDocumentId = contentVersionSelect.ContentDocumentId;
            cDe.LinkedEntityId = LinkEntityID;
            cDe.ShareType = 'I'; 
            cDe.Visibility = 'AllUsers';          
            
            insert  cDe; 
    
    
    }
     
    
}