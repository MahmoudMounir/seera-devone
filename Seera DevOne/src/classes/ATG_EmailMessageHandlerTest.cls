/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class ATG_EmailMessageHandlerTest {

    static testMethod void ATG_EmailMessageHandlerTest1() {
        
        
        ATG_EmailMessageHandler objClass = new ATG_EmailMessageHandler();
        
        Case c = new Case();
        c.Subject = 'Test Case';  
        c.Status ='New';
        c.Priority = 'Medium';
        c.Origin = 'Email';
        c.ATG_BU_From_Case__c = 'Al Tayyar';
        insert c;

        //Insert emailmessage for case
        EmailMessage email = new EmailMessage();
        email.FromAddress = 'test@gmail.org';
        email.Incoming = True;
        email.ToAddress= 'test@altayyer.org';
        email.Subject = 'Test email';
        email.HtmlBody = 'Test email body';
        email.ParentId = c.Id; 
        insert email;
        
        email.Subject = 'Test Subject Update';
        update email;
        
        delete email;
    }
    
    static testMethod void ATG_EmailMessageHandlerTest() {
        
        
        ATG_EmailMessageHandler objClass = new ATG_EmailMessageHandler();
        
        Case c = new Case();
        c.Subject = 'Test Case';  
        c.Status ='New';
        c.Priority = 'Medium';
        c.Origin = 'Email';
        c.RecordTypeId = ATG_ConstantsUtility.AMH_CASE_RECORDTYPE;
        c.ATG_BU_From_Case__c = 'Al Mosafer';
        insert c;
        
        ATG_Booking__C booking = new ATG_Booking__c();
        booking.Name ='H802948';
        booking.ATG_Business_Unit__c ='Al Mosafer';
        insert booking;
        
        ATG_Email_to_Case_Configurations__c cc= new ATG_Email_to_Case_Configurations__c();
        cc.ATG_From_Email__c = 'test@gmail.org';
        cc.ATG_To_Email__c = 'test@almosafer.org';
        cc.ATG_Is_Customer_Linking_to_be_excluded__c =true;
        cc.Name='test';
        insert cc;

        //Insert emailmessage for case
        EmailMessage email = new EmailMessage();
        email.FromAddress = 'test@gmail.org';
        email.Incoming = True;
        email.ToAddress= 'test@almosafer.org';
         email.HtmlBody = 'Test email body';
        email.ParentId = c.Id; 
        
        email.Subject = 'ID-H802948:';
        insert email;
        
        List<EmailMessage> soNewList = new List<EmailMessage>();
        soNewList.Add(email);
        ATG_EmailMessageHandler handler = new ATG_EmailMessageHandler();
        handler.beforeInsert(soNewList);
        handler.afterInsert(soNewList);
    }
}