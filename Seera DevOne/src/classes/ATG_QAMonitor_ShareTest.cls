@isTest
private class ATG_QAMonitor_ShareTest {

    static testMethod void ATG_QAMonitor_ShareTest() {
    
        Profile p = [SELECT Id FROM Profile WHERE Name='System Administrator']; 
        User u = new User(Alias = 'standt', Email='standarduser@testorg.com', 
        EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
        LocaleSidKey='en_US', ProfileId = p.Id, 
        TimeZoneSidKey='America/Los_Angeles', UserName='standarduser@testorg.com');
        
        QA_Monitoring__c qa = new QA_Monitoring__c();
        qa.Line_of_Business__c = 'Corporate';
        qa.Transaction_Subtype__c = 'Recorded';
        qa.Call_Type__c = 'Inbound';
        qa.Status__c = 'Monitoring Completed';
        qa.Agent_Name__c = u.Id;
        qa.Team_Lead_Name__c = u.Id;
        qa.Flights_Destination_with_Airport_Name__c= 'Yes';
qa.Flights_Date__c= 'Yes';
qa.Flights_No_of_Passengers__c= 'Yes';
qa.Flights_Time__c= 'Yes';
qa.Hotels_Favorite_Hotel__c= 'Yes';
qa.Hotels_Favorite_Location__c= 'Yes';
qa.Hotels_Meals__c= 'Yes';
qa.Hotels_No_of_Rooms__c= 'Yes';
qa.Transport_Airport_to_Hotel__c= 'Yes';
qa.Transport_Inside_City__c= 'Yes';
qa.Tours_With_Tour_Guide__c= 'Yes';
qa.Tours_Tour_Guide_Language__c= 'Yes';
qa.Tours_Intercity__c= 'Yes';
qa.Customer_Data_Email__c= 'Yes';
qa.Customer_Data_Mobile__c= 'Yes';
qa.Customer_Data_Name__c= 'Yes';
qa.Customer_Data_Passport_or_Other_ID__c= 'Yes';
qa.Correct_use_of_systems__c= 'Yes';
qa.Cross_Upselling__c= 'Yes';
qa.Suggest_alternative_payment_method__c= 'Yes';
qa.Ask_for_Passport_Copy__c= 'Yes';
qa.Reconfirm_reservation_information__c= 'Yes';
qa.Provide_correct_complete_information__c= 'Yes';
qa.Effective_appropriate_troubleshooting__c= 'Yes';
qa.Case_raised_a_ticket__c= 'Yes';
qa.Direct_customer_to_appropriate_external__c= 'Yes';
qa.Direct_customer_to_appropriate_internal__c= 'Yes';
qa.Agent_raise_update_ticket__c= 'Yes';
qa.Agent_raise_right_ticket_description__c= 'Yes';
qa.Release_the_call__c= 'Yes';
qa.Inappropriate_behavior__c= 'Yes';
qa.Unnecessary_Vendor_Referral__c= 'Yes';
qa.Speed_of_answer__c= 'Yes';
qa.Appropriate_welcome__c= 'Yes';
qa.Ask_for_customer_name__c= 'Yes';
qa.Welcome_customer_by_name__c= 'Yes';
qa.Permission_for_hold__c= 'Yes';
qa.Specify_hold_time__c= 'Yes';
qa.Specify_hold_reason__c= 'Yes';
qa.Used_customer_name__c= 'Yes';
qa.Thank_customer_for_waiting__c= 'Yes';
qa.Intonation__c= 'Yes';
qa.Language__c= 'Yes';
qa.Active_Listening__c= 'Yes';
qa.Used_customer_name_2__c= 'Yes';
qa.Call_control__c= 'Yes';
qa.Reconfirm_reservation_information_2__c= 'Yes';
qa.Confirm_solution__c= 'Yes';
qa.Call_Closure__c = 'Yes';

        insert qa;
        
        System.Assert(qa.Id != null, 'The Test Account did not insert properly, please check validation rules and other mechanisms');

        
    }
}