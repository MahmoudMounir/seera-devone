/**
 * Created by mnazir001 on 11/26/18.
 */

@isTest(seeAllData=true)
private class ATG_PortalHomePageUtility_Test {


    static void initialize() {
        
        List<RecordType> travelRecordTypes = [select id, DeveloperName from RecordType WHERE sobjecttype='Case' and DeveloperName='ATG_Electronic_Travel_Order'];
        List<Case> cases = new List<Case>();

        cases.add(new Case(status=ATG_PortalHomePageUtility.STATUS_NEW, RecordType=travelRecordTypes[0]));
        cases.add(new Case(status=ATG_PortalHomePageUtility.STATUS_APPROVED, RecordType=travelRecordTypes[0], ATG_Approved_Date__c= Date.today().addDays(-1)));
        cases.add(new Case(status=ATG_PortalHomePageUtility.STATUS_SENT_BACK, RecordType=travelRecordTypes[0], ATG_Sent_Back__c=Date.today().addDays(-1)));
        cases.add(new Case(status=ATG_PortalHomePageUtility.STATUS_ASSIGNED, RecordType=travelRecordTypes[0], ATG_Approved_Date__c= Date.today().addDays(-1), ATG_Assigned_Date__c=Date.today().addDays(-1)));
        cases.add(new Case(status=ATG_PortalHomePageUtility.STATUS_REJECTED, RecordType=travelRecordTypes[0], ATG_Approved_Date__c= Date.today().addDays(-1), ATG_Rejected_Date__c=Date.today().addDays(-1)));
        cases.add(new Case(status=ATG_PortalHomePageUtility.STATUS_COMPLETED, RecordType=travelRecordTypes[0], ATG_Approved_Date__c= Date.today().addDays(-1)));
        cases.add(new Case(status=ATG_PortalHomePageUtility.STATUS_APPROVAL_PENDING, RecordType=travelRecordTypes[0]));

        insert cases;

    }

    @isTest static void testGetUserInfo() {

        ATG_PortalHomePageUtility_Test.initialize();
        
        ATG_PortalHomePageUtility.GetMostRecentRequests();
        ATG_PortalHomePageUtility.GetUserDepartments();

        ATG_PortalHomePageUtility.GetStageConfigs();

        ATG_PortalHomePageUtility.getProfileFields();

        ATG_PortalHomePageUtility.GetUserSettings();
        
        try{
            ATG_PortalHomePageUtility.GetUserDetails('ar');
        }catch(Exception ex){
            System.debug('User Details failed' + ex.getMessage());
        }

    }

    @isTest static void testGetDashboard() {

        ATG_PortalHomePageUtility_Test.initialize();

        ATG_PortalHomePageUtility.getAccessibleDashboard();

        ATG_PortalHomePageUtility.getDashboardRecordsCount('123456');


    }

    @isTest static void testGetChartData() {
        ATG_PortalHomePageUtility.GetChartData('1');
        ATG_PortalHomePageUtility.GetChartData('2');
        ATG_PortalHomePageUtility.GetChartData('3');
        ATG_PortalHomePageUtility.GetChartData('4');
        ATG_PortalHomePageUtility.GetChartData('5');
    }
}