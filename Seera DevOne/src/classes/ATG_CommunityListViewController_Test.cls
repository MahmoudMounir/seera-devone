/**
 * Created by mnazir001 on 11/26/18.
 */

@IsTest
private class ATG_CommunityListViewController_Test {

    @IsTest
    static void testListViewColumnDetails() {



        ATG_CommunityListViewController.GetColumnDetails('Programs');

        List<String> fieldsList = new List<String>(new String[]{'ATG_ProgramName__c'});


        List<ATG_Master__c> masterData = ATG_TestDataFactory.createMaster(10, 'p');
        List<Case> casesList = ATG_TestDataFactory.CreateETOCase(10);
        for(Integer i = 0; i < casesList.size(); i++){

            casesList[i].ATG_Program__c = masterData[i].Id;
            casesList[i].Status = ATG_CommunityListViewController.STATUS_APPROVAL_PENDING;
            casesList[i].ATG_ProgramName__c = masterData[i].Name;
        }
        insert casesList;

        ATG_CommunityListViewController.GetTableData('Pending_Approvals',fieldsList, 5, 0, 'Prog', fieldsList);

    }
}