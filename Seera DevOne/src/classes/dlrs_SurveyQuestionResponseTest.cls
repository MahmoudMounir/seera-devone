/**
 * Auto Generated and Deployed by the Declarative Lookup Rollup Summaries Tool package (dlrs)
 **/
@IsTest
private class dlrs_SurveyQuestionResponseTest
{
    @IsTest
    private static void testTrigger()
    {
        // Force the dlrs_SurveyQuestionResponseTrigger to be invoked, fails the test if org config or other Apex code prevents this.
        dlrs.RollupService.testHandler(new SurveyQuestionResponse__c());
    }
}