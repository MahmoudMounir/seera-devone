/**************************************************************************************************
* Name               : ATG_ContractHandler                                                               
* Description        : Trigger Handler for the Contract SObject. This class implements the ITrigger
                       interface to help ensure the trigger code is bulkified and all in one place.                                       
* Created Date       : 24th June 2019                                                              *
* Created By         : Leeba Shibu (PWC Digital)                                              
* -------------------------------------------------------------------------------------------------
* Version   Author      Date            Comment                                                        *


**************************************************************************************************/

public without sharing class ATG_ContractHandler implements ATG_TriggerInterface {

    public void beforeInsert(list<SObject> soNewList){
    	populateFields(soNewList);    
    }
    
    public void afterInsert(list<SObject> soNewList){
        try{
             ATG_ContractHandler.UpdateExpiryonAccount((list<Contract>)soNewList,null);
             ATG_ContractHandler.UpdateContractonCase ((list<Contract>)soNewList,null);
           }catch(exception ex){
            ATG_utility.LogException(ex);
        }
    }
    public void beforeUpdate(list<sobject> soNewList, map<id,sobject> soOldMap){
        SourcingCoEValidations( (list<Contract>) soNewList, (map<id,Contract>)soOldMap );    
    }
    
    public void afterUpdate(list<sobject> soNewList, map<id,sobject> soOldMap){
          try{
                ATG_ContractHandler.UpdateExpiryonAccount((list<Contract>)soNewList,(map<id,Contract>)soOldMap);
             }catch(exception ex){
            ATG_utility.LogException(ex);
        }
    }
    public void beforeDelete(list<SObject> soNewList){}
    public void afterDelete(list<SObject> soNewList){
            try{
                ATG_ContractHandler.UpdateExpiryonAccount((list<Contract>)soNewList,null);
               }catch(exception ex){
            ATG_utility.LogException(ex);
        }
    }
    
    public static void populateFields(list<Contract> lstContracts){
        map<Id,Account> mapAccounts = new map<Id, Account>();
        
        for(Contract obj : lstContracts){
            if(obj.AccountId != null){
                mapAccounts.put(obj.AccountId, null);
            }
        }
        
        if(!mapAccounts.isEmpty()){
            mapAccounts = new map<Id,Account>([select Id,Name from Account where Id IN : mapAccounts.keySet()]);
            for(Contract obj : lstContracts){
                if(mapAccounts.get(obj.AccountId) != null){
                    obj.ATG_Account_Name__c = mapAccounts.get(obj.AccountId).Name;
                }
            }
        }
    }
    
    // Logic to update the Active Contract Expiry date and Status from Contract at Account Level 
    private static void UpdateExpiryonAccount(list<Contract> contractList,map<id,Contract> contractOldMap){
        
        Set<Id> accountIds = new set<Id>();
        //List<Account> accountsToUpdate = new List<account>();
        Map < Id, Account > MapAccountsToUpdate = new Map < Id, Account > ();
        Set < Id > accountIds1 = new set < Id > ();
         
        if(contractList !=null && !contractList.isEmpty()){
            for(Contract con : contractList){
                if(con.AccountId !=null && (contractOldMap ==null || (contractOldMap !=null && con.EndDate != contractOldMap.get(con.Id).EndDate)|| (contractOldMap !=null && con.Status != contractOldMap.get(con.Id).Status) ))
                    accountIds.add(con.accountId);
                 //When the contract status changes from or to "Live"
                if(contractOldMap !=null && con.Status != contractOldMap.get(con.Id).Status && (con.Status == 'Live' || contractOldMap.get(con.Id).Status == 'Live'))
                    accountIds1.add(con.accountId);
                   
            }
            
            
            
                if(accountIds!=null && accountIds.size()>0){
                    for(AggregateResult ar :[SELECT AccountId, MAX(EndDate) maxDate
                            FROM Contract
                            WHERE AccountId IN :accountIds AND Status = 'Activated'
                            GROUP BY AccountId]){
                        /*accountsToUpdate.add(new Account(
                            Id = (Id)ar.get('AccountId'),
                        ATG_Expiration_Date__c = (Date)ar.get('maxDate')
                        ));*/
                         MapAccountsToUpdate.put((Id) ar.get('AccountId'), new Account(Id = (Id) ar.get('AccountId'), ATG_Expiration_Date__c = (Date) ar.get('maxDate')));
                    }
                }  
            
                if(accountIds1 != null && accountIds1.size()>0){
                
                //Get all the child contracts of the account and check if there is a contract with Live Status
                List < Contract > conList = [SELECT AccountId, Status, Id FROM Contract WHERE AccountId IN: accountIds1 and Status = 'Live'];

                //Check if there is atleast one contract with Live status. If yes then make the status of the  account as Live if the previous status of the account is Active

                if (conList != null && !conList.isEmpty()) {
                    for (Account objAcc: [select Id, ATG_Status__c from Account where Id IN: accountIds1]) {
                        if (objAcc.ATG_Status__c == 'Active') {
                            Account objAccUpdate = new Account(Id = objAcc.Id);
                            If(MapAccountsToUpdate.get(objAcc.Id) != null)
                            objAccUpdate = MapAccountsToUpdate.get(objAcc.Id);
                            objAccUpdate.ATG_Status__c = 'Live';
                            MapAccountsToUpdate.put(objAccUpdate.Id, objAccUpdate);
                            
                        }
                    }
                } else {
                    // If there is no contract with Live Status then change the status of the Account to Active if the previous status of the account is Live
                    for (Account objAcc: [select Id, ATG_Status__c from Account where Id IN: accountIds1]) {
                        if (objAcc.ATG_Status__c == 'Live') {
                            Account objAccUpdate = new Account(Id = objAcc.Id);
                            If(MapAccountsToUpdate.get(objAcc.Id) != null)
                            objAccUpdate = MapAccountsToUpdate.get(objAcc.Id);
                            objAccUpdate.ATG_Status__c = 'Active';
                            MapAccountsToUpdate.put(objAccUpdate.Id, objAccUpdate);
                            
                        }
                    }
                }

            }     
        }
        /*
        if(accountsToUpdate!=null && accountsToUpdate.size()>0)
            update accountsToUpdate;
         */
        if (MapAccountsToUpdate.size() > 0)
            update MapAccountsToUpdate.values();
    
    }   
    
    // Logic to populate the Contract Lookup on Case for Sourcing Contract Requests
    public static void UpdateContractonCase(list<Contract> contractList,map<id,Contract> contractOldMap){
        Set<Id> caseIds = new set<Id>();
        map<string,id> mapcaseId = new map<string,id>();
        List<Case> caseToUpdate = new List<Case>();
        if(contractList !=null && !contractList.isEmpty()){
            for(Contract con : contractList){
                if(con.AccountId !=null && con.sys_Case_Id__c!=null){
                    caseIds.add(con.sys_Case_Id__c);
                    mapcaseId.put(con.sys_Case_Id__c, con.id);
                }
            
            }
            
            
            
            if(caseIds!=null && caseIds.size()>0){
                for(Case obj : [select id,ATG_Contract__c from Case where id IN : caseIds]){
                    obj.ATG_Contract__c = mapcaseId.get(obj.id);
                    caseToUpdate.add(obj);
                }
            
            }
        
        }
         if(caseToUpdate!=null && caseToUpdate.size()>0)
            update caseToUpdate;
    }
    
    public static void SourcingCoEValidations(list<Contract> contractList,map<id,Contract> contractOldMap){
        map<Id,Boolean> mapContractFiles = new map<Id, Boolean>();
        for(Contract objContract : contractList){
            if(contractOldMap.containsKey(objContract.Id)){
                if(contractOldMap.get(objContract.Id).Status != objContract.Status && objContract.Status == ATG_ConstantsUtility.STR_Activated){
                    mapContractFiles.put(objContract.Id, false);
                }
            }
        }
        
        if(!mapContractFiles.isEmpty()){
            for(ContentDocumentLink objCDL : [select LinkedEntityId from ContentDocumentLink where LinkedEntityId IN : mapContractFiles.keySet() ] ){
                if(mapContractFiles.containsKey(objCDL.LinkedEntityId)){
                    mapContractFiles.put(objCDL.LinkedEntityId,true);
                }
            }
            for(Contract objContract : contractList){
                if(mapContractFiles.get(objContract.Id) == false){
                    objContract.addError(Label.Attachment_Upload_Error_Message);
                }
            }
        }
    }
}