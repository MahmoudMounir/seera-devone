/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class ATG_Batch_BookingToCustomerLinkTest {

    static testMethod void ATG_Batch_BookingToCustomerLink1() {
        
        ATG_TestUtility objTest = new ATG_TestUtility();
		Account corp = objTest.createCorporateAccount();
		
		list<Account> temp = [select id, ispersonAccount,ATG_Tapro_ID__c,recordTypeID from Account where id =: corp.id];
		
        string bookingID = objTest.createflightbooking();
        
        Test.startTest();
            ATG_Batch_BookingToCustomerLink obj = new ATG_Batch_BookingToCustomerLink();
            DataBase.executeBatch(obj); 
        Test.stopTest();
    }
}