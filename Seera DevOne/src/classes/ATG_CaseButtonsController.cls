/**************************************************************************************************
* Name               : ATG_CaseButtonsController 
* Description        : Utility Class for Case Actions components                                        
* Created Date       : 08 Jul 2018                                                               *
* Created By         : Sravan Booragadda (PWC Digital)                                              
* -------------------------------------------------------------------------------------------------
* Version       Author      Date        Comment                                                               *

**************************************************************************************************/

public without sharing class ATG_CaseButtonsController { 
  
    
    @AuraEnabled
    public static string UpdateCaseComments(boolean isPublic,string comment,string caseID,string buttonEvent,string ccgrNumberId){
        CaseComment cc = new CaseComment(parentId=caseID,CommentBody=comment,IsPublished=isPublic);
        Savepoint sp;
        try{
             
            insert cc; 
            // Handle Various Events 
            
            if(buttonEvent =='updatecc')
                 ATG_CaseButtonsController.UpdateCaseCCGR(caseID,ccgrNumberId);           
            else if(buttonEvent =='3')
                ATG_CaseButtonsController.ProcessApproval(CaseId,comment,'Approve');   
            else if(buttonEvent =='4')
               ATG_CaseButtonsController.ProcessApproval(CaseId,comment,'Return');
            else if(buttonEvent =='5')
                ATG_CaseButtonsController.ProcessApproval(CaseId,comment,'Reject');
            else if(buttonEvent =='assigntosv')
                ATG_CaseButtonsController.AssignToQueue(caseID,Label.ATG_Supervisor_Queue_Name);    
            else if(buttonEvent =='assigntoreq')
                ATG_CaseButtonsController.AssignToRequestor(caseID);
            else if(buttonEvent =='assigntokam')
                ATG_CaseButtonsController.AssignToQueue(caseID,Label.ATG_KAM_Queue_Name);                       
            
            return 'Success';
        }catch(exception e){
            return 'Error';
        }    
        
    }   
     
     public static string AssignToQueue(string caseId,string QueueName){         
         Group SupervisorQueue;
         for(Group grp : [select id from group where Type='Queue' and DeveloperName =: QueueName])
             SupervisorQueue = grp;
         
         if(SupervisorQueue !=null){
             Case caseRec = new case(id=caseId,ownerId = SupervisorQueue.Id);       
             update caseRec;
             return 'success'; 
         }             
         return 'Error';    
     }
    
     @AuraEnabled
     public static list<ATG_Master__c> GetRelevantCCGR_CC_Account(string caseId){
         list<ATG_Master__c> ccgrMasterList = new list<ATG_Master__c>();
         Case caseObj = [select id,ATG_Program__c,ATG_Department__c,ATG_CC_CCGR_Number__c,ATG_CC_CCGR_Number__r.Id,ATG_CC_CCGR_Number__r.Name from Case where id =: caseId] ;         
         ccgrMasterList.add(caseObj.ATG_CC_CCGR_Number__r);
         for(ATG_CCGR_CC_Relationship__c ccgrRel : [select id,CC_CCGR_Number__c,CC_CCGR_Number__r.Name,CC_CCGR_Number__r.Id,CC_CCGR_Number__r.ATG_Type__c from ATG_CCGR_CC_Relationship__c where 
                                                           Department__c =: caseObj.ATG_Department__c and Program__c =: caseObj.ATG_Program__c and CC_CCGR_Number__r.ATG_Active__c = true and CC_CCGR_Number__c !=: caseObj.ATG_CC_CCGR_Number__c])
            ccgrMasterList.add(ccgrRel.CC_CCGR_Number__r);     
             
         return ccgrMasterList;  
     }
     
     @AuraEnabled
     public static list<string> GetAccessibleButtons(string caseID){         
         user userObj = [select id,profile.Name,User_Type__c,LanguageLocaleKey from user where id =:userInfo.getUserId()];        
         list<string> buttonNames = new list<string>();
         for(ATG_CaseButtons_Visibility__c atc : ATG_CaseButtons_Visibility__c.getAll().Values()){
             if((atc.Profile_Name__c == userObj.Profile.Name) || (atc.ATG_User_Type__c == userObj.User_Type__c && !string.isBlank(atc.ATG_User_Type__c))){
                 if(userObj.LanguageLocaleKey == 'ar' && !string.IsBlank(atc.Button_Name_Arabic__c))
                     buttonNames.add(atc.Button_Name_Arabic__c); 
                 else
                     buttonNames.add(atc.Button_Name__c);  
                             
             }
         }
         
         return buttonNames;
         
     }
     
    private static string UpdateCaseCCGR(string caseId,string CCGRID){        
        case caseObj = new case(id=caseId,ATG_CC_CCGR_Number__c = CCGRID,ATG_KAM__c=userInfo.getUserID());
        update  caseObj ;  
        
        return 'Success';
        
    }
    
    
    private static string AssignToRequestor(string caseId){
        case caseObj ;
        for(ATG_Request_Timeline__c rt : [select ATG_User__c from ATG_Request_Timeline__c where ATG_Department__c =: Label.User_Type_Requestor and ATG_Request_Number__c = :caseId Order By CreatedDate Desc limit 1]){
            caseObj = new Case(id=caseId, ownerId=rt.ATG_User__c);
            break;
        }
        
        if(caseObj !=null){
            //update caseObj;
            return 'Success';
        }  
        
        return 'Error';       
     }
     
     
     private static string ProcessApproval(Id CaseId,string comments,string status){
        return ATG_CommunityApprovalProcessController.ProcessApproval(new list<string>{CaseId},comments,status);
     }
     
     @AuraEnabled
     public static case GetCaseDetails(string CaseId){          
         list<string> caseFields = new list<string>(ATG_utility.GetAccessibleFields('Case'));
         string caseQueryString = 'select ';
         caseQueryString  += string.Join(caseFields,',') +',ATG_CC_CCGR_Number__r.ATG_Type__c from Case where Id = : CaseId limit 1';                 
         return Database.Query(caseQueryString);
     }
     
     
    @AuraEnabled
    public static map<string,string> ATG_FetchPNR(string PNRNumber,string CaseId,string browserName){    
        return ATG_FetchPNR.getCredentials(PNRNumber,CaseId,Auth.SessionManagement.getCurrentSession().get('SourceIp'),'Chrome');  
        return null; 
    } 
    
    @AuraEnabled
    public static user getUserDetails(){    
        return ATG_PortalHomePageUtility.GetUserDetails('');
    }
    
    @AuraEnabled
    public static string CreateDownloadLog(string CaseId){
        try{
            ETO_Download_Log__c etod = new ETO_Download_Log__c(Downloaded_Date__c = DateTime.Now(),ATG_User__c = userInfo.getUserId(),ATG_Request_Number__c = CaseId) ;
            insert etod;             
            return 'Success';
         }catch(exception e){ return e.getMessage();}
    }
    
    @AuraEnabled
    public static string CreateDownloadLog(string CaseId,string Downloaded_UserRole){
        try{
            system.debug('***********Downloaded_UserRole*******'+Downloaded_UserRole);
            ETO_Download_Log__c etod = new ETO_Download_Log__c(Downloaded_Date__c = DateTime.Now(),ATG_User__c = userInfo.getUserId(),ATG_Request_Number__c = CaseId,Downloaded_UserRole__c = Downloaded_UserRole)   ;
            insert etod;           
            return 'Success';
         }catch(exception e){ return e.getMessage();}
    }
     
    
     
     
}