@isTest
private class bulkComm_Test {


static testMethod void createTestData() {

        Account obj = new Account();
        obj.firstName = 'Test';
        obj.lastName = 'Test';
        obj.personemail = 'testing@test.com';
        obj.recordTypeID = ATG_ConstantsUtility.PERSON_ACCOUNT_RECORDTYPEID;
        obj.ATG_Business_Unit_Picklist__c = 'Al Tayyar';
        obj.PersonMobilePhone = '+966 555555555';
        obj.Type = 'Leisure';
        insert obj;
        
        Account obj1 = new Account();
        obj1.firstName = 'Test1';
        obj1.lastName = 'Test';
        obj1.personemail = 'check@check.com';
        obj1.recordTypeID = ATG_ConstantsUtility.AMH_PERSON_ACCOUNT_RECORDTYPEID;
        obj1.ATG_Business_Unit_Picklist__c = 'Al Mosafer';
        obj1.PersonMobilePhone = '+966 535353535';
        obj1.Type = 'Leisure';
        insert obj1;
        
        Account obj2 = new Account();
        obj2.firstName = 'Test';
        obj2.lastName = 'Test';
        obj2.personemail = 'duck@duck.com';
        obj2.recordTypeID = ATG_ConstantsUtility.TJW_PERSON_ACCOUNT_RECORDTYPEID;
        obj2.ATG_Business_Unit_Picklist__c = 'Tajawal';
        obj2.PersonMobilePhone = '+966 525252525';
        obj2.Type = 'Leisure';
        insert obj2;

        ATG_Master__c objAirlines = new ATG_Master__c();
        objAirlines.recordTypeID = Schema.SObjectType.ATG_Master__c.getRecordTypeInfosByDeveloperName().get('Airline').getRecordTypeId();
        objAirlines.name = '(IATA – XY) - Flynas';
        insert objAirlines;
        
        ATG_Booking__c book = new ATG_Booking__c(Name = 'TestBook',
                              ATG_Product_Type__c='Flight',
                              ATG_Contact_Email__c ='testing@test.com',//'test@test.com',
                              ATG_Mobile__c = '+966 578555555',
                              ATG_Customer_Name1__c = 'zxc',
                              ATG_Contact_Person__c = obj.name,
                              ATG_Business_Unit__c ='Al Tayyar');
                              
        insert book;                      
                                    
        ATG_Booking__c book1 = new ATG_Booking__c(Name = 'TestBook',
                              ATG_Product_Type__c='Flight',
                              ATG_Contact_Email__c ='testing@test.com',//'test@test.com',
                              ATG_Mobile__c = '+966 578555555',
                              ATG_Customer_Name1__c = 'zxc',
                              ATG_Contact_Person__c = obj.name,
                              ATG_Business_Unit__c ='Al Tayyar');
                              
        insert book1;
        
        ATG_Booking__c book2 = new ATG_Booking__c(Name = 'TestBook',
                              ATG_Product_Type__c='Flight',
                              ATG_Contact_Email__c ='rest@test.com',//'test@test.com',
                              ATG_Mobile__c = '+966 578455555',
                              ATG_Contact_Person__c = obj.name,
                              ATG_Business_Unit__c ='Al Tayyar');
                                                       
        insert book2;
        
     
        ATG_Booking_Detail__c objBD = new ATG_Booking_Detail__c();
        objBD.ATG_Booking__c = book.Id;
        insert objBD;
               
        ATG_Booking_Detail__c objBD0 = new ATG_Booking_Detail__c();
        objBD0.ATG_Booking__c = book1.Id;
        insert objBD0;
                
        ATG_Booking_Detail__c objBD1 = new ATG_Booking_Detail__c();
        objBD1.ATG_Booking__c = book2.Id;
        insert objBD1;
        
        Segment_Detail__c segDet = new Segment_Detail__c();
        segDet.ATG_Airline__c = 'FLYNAS';
        segDet.ATG_Arrival_Date_Time__c = system.today();
        segDet.ATG_Booking_Detail__c = objBD.Id;
        segDet.ATG_Departure_Date_Time__c = system.today();
        segDet.ATG_Destination__c = 'Dubai';
        segDet.ATG_Origin__c = 'Chennai';
        
        Segment_Detail__c segDet1 = new Segment_Detail__c();
        segDet1.ATG_Airline__c = 'FLYNAS';
        segDet1.ATG_Arrival_Date_Time__c = system.today();
        segDet1.ATG_Booking_Detail__c = objBD0.Id;
        segDet1.ATG_Departure_Date_Time__c = system.today();
        segDet1.ATG_Destination__c = 'Dubai';
        segDet1.ATG_Origin__c = 'Chennai';
        
        Segment_Detail__c segDet2 = new Segment_Detail__c();
        segDet2.ATG_Airline__c = 'FLYNAS';
        segDet2.ATG_Arrival_Date_Time__c = system.today();
        segDet2.ATG_Booking_Detail__c = objBD1.Id;
        segDet2.ATG_Departure_Date_Time__c = system.today();
        segDet2.ATG_Destination__c = 'Dubai';
        segDet2.ATG_Origin__c = 'Chennai';
        
        list<Segment_Detail__c> soNewList = new list<Segment_Detail__c>();
        soNewList.add(segDet);
        soNewList.add(segDet1);
        soNewList.add(segDet2);
        insert soNewList;
    
        List<String> returnList = new List<String>();
returnList.add(segDet.Id);
returnList.add(segDet1.Id);
returnList.add(segDet2.Id);
       
        EmailTemplate result = bulkCommController.getEmailTemplates('Involuntary Change','English','Email','Seera');
        
        List<sObject> result1 = bulkCommController.fetchLookUpValues('FLYNAS');

        bulkCommController.sendemail(returnList,'Involuntary Change','Hello there','Tajawal','English','Email');
    
        bulkCommController.sendMessage(returnList,'Involuntary Change','Hello there');  
    
        bulkCommController.updateSegment(returnList,'Email');
    
        bulkCommController.updateSegment(returnList,'SMS');
        bulkCommController.ContactDataTableWrapper wrapper= new bulkCommController.ContactDataTableWrapper ();
        wrapper.totalRecords = 1;
        wrapper.recordStart = 1;
        wrapper.recordEnd = 1;
        wrapper.pageNumber = 1;
        wrapper.pageSize = 1;
        wrapper.contactList = soNewList;
        bulkCommController.ContactDataTableWrapper.fetchSegmentDetails('Flynas','2019-08-12','2019-08-12',1.0,1.0);
}




}