/**************************************************************************************************
* Name               : ATG_CommunityETORequestController                                                               
* Description        : Controller to manage user creation screen                                   
* Created Date       : 30 Jul 2018                                                              *
* Created By         : PWC Digital                                              
* -------------------------------------------------------------------------------------------------
* Version       Author      Date        Comment                                                               *

**************************************************************************************************/

public without sharing class ATG_CommunityETORequestController {



    // initialize booking case
    @AuraEnabled
    public static ETORequestDetail getBooking(String caseId){
        if(caseId != null && caseId.length() >= 15){
            return ATG_CommunityETORequestController.GetETODetails(caseId);
        }

        return new ETORequestDetail();
    }


    // Method to fetch the CCGR Requested.
    
    @AuraEnabled
    public static list<ATG_CCGR_CC_Relationship__c> FetchCCGRDetails(){
        set<id> departAndProgIdSet = new set<id>();
        list<string> ccListType = new list<string>();
        for(CCGR_CC_Requestor_Settings__c ccreq : CCGR_CC_Requestor_Settings__c.getAll().Values()){
        
            if(ccreq.ATG_Searchable__c){
                if(ccreq.Name == 'CCGR')
                    ccListType.add('CCGR');
                if(ccreq.Name == 'CC No')
                    ccListType.add('CC No');
            }
        }
        // Get Related Departments and Programs of login User
        for(User_Department__c ud : [select id,Department__c,Program__c from User_Department__c where user__c =:userInfo.getUserId()]){
            departAndProgIdSet.add(ud.Department__c);
            departAndProgIdSet.add(ud.Program__c);
        }
        
        return [Select id,CC_CCGR_Number__c,CC_CCGR_Number__r.Name,program__r.Name,program__r.Name_in_Arabic__c,program__c,Department__r.Name,Department__c,Department__r.Name_in_Arabic__c,CC_CCGR_Number__r.Name_in_Arabic__c,
                    CC_CCGR_Number__r.ATG_Type__c from ATG_CCGR_CC_Relationship__c
                    where program__c in : departAndProgIdSet and Department__c in : departAndProgIdSet and CC_CCGR_Number__r.ATG_Type__c in : ccListType];
    }
    
    
    @AuraEnabled
    public static  ETORequestDetail CreateNewETORequest(Case caseObj){
        savepoint sp;
        try{
            Database.setsavepoint();
            
            // Get Current User Details
            list<account> acc = new list<account>();
            acc = [select Id,ATG_Company_Name__c from Account where ATG_Community_User__c =:userInfo.getUserId() limit 1];

            caseObj.RecordTypeId=ATG_ConstantsUtility.ATG_CASE_ETO_RECORDTYPE;

            if(caseObj.Id == null)
                caseObj.status ='New';

            if(acc !=null && acc.size()>0)
                caseObj.AccountId= acc[0].ATG_Company_Name__c;

            
            if(caseObj.ATG_ETO_Requestor__c ==null)
                caseObj.ATG_ETO_Requestor__c  = userInfo.getUserId();
            
            upsert caseObj;
        }catch(exception e){
           

              Database.RollBack(sp);              
              return null;
        }
        
        return ATG_CommunityETORequestController.GetETODetails(caseObj.Id);
    }
    
    // Method to save Case and Traveller Info.
    
    @AuraEnabled
    public static ETORequestDetail SaveTravellerInfo(string caseId,ATG_Requested_Traveller__c travellerDetail,string type){
        savepoint sp;
        map<string,RT_Required_Docs_Community__c> requestedTravellerDocs = RT_Required_Docs_Community__c.getAll();
        list<ATG_Document_Details__c>  docsToInsert = new list<ATG_Document_Details__c>();
        try{
            Database.setSavepoint();
            if(travellerDetail !=null && !string.IsBlank(caseId) && !string.isBlank(type)){              
                travellerDetail.Request_Number__c = caseId;
                if(type == 'Employee')
                    travellerDetail.RecordTypeId = ATG_ConstantsUtility.RequestedTraveller_Employee_RT;
                else if(type == 'Dependant')
                    travellerDetail.RecordTypeId = ATG_ConstantsUtility.RequestedTraveller_Dependant_RT;
                
                upsert  travellerDetail;
                
               
                
              
                
                
                
                map<string,ATG_Document_Details__c> docDetailsMap = ATG_CommunityETORequestController.GetDocumentMasterDocs(travellerDetail.ATG_Employee__c); 
                
                for(RT_Required_Docs_Community__c rtDocs : RT_Required_Docs_Community__c.getAll().Values()){
                    if(rtDocs.Is_Default__c && docDetailsMap.containskey(rtDocs.Name))
                       docsToInsert.add(new ATG_Document_Details__c(ATG_Document_Name__c =rtDocs.Name,ATG_Document_Description__c = rtDocs.Document_Discription__c,Requested_Travelers__c=travellerDetail.id,ATG_Document_ID__c =docDetailsMap.get(rtDocs.Name).ATG_Document_ID__c)); 
                }                
               
                insert docsToInsert; 
                
            }
            
        }Catch(exception e){
            Database.rollback(sp);
           return null;

        }
        return ATG_CommunityETORequestController.GetETODetails(caseId);        
    }
    
    
    public static map<string,ATG_Document_Details__c> GetDocumentMasterDocs(id AccountID){        
        map<string,ATG_Document_Details__c> docDetailsMap = new map<string,ATG_Document_Details__c>();
        for(ATG_Document_Details__c doc : [select id,ATG_Document_Description__c,ATG_Document_Name__c,ATG_Document_ID__c from ATG_Document_Details__c where ATG_Employee__c =:AccountID]){
            docDetailsMap .put(doc.ATG_Document_Name__c,doc);
        }
        return docDetailsMap;
    }
    
    
     // method to save Case and Case Details
    @AuraEnabled
    public static ETORequestDetail UpdateCaseDetails(Case caseETO,list<ATG_Case_Details__c> caseDetailList){
        savepoint sp;
      //  try{
            Database.SetSavepoint();       
            Case caseToUpdate = new case(id=caseETO.Id,Cabin_Class__c = caseETO.Cabin_Class__c,ATG_Purpose_of_Visit__c=caseETO.ATG_Purpose_of_Visit__c,ATG_Trip_Type__c = caseETO.ATG_Trip_Type__c);
            if(caseETO !=null && (caseDetailList !=null && !caseDetailList.isEmpty())){

                // 1 - Delete existing CaseDetails
                List<ATG_Case_Details__c> oldCaseDetails = [SELECT id FROM ATG_Case_Details__c WHERE ATG_Case_Number__c =:caseETO.id];

                if(oldCaseDetails != null)
                    delete oldCaseDetails;            
               

                Integer flightOrder = 1;
                for(ATG_Case_Details__c caseDetail: caseDetailList){
                   caseDetail.id = null;

                    caseDetail.ATG_Flight_Order__c = flightOrder;
                    flightOrder = flightOrder + 1;

                   caseDetail.ATG_Case_Number__c = caseETO.id;

                }
                
                if(caseETO.ATG_Trip_Type__c == 'RoundTrip' && caseDetailList.size() == 2){
                    caseDetailList[0].ATG_To_Airport__c  = caseDetailList[1].ATG_To_Airport__c;
                    caseDetailList[1].ATG_From_Airport__c  = caseDetailList[0].ATG_To_Airport__c;
                    caseDetailList[1].ATG_To_Airport__c = caseDetailList[0].ATG_From_Airport__c;
                    caseDetailList[1].ATG_Date__c = caseDetailList[1].ATG_Return_Date__c;
                    caseDetailList[0].ATG_Return_Date__c = caseDetailList[1].ATG_Return_Date__c;             
                }
                
                if(caseDetailList.size() >=1){
                    if(caseDetailList[0].ATG_Date__c !=null && caseDetailList[0].ATG_Date__c > Date.Today() && Date.Today().daysBetween(caseDetailList[0].ATG_Date__c) <= integer.ValueOf(Label.Urgent_Priority_Days))
                        caseToUpdate.Priority = 'Urgent';
                    else
                        caseToUpdate.Priority = 'Regular';                  
                }

                if(caseDetailList !=null && caseDetailList.size()>0)
                    insert caseDetailList;
            }
            
            
            
            update caseToUpdate;

                    
            
                
       /*  }catch(exception e){
            if(sp != null)
                Database.RollBack(sp);
           
            return null;
       }  */
        
        return ATG_CommunityETORequestController.GetETODetails(caseETO.id);
    }

    @AuraEnabled
    public static ETORequestDetail GetETODetails(string caseId){
        
        ETORequestDetail etoReqObj = new ETORequestDetail();
        // Query All the Details 
        List<string> fieldsList = new List<string>(ATG_utility.GetAccessibleFields('Case'));
        fieldsList.add('ATG_CC_CCGR_Number__r.Name');
        fieldsList.add('ATG_CC_CCGR_Number__r.Name_in_Arabic__c');
        fieldsList.add('ATG_Department__r.Name');
        fieldsList.add('ATG_Department__r.Name_in_Arabic__c');
        fieldsList.add('ATG_Program__r.Name');
        fieldsList.add('ATG_Program__r.Name_in_Arabic__c');
        string CaseQueryString = 'Select ';
        CaseQueryString +=string.join(fieldsList,',');


        // Query Case detail fields
        string CaseDetailsQueryString ='Select ';
        fieldsList = new List<string>(ATG_utility.GetAccessibleFields('ATG_Case_Details__c'));
        CaseDetailsQueryString += string.join(fieldsList,',')+' from Case_Details__r ORDER BY ATG_Flight_Order__c ASC';
        
        // Query Requested Traveler Info
        string requestedTravellersQueryString ='Select ';
        fieldsList = new List<string>(ATG_utility.GetAccessibleFields('ATG_Requested_Traveller__c'));
        requestedTravellersQueryString += string.join(fieldsList,',') +', ATG_Employee__r.ATG_Job_Title__c, RecordType.DeveloperName from Requested_Travelers__r order by CreatedDate ASC';
        
        CaseQueryString += ',('+CaseDetailsQueryString +'),'+'('+ requestedTravellersQueryString +') from case where id=\''+caseId+'\'';
                
        
        
        for(Case caseObj : Database.Query(CaseQuerySTring)){
            etoReqObj.bookingCase = caseObj;
            etoReqObj.requestedTravelerInfo = caseObj.Requested_Travelers__r;
            etoReqObj.caseDetails = caseObj.Case_Details__r;                
        }
        
        fieldsList = new List<string>(ATG_utility.GetAccessibleFields('ATG_Document_Details__c'));
        
        list<ATG_Requested_Traveller__c> rtList = etoReqObj.requestedTravelerInfo;
        String DocumentDetailQuerySTring = 'Select '+ string.join(fieldsList,',')+' from ATG_Document_Details__c where Requested_Travelers__c in : rtList ';
        
        for(ATG_Document_Details__c docObj: Database.Query(DocumentDetailQuerySTring)){ //[Select id,Requested_Travelers__c from ATG_Document_Details__c where Requested_Travelers__c in :etoReqObj.requestedTravelerInfo]){         
            if(etoReqObj.TravellerDocuments.containsKey(docObj.Requested_Travelers__c))
                etoReqObj.TravellerDocuments.get(docObj.Requested_Travelers__c).add(docObj);
            else{
                etoReqObj.TravellerDocuments.put(docObj.Requested_Travelers__c,new list<ATG_Document_Details__c>());
                etoReqObj.TravellerDocuments.get(docObj.Requested_Travelers__c).add(docObj);
            }
                
        }
            
        
        etoReqObj.ContentIdsForDocs =    ATG_CommunityETORequestController.GetDocumentDetails(etoReqObj.bookingCase.Id,new list<string>());


        // populate old flow paused state
        if(caseId != null){
            List<FlowRecordRelation> oldFlowRecordRelations = [SELECT
                    ParentId, Parent.InterviewLabel, Parent.PauseLabel,
                    Parent.CurrentElement, Parent.CreatedDate, Parent.Owner.Name
            FROM FlowRecordRelation
            WHERE RelatedRecordId = :caseId ORDER BY Parent.CreatedDate DESC];

            if(oldFlowRecordRelations.size() > 0)
                etoReqObj.oldBookingFlowState = oldFlowRecordRelations[0];

        }
        
        
        return ATG_CommunityETORequestController.validateCase(etoReqObj);

        //return  etoReqObj;      
    }
    
    
 // ETO Validations
    
   private static ETORequestDetail  validateCase(ETORequestDetail etodetail){       
        
        map<string,object> validationMap = new map<string,object>();
        set<string> dependantValidationPurposeffVisit = new set<string>();
        dependantValidationPurposeffVisit.addAll(Label.Max_Dependant_Purposeofvisit_Val.split(','));
        string messageString ='';       
        if(etodetail !=null){            
            if(etodetail.requestedTravelerInfo.size() > 0){
                if(string.isBlank(messageString) && dependantValidationPurposeffVisit.contains(etodetail.bookingCase.ATG_Purpose_of_Visit__c)){
                    string employeeRT = ATG_ConstantsUtility.RequestedTraveller_Employee_RT;
                    string VistorRT = ATG_ConstantsUtility.RequestedTraveller_Visitor_RT ;
                    map<string,integer> ptDependantsCountMap = new map<string,Integer>();
                    map<string,ATG_Requested_Traveller__c> ptDetailsMap = new map<string,ATG_Requested_Traveller__c>();
                    for(ATG_Requested_Traveller__c rt : etodetail.requestedTravelerInfo){                    
                        if(rt.RecordTypeId == employeeRT || rt.RecordTypeId == VistorRT){                       
                            ptDetailsMap.put(rt.Id,rt);
                            if(!ptDependantsCountMap.containsKey(rt.Id))
                                ptDependantsCountMap.put(rt.Id,0);                  
                        }else if(rt.ATG_Employee_Traveler__c !=null){
                            if(!ptDependantsCountMap.containsKey(rt.ATG_Employee_Traveler__c))
                                ptDependantsCountMap.put(rt.ATG_Employee_Traveler__c,1);
                            else if(ptDependantsCountMap.containsKey(rt.ATG_Employee_Traveler__c)){
                                ptDependantsCountMap.put(rt.ATG_Employee_Traveler__c,ptDependantsCountMap.get(rt.ATG_Employee_Traveler__c)+1);
                            }   
                        }               
                    }
                    
                    for(string  ptId :ptDependantsCountMap.keySet()){
                        if(ptDependantsCountMap.get(ptId) > integer.valueOf(Label.Max_Dependants)){
                            if(string.IsBlank(messageString))
                                messageString +=Label.Max_Dependants_Validation+' '+Label.Max_Dependants+'.'+'\r\n';                             
                            messageString += ptDetailsMap.get(ptId).ATG_Traveller_Full_Name__c+'\r\n';  
                        }
                    }
                    
                    validationMap.put('Error',messageString );
                }
                
                if(string.isBlank(messageString)){
                    for(ATG_Requested_Traveller__c rt : etodetail.requestedTravelerInfo){               
                        if(rt.ATG_ID_Type__c != 'Passport' && etodetail.bookingCase.ATG_Outside_GCC_Travel_Identifier__c > 0){
                            if(string.IsBlank(messageString))
                                messageString +=Label.Passport_Mandatory_Validation+'\r\n';    
                            messageString +=  rt.ATG_Traveller_Full_Name__c+'\r\n';  
                            validationMap.put('Error',messageString );               
                        }else if(rt.ATG_ID_Type__c != 'Passport' && rt.ATG_Nationality__c != 'SA' && etodetail.bookingCase.ATG_Outside_GCC_Travel_Identifier__c == 0 && etodetail.bookingCase.ATG_Domestic_Trip_Count__c == 0){
                            if(string.IsBlank(messageString))
                                messageString +=Label.Passport_Mandatory_Validation+'\r\n';    
                            messageString +=  rt.ATG_Traveller_Full_Name__c+'\r\n';  
                            validationMap.put('Error',messageString );        
                        }   
                            
                    }
                }
                    
            }   
        }
        
        etodetail.messageMap = validationMap;
        
        return etodetail;   
    }   
    
    
    
  
    
   // Case detail inner class that contains all 
    
    public class ETORequestDetail{
        @AuraEnabled
        public Case bookingCase;
        @AuraEnabled
        public Map<String, String> travellerRecordTypes;
        @AuraEnabled
        public list<ATG_Requested_Traveller__c> requestedTravelerInfo;
        @AuraEnabled
        public list<ATG_Case_Details__c> caseDetails;
        @AuraEnabled
        public map<id,list<ATG_Document_Details__c>> TravellerDocuments;
        @AuraEnabled
        public map<string,list<string>> ContentIdsForDocs;
        @AuraEnabled
        public FlowRecordRelation oldBookingFlowState; // old flow saved state
        @AuraEnabled
        public map<string,object> messageMap;
        
        public ETORequestDetail(){
            bookingCase = new Case(Subject='New MoH Booking', ATG_CC_CCGR_Number__c=null, ATG_Department__c=null, ATG_Program__c=null, Origin='MOH Community');
            requestedTravelerInfo = new list<ATG_Requested_Traveller__c>();
            caseDetails = new list<ATG_Case_Details__c>();
            TravellerDocuments = new map<id,list<ATG_Document_Details__c>>();
            travellerRecordTypes = travellerRecordTypes = new Map<String, String>();
            ContentIdsForDocs = new map<string,list<string>>();
            messageMap = new map<string,object>();

            // load record types

            Id RequestedTraveller_Employee_RT = Schema.SObjectType.ATG_Requested_Traveller__c.getRecordTypeInfosByDeveloperName().get('Employee').getRecordTypeId();
            Id RequestedTraveller_Dependant_RT = Schema.SObjectType.ATG_Requested_Traveller__c.getRecordTypeInfosByDeveloperName().get('Dependant').getRecordTypeId();
            Id RequestedTraveller_Visitor_RT = Schema.SObjectType.ATG_Requested_Traveller__c.getRecordTypeInfosByDeveloperName().get('Visitor').getRecordTypeId();
            


           

            travellerRecordTypes.put('employee', RequestedTraveller_Employee_RT);
            travellerRecordTypes.put('newemployee', RequestedTraveller_Employee_RT);
            travellerRecordTypes.put('dependent', RequestedTraveller_Dependant_RT);
            travellerRecordTypes.put('visitor', RequestedTraveller_Visitor_RT);

        }               
    }
    
    // Method to Link Employee and Dependant Requested Traveller
    @AuraEnabled
    public static string UpdateRequestedTravelerRelationship(string employeeId,string dependentId){

        
        ATG_Requested_Traveller__c rtObj = new ATG_Requested_Traveller__c(id=dependentId,ATG_Employee_Traveler__c=employeeId);
        try{
             if(employeeId !=null && employeeId !=null)
                update rtObj;
            else
                return 'Error updating relationship';
            
            return 'Success' ;   
        }catch(exception e){return 'Error';}
    }
    
    
   
  
   
   
   @AuraEnabled
   public static map<string,Object> submitForApproval(string caseId, string comments){    
       
        map<string,Object> responseMap = new map<string,Object>();   
        string status = ATG_CommunityApprovalProcessController.SubmitETOForApproval(caseId,UserInfo.getUserId(), comments);      
        if(status  == 'Success'){           
           responseMap.put('status','Success');
           responseMap.put('message',Label.ApprovalSubmissionSuccessMessage);
           responseMap.put('caseids',ATG_CommunityApprovalProcessController.caseList);       
        }else{           
           responseMap.put('status','Error');
           responseMap.put('message',Label.ApprovalSubmissionErrorMessage);
        }  
        
        return responseMap ;    
   }
   
   

   
   
   
  @InvocableMethod(label='Create Documents' description='Creates a Document detail records on Requested Traveller creation')
   public static void CreateDocuments(list<id> RTIds){
      
       
       map<id,ATG_Requested_Traveller__c> RT_AccountMap = new map<id,ATG_Requested_Traveller__c>();
       map<id,ATG_Document_Details__c> documentDetailMap = new  map<id,ATG_Document_Details__c>();
       list<Attachment> attachmentsList = new list<Attachment>();
       map<string,RT_Required_Docs_Community__c> docNames = new map<string,RT_Required_Docs_Community__c>();
       map<id,set<string>> docsForEachEmp = new map<id,set<string>>();
       list<id> contentDocsList = new list<id>();
       
       list<ATG_Requested_Traveller__c> RTDependantList = new List<ATG_Requested_Traveller__c>();
       
       list<ATG_Document_Details__c> documentDetailsToInsert = new list<ATG_Document_Details__c>();
       
       Savepoint sp;
       
       try{
        for(RT_Required_Docs_Community__c rtDocs : RT_Required_Docs_Community__c.getAll().Values()){
            docNames.put(rtDocs.Name,rtDocs);
        }
        
        
       
        for(ATG_Requested_Traveller__c rt : [select id,ATG_Employee__c,RecordTypeId,Request_Number__c from ATG_Requested_Traveller__c where id in : RTIds]){
           if(rt.RecordTypeId  ==  ATG_ConstantsUtility.RequestedTraveller_Employee_RT ){
               RT_AccountMap.put(rt.ATG_Employee__c,rt);
               docsForEachEmp.put(rt.ATG_Employee__c,docNames.keySet());
           }else{
               RTDependantList.add(rt); 
           }
        }  
        
        
                      
       // Get the documents        
       
       if(RT_AccountMap !=null && RT_AccountMap.size()>0){
           for(ATG_Document_Details__c docdetails : [select id,ATG_Document_Name__c,ATG_Document_Description__c,ATG_Employee__c,ATG_Document_ID__c,ATG_Request_Number__c  from ATG_Document_Details__c where ATG_Employee__c in : RT_AccountMap.KeySet() and ATG_Document_Name__c in : docNames.keySet() and ATG_Employee__c !=null]){     
               ATG_Document_Details__c  doc = docdetails.clone(false,false,false,false);
               doc.ATG_Employee__c  = null;
               //doc.ATG_Document_ID__c = '';   
               doc.Requested_Travelers__c = RT_AccountMap.get(docdetails.ATG_Employee__c).Id;   
               doc.ATG_Request_Number__c =  RT_AccountMap.get(docdetails.ATG_Employee__c).Request_Number__c;    
               
               documentDetailMap.put(docdetails.Id,doc);
               if(!STRING.ISBLANK(docdetails.ATG_Document_ID__c) && docdetails.ATG_Document_ID__c.containsAny(','))
                   contentDocsList.addAll(docdetails.ATG_Document_ID__c.split(','));
               else if(!STRING.ISBLANK(docdetails.ATG_Document_ID__c))
                   contentDocsList.add(docdetails.ATG_Document_ID__c);
               
               // Remove the docs from the map to filter the missing documents per emp              
               if(docsForEachEmp.get(docdetails.ATG_Employee__c).contains(docdetails.ATG_Document_Name__c) ){                  
                  if(docsForEachEmp.get(docdetails.ATG_Employee__c).size() == 1)
                    docsForEachEmp.remove(docdetails.ATG_Employee__c);
                  else
                    docsForEachEmp.get(docdetails.ATG_Employee__c).remove(docdetails.ATG_Document_Name__c);                 
                }  
                
                
                
               
           } 
           
           

            if(documentDetailMap.values().size()>0){
                insert documentDetailMap.values(); 
                
                // Insert ContentDocs for New Doc Detail Recs Created                
                if(contentDocsList !=null && !contentDocsList.isEmpty() && documentDetailMap !=null && documentDetailMap.size()>0)
                    ATG_CommunityETORequestController.CreateContentDocuments(documentDetailMap,contentDocsList); 
               
            }
            
            
            
             if(docsForEachEmp.keySet().size()>0){
               
               for(id empId : docsForEachEmp.KeySet()){
                   if(!docsForEachEmp.get(empId).isEmpty()){
                    list<string> docNamesList = new list<string>(docsForEachEmp.get(empId));
                           for(string docName : docNamesList){
                               ATG_Document_Details__c  doc = new ATG_Document_Details__c(ATG_Document_Name__c =docName,Requested_Travelers__c= RT_AccountMap.get(empId).Id,ATG_Document_Description__c = docNames.get(docName).Document_Discription__c);
                               doc.ATG_Request_Number__c =  RT_AccountMap.get(empId).Request_Number__c; 
                               documentDetailsToInsert.add(doc);
                           }
                   }
               }

                
            
            }    
       }

       if(RTDependantList !=null && RTDependantList.size()>0){
            for(ATG_Requested_Traveller__c rt : RTDependantList){
                for(RT_Required_Docs_Community__c docNam : docNames.Values()){
                    ATG_Document_Details__c  doc = new ATG_Document_Details__c(ATG_Document_Name__c =docNam.Name,Requested_Travelers__c= rt.Id,ATG_Document_Description__c = docNam.Document_Discription__c,ATG_Request_Number__c = rt.Request_Number__c);
                    documentDetailsToInsert.add(doc);
                }
            }
       }
        
        if(documentDetailsToInsert !=null && documentDetailsToInsert.size()>0)
            insert documentDetailsToInsert;
       
     }catch(exception e){
         
         ATG_utility.LogException(e);
     }  
                            
   }

    @AuraEnabled
    public static User fetchUserDetails(){
        return  ATG_PortalHomePageUtility.GetUserDetails(null);
    }
    
    
 /*   
   @AuraEnabled
   public static string ProcessApproval(string caseId,string comments,string priority,string approvalEvent){  // 1 --> Approval 2 --> Send Back 3--> Reject
       
        if(!string.isEmpty(caseId)){
             if(approvalEvent == '1')
                 return ATG_CommunityApprovalProcessController.ProcessApproval(new list<string>{caseId},comments,'Approve');
              else if(approvalEvent == '2')
                 return ATG_CommunityApprovalProcessController.ProcessApproval(new list<string>{caseId},comments,'Send Back'); 
               else if(approvalEvent == '3')
                 return ATG_CommunityApprovalProcessController.ProcessApproval(new list<string>{caseId},comments,'Reject');                         
        }
        
        return 'Error';   
      
       
   }
  */ 
   
   
   @AuraEnabled
    public static map<string,list<string>> GetDocumentDetails(string caseId,list<string> documentIdList){
        map<string,list<string>> documentContentMap = new map<string,list<string>>();
        
        if((documentIdList == null || documentIdList.isEmpty()) && caseId !=null){
            for(ATG_Document_Details__c dd : [select Id from ATG_Document_Details__c where ATG_Request_Number__c = : caseId]){
                documentIdList.add(dd.Id);  
                documentContentMap.put(dd.Id,new list<string>());
            }   
        }       
        
        
        
        if(documentIdList !=null && !documentIdList.isEmpty()){
            for(ContentDocumentLink cl : [select LinkedEntityId,contentDocumentId,contentDocument.Createddate from ContentDocumentLink where LinkedEntityId in : documentIdList order by contentDocument.Createddate Desc])
                    documentContentMap.get(cl.LinkedEntityId).add(cl.contentDocumentId);
        } 
        
        return documentContentMap;  
        
    }
    
    
    
    public static void CreateContentDocuments(map<id,ATG_Document_Details__c> oldDoc_NewDocDetails,list<Id> contentDocsList){ 
        
        // Get All the Content Type Ids
        
        
        set<id> DocIdSet = oldDoc_NewDocDetails.keySet();
        map<id,List<ContentVersion>> contentVersionMap = new map<id,List<ContentVersion>>();
        map<string,ContentVersion> Doc_ContentDocversion = new map<string,ContentVersion>();
        map<string,string> Doc_ContentDocMap = new map<string,string>();
        list<Id> contentDocList = new list<Id>();
        map<id,id> versionId_OldDocId = new map<id,Id>();
        map<id,id> versionId_NewDocId = new map<id,Id>();        
        
        string ContentDocumentLinkQuery = 'select ID,ContentDocumentId,LinkedEntityId from ContentDocumentLink where LinkedEntityId in : DocIdSet   and  ContentDocumentId IN  (' ;
        string docIdStrings =''; 
        for(string docI : contentDocsList)
            docIdStrings = '\''+docI+'\',';
            
            docIdStrings += docIdStrings.removeEnd(',');
        
        ContentDocumentLinkQuery +=docIdStrings +')';
        
        
        
        for(ContentDocumentLink cd : Database.Query(ContentDocumentLinkQuery)) // select ID,ContentDocumentId,LinkedEntityId from ContentDocumentLink where LinkedEntityId IN : DocIdSet and ContentDocumentId in : contentDocsList
       //  for(ContentDocumentLink cd : [select ID,ContentDocumentId,LinkedEntityId from ContentDocumentLink where LinkedEntityId IN : DocIdSet and ContentDocumentId in : contentDocsList and LinkedEntityId  !=null and ContentDocumentId !=null])
            Doc_ContentDocMap.put(cd.ContentDocumentId,cd.LinkedEntityId);               
            
       
        
        // Clone Content Version 
        
        for(ContentVersion cv : [select id,Description,ContentDocumentId,FileExtension,FileType,VersionData,ContentLocation,PathOnClient,
                                        Title from contentVersion where IsLatest=true and contentDocumentId in : Doc_ContentDocMap.keySet() and contentDocumentId  !=null]){                
                ContentVersion cvObj = new  ContentVersion(ContentLocation = cv.ContentLocation,PathOnClient =cv.PathOnClient,Title =cv.Title,VersionData =cv.VersionData);
                Doc_ContentDocversion.put(cv.ContentDocumentId,cvObj);                           
                                        
        }
        
        
        

        if(Doc_ContentDocversion !=null && Doc_ContentDocversion.size() >0)
            insert Doc_ContentDocversion.values();
        
        
        
        for(Id oldDocId : Doc_ContentDocversion.keySet())
            versionId_OldDocId.put(Doc_ContentDocversion.get(oldDocId).Id,oldDocId);
        
        for(ContentVersion cvNew : [select id,Description,ContentDocumentId,FileExtension,FileType,VersionData,ContentLocation,PathOnClient,
                                        Title from contentVersion where IsLatest=true and Id in : versionId_OldDocId.keySet()]){
                                        
            versionId_NewDocId.put(cvNew.Id,cvNew.ContentDocumentId);                           
        }   

        // Create Content Document Links
        List<ContentDocumentLink> cdLinkToInsert = new list<ContentDocumentLink>();
        for(Id versionID : versionId_NewDocId.keySet()){
            ContentDocumentLink cDe = new ContentDocumentLink();
            cDe.ContentDocumentId = versionId_NewDocId.get(versionID);
            cDe.LinkedEntityId = oldDoc_NewDocDetails.get(Doc_ContentDocMap.get(versionId_OldDocId.get(versionID))).Id;
            cDe.ShareType = 'I'; 
            cDe.Visibility = 'AllUsers';
            cdLinkToInsert.add(cDe);    
        }
        
        if(cdLinkToInsert !=null)
            insert cdLinkToInsert;         
         
         
         // Get the ContentDocument for New Versions
         
        
            
   }


    @AuraEnabled
    public static Map<String, List<Portal_Traveller_Field_Setting__mdt>> getTravellerFieldMetadata(){


        List<Portal_Traveller_Field_Setting__mdt> employeeFields = [
                SELECT Field_Api_Name__c, Portal_Account__c, TravellerType__c, Readonly__c, Required__c, Order__c, ATG_ID_Type_Field__c
                FROM Portal_Traveller_Field_Setting__mdt
                WHERE Active__c = true AND TravellerType__c
                        in (SELECT id FROM Portal_Traveller_Type__mdt WHERE DeveloperName='Employee')
                ORDER BY Order__c ASC
        ];

        List<Portal_Traveller_Field_Setting__mdt> dependentFields = [
                SELECT Field_Api_Name__c, Portal_Account__c, TravellerType__c, Readonly__c, Required__c, Order__c, ATG_ID_Type_Field__c
                FROM Portal_Traveller_Field_Setting__mdt
                WHERE Active__c = true AND TravellerType__c
                        in (SELECT id FROM Portal_Traveller_Type__mdt WHERE DeveloperName='Dependent')
                ORDER BY Order__c ASC
        ];

        List<Portal_Traveller_Field_Setting__mdt> newEmployeeFields = [
                SELECT Field_Api_Name__c, Portal_Account__c, TravellerType__c, Readonly__c, Required__c, Order__c, ATG_ID_Type_Field__c
                FROM Portal_Traveller_Field_Setting__mdt
                WHERE Active__c = true AND TravellerType__c
                        in (SELECT id FROM Portal_Traveller_Type__mdt WHERE DeveloperName='New_Employee')
                ORDER BY Order__c ASC
        ];

        List<Portal_Traveller_Field_Setting__mdt> visitorFields = [
                SELECT Field_Api_Name__c, Portal_Account__c, TravellerType__c, Readonly__c, Required__c, Order__c, ATG_ID_Type_Field__c
                FROM Portal_Traveller_Field_Setting__mdt
                WHERE Active__c = true AND TravellerType__c
                        in (SELECT id FROM Portal_Traveller_Type__mdt WHERE DeveloperName='Visitor')
                ORDER BY Order__c ASC
        ];



        Map<string, List<Portal_Traveller_Field_Setting__mdt>> fieldsMetadata = new Map<string, List<Portal_Traveller_Field_Setting__mdt>>();

        fieldsMetadata.put('EmployeeFieldSet', employeeFields);
        fieldsMetadata.put('DependentFieldSet', dependentFields);
        fieldsMetadata.put('NewEmployeeFieldSet', newEmployeeFields);
        fieldsMetadata.put('VisitorFieldSet', visitorFields);

        return fieldsMetadata;
    }

    /**************** Resume flows *****************/
    @AuraEnabled
    public static List<FlowRecordRelation> getInterviews(Id recordId) {
        if(recordId != null){
            return [ SELECT ParentId, Parent.InterviewLabel, Parent.PauseLabel,Parent.CurrentElement, Parent.CreatedDate, Parent.Owner.Name  FROM FlowRecordRelation WHERE RelatedRecordId = :recordId ];
        }
        return [ SELECT ParentId, Parent.InterviewLabel, Parent.PauseLabel,Parent.CurrentElement, Parent.CreatedDate, Parent.Owner.Name   FROM FlowRecordRelation  LIMIT 10];
    }

    @AuraEnabled
    public static FlowInterview deleteInterview(Id interviewId) {
        list<FlowInterview> interview = [Select Id from FlowInterview Where Id = :interviewId];
        if(interview !=null && !interview.isEmpty()){
            delete interview;
            return interview[0];
        }
        
        return null;
    }
    
    @AuraEnabled
    public static List<ContentVersion> getDocumentsList(string csId) {
        list<string> documentIdList = new list<string>();
        list<Id> contentdocumentList = new list<Id>();
        system.debug('Here is csId'+csId);
        
        if (csId !=null){
            for(ATG_Document_Details__c dd : [select Id,ATG_Document_Name__c,ATG_Document_ID__c,ATG_Request_Number__c,ATG_Is_Uploaded__c from ATG_Document_Details__c where ATG_Request_Number__c = : csId and ATG_Is_Uploaded__c=TRUE]){
                documentIdList.add(dd.Id);  
            }  
        }
        system.debug('Here is documentIdList'+documentIdList);
        
        List<ContentDocumentLink> toUpdate = new List<ContentDocumentLink>();
        for(ContentDocumentLink link : [select Id, Visibility, LinkedEntityId, ShareType from ContentDocumentLink where Visibility != 'AllUsers' and LinkedEntityId in : documentIdList]){
            link.Visibility = 'AllUsers';
            link.ShareType='V';
            toUpdate.add(link);
        }
        update toUpdate;
        
        System.debug('toUpdate--->>'+toUpdate);
        system.debug('documentIdList--->>'+documentIdList);
        
        if(documentIdList !=null && !documentIdList.isEmpty()){
            for(ContentDocumentLink cl : [select Id,LinkedEntityId,contentDocumentId,contentDocument.Createddate,ContentDocument.LatestPublishedVersionId from ContentDocumentLink where LinkedEntityId in : documentIdList order by contentDocument.Createddate Desc]){
                contentdocumentList.add(cl.contentDocumentId);
            }
        }
        system.debug('Here is contentdocumentList==>'+contentdocumentList);
        return [SELECT Id,Title,ContentUrl,ContentDocumentId,PublishStatus,ContentSize FROM ContentVersion WHERE ContentDocumentId IN : contentdocumentList];
        //return [select Id,LinkedEntityId,contentDocumentId,contentDocument.Createddate,ContentDocument.LatestPublishedVersionId from ContentDocumentLink where LinkedEntityId in : documentIdList order by contentDocument.Createddate Desc];
    }
    
       
    
    
    
    
    
}