@istest
private class ViewSurveyController_Test{
    //------------------------------------------------------------------------------//
    //------------------------------------------------------------------------------//
    private static Testmethod void testViewSurveyController() {
        SurveyTestingUtil tu = new SurveyTestingUtil();
        Apexpages.currentPage().getParameters().put('id',tu.surveyId);
        
        //Account
        Account newAcc= new Account();
        newAcc.LastName = 'Test';
        newAcc.PersonMobilePhone = '+971 555555555';
        insert newAcc;
        
        //Contact
        Contact newContact= new Contact();
        newContact.LastName = 'Test';
        insert newContact;
        
        Case objcs = new Case();
        objcs.AccountId = newAcc.Id;
        for(QueueSobject obj : [select Id,QueueId from QueueSobject where SobjectType = 'Case' limit 1]){
            objcs.OwnerId = obj.QueueId;
        }
        objcs.RecordTypeId = ATG_ConstantsUtility.AMH_CASE_RECORDTYPE;
        objcs.Follow_up_Date__c = system.today().addDays(5);
        objcs.Mobile_Number__c = '123456';
        objcs.Country_Code__c = 'Afghanistan (+93)';
        insert objcs;
        
        ATG_Booking__c book = new ATG_Booking__c(Name = 'TestBook',
                              ATG_Product_Type__c='Flight',
                              ATG_Email__c ='test@test.com',
                              ATG_Travel_End_Date__c = system.today());
                              
        insert book;
                              
        Apexpages.currentPage().getParameters().put('aId',newAcc.Id);
        Apexpages.currentPage().getParameters().put('bID',book.Id);
        Apexpages.currentPage().getParameters().put('caId',objcs.Id);
        Apexpages.currentPage().getParameters().put('cId',newContact.Id);
        Apexpages.currentPage().getParameters().put('oId',userInfo.getUserID());
        Apexpages.currentPage().getParameters().put('chatKey','dummy');
        
        ATG_Survey_Rating__c config = new ATG_Survey_Rating__c();
        config.name = '1';
        config.ATG_CSAT_Score__c = 1;
        config.ATG_Survey_Response__c = 'three';
        insert config;
        
        LiveChatVisitor lcv = new LiveChatVisitor();
        insert lcv;
        
        LiveChatTranscript lct = new LiveChatTranscript();
        lct.LiveChatVisitorid = lcv.id;
        lct.ChatKey__c = 'dummy';
        lct.accountID = newAcc.Id;
        lct.caseId = objcs.id;
        lct.contactId = newContact.id;
        lct.Last_Name__c = 'Test';
        lct.Mobile_Country_Code__c = 'United Arab Emirates (+971)';
        lct.Mobile_Number__c = '555555555';
        insert lct;
        
        LiveChatTranscriptEvent le = new LiveChatTranscriptEvent();
        le.LiveChatTranscriptId = lct.id;
        le.type = 'ChatRequest';
        le.time = system.now();
        insert le;
         
        Apexpages.Standardcontroller stc;
        ViewSurveyController vsc = new ViewSurveyController(stc); 
        vsc.init();

        vsc.submitResults();
        for (SFQuestion q : vsc.allQuestions)
        {
            q.selectedOption = String.valueof(2);
            q.choices = String.valueof(2);
            q.selectedOptions = new List<String>();
            q.selectedOptions.add(String.valueof(2));
            q.renderFreeText = 'true';
            vsc.submitResults();
        }
        
        vsc.anonymousAnswer = 'Anonymous';
        vsc.submitResults();
        
        vsc.resultPage();
        vsc.getResponses();
        vsc.checkExistingSurvey();
        vsc.updateSurveyName();
        vsc.updateSurveyThankYouAndLink();
        vsc.refreshQuestionList(); 
        
        Survey_Question__c tempSQ = new Survey_Question__c();
        tempSQ.Required__c = true;
        
        ViewSurveyController.checkRequired(null, tempSQ);
    }
    
    private static Testmethod void testupdateSurveyThankYouAndLink_Component() {
        SurveyTestingUtil tu = new SurveyTestingUtil();
        Apexpages.currentPage().getParameters().put('id',tu.surveyId);
        Apexpages.Standardcontroller stc;
        viewShareSurveyComponentController vsc;
        ViewSurveyController vscNew = new ViewSurveyController(vsc);
        
        
        vscNew.anonymousAnswer = 'Anonymous';
        vscNew.questionName = '';
        vscNew.questionType = '';
        vscNew.singleOptions = new list<SelectOption>();
        vscNew.responses = new list<string>();
        vscNew.questionRequired = false;
        vscNew.qRequired = false;
        vscNew.thumbsUpQuestionMap = new Map<String,String>();
        
        for (SFQuestion q : vscNew.allQuestions)
        {
            q.selectedOption = String.valueof(2);
            q.choices = String.valueof(2);
            q.selectedOptions = new List<String>();
            q.selectedOptions.add(String.valueof(2));
            
            vscNew.submitResults();
        }
    }
    
   

}