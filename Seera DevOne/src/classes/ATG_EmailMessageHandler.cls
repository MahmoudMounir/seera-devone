/**************************************************************************************************
* Name               : ATG_EmailMessageHandler                                                               
* Description        : Trigger Handler for the EmailMessage SObject. This class implements the ITrigger
             interface to help ensure the trigger code is bulkified and all in one place.                                       
* Created Date       : 14th Oct2018                                                                *
* Created By         : Leeba Shibu (PWC Digital)                                              
* -------------------------------------------------------------------------------------------------
* Version    Author    Date         Comment                  
MVP2 V1.0    Sravan    04-02-2019   Added a custom logic to skip Account linking when case is created
                                    through email2Case through specific emails.                                              *
MVP2 V1.1    Diana     20-02-2019   If the Email from Fulfillment Team contains booking ID then link the booking to 
                                    the case created.
MVP2 V1.2    Diana     27-02-2019   If the Email comes from bank then exclude customer linking to case 
MVP2 V1.3    Diana     17-03-2019   Mark Is External Communication flag on Case
MVP2 V1.4    Ravi      02-04-2019   Added the logic to change the status to In Progress and assign it back to Queue
CoE  V1.5    PwC       30-07-2019   Added the logic to make case status as in progress if the any new email comes
**************************************************************************************************/
 
 
public without sharing class ATG_EmailMessageHandler implements ATG_TriggerInterface {
    // Constructor
    public void ATG_EmailMessageHandler(){
    }
 
    public void beforeInsert(list<SObject> soNewList){
    }
  
    public void beforeUpdate(list<sobject> soNewList, map<id,sobject> soOldMap){
    }
 
    public void beforeDelete(list<SObject> soNewList){}
 
    public void afterInsert(list<SObject> soNewList){
        try{
            UpdateCaseOwner(soNewList);
            ATG_EmailMessageHandler.UpdateCase_Email2CaseConfigs((list<EmailMessage>)soNewList);
            updateExternalCommunicationFlag(soNewList);//v1.3
        }catch(exception ex){ 
            ATG_utility.LogException(ex);
        }
    }
 
    public void afterUpdate(list<SObject> soNewList,map<id,SObject> soOldMap){
 
    }
    
    public void afterDelete(list<SObject> soNewList){}
    
    //Method to update the Case Owner to queue if the Owner is not Online
    public static void UpdateCaseOwner(list<EmailMessage> lstNew){
        set<string> setCaseOwnerIds = new set<string>();
        map<string,boolean> MapUserOnlineStatus = new map<string,boolean>();
        map<string,Case> MapCaseTBU = new map<string,Case>();
        set<string> setCluRecTypes = new set<string>{
            ATG_constantsUtility.Case_Contract_Generic_RecordType,
            ATG_constantsUtility.Case_Contract_Generic_RecordType.subString(0,15),
            ATG_constantsUtility.Case_Contract_Stop_Sale_RecordType,
            ATG_constantsUtility.Case_Contract_Stop_Sale_RecordType.subString(0,15)
        };
        
        for(EmailMessage msg:lstNew){
            if(msg.Incoming && msg.Case_Owner_Id__c.startswith('005') && 
               (msg.BU_From_case__c==ATG_constantsUtility.ALTAYYAR_BusinessUnit || setCluRecTypes.contains(msg.ATG_Case_Record_Type_Id__c) ||
                    (msg.BU_From_case__c == ATG_ConstantsUtility.ALMOSAFER_BusinessUnit &&
                    msg.ATG_Case_Queue_Name__c != null && msg.ATG_Case_Queue_Name__c.contains('Sales')))){
                setCaseOwnerIds.add(msg.Case_Owner_Id__c);
            }
        }
        if(setCaseOwnerIds.size()>0)
            MapUserOnlineStatus = ATG_utility.getUserPresenceStatus(setCaseOwnerIds);
            
        for(EmailMessage msg:lstNew){
            if(setCaseOwnerIds.size()>0 && msg.Incoming && msg.Case_Owner_Id__c.startswith('005') && MapUserOnlineStatus.get(msg.Case_Owner_Id__c)==null && msg.Case_Queue_Id__c!=null){
                MapCaseTBU.put(msg.ParentId,new Case(Id=msg.ParentId,OwnerId=msg.Case_Queue_Id__c));
            }
            system.debug('msg.ATG_Case_Record_Type_Id__c '+msg.ATG_Case_Record_Type_Id__c);
            system.debug('setCluRecTypes '+setCluRecTypes);
            //1.4
            if( msg.Incoming && msg.Case_Owner_Id__c.startswith('005') && msg.Case_Queue_Id__c != null && msg.Case_Status__c != 'Closed' &&
              (msg.BU_From_case__c == ATG_ConstantsUtility.ALMOSAFER_BusinessUnit || msg.BU_From_case__c == ATG_ConstantsUtility.TAJAWAL_BusinessUnit || setCluRecTypes.contains(msg.ATG_Case_Record_Type_Id__c))){
                
				if(MapCaseTBU.containsKey(msg.ParentId)){
                    MapCaseTBU.get(msg.ParentId).Status = ATG_ConstantsUtility.STR_IN_PROGRESS;
                }else{
                    MapCaseTBU.put(msg.ParentId,new Case(Id = msg.ParentId, Status = ATG_ConstantsUtility.STR_IN_PROGRESS));
				}
                if(!setCluRecTypes.contains(msg.ATG_Case_Record_Type_Id__c))
                	MapCaseTBU.get(msg.ParentId).OwnerId = msg.Case_Queue_Id__c;
            }
            
        }
        
        if(MapCaseTBU.size()>0)
            update MapCaseTBU.values();
    }
 
 
    //MVP2 v1.0 Method to handle various actions on case based on Email2Case Config custom setting
    private static void UpdateCase_Email2CaseConfigs(list<EmailMessage> emailsNewList){
        map<string,Case> caseMapToUpdate = new map<string,case>();
        set<string> fulfillmentTeamEmailSet = new set<string>();
        Set<String> excludeCustomerLinkingSet = new Set<String>();
        boolean isFulfillmentEmail = false;      
        Set<Id> caseId = new Set<Id>();
        for(ATG_Email_to_Case_Configurations__c ec : ATG_Email_to_Case_Configurations__c.getAll().values()){
           
              //MVP2 V1.2
            if(ec.ATG_Is_Customer_Linking_to_be_excluded__c && !string.isBlank(ec.ATG_From_Email__c)){
                excludeCustomerLinkingSet.add(ec.ATG_From_Email__c.toLowerCase());
            }
            if(ec.ATG_Is_Customer_Linking_to_be_excluded__c && !string.isBlank(ec.ATG_To_Email__c)){
                excludeCustomerLinkingSet.add(ec.ATG_To_Email__c.toLowerCase());
            }
        }
        
        //MVP2 V1.1 :START
        // Iterate over the emailmessage list, and get the booking number from the subject
        // The Subject Pattern should be ID-BookingNo::
        // To link it to the right case use the map of email message id and bookingno
        Map<Id,Id> EmailIdAndBookingIdMap = new Map<Id,Id>();
        Map<String,String> EmailIdBookingNoMap = new Map<String,String>();
        Set<String> bookingNoSet = new Set<String>();
        Map<Id,Id> BookingIdAndAccountMap = new Map<Id,Id>();
        Map<Id,Id> BookingIdAndContactMap = new Map<Id,Id>();
        Map<Id,Id> caseAccountMap = new Map<Id,Id>();
        for(EmailMessage emailMsg:emailsNewList){
            String bookingID = null;
            if(null != emailMsg.Subject && emailMsg.Subject.indexOf('ID-')>-1 && emailMsg.Subject.indexOf(':')>-1){
                 bookingID = emailMsg.Subject.substring(emailMsg.Subject.indexOf('ID-')+3,emailMsg.Subject.lastIndexOf(':'));
                if(null != bookingID){
                    bookingNoSet.add(bookingID);
                    EmailIdBookingNoMap.put(bookingID,emailMsg.Id);
                    
                }
            }
            if(emailMsg.ParentId != null && 
               String.valueOf(emailMsg.ParentId).startsWith(ATG_Utility.getKeyPrefix('Case'))){
              caseId.add(emailMsg.ParentID);
            }
            system.debug('$$$BookingID'+bookingID);
        }
        
        //get the salesforce lookup id based on the booking number
        if(!bookingNoSet.isEmpty()){
          for(ATG_Booking__c objBooking:[SELECT Id,Name,ATG_Customer__c,ATG_Customer__r.PersonContactId FROM ATG_Booking__C WHERE Name IN: bookingNoSet]){
              EmailIdAndBookingIdMap.put(EmailIdBookingNoMap.get(objBooking.Name),objBooking.Id);
              BookingIdAndAccountMap.put(objBooking.Id,objBooking.ATG_Customer__c);
              BookingIdAndContactMap.put(objBooking.Id,objBooking.ATG_Customer__r.PersonContactId);
          }
        }
        //MVP2 1.1 END
        //
        
        system.debug('^caseId:'+caseId);
        
        for(Case objCase:[SELECT Id,AccountId,ATG_BU_From_Case__c FROM Case WHERE Id IN:caseId AND
                          (ATG_BU_From_Case__c=:ATG_ConstantsUtility.ALMOSAFER_BusinessUnit OR
                           ATG_BU_From_Case__c=:ATG_ConstantsUtility.TAJAWAL_BusinessUnit) AND AccountId!=null]){
              caseAccountMap.put(objCase.Id,objCase.AccountId);                 
                                 
        }
        
        system.debug('^caseAccountMap'+caseAccountMap);
        
        for(EmailMessage em : emailsNewList){
            
            system.debug('^accountId'+caseAccountMap.get(em.ParentId));
          
            for(string eml : em.fromAddress.split(';')){
                if( em.Incoming && em.ParentId !=null && !EmailIdBookingNoMap.isEmpty() && 
                  EmailIdAndBookingIdMap.containsKey(em.Id)){
                    //MVP2 V1.1 - Added Booking
                    String accountId = (EmailIdAndBookingIdMap.get(em.Id) != null && !BookingIdAndAccountMap.isEmpty() && BookingIdAndAccountMap.get(EmailIdAndBookingIdMap.get(em.Id)) != null)?BookingIdAndAccountMap.get(EmailIdAndBookingIdMap.get(em.Id)):null;
                    system.debug('accountId'+accountId);
                    String contactId = (EmailIdAndBookingIdMap.get(em.Id) != null && !BookingIdAndContactMap.isEmpty() && BookingIdAndContactMap.get(EmailIdAndBookingIdMap.get(em.Id)) != null)?BookingIdAndContactMap.get(EmailIdAndBookingIdMap.get(em.Id)):null;
                   
                    caseMapToUpdate.put(em.ParentId,new case(id=em.ParentId,accountID=accountId,ContactId=contactId,ATG_Booking__C=EmailIdAndBookingIdMap.get(em.Id)));
                
                }//MVP2 V1.2
                
                else if(excludeCustomerLinkingSet != null && 
                        !excludeCustomerLinkingSet.isEmpty() && em.Incoming &&
                        em.ParentId != null && excludeCustomerLinkingSet.contains(eml.toLowerCase()) 
                       && caseAccountMap.get(em.ParentId) == null){
                             caseMapToUpdate.put(em.ParentId,new Case(id=em.ParentId,accountID=null,contactID=null));
                           
                }
            }
            if(em.fromAddress != null){ //added By ravi for null check
              for(string emlFrom : em.fromAddress.split(';')){
                  //MVP2 V1.2
                  if(excludeCustomerLinkingSet != null && !excludeCustomerLinkingSet.isEmpty() &&
                       em.Incoming && em.ParentId != null &&
                       excludeCustomerLinkingSet.contains(emlFrom.toLowerCase())
                       ){
                           
                           if(caseAccountMap.get(em.ParentId) == null){
                             caseMapToUpdate.put(em.ParentId,new Case(id=em.ParentId,accountID=null,contactID=null,ATG_Do_not_send_auto_emails__c=true));
                           }else{
                                 caseMapToUpdate.put(em.ParentId,new Case(id=em.ParentId,ATG_Do_not_send_auto_emails__c=true));
                           }
                    }
              }
            }
        }
        
        if(caseMapToUpdate !=null && !caseMapToUpdate.values().isEmpty()){
            ATG_ConstantsUtility.CREATE_ACCOUNTONCASECREATE = false;
            Update caseMapToUpdate.values();   
        }        
    }
    
     //v1.3
    //If the From Address is same as case contact email then set the external communication flag
    private static void updateExternalCommunicationFlag(list<EmailMessage> emailsNewList){
        List<Case> casesToUpdateList = new List<Case>();
       
        for(EmailMessage emailMsgObj:emailsNewList){
            system.debug('^^^^'+emailMsgObj);
            if(emailMsgObj.Incoming && 
               !emailMsgObj.ATG_Case_External_Communication_Flag__c && emailMsgObj.BU_From_Case__c != null &&
               (emailMsgObj.BU_From_Case__c.equalsIgnoreCase(ATG_ConstantsUtility.ALMOSAFER_BusinessUnit) || 
               emailMsgObj.BU_From_Case__c.equalsIgnoreCase(ATG_ConstantsUtility.TAJAWAL_BusinessUnit))
             ){
                 casesToUpdateList.add(new Case(Id=emailMsgObj.ParentId,ATG_Email_Message_From_Address__c=emailMsgObj.FromAddress));

            }
        }
        
        system.debug('^^^'+casesToUpdateList);
        if(!casesToUpdateList.isEmpty() && casesToUpdateList.size()>0){
            update casesToUpdateList;
        }
    }    
}