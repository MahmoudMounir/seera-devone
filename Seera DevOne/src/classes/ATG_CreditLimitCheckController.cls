/**************************************************************************************************
* Name               : ATG_CreditLimitCheckController                                             *                                                                        
* Description        : Used to get the Credit Limit of a customer                                 *
* Created Date       : 12/17/2018                                                                 * 
* Created By         : PWC                                                                        *
* ------------------------------------------------------------------------------------------------*
* VERSION  AUTHOR    DATE            COMMENTS                                                     *
* 1.0      Diana    12/17/2018      Initial Draft.                                                *         
**************************************************************************************************/
public class ATG_CreditLimitCheckController {
    
    /****************************************************************************************
    @Description - This method is used to get the credit limit of a customer
    @Param - Account Record Id
    @Return - Message 
    ****************************************************************************************/
    @AuraEnabled
    public static string CreditLimitCheckForCustomer(String recordId){    
        
        String customerId = null;   
         
        for(Account accObj:[SELECT ATG_Tapro_ID__c FROM Account WHERE Id=:recordId]){
            customerId = accObj.ATG_Tapro_ID__c;
        }
        
        if(customerId != null){
            ATG_FinCreditlimitcheck.Record_element response = ATG_WebserviceUtilityController.creditLimitCheck(customerId);
            system.debug('ATG_CreditLimitCheckController - CreditLimitCheckForCustomer '+response);
            if(response != null && response.Message == null && response.CreditAvailable != null){
                
               /* Account updateAccount = new Account(Id=recordId);
                updateAccount.ATG_Credit_Limit__c = response.CreditAvailable;
                update updateAccount;*/
                
                return Label.Credit_Limit_Available_Message+response.CreditAvailable;
            }else if(response.Message != null){
                return response.Message;
            }else{
                return Label.Exception_Message;
            }
        }else{
            return Label.Invalid_Customer_ID;
        }
        
        
    }
    
    
    
    
    /****************************************************************************************
    @Description - Method to check accessibility using ATG_Credit_Check_Accessibility__c  CS
    @Return - Message 
    ****************************************************************************************/
    
    @AuraEnabled
    public static string CheckAccessibility(){        
        
        map<string,ATG_Credit_Check_Accessibility__c> creditCheckAccessMap = new map<string,ATG_Credit_Check_Accessibility__c>();
        User usrDtl = ATG_PortalHomePageUtility.GetUserDetails('');
        for(ATG_Credit_Check_Accessibility__c ac : ATG_Credit_Check_Accessibility__c.getAll().values()){
            if(ac.ATG_Active__c)
                creditCheckAccessMap.put(ac.ATG_Profile_Name__c.toLowerCase(),ac);
        } 
        
        if(creditCheckAccessMap !=null && creditCheckAccessMap.size()>0 && creditCheckAccessMap.containsKey(usrDtl.Profile.Name.tolowerCase()))   
            return 'Success';
        else 
            return Label.ATG_No_Access_Message;   
    
    }
    
    
    
    
}