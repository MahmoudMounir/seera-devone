/**************************************************************************************************
* Name               : ATG_FetchPNR                                                               
* Description        : Class contains method for Retrieve GDS Booking function                                       
* Created Date       : 10th June 2018                                                                *
* Created By         : PWC Digital                                          
* -------------------------------------------------------------------------------------------------
* Version       Author      Date        Comment                                                               *

**************************************************************************************************/
global without sharing class ATG_FetchPNR {
    
    static string strKey = 'U8907654HG0765835671KLM67BTSRA12';
    static Blob cryptoKey = Blob.valueOf(strKey);
    global static string userName = '';
    global static string password = '';  
    global static string endPointURL = '';  
    global static string accessToken = '';  
    global static string PNRNumber {get; set;}
    global static string caseId {get; set;} 
    global static string ipAddress {get; set;}
    global static string browserName {get; set;}
    
    public static fetchPNR objPNR;
    public static case objCase; 
    public static map<id,ATG_Requested_Traveller__c> requestedTraveller;
    
    public static PNR__c pnr;
    
    public static map<string,string> message = new map<string,string>();
    
    public ATG_FetchPNR(){ 
        ATG_FetchPNR.PNRNumber = '';
        ATG_FetchPNR.caseId = '';
        ATG_FetchPNR.ipAddress = '';
        ATG_FetchPNR.browserName = '';
    }   
    
    
    @AuraEnabled public static map<string, string> getCredentials(string PNRNumber,string caseID,string ipAddress,string browserName){
        message.put('Success', 'Nothing Happened');
        try{
            ATG_FetchPNR.PNRNumber = PNRNumber;
            ATG_FetchPNR.caseId = caseID;
            ATG_FetchPNR.ipAddress = ipAddress;
            ATG_FetchPNR.browserName = browserName;
            
            MOH_Fetch_PNR__c objCredentials = new MOH_Fetch_PNR__c();
            objCredentials = MOH_Fetch_PNR__c.getValues('MOH');
            if(objCredentials!=null){
                userName =  ATG_Utility.DecryptPassword(objCredentials.ATG_UserName__c);
                password =  EncodingUtil.urlEncode(ATG_Utility.DecryptPassword(objCredentials.ATG_Password__c), 'UTF-8');
               
                endPointURL = objCredentials.ATG_Endpoint_URL__c;
            }
            
            if(string.isNotBlank(userName) && string.isNotBlank(password) && string.isNotBlank(endPointURL)){
                getAccessToken();
            }
        }catch(exception ex){
          message = new map<string,string>();
            message.put('Error', 'Some error occured, Please try again or contact system Admin.');
            ATG_utility.createErrorLog('getCredentials','Some error occured, Please try again or contact system Admin.','ATG_FetchPNR',PNRNumber,caseID);
            system.debug('-----msg & Line----'+ex.getmessage() +'-'+ex.getLineNumber());
        }
        return message;
    } 
    
    
    webservice static void getAccessToken(){
        HttpRequest req = new HttpRequest();
        req.setEndpoint(endPointURL+'oauth/token?username='+userName+'&password='+password);
        req.setMethod('POST');          
        req.setHeader('Content-Type','application/x-www-form-urlencoded');
        
        Http http = new Http();
        
        HttpResponse res = new HttpResponse();
    if(!test.isRunningTest())
          res = http.send(req);  
        
        JSONParser parser;
        if(res!=null)
          parser = JSON.createParser(res.getBody());
        
        while (parser!=null && parser.nextToken() != null) {
            if(res.getStatusCode() == 200){
                if ((parser.getCurrentToken() == JSONToken.FIELD_NAME) && (parser.getText() == 'access_token')) {
                    parser.nextToken();                
                    accessToken= parser.getText();
                }
            }
        }
        
        if(test.isRunningTest())
          accessToken = 'eyJhbGciOiJIUzI1NiJ9.eyJzdWIiOiJlZ292YXBpIiwic2NvcGVzIjpbInJlYWQiLCJ3cml0ZSJdLCJhdXRob3JpdGllcyI6W3siYXV0aG9yaXR5IjoiQWRkaXRpb25hbEluZm8jMzE4MTU3I00wMDAwMDIjQ0dBIn0seyJhdXRob3JpdHkiOiJBUElfTU9IIn1dLCJpc3MiOiJhdG8tY2xpZW50IiwiYXVkIjoiYXRvLXNlY3JldCIsImlhdCI6MTUzOTg0ODUzNSwiZXhwIjoxNTM5ODUwMzM1fQ.brklUehimvIoiXjYjtqGXea8S3ropuhvGw4O1owJIWA';
        
        if(string.isNotBlank(accessToken)){
            initializePNR();
        }
    }
    
    public static string requestBody = '';
    
    //initialize wrapper class variable objPNR
    public static void initializePNR(){
    
        
        JSONGenerator gen = JSON.createGenerator(true);
        string travelOrder = '';
        string ccgrNum = '';
        
        objPNR = new fetchPNR();
        ATG_FetchPNR.requestedTraveller = new map<Id, ATG_Requested_Traveller__c>();
        map<string,PNR_Passenger__c> pnrPassenger = new map<string,PNR_Passenger__c>();
        list<PNR_Passenger__c> pnrPassengerList = new list<PNR_Passenger__c>();        
        set<id> pnrPass = new set<id>();
        system.debug('----PNRNumber-----'+PNRNumber);
        
        if(string.isNotBlank(ATG_FetchPNR.PNRNumber)){
            list<PNR__c> tempPNRList = [select id,Name,ATG_Number_of_Adults__c,ATG_Trip_Type__c,ATG_Number_Of_Children__c,
                                               ATG_Number_Of_Infants__c,ATG_Number_Of_Passengers__c,
                                               ATG_airlineClass__c,ATG_infantSeatOrLap__c,                                           
                                               (select id,Name,ATG_AirLine_Code__c,ATG_First_Name__c,ATG_Last_Name__c,ATG_Travel_Order__c,
                                                       ATG_Passenger_Type__c,PNR__c,ATG_Route__c,ATG_Ticket_Number__c,
                                                       ATG_email__c,ATG_Nationality__c,ATG_ID_Type__c,ATG_National_ID__c,
                                                       ATG_Iqama_ID__c, ATG_Passport_Number__c, ATG_Passport_Issue_Country__c,
                                                       ATG_Passport_Expiry_Date__c,ATG_Mobile_Number__c,ATG_Gender__c,Status__c,Status_Length__c,ATG_Date_Of_Birth__c,ATG_SYS_CCGR__c,ATG_TripType__c,ATG_Booking_Class__c
                                                from PNR_Passengers__r where Status__c = 'Original' AND (Status_Length__c = 6 OR ATG_SYS_CCGR__c = 'INV'))
                                        from PNR__c where Name=:ATG_FetchPNR.PNRNumber];
            if(tempPNRList!=null && tempPNRList.size()>0){
                pnr = new PNR__c();
                pnr = tempPNRList[0];
                objPNR.requestId = pnr.id;
                objPNR.lang = 'en';
                objPNR.objReferer.ipAddress = ATG_FetchPNR.ipAddress;
                objPNR.objReferer.userAgent = ATG_FetchPNR.browserName;
                
                objPNR.objReferer.salesforceId = userInfo.getUserID();
                objPNR.objReferer.successFactorId = getEmployeeNumber();
                objPNR.ignoreWarning = 'Y';
                objPNR.pnrNumber = ATG_FetchPNR.PNRNumber;
               // objPNR.objCriteria.tripType = pnr.ATG_Trip_Type__c;
                objPNR.objCriteria.adult = integer.valueOf(pnr.ATG_Number_of_Adults__c);
                objPNR.objCriteria.child = integer.valueOf(pnr.ATG_Number_Of_Children__c);
                objPNR.objCriteria.infant = integer.valueOf(pnr.ATG_Number_Of_Infants__c);
               // objPNR.objCriteria.airlineClass = pnr.ATG_airlineClass__c;
                if(pnr.ATG_infantSeatOrLap__c!= '' && pnr.ATG_infantSeatOrLap__c!=null)
                    objPNR.objCriteria.infantSeatOrLap = pnr.ATG_infantSeatOrLap__c;
                else
                    objPNR.objCriteria.infantSeatOrLap = 'lap';
                    
                if(pnr.PNR_Passengers__r!=null && pnr.PNR_Passengers__r.size()>0){
                    for(PNR_Passenger__c obj : tempPNRList[0].PNR_Passengers__r){
                        if(string.isNotBlank(obj.ATG_Iqama_ID__c))
                            pnrPassenger.put(obj.ATG_Iqama_ID__c, obj);                        
                        if(string.isNotBlank(obj.ATG_National_ID__c))
                            pnrPassenger.put(obj.ATG_National_ID__c, obj);
                        
                        if(string.isNotBlank(obj.ATG_Passport_Number__c) )
                            pnrPassenger.put(obj.ATG_Passport_Number__c, obj);
                       
                            pnrPassengerList.add(obj);  
                        
                        pnrPass.add(obj.id);                         
                      travelOrder = obj.ATG_Travel_Order__c;
                      ccgrNum = obj.ATG_SYS_CCGR__c;
                      
                      objPNR.objCriteria.tripType = obj.ATG_TripType__c;
                      objPNR.objCriteria.airlineClass = obj.ATG_Booking_Class__c;
                    }
                    
                    
                    
                    
                    
                }
            }
        }
        
        system.debug('----pnrPassenger-------'+pnrPassenger);
        system.debug('----caseId-----'+ATG_FetchPNR.caseId); 
        if(string.isNotBlank(ATG_FetchPNR.caseId) && ((pnrPassenger!=null && pnrPassenger.size()>0) || pnrPassengerList.size()>0)){
            objCase = new case();
            list<case> tempCaseList = new list<case> ();
            tempCaseList = [select id,ATG_Total_PNR_Downloaded__c,Bypass_Passenger_Count_Validation__c,ATG_Total_Requested_Traveller__c,CaseNumber,ATG_Reference_Number__c,PNR_Number__c,ATG_Purpose_of_Visit__c,ATG_Traveller_Type__c,accountId,Account.Name,
                                   ATG_CC_CCGR_Number__r.name, ATG_CC_CCGR_Number__r.ATG_CCGR_Name__c,ATG_CC_CCGR_Number__r.ATG_Type__c,
                                   (select id,ATG_Nationality__c, ATG_National_ID__c,ATG_National_ID_Expiry_Date__c,ATG_Passport_Expiry_Date__c,
                                           ATG_Mobile_Number__c,ATG_Last_Name__c,ATG_ID_Type__c,ATG_Gender__c,ATG_First_Name__c,ATG_Passport_ID__c,
                                           ATG_Email_Address__c,ATG_Date_of_Birth__c,ATG_Travel_Order_Number__c,ATG_Passport_Issuing_Country__c, ATG_PNR_Downloaded__c
                                    from Requested_Travelers__r where ATG_PNR_Downloaded__c=false)
                            from case where id=:ATG_FetchPNR.caseId];       
            system.debug('----tempCaseList-------'+tempCaseList);
            if(tempCaseList!=null && tempCaseList.size()>0){
                objCase = tempCaseList[0];
                Boolean checkByPassCount = objCase.Bypass_Passenger_Count_Validation__c;
                system.debug('----1-------'+travelOrder);
                system.debug('----2-------'+objCase.ATG_Reference_Number__c);
                if(objCase.ATG_Total_Requested_Traveller__c <= objCase.ATG_Total_PNR_Downloaded__c){
                   if(!checkByPassCount)
                   {
                    message = new map<string,string>();
                    message.put('Error','PNR downloaded for all requested travellers, Please add more RT'); 
                    ATG_utility.createErrorLog('initializePNR','PNR downloaded for all requested travellers, Please add more RT','ATG_FetchPNR',ATG_FetchPNR.PNRNumber,ATG_FetchPNR.caseId);
                    return; 
                   }
                }else if(objCase.ATG_CC_CCGR_Number__r.ATG_Type__c == 'CCGR'  && (travelOrder != objCase.ATG_Reference_Number__c  || ccgrNum != objCase.ATG_CC_CCGR_Number__r.name) && ccgrNum!='INV'){
                  message = new map<string,string>();
                    message.put('Error','Travel order or CCGR number on ETO does not match with PNR Details');
                    ATG_utility.createErrorLog('initializePNR','Travel order or CCGR number on ETO does not match with PNR Details','ATG_FetchPNR',ATG_FetchPNR.PNRNumber,ATG_FetchPNR.caseId);
                    return;
                }
                else{
                  objPNR.objTravelOrder.travelPurpose = objCase.ATG_Purpose_of_Visit__c;
                  objPNR.objTravelOrder.travelOrderNo = objCase.ATG_Reference_Number__c;
                
                  if(objCase.ATG_CC_CCGR_Number__r.ATG_Type__c == 'CCGR')
                      objPNR.objTravelOrder.travelOrderType = 'C';
                  else if(objCase.ATG_CC_CCGR_Number__r.ATG_Type__c == 'CC No')
                      objPNR.objTravelOrder.travelOrderType = 'R';
                    
                  objPNR.objTravelOrder.customerId = objCase.ATG_CC_CCGR_Number__r.name;
                  objPNR.objTravelOrder.customerName   = objCase.ATG_CC_CCGR_Number__r.ATG_CCGR_Name__c;
                
                  system.debug('----objCase.Requested_Travelers__r-------'+objCase.Requested_Travelers__r);
                
                  if(objCase.Requested_Travelers__r!=null && objCase.Requested_Travelers__r.size()>0){
                      integer tempCnt = 0;
                      for(ATG_Requested_Traveller__c objRT : objCase.Requested_Travelers__r){
                        
                         
                          
                         // if(travelOrder == objRT.ATG_Travel_Order_Number__c){ 
                            passenger objPass = new passenger();
                            string pgrType = '';                  
                            
                                if(string.isNotBlank(objRT.ATG_National_ID__c) && pnrPassenger.containsKey(objRT.ATG_National_ID__c) &&
                  objCase.ATG_CC_CCGR_Number__r.ATG_Type__c == 'CC No'){                                  
                  pgrType = pnrPassenger.get(objRT.ATG_National_ID__c).ATG_Passenger_Type__c;
                  objPass.idType = pnrPassenger.get(objRT.ATG_National_ID__c).ATG_ID_Type__c;
                  objPass.firstName = pnrPassenger.get(objRT.ATG_National_ID__c).ATG_First_Name__c;
                  objPass.lastName = pnrPassenger.get(objRT.ATG_National_ID__c).ATG_Last_Name__c;
                  objRT.ATG_Ticket_Number__c = pnrPassenger.get(objRT.ATG_National_ID__c).ATG_Ticket_Number__c;
                  objRT.ATG_PNR_Downloaded__c = true;
                  ATG_FetchPNR.requestedTraveller.put(objRT.id,objRT);                                
                                }else if(string.isNotBlank(objRT.ATG_Passport_ID__c) && pnrPassenger.containsKey(objRT.ATG_Passport_ID__c) &&
                                    objCase.ATG_CC_CCGR_Number__r.ATG_Type__c == 'CC No'){                                  
                                    pgrType = pnrPassenger.get(objRT.ATG_Passport_ID__c).ATG_Passenger_Type__c;
                                    objPass.idType = pnrPassenger.get(objRT.ATG_Passport_ID__c).ATG_ID_Type__c;
                                    objPass.firstName = pnrPassenger.get(objRT.ATG_Passport_ID__c).ATG_First_Name__c;
                                    objPass.lastName = pnrPassenger.get(objRT.ATG_Passport_ID__c).ATG_Last_Name__c;
                                    objRT.ATG_Ticket_Number__c = pnrPassenger.get(objRT.ATG_Passport_ID__c).ATG_Ticket_Number__c;
                                    objRT.ATG_PNR_Downloaded__c = true;
                                    ATG_FetchPNR.requestedTraveller.put(objRT.id,objRT);
                                }else if(objCase.ATG_CC_CCGR_Number__r.ATG_Type__c == 'CCGR' && pnrPassengerList.size() > tempCnt){                              
                                    pgrType = pnrPassengerList[tempCnt].ATG_Passenger_Type__c;
                                    objPass.idType = pnrPassengerList[tempCnt].ATG_ID_Type__c;
                                    objPass.firstName = pnrPassengerList[tempCnt].ATG_First_Name__c;
                                    objPass.lastName = pnrPassengerList[tempCnt].ATG_Last_Name__c;
                                    objRT.ATG_Ticket_Number__c = pnrPassengerList[tempCnt].ATG_Ticket_Number__c;
                                    objRT.ATG_PNR_Downloaded__c = true;
                                    ATG_FetchPNR.requestedTraveller.put(objRT.id,objRT);                                    
                                }else{
                                    tempCnt ++;
                                    continue;    
                                }
                                //system.debug('objPass.lastName'+objPass.lastName);
                            
                            if(string.isNotBlank(pgrType)){
                              if(pgrType == 'Adult')
                                    objPass.type = 'A';
                                else if(pgrType == 'Child')
                                    objPass.type = 'C';
                                else if(pgrType == 'Infant')
                                    objPass.type = 'I';
                            }
                            
                            if(string.isNotBlank(objRT.ATG_National_ID__c))
                              objPass.idNumber = objRT.ATG_National_ID__c;  
                            else if(string.isNotBlank(objRT.ATG_Passport_ID__c))
                              objPass.idNumber = objRT.ATG_Passport_ID__c;  
                              
                            if(string.isNotBlank(objRT.ATG_Passport_ID__c)){
                                objPass.passportNumber = objRT.ATG_Passport_ID__c;
                                objPass.issuingCountry = objRT.ATG_Passport_Issuing_Country__c;
                                
                                if(objRT.ATG_Passport_Expiry_Date__c !=null){
                                    datetime tempExpiryDate = datetime.newInstance(objRT.ATG_Passport_Expiry_Date__c.year(), objRT.ATG_Passport_Expiry_Date__c.month(),objRT.ATG_Passport_Expiry_Date__c.day());
                                    objPass.expiryDate = tempExpiryDate.format('dd-MM-yyyy');
                                }    
                            }
                               
                              objPass.email = !string.isBlank(objRT.ATG_Email_Address__c) ? objRT.ATG_Email_Address__c : '';
                              objPass.mobileNumber = !string.isBlank(objRT.ATG_Mobile_Number__c) ? objRT.ATG_Mobile_Number__c.replaceAll( '\\s+', '') : '';
                              objPass.mobileNumber = !string.isBlank(objPass.mobileNumber) ? objPass.mobileNumber.replaceAll( '-', '') : ''; 
                              
                              if(objRT.ATG_Gender__c=='Male')
                                  objPass.gender = 'M';
                              else if(objRT.ATG_Gender__c=='Female')
                                  objPass.gender = 'F';
                                  
                              datetime tempDOB = datetime.newInstance(objRT.ATG_Date_Of_Birth__c.year(), objRT.ATG_Date_Of_Birth__c.month(),objRT.ATG_Date_Of_Birth__c.day());
                              objPass.dob = tempDOB.format('dd-MM-yyyy');
                              objPass.nationality = objRT.ATG_Nationality__c;
                              
                              if(objCase.ATG_Reference_Number__c == pnrPassengerList[tempCnt].ATG_Travel_Order__c && objCase.ATG_CC_CCGR_Number__r.ATG_Type__c == 'CCGR')                              
                                objPNR.passengerList.add(objPass);
                              else if(objCase.ATG_CC_CCGR_Number__r.ATG_Type__c == 'CC No'){
                                   if(string.isNotBlank(objRT.ATG_National_ID__c) && pnrPassenger.containsKey(objRT.ATG_National_ID__c) 
                                       ){
                                        
                                        objPNR.passengerList.add(objPass);
                                    
                                    }else if(string.isNotBlank(objRT.ATG_Passport_ID__c) && pnrPassenger.containsKey(objRT.ATG_Passport_ID__c) 
                                       ){
                                        
                                        objPNR.passengerList.add(objPass);
                                    }
                              } 
                              
                      //    }
                          
                          tempCnt++;
                      }   
                      
                  }else{
                    message = new map<string,string>();
                    message.put('Error', 'No Requested Traveller found.');
                    ATG_utility.createErrorLog('initializePNR','No Requested Traveller found.','ATG_FetchPNR',ATG_FetchPNR.PNRNumber,ATG_FetchPNR.caseId);
                    return;
                  }
                }
          }
          
          System.debug('pnrPassenger.values().size()---->'+pnrPassenger.values().size()+'   objPNR.passengerList.size()--->'+objPNR.passengerList.size());
          System.debug('objCase.ATG_CC_CCGR_Number__r.ATG_Type__c'+objCase.ATG_CC_CCGR_Number__r.ATG_Type__c);
          System.debug((pnrPassenger.values().size() != objPNR.passengerList.size() && objCase.ATG_CC_CCGR_Number__r.ATG_Type__c == 'CC No'));
          System.debug((pnrPassengerList.size() != objPNR.passengerList.size() && objCase.ATG_CC_CCGR_Number__r.ATG_Type__c == 'CCGR'));
          System.debug('objPNR.passengerList--->'+objPNR.passengerList);
          System.debug('pnrPassenger--->'+pnrPassenger+' pnrPassengerList--->'+pnrPassengerList);

          if((pnrPassenger.values().size() != objPNR.passengerList.size() && objCase.ATG_CC_CCGR_Number__r.ATG_Type__c == 'CC No') || (pnrPassengerList.size() != objPNR.passengerList.size() && objCase.ATG_CC_CCGR_Number__r.ATG_Type__c == 'CCGR')){
            message = new map<string,string>();  
            message.put('Error', 'Unable to process the PNR details due to the error in the Passenger data,Please correct and try again.'); 
            ATG_utility.createErrorLog('initializePNR','Unable to process the PNR details due to the error in the Passenger data,Please correct and try again.','ATG_FetchPNR',ATG_FetchPNR.PNRNumber,ATG_FetchPNR.caseId);
            return;
          }
          system.debug('---message------'+message);
          map<string, PNR_Segment__c> pnrSegment = new map<string, PNR_Segment__c>();
          if(!message.containsKey('Error')){
            system.debug('---pnrPass------'+pnrPass);
            
            if(pnrPass!=null && pnrPass.size()>0){
                for(PNR_Segment__c obj : [select id, ATG_Origin__c, ATG_Destination__c, ATG_Departure_Date_Time__c,
                                   ATG_Arrival_Date_Time__c,PNR_Passenger__r.ATG_Passport_Number__c
                            from PNR_Segment__c where PNR_Passenger__c IN : pnrPass]){
                    
                    system.debug('---obj1------'+obj );
                    pnrSegment.put(obj.ATG_Origin__c+'-'+obj.ATG_Destination__c, obj);
                }
            }else{
              message = new map<string,string>();
              message.put('Error', 'No Requested Traveller found.');
              ATG_utility.createErrorLog('initializePNR','No Requested Traveller found.','ATG_FetchPNR',ATG_FetchPNR.PNRNumber,ATG_FetchPNR.caseId);
              return;
            }
          }
            
          if(pnrSegment!=null && pnrSegment.size()>0 && !message.containsKey('Error')){
            for(PNR_Segment__c obj : pnrSegment.values()){
              system.debug('---obj2------'+obj );
              Trip objTrip = new Trip();
                  objTrip.fromTrip =  obj.ATG_Origin__c;
                  objTrip.to = obj.ATG_Destination__c;
                   if(obj.ATG_Departure_Date_Time__c !=null)
                    objTrip.departDate = obj.ATG_Departure_Date_Time__c.format('dd-MM-yyyy');
                  if(obj.ATG_Arrival_Date_Time__c !=null)
                    objTrip.returnDate = obj.ATG_Arrival_Date_Time__c.format('dd-MM-yyyy');
                  objPNR.objCriteria.tripList.add(objTrip);
            }  
          }
        
        if(!message.containsKey('Error')){
            system.debug('---objPNR------'+objPNR);
            if(objPNR!=null && string.isNotBlank(objPNR.pnrNumber)){ 
                requestBody = createRequestBody();
            }
            
            if(string.isNotBlank(requestBody)){
                fetchPNR();
            }
          }
        }else{
          message = new map<string,string>();
          message.put('Error', 'No PNR Passenger found.');
          ATG_utility.createErrorLog('initializePNR','No PNR Passenger found.','ATG_FetchPNR',ATG_FetchPNR.PNRNumber,ATG_FetchPNR.caseId);
          return;
        }
    }
    
    //initialize wrapper class variable objPNR
    public static string createRequestBody(){
        
        requestBody = 'requestId='+objPNR.requestId+'&';
        requestBody = requestBody+'lang='+objPNR.lang+'&';
        
        requestBody = requestBody+'referer.ipAddress='+objPNR.objReferer.ipAddress+'&';
        requestBody = requestBody+'referer.userAgent='+objPNR.objReferer.userAgent+'&';
        requestBody = requestBody+'referer.salesforceId='+objPNR.objReferer.salesforceId+'&';
        requestBody = requestBody+'referer.successFactorId='+objPNR.objReferer.successFactorId+'&';
        
        requestBody = requestBody+'ignoreWarning='+objPNR.ignoreWarning+'&';
        requestBody = requestBody+'pnrNumber='+objPNR.pnrNumber+'&';
        
        if(objPNR.objCriteria.tripList!=null && objPNR.objCriteria.tripList.size()>0){
            for(integer i=0;i<objPNR.objCriteria.tripList.size();i++)   {
                requestBody = requestBody+'criteria.trip['+i+'].from='+objPNR.objCriteria.tripList[i].fromTrip+'&';
                requestBody = requestBody+'criteria.trip['+i+'].to='+objPNR.objCriteria.tripList[i].to+'&';
                requestBody = requestBody+'criteria.trip['+i+'].departDate='+objPNR.objCriteria.tripList[i].departDate+'&';
                requestBody = requestBody+'criteria.trip['+i+'].returnDate='+objPNR.objCriteria.tripList[i].returnDate+'&';
            }
        }
        
        requestBody = requestBody+'criteria.tripType='+objPNR.objCriteria.tripType+'&';
        requestBody = requestBody+'criteria.adult='+objPNR.objCriteria.adult+'&';
        requestBody = requestBody+'criteria.child='+objPNR.objCriteria.child+'&';
        requestBody = requestBody+'criteria.infant='+objPNR.objCriteria.infant+'&';
        requestBody = requestBody+'criteria.airlineClass='+objPNR.objCriteria.airlineClass+'&';
        requestBody = requestBody+'criteria.infantSeatOrLap='+objPNR.objCriteria.infantSeatOrLap+'&';
        
        if(objPNR.passengerList!=null && objPNR.passengerList.size()>0){
            for(integer i=0;i<objPNR.passengerList.size();i++)  {
                requestBody = requestBody+'passengers['+i+'].type='+objPNR.passengerList[i].type+'&';
                requestBody = requestBody+'passengers['+i+'].firstName='+objPNR.passengerList[i].firstName+'&';
                requestBody = requestBody+'passengers['+i+'].lastName='+objPNR.passengerList[i].lastName+'&';
                requestBody = requestBody+'passengers['+i+'].email='+objPNR.passengerList[i].email+'&';
                requestBody = requestBody+'passengers['+i+'].mobileNumber='+objPNR.passengerList[i].mobileNumber+'&';
                requestBody = requestBody+'passengers['+i+'].gender='+objPNR.passengerList[i].gender+'&';
                requestBody = requestBody+'passengers['+i+'].dob='+objPNR.passengerList[i].dob+'&';
                requestBody = requestBody+'passengers['+i+'].nationality='+objPNR.passengerList[i].nationality+'&';
                requestBody = requestBody+'passengers['+i+'].idType='+objPNR.passengerList[i].idType+'&';
                requestBody = requestBody+'passengers['+i+'].idNumber='+objPNR.passengerList[i].idNumber+'&';
                requestBody = requestBody+'passengers['+i+'].passportNumber='+objPNR.passengerList[i].passportNumber+'&';
                requestBody = requestBody+'passengers['+i+'].issuingCountry='+objPNR.passengerList[i].issuingCountry+'&';
                requestBody = requestBody+'passengers['+i+'].expiryDate='+objPNR.passengerList[i].expiryDate+'&';
            }
        }
        
        requestBody = requestBody+'travelOrder.travelPurpose='+objPNR.objTravelOrder.travelPurpose+'&';
        requestBody = requestBody+'travelOrder.travelOrderNo='+objPNR.objTravelOrder.travelOrderNo+'&';
        requestBody = requestBody+'travelOrder.travelOrderType='+objPNR.objTravelOrder.travelOrderType+'&';
        requestBody = requestBody+'travelOrder.customerId='+objPNR.objTravelOrder.customerId+'&';
        requestBody = requestBody+'travelOrder.customerName='+objPNR.objTravelOrder.customerName;
        requestBody = requestBody.replaceAll('null','');
        return requestBody;
    }
    
    // webservice call out to fetch PNR
    webservice static void fetchPNR(){
        
        system.debug('---requestBody------'+requestBody);
        if(!string.isEmpty(requestBody)){
            system.debug('---accessToken------'+accessToken);
            string urlencodedBody =  EncodingUtil.urlEncode(requestBody, 'UTF-8');
            
            system.debug('---urlencodedBody------'+urlencodedBody);
            
            HttpRequest request = new HttpRequest();
            request.setEndpoint(endPointURL+'moh/v1/fetchpnr');
            request.setHeader('Authorization','Bearer '+accessToken);
            request.setHeader('Content-Type', 'application/x-www-form-urlencoded');
            request.setHeader('cache-control', 'no-cache');
            request.setTimeout(120000);
            request.setMethod('POST');
            request.setBody(requestBody);
            
            
            Http h = new Http();
            HttpResponse res = new HttpResponse();
            if(!test.isRunningTest())
              res = h.send(request);  
            
            
            JSONParser parser = JSON.createParser(res.getBody());
            
            system.debug('--res----'+res.getBody());
            system.debug('--parser----'+parser);
      
      boolean updateRt = false;
            
      while (parser.nextToken() != null) {
                if(res.getStatusCode() == 200){
                    if ((parser.getCurrentToken() == JSONToken.FIELD_NAME) && (parser.getText() == 'bookingReference')) {
                        parser.nextToken();              
                        
                        if(string.isNotBlank(objCase.PNR_Number__c)){  
                             objCase.PNR_Number__c = objCase.PNR_Number__c + ';' + parser.getText();
                             message = new map<string,string>();
                             message.put('Success', 'Booking created successfully.');
               updateRt = true;
                        }else{
                            objCase.PNR_Number__c = parser.getText();
                            message = new map<string,string>();
              message.put('Success', 'Booking created successfully.');
              updateRt = true;
                        }
                    }else if((parser.getCurrentToken() == JSONToken.FIELD_NAME) && (parser.getText() == 'error')){
                        string errorText='';
                        
                        while (parser.nextToken() != null) {
                            if(parser.getText() == 'code'){
                                parser.nextToken();                                
                                errorText+=parser.getText()+' ';
                            }else if(parser.getText() == 'message'){                              
                                parser.nextToken();
                                system.debug('--c----'+parser.getText());
                                errorText+=parser.getText()+',';
                            }
                            
                        }
                        message = new map<string,string>();
                        message.put('Error', errorText);
                        ATG_utility.createErrorLog('fetchPNR',errorText,'ATG_FetchPNR',ATG_FetchPNR.PNRNumber,ATG_FetchPNR.caseId);
                    }
                }
                else if((parser.getCurrentToken() == JSONToken.FIELD_NAME) && (parser.getText() == 'error')){
                    string errorText='';
                    while (parser.nextToken() != null) {
                        errorText+=parser.getText()+' ';
                    }
                    message = new map<string,string>();
                    system.debug('--errorText----'+errorText);
                    message.put('Error', 'Some error occured, Please try again or contact admin.');
                    ATG_utility.createErrorLog('fetchPNR','Some error occured, Please try again or contact admin.','ATG_FetchPNR',ATG_FetchPNR.PNRNumber,ATG_FetchPNR.caseId);
                }
            }
            
            system.debug('--message----'+message);
            if(updateRt){
                update objCase; // update booking reference if success 
        
                if(ATG_FetchPNR.requestedTraveller!=null && ATG_FetchPNR.requestedTraveller.size()>0)        
          update ATG_FetchPNR.requestedTraveller.values();
              
            }
        } 
    }
    
    //get logged in agent's EmployeeNumber to set as parameter
    private static string getEmployeeNumber(){
        list<User> tempUserList = [select id, EmployeeNumber from User where id=:userinfo.getUserId()]; 
        if(tempUserList!=null && tempUserList.size()>0){
            return tempUserList[0].EmployeeNumber;
        }
        return '';
    }
    
    
    //begining of Wrapper classes used for fetch PNR API
    public class fetchPNR{
        public string requestId = '';
        public string lang = '';
        public referer objReferer = new referer();
        public string ignoreWarning = '';
        public string pnrNumber = '';
        public criteria objCriteria = new criteria();
        public list<passenger> passengerList = new list<passenger>();
        public travelOrder objTravelOrder = new travelOrder();
    }
    
    public class referer{
        public string ipAddress = '';
        public string userAgent = '';
        public string salesforceId = '';
        public string successFactorId = '';
    }
    
    public class criteria{
        public list<Trip> tripList = new list<Trip>();
        public string tripType = '';
        public integer adult = 0;
        public integer child = 0;
        public integer infant = 0;
        public string airlineClass = '';
        public string infantSeatOrLap = '';
    }
    
    public class passenger{
        public string type = '';
        public string firstName = '';
        public string lastName = '';
        public string email = '';
        public string mobileNumber = '';
        public string gender = '';
        public string dob = '';
        public string nationality = '';
        public string idType = '';
        public string idNumber = '';
        public string passportNumber = '';
        public string issuingCountry = '';
        public string expiryDate = '';
    }
    
    public class Trip{
        public string fromTrip = '';
        public string to = '';
        public string departDate = '';
        public string returnDate = '';
    }
    
    public class travelOrder{
        public string travelPurpose = '';
        public string travelOrderNo = '';
        public string travelOrderType = '';
        public string customerId = '';
        public string customerName = '';
        public list<contentVersion> travelOrderFile;
    }
}