@isTest
private class ATG_UrlRewriterTest {
    
    static testmethod void mapRequestUrl(){
        ATG_UrlRewriter urlRewriter = new ATG_UrlRewriter();
        Case objCase = new Case();
        objCase.Subject = 'tajawal';
        insert objCase;
        String csNo ='';
        for(Case obj:[SELECT CaseNumber From Case where Id=:objCase.Id]){
           csNo = obj.CaseNumber; 
        }
        
        urlRewriter.mapRequestUrl(new PageReference('/socialmedia/09340545/'+csNo+'/359435'));
        urlRewriter.mapRequestUrl(new PageReference('/socialmedia/04906565/'+csNo+'/3454jdf'));
        urlRewriter.mapRequestUrl(new PageReference('/email/04906565/90889/3454jdf/90909'));
        urlRewriter.mapRequestUrl(new PageReference('/booking/04906565/90889/9028097'));
        List<PageReference> salesforceURLS = new List<PageReference>();
        salesforceURLS.add(new PageReference('survey.tajawal.com'));
        urlRewriter.generateUrlFor(salesforceURLS);
    }

}