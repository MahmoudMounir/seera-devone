/**************************************************************************************************
* Name               : ATG_LiveChatTranscriptHandler 
* Description        : Used to Created or Fetch Accounts when Agent accepts chat.
* Created Date       : 16th August 2018                                                                *
* Created By         : Sravan Booragadda (PWC Digital)                                              
* -------------------------------------------------------------------------------------------------
* Version       Author      Date        Comment                                                               *
  1.1           Diana       12/18/2018  Mobile Number to be combination of country code and mobile number
  1.2           Diana       01/08/2019  Updated account matching logic - based on business unit
**************************************************************************************************/
public with sharing class ATG_LiveChatTranscriptHandler implements ATG_TriggerInterface {
    
    // Constructor
    public void ATG_LiveChatTranscriptHandler(){}

    public void beforeInsert(list<SObject> soNewList){
        system.debug('***** before insert');
        updateOwnerManager((List<liveChatTranscript>)soNewList,null,true);
    }   
    public void beforeUpdate(list<SObject> soNewList,map<id,SObject> soOldMap){
          ATG_LiveChatTranscriptHandler.updateAccountForLiveChat((List<liveChatTranscript>)soNewList);
           updateOwnerManager((List<liveChatTranscript>)soNewList,(map<Id,liveChatTranscript>)soOldMap,false);
    }

    public void beforeDelete(list<SObject> soNewList){}

    public void afterInsert(list<SObject> soNewList){}

    public void afterUpdate(list<SObject> soNewList,map<id,SObject> soOldMap){
    }

    public void afterDelete(list<SObject> soNewList){}
    
    public static boolean updateTranscript = true;
    
    public static void updateAccountForLiveChat(List<LiveChatTranscript> liveChatTranscriptList){
        system.debug('entered');
        if(liveChatTranscriptList !=null && !liveChatTranscriptList.isEmpty()) {
            SavePoint sp;
            LiveChatTranscript lctObj =  new LiveChatTranscript();
            try{
                Database.setSavePoint();
                string PersonMobileWithCountryCode = '';
                 if(liveChatTranscriptList[0].Mobile_Country_Code__c != null && liveChatTranscriptList[0].Mobile_Number__c!=null && 
                    string.isNotBlank(liveChatTranscriptList[0].Mobile_Country_Code__c) && string.isNotBlank(liveChatTranscriptList[0].Mobile_Number__c)){
                    
                    string countryCode = liveChatTranscriptList[0].Mobile_Country_Code__c.substringAfter('(').remove(')');
                    PersonMobileWithCountryCode = countryCode+' ' +liveChatTranscriptList[0].Mobile_Number__c;
                    system.debug('******* personmobile'+PersonMobileWithCountryCode);
                }
                
                string recordTypeID = ATG_ConstantsUtility.PERSON_ACCOUNT_RECORDTYPEID; //defaulted to Al Tayyer
                
                String lctRecordTypeID = ATG_ConstantsUtility.ATG_LIVECHAT_RECORDTYPE; //defaulted to altayyar record type
                system.debug('******* recordTypeID'+recordTypeID);
                if(liveChatTranscriptList[0].Business_Unit__c == ATG_ConstantsUtility.TAJAWAL_BusinessUnit){
                    recordTypeID = ATG_ConstantsUtility.TJW_PERSON_ACCOUNT_RECORDTYPEID;
                    lctRecordTypeID = ATG_ConstantsUtility.TJW_LIVECHAT_RECORDTYPE;
                }else if(liveChatTranscriptList[0].Business_Unit__c == ATG_ConstantsUtility.ALMOSAFER_BusinessUnit){
                    recordTypeID = ATG_ConstantsUtility.AMH_PERSON_ACCOUNT_RECORDTYPEID;
                    lctRecordTypeID = ATG_ConstantsUtility.AMH_LIVECHAT_RECORDTYPE;
                }                
                
               list<Account> tempList = new List<Account>();
                if(liveChatTranscriptList[0].Business_Unit__c == ATG_ConstantsUtility.ALTAYYAR_BusinessUnit){
                    tempList = [select id from Account where  
                                (personEmail=:liveChatTranscriptList[0].Email__c
                                      AND ATG_Mobile_Formula__c=:PersonMobileWithCountryCode.right(9)) and recordTypeID =: recordTypeID];
                    if(tempList.size() == 0){
                         tempList = [select id from Account where
                                (personEmail=:liveChatTranscriptList[0].Email__c) and recordTypeID =: recordTypeID];
                    }
                    if(tempList.size() == 0){
                         tempList = [select id from Account where 
                                (ATG_Mobile_Formula__c=:PersonMobileWithCountryCode.right(9)) and recordTypeID =: recordTypeID];
                    }
                    
                }else{
                    tempList = [select id from Account where 
                                (personEmail=:liveChatTranscriptList[0].Email__c
                                          ) and recordTypeID =: recordTypeID];
                
                }
                
                
                String accountId = '';
                
                if(tempList!=null && tempList.size()>0){
                    liveChatTranscriptList[0].accountId = tempList[0] .id; 
                    accountId = tempList[0].Id;
                }else{
                    
                    Account accObj = new Account(lastName=liveChatTranscriptList[0].Last_Name__c,
                                                 firstName=liveChatTranscriptList[0].First_Name__c,
                                                 PersonEmail=liveChatTranscriptList[0].Email__c,
                                                 PersonMobilePhone=PersonMobileWithCountryCode,
                                                 Phone=PersonMobileWithCountryCode,ATG_Business_Unit_Picklist__c =liveChatTranscriptList[0].Business_Unit__c,RecordTypeId =recordTypeId);
                    insert accObj ;
                    accountId = accObj.Id;
                }
                liveChatTranscriptList[0].ChatKey__c = liveChatTranscriptList[0].ChatKey;
                 liveChatTranscriptList[0].accountId = accountId;
                 liveChatTranscriptList[0].RecordTypeId = lctRecordTypeID;
                
            
            
            }catch(exception e){
               ATG_utility.LogException(e);
            }
        }
        
    }
    
    //update owner team lead
    public static void updateOwnerManager(List<LiveChatTranscript> livechatTranscriptList,
                                           Map<Id,LiveChatTranscript> TriggerOldMap,Boolean isInsert){
        Map<Id,LiveChatTranscript> userId = new Map<Id,LiveChatTranscript>();
        for(LiveChatTranscript lctObj:livechatTranscriptList){
            system.debug(lctObj);
            if(isInsert || (!isInsert && lctObj.OwnerId != TriggerOldMap.get(lctObj.Id).OwnerId) || (!isInsert && lctObj.ATG_Owner_Team_Lead__c == null)){
                userId.put(lctObj.OwnerId,lctObj);
            }
        }    
        if(!userId.isEmpty() && userId.size()>0){
            for(User userObj: [select Id, ManagerID, Manager.ManagerID from User where Id IN : userId.keySet()]){
                if(userObj.ManagerId != null){
                    userId.get(userObj.Id).ATG_Owner_Team_Lead__c = userObj.ManagerId;
                }
            }
        }
        
    }
    
    

}