/**************************************************************************************************
Name               : ATG_QAMonitor_Share                                                               
Description        : Trigger Handler on QA_Monitoring__c Object is used to share the QA monitor record to
           the agent(user lookup) on the record.
Created Date       : March 7th 2019                                                            
Created By         : Diana Correa     
Modification History
v1.1    Ravi        18th June, 2019     Added the logic to update the grade score for mistakes noted
**************************************************************************************************/

public class ATG_QAMonitorHandler implements ATG_TriggerInterface {
    
    public static map<string,decimal> MistakeScores = new map<string, decimal>();
    
    public void beforeInsert(list<SObject> soNewList){
        //v1.1
        calculateGradeFromMistakes(soNewList, null);
    }
  
    public void beforeUpdate(list<SObject> soNewList,map<id,SObject> soOldMap){
        //v1.1
        calculateGradeFromMistakes(soNewList, (map<Id, QA_Monitoring__c>) soOldMap);
    }

    public void beforeDelete(list<SObject> soNewList){}

    public void afterInsert(list<SObject> soNewList){
        shareQARecordWithAgent(soNewList,null,true);
    }

    public void afterUpdate(list<SObject> soNewList,map<id,SObject> soOldMap){
        shareQARecordWithAgent(soNewList,(map<Id,QA_Monitoring__c>)soOldMap,false);
    }

    public void afterDelete(list<SObject> soNewList){}
    
    //Manually share the record with the agent name mentioned in the QA_Monitoring__c in the below scenarios
    // Al Tayyar - when the status is not in draft
    // Al Mosafer and Tajawal when the status is coaching completed
    private void shareQARecordWithAgent(list<QA_Monitoring__c> soNewList,map<id,QA_Monitoring__c> soOldMap,Boolean isInsert){
        
        List<QA_Monitoring__Share> jobShares = new List<QA_Monitoring__Share>();
        for(QA_Monitoring__c qaMonitoringObj : soNewList){
          if(qaMonitoringObj.Agent_Name__c != null &&
              (qaMonitoringObj.ATG_Business_Unit__c != null && qaMonitoringObj.ATG_Business_Unit__c.equalsIgnoreCase(ATG_ConstantsUtility.ALTAYYAR_BusinessUnit) && 
               ((qaMonitoringObj.Status__c != 'Draft' && isInsert)|| 
               (!isInsert && qaMonitoringObj.Status__c != soOldMap.get(qaMonitoringObj.Id).Status__c &&
                qaMonitoringObj.Status__c != 'Draft') ||
               (!isInsert && qaMonitoringObj.Agent_Name__c != soOldMap.get(qaMonitoringObj.Id).Agent_Name__c 
                && qaMonitoringObj.Status__c != 'Draft')
              )) ||
              (qaMonitoringObj.ATG_Business_Unit__c != null && (qaMonitoringObj.ATG_Business_Unit__c.equalsIgnoreCase(ATG_ConstantsUtility.TAJAWAL_BusinessUnit) ||
                qaMonitoringObj.ATG_Business_Unit__c.equalsIgnoreCase(ATG_ConstantsUtility.ALMosafer_BusinessUnit))
               && ((qaMonitoringObj.Status__C == 'Coaching Completed' && isInsert) ||
                (!isInsert && qaMonitoringObj.Status__c != soOldMap.get(qaMonitoringObj.Id).Status__c &&
                qaMonitoringObj.Status__c == 'Coaching Completed') ||
               (!isInsert && qaMonitoringObj.Agent_Name__c != soOldMap.get(qaMonitoringObj.Id).Agent_Name__c 
                && qaMonitoringObj.Status__c == 'Coaching Completed')
              ))){
              QA_Monitoring__Share agentRecord = new QA_Monitoring__Share();    
              agentRecord.ParentId = qaMonitoringObj.Id;
              agentRecord.UserOrGroupId = qaMonitoringObj.Agent_Name__c;
              agentRecord.AccessLevel = 'Edit';
              jobShares.add(agentRecord);
          }
       }       
       
        if(!jobShares.isEmpty()){
            Database.SaveResult[] jobShareInsertResult = Database.insert(jobShares,false);
        }
        
    }
    
    //v1.1
    public static void calculateGradeFromMistakes(list<QA_Monitoring__c> lstNew, map<Id,QA_Monitoring__c> mapOld){
        try{
            list<QA_Monitoring__c> lstQAMs = new list<QA_Monitoring__c>();
            for(QA_Monitoring__c objQAM : lstNew ){
                if(objQAM.Case_Number__c != null && objQAM.RecordTypeId == ATG_ConstantsUtility.CLU_Quality_Check_RecordType && 
                   ( (mapOld == null && objQAM.ATG_Mistakes__c != null) || (mapOld.containsKey(objQAM.Id) && objQAM.ATG_Mistakes__c != mapOld.get(objQAM.Id).ATG_Mistakes__c)  )){
                    lstQAMs.add(objQAM);
                }
            }
            if(!lstQAMs.isEmpty() ){
                if(MistakeScores.isEmpty())
                    MistakeScores = getMistakeScore();
                for(QA_Monitoring__c objQAM : lstNew ){
                    decimal d = decimal.valueOf(System.label.Default_Grade_Score);
                    if(objQAM.ATG_Mistakes__c != null){
                        //if( ! ATG_ConstantsUtility.Case_Contract_Stop_Sale_RecordType.contains(objQAM.ATG_Case_Record_Type_Id__c)){
                            for(string mis : objQAM.ATG_Mistakes__c.split(';')){
                                d += MistakeScores.get(mis);
                            }
                        /*}else{
                            d = 0;
                        }*/
                    }
                    objQAM.ATG_Grade__c = (d < 0 ? 0 : d);
                }
            }
        }catch(Exception ex){
        
        }
    }
    
    public static map<string, decimal> getMistakeScore(){
        map<string, decimal> mapScores = new map<string, decimal>();
        for(Mistake_Score__mdt objMD : [select DeveloperName,Mistake_Picklist_API_Name__c,Grade_Value__c from Mistake_Score__mdt where Mistake_Picklist_API_Name__c != null AND Grade_Value__c != null ]){
            mapScores.put(objMD.Mistake_Picklist_API_Name__c, objMD.Grade_Value__c);
        }
        return mapScores;
    }
}