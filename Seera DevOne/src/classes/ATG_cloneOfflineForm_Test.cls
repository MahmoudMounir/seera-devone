/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class ATG_cloneOfflineForm_Test {

    static testMethod void myUnitTest() {
        ATG_TestUtility objTest = new ATG_TestUtility();
        
        Account corp = objTest.createLeisureAccount();
        Account corp2 = objTest.createCorporateAccount();
        
        Case objcs = new Case();
        objcs.AccountId = corp.Id;
        for(QueueSobject obj : [select Id,QueueId from QueueSobject where SobjectType = 'Case' limit 1]){
            objcs.OwnerId = obj.QueueId;
        }
        
        
        ATG_Booking__c book = new ATG_Booking__c(Name = 'TestBook',
                              ATG_Product_Type__c='Flight',
                              ATG_Email__c ='test@test.com',
                              ATG_Travel_End_Date__c = system.today());
        insert book;
                         
        objcs.RecordTypeId = ATG_ConstantsUtility.AMH_CASE_RECORDTYPE;
        objcs.Follow_up_Date__c = system.today().addDays(5);
        objcs.Mobile_Number__c = '123456';
        objcs.Country_Code__c = 'Afghanistan (+93)';
        objcs.ATG_Booking__c = book.id;
        insert objcs;
        
        ATG_Booking_Detail__c objBD = new ATG_Booking_Detail__c();
        objBD.ATG_Booking__c = book.id;
        objBD.ATG_Departure_Date__c = system.today().adddays(2);
        objBD.ATG_PNR__c = '456';
        objBD.recordtypeid = Schema.SObjectType.ATG_Booking_Detail__c.getRecordTypeInfosByName().get('ATG Flight').getRecordTypeId();
        insert objBD;
        
        ATG_Traveller__c objTraveller = new ATG_Traveller__c();
        objTraveller.ATG_Booking__c = book.id;
        objTraveller.ATG_Email__c = 'tets@test.test';
        objTraveller.ATG_Last_Name__c = 'sadsad';
        insert objTraveller;
        
        ATG_cloneOfflineForm objClone = new ATG_cloneOfflineForm();
        list<id> caseID = new list<id>();
        caseID.add(objcs.id);
        ATG_cloneOfflineForm.cloneOldBooking(caseID);
    }
}