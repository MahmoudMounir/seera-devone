/**
 * Created by mnazir001 on 11/27/18.
 */

@IsTest
private class HijriCalendar_Test {
    @IsTest
    static void testHijriCalendar() {
        Date d = Date.today();
        HijriCalendar calendar = new HijriCalendar(d.year(), d.month(), d.day());
        System.debug('Hijri Day' + calendar.getDay());
        System.debug('Hijri month' + calendar.getHijriMonth());
        System.debug('Hijri year' + calendar.getHijriYear());

        System.debug('Hijri Takvim' + calendar.getHicriTakvim());

        System.debug('Hijri Date' + calendar.getDate());

        System.debug('Hijri formatted' + calendar.formateToArabic());

    }
}