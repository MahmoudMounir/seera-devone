/**************************************************************************************************
* Name               : ATG_TriggerInterface                                                               
* Description        : Interface containing methods Trigger Handlers 
* Created Date       : 10th June 2018                                                                *
* Created By         : Swati Sehrawat (PWC Digital)                                              
* -------------------------------------------------------------------------------------------------
* Version		Author		Date		Comment                                                               *

**************************************************************************************************/


public interface ATG_TriggerInterface {
   
   /**
 	* Interface containing methods Trigger Handlers must implement to enforce best practice
 	* and bulkification of triggers.
 	*/
 	
	void beforeInsert(list<SObject> soNewList);
	
	void beforeUpdate(list<SObject> soNewList,map<id,SObject> soOldMap);

	void beforeDelete(list<SObject> soNewList);

	void afterInsert(list<SObject> soNewList);

	void afterUpdate(list<SObject> soNewList,map<id,SObject> soOldMap);

	void afterDelete(list<SObject> soNewList);
 
}