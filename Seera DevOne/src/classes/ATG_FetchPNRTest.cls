/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class ATG_FetchPNRTest {
    
    static testMethod void myUnitTest1() {
        ATG_TestUtility objTest = new ATG_TestUtility();
        
        Account corp = objTest.createLeisureAccount();
        
        Case caseID = objTest.createbookingCase(corp.id);
        caseID.ATG_Reference_Number__c = '2290058';
        update caseID;
        
        ATG_Requested_Traveller__c objRT = new ATG_Requested_Traveller__c();
        objRT.ATG_Nationality__c = 'SA';
        objRT.ATG_National_ID__c = '';
        objRT.ATG_Passport_Expiry_Date__c = system.today().addyears(12);
        objRT.ATG_Mobile_Number__c = '+966 555555555';
        objRT.ATG_Last_Name__c = 'ABRAHAM';
        objRT.ATG_ID_Type__c = 'Passport';
        objRT.ATG_Gender__c = 'Female';
        objRT.ATG_First_Name__c = 'NEETHU';
        objRT.ATG_Passport_ID__c = 'L9686998';
        objRT.ATG_Email_Address__c = 'test@altayyargroup.com';
        objRT.ATG_Date_of_Birth__c = system.today().addyears(-12);
        objRT.ATG_Passport_Issuing_Country__c = 'SA';
        objRT.ATG_PNR_Downloaded__c = false;
        objRT.Request_Number__c = caseID.id;
        objRT.ATG_Employee_Id__c = '4844676';
        objRT.ATG_Grade__c = '79';
        objRT.ATG_Level__c = '3';
        insert objRT;
        
        MOH_Fetch_PNR__c objCredentials = new MOH_Fetch_PNR__c();
        objCredentials.name = 'MOH';
        objCredentials.ATG_UserName__c = 'NAoorble1sNqTrighbAIzImOWRvV8544q3zfDyeRHb8=';
        objCredentials.ATG_Password__c = 'mYGZIahSpgJm6CXK6spSw91xFe+t4VVVN5cykswHF00=';
        objCredentials.ATG_Endpoint_URL__c = 'http://testing.altayyargroup.com/atoservices/';
        insert objCredentials;
        
        PNR__c objPNR = new PNR__c();
        objPNR.Name = 'JOSEDP';
        objPNR.ATG_Trip_Type__c = 'R';
        objPNR.ATG_airlineClass__c = 'E';
        objPNR.ATG_infantSeatOrLap__c = 'lap';
        insert objPNR;
        
        PNR_Passenger__c objPax = new PNR_Passenger__c();
        objPax.ATG_AirLine_Code__c = 'SV';
        objPax.ATG_First_Name__c = 'ABRAHAM/NEETHU MS';
        objPax.ATG_Last_Name__c = 'ABRAHAM/NEETHU MS';
        objPax.ATG_Travel_Order__c = '2290058';
        objPax.ATG_Passenger_Type__c = 'Adult';
        objPax.PNR__c = objPNR.id;
        objPax.ATG_Route__c = 'ELQ-RUH-COK-RUH-ELQ';
        objPax.ATG_Ticket_Number__c = '2783467235';
        objPax.ATG_email__c = 'test@test.com';
        objPax.ATG_Nationality__c = 'Indian';
        objPax.ATG_ID_Type__c = '';
        objPax.ATG_National_ID__c = '';
        objPax.ATG_Iqama_ID__c = '';
        objPax.ATG_Passport_Number__c = 'L9686998';
        objPax.ATG_Passport_Issue_Country__c = 'IND';
        objPax.ATG_Passport_Expiry_Date__c = system.today().addyears(12);
        objPax.ATG_Mobile_Number__c = '';
        objPax.ATG_Gender__c = 'F';
        objPax.Status__c = 'Original';
        objPax.ATG_Date_Of_Birth__c = system.today().addyears(-12);
        insert objPax;    
        
        PNR_Segment__c objSeg = new PNR_Segment__c();   
        objSeg.ATG_AirLine_Code__c = 'SV';                                        
        objSeg.ATG_Arrival_Date_Time__c = system.now(); 
        objSeg.ATG_Booking_Class__c = 'H'; 
        objSeg.ATG_Departure_Date_Time__c = system.now(); 
        objSeg.ATG_Destination__c = 'RUH'; 
        objSeg.ATG_Origin__c = 'ELQ'; 
        objSeg.ATG_PNR__c = objPNR.id; 
        objSeg.PNR_Passenger__c = objPax.id; 
        objSeg.ATG_Ticket_Number__c = '2783467235'; 
        insert objSeg;
        
        ATG_FetchPNR objClass = new ATG_FetchPNR();
        map<string,string> message = new map<string,string>();
        ATG_FetchPNR.getCredentials('JOSEDP', caseID.id, '123.123.23.232', 'Chrome');
    }
    
    static testMethod void myUnitTest2() {
        ATG_TestUtility objTest = new ATG_TestUtility();
        
        Account corp = objTest.createLeisureAccount();
        
        Case caseID = objTest.createbookingCase(corp.id);
        caseID.ATG_Reference_Number__c = '2290058';
        update caseID;
        
        ATG_Requested_Traveller__c objRT = new ATG_Requested_Traveller__c();
        objRT.ATG_Nationality__c = 'SA';
        objRT.ATG_National_ID__c = '323232';
        objRT.ATG_Passport_Expiry_Date__c = system.today().addyears(12);
        objRT.ATG_Mobile_Number__c = '+966 555555555';
        objRT.ATG_Last_Name__c = 'ABRAHAM';
        objRT.ATG_ID_Type__c = 'Passport';
        objRT.ATG_Gender__c = 'Male';
        objRT.ATG_First_Name__c = 'NEETHU';
        objRT.ATG_Passport_ID__c = 'L9686998';
        objRT.ATG_Email_Address__c = 'test@altayyargroup.com';
        objRT.ATG_Date_of_Birth__c = system.today().addyears(-12);
        objRT.ATG_Passport_Issuing_Country__c = 'SA';
        objRT.ATG_PNR_Downloaded__c = false;
        objRT.Request_Number__c = caseID.id;
        objRT.ATG_Employee_Id__c = '4844676';
        objRT.ATG_Grade__c = '79';
        objRT.ATG_Level__c = '3';
        insert objRT;
        
        MOH_Fetch_PNR__c objCredentials = new MOH_Fetch_PNR__c();
        objCredentials.name = 'MOH';
        objCredentials.ATG_UserName__c = 'NAoorble1sNqTrighbAIzImOWRvV8544q3zfDyeRHb8=';
        objCredentials.ATG_Password__c = 'mYGZIahSpgJm6CXK6spSw91xFe+t4VVVN5cykswHF00=';
        objCredentials.ATG_Endpoint_URL__c = 'http://testing.altayyargroup.com/atoservices/';
        insert objCredentials;
        
        PNR__c objPNR = new PNR__c();
        objPNR.Name = 'JOSEDP';
        objPNR.ATG_Trip_Type__c = 'R';
        objPNR.ATG_airlineClass__c = 'E';
        objPNR.ATG_infantSeatOrLap__c = 'lap';
        insert objPNR;
        
        PNR_Passenger__c objPax = new PNR_Passenger__c();
        objPax.ATG_AirLine_Code__c = 'SV';
        objPax.ATG_First_Name__c = 'ABRAHAM/NEETHU MS';
        objPax.ATG_Last_Name__c = 'ABRAHAM/NEETHU MS';
        objPax.ATG_Travel_Order__c = '2290058';
        objPax.ATG_Passenger_Type__c = 'Adult';
        objPax.PNR__c = objPNR.id;
        objPax.ATG_Route__c = 'ELQ-RUH-COK-RUH-ELQ';
        objPax.ATG_Ticket_Number__c = '2783467235';
        objPax.ATG_email__c = 'test@test.com';
        objPax.ATG_Nationality__c = 'Indian';
        objPax.ATG_ID_Type__c = '';
        objPax.ATG_National_ID__c = '';
        objPax.ATG_Iqama_ID__c = '';
        objPax.ATG_Passport_Number__c = 'L9686998';
        objPax.ATG_Passport_Issue_Country__c = 'IND';
        objPax.ATG_Passport_Expiry_Date__c = system.today().addyears(12);
        objPax.ATG_Mobile_Number__c = '';
        objPax.ATG_Gender__c = 'F';
        objPax.Status__c = 'Original';
        objPax.ATG_Date_Of_Birth__c = system.today().addyears(-12);
        insert objPax;    
        
        PNR_Segment__c objSeg = new PNR_Segment__c();   
        objSeg.ATG_AirLine_Code__c = 'SV';                                        
        objSeg.ATG_Arrival_Date_Time__c = system.now(); 
        objSeg.ATG_Booking_Class__c = 'H'; 
        objSeg.ATG_Departure_Date_Time__c = system.now(); 
        objSeg.ATG_Destination__c = 'RUH'; 
        objSeg.ATG_Origin__c = 'ELQ'; 
        objSeg.ATG_PNR__c = objPNR.id; 
        objSeg.PNR_Passenger__c = objPax.id; 
        objSeg.ATG_Ticket_Number__c = '2783467235'; 
        insert objSeg;
        
        ATG_FetchPNR objClass = new ATG_FetchPNR();
        map<string,string> message = new map<string,string>();
        ATG_FetchPNR.getCredentials('JOSEDP', caseID.id, '123.123.23.232', 'Chrome');
    }
    
    static testMethod void myUnitTest3() { 
        ATG_TestUtility objTest = new ATG_TestUtility();
        
        Account corp = objTest.createLeisureAccount();
        
        Case caseID = objTest.createbookingCase(corp.id);
        
        ATG_Requested_Traveller__c objRT = new ATG_Requested_Traveller__c();
        objRT.ATG_Nationality__c = 'SA';
        objRT.ATG_National_ID__c = 'L9686998';
        objRT.ATG_Passport_Expiry_Date__c = system.today().addyears(12);
        objRT.ATG_Mobile_Number__c = '+966 555555555';
        objRT.ATG_Last_Name__c = 'ABRAHAM';
        objRT.ATG_ID_Type__c = 'Passport';
        objRT.ATG_Gender__c = 'Male';
        objRT.ATG_First_Name__c = 'NEETHU';
        objRT.ATG_Passport_ID__c = '23232';
        objRT.ATG_Email_Address__c = 'test@altayyargroup.com';
        objRT.ATG_Date_of_Birth__c = system.today().addyears(-12);
        objRT.ATG_Passport_Issuing_Country__c = 'SA';
        objRT.ATG_PNR_Downloaded__c = false;
        objRT.Request_Number__c = caseID.id;
        objRT.ATG_Employee_Id__c = '4844676';
        objRT.ATG_Grade__c = '79';
        objRT.ATG_Level__c = '3';
        insert objRT;
        
        MOH_Fetch_PNR__c objCredentials = new MOH_Fetch_PNR__c();
        objCredentials.name = 'MOH';
        objCredentials.ATG_UserName__c = 'NAoorble1sNqTrighbAIzImOWRvV8544q3zfDyeRHb8=';
        objCredentials.ATG_Password__c = 'mYGZIahSpgJm6CXK6spSw91xFe+t4VVVN5cykswHF00=';
        objCredentials.ATG_Endpoint_URL__c = 'http://testing.altayyargroup.com/atoservices/';
        insert objCredentials;
        
        PNR__c objPNR = new PNR__c();
        objPNR.Name = 'JOSEDP';
        objPNR.ATG_Trip_Type__c = 'R';
        objPNR.ATG_airlineClass__c = 'E';
        insert objPNR;
        
        PNR_Passenger__c objPax = new PNR_Passenger__c();
        objPax.ATG_AirLine_Code__c = 'SV';
        objPax.ATG_First_Name__c = 'ABRAHAM/NEETHU MS';
        objPax.ATG_Last_Name__c = 'ABRAHAM/NEETHU MS';
        objPax.ATG_Travel_Order__c = '2290058';
        objPax.ATG_Passenger_Type__c = 'Adult';
        objPax.PNR__c = objPNR.id;
        objPax.ATG_Route__c = 'ELQ-RUH-COK-RUH-ELQ';
        objPax.ATG_Ticket_Number__c = '2783467235';
        objPax.ATG_email__c = 'test@test.com';
        objPax.ATG_Nationality__c = 'Indian';
        objPax.ATG_ID_Type__c = '';
        objPax.ATG_National_ID__c = 'L9686998';
        objPax.ATG_Iqama_ID__c = '';
        objPax.ATG_Passport_Number__c = '';
        objPax.ATG_Passport_Issue_Country__c = 'IND';
        objPax.ATG_Passport_Expiry_Date__c = system.today().addyears(12);
        objPax.ATG_Mobile_Number__c = '';
        objPax.ATG_Gender__c = 'M';
        objPax.Status__c = 'Original';
        objPax.ATG_Date_Of_Birth__c = system.today().addyears(-12);
        insert objPax;    
        
        PNR_Segment__c objSeg = new PNR_Segment__c();   
        objSeg.ATG_AirLine_Code__c = 'SV';                                        
        objSeg.ATG_Arrival_Date_Time__c = system.now(); 
        objSeg.ATG_Booking_Class__c = 'H'; 
        objSeg.ATG_Departure_Date_Time__c = system.now(); 
        objSeg.ATG_Destination__c = 'RUH'; 
        objSeg.ATG_Origin__c = 'ELQ'; 
        objSeg.ATG_PNR__c = objPNR.id; 
        objSeg.PNR_Passenger__c = objPax.id; 
        objSeg.ATG_Ticket_Number__c = '2783467235'; 
        insert objSeg;
        
        ATG_FetchPNR objClass = new ATG_FetchPNR();
        map<string,string> message = new map<string,string>();
        ATG_FetchPNR.getCredentials('JOSEDP', caseID.id, '123.123.23.232', 'Chrome');
    }
    
    static testMethod void myUnitTest4() {
        ATG_TestUtility objTest = new ATG_TestUtility();
        
        Account corp = objTest.createLeisureAccount();
        
        
        list<ATG_Master__c> masterList = ATG_TestDataFactory.createMaster(1,'c');
        masterList[0].ATG_Type__c = 'CCGR';
        masterList[0].Name  = '635101';
        update  masterList;
        
        
        Case caseID = objTest.createbookingCase(corp.id);
        caseID.ATG_Reference_Number__c = '2290058';
        caseID.ATG_CC_CCGR_Number__c = masterList[0].Id;
        update caseID;
        
        ATG_Requested_Traveller__c objRT = new ATG_Requested_Traveller__c();
        objRT.ATG_Nationality__c = 'SA';
        objRT.ATG_National_ID__c = 'L9686998';
        objRT.ATG_Passport_Expiry_Date__c = system.today().addyears(12);
        objRT.ATG_Mobile_Number__c = '+966 555555555';
        objRT.ATG_Last_Name__c = 'ABRAHAM';
        objRT.ATG_ID_Type__c = 'Passport';
        objRT.ATG_Gender__c = 'Female';
        objRT.ATG_First_Name__c = 'NEETHU';
        objRT.ATG_Passport_ID__c = '323232';
        objRT.ATG_Email_Address__c = 'test@altayyargroup.com';
        objRT.ATG_Date_of_Birth__c = system.today().addyears(-12);
        objRT.ATG_Passport_Issuing_Country__c = 'SA';
        objRT.ATG_PNR_Downloaded__c = false;
        objRT.Request_Number__c = caseID.id;
        objRT.ATG_Employee_Id__c = '4844676';
        objRT.ATG_Grade__c = '79';
        objRT.ATG_Level__c = '3';
        insert objRT;
        
        MOH_Fetch_PNR__c objCredentials = new MOH_Fetch_PNR__c();
        objCredentials.name = 'MOH';
        objCredentials.ATG_UserName__c = 'NAoorble1sNqTrighbAIzImOWRvV8544q3zfDyeRHb8=';
        objCredentials.ATG_Password__c = 'mYGZIahSpgJm6CXK6spSw91xFe+t4VVVN5cykswHF00=';
        objCredentials.ATG_Endpoint_URL__c = 'http://testing.altayyargroup.com/atoservices/';
        insert objCredentials;
        
        PNR__c objPNR = new PNR__c();
        objPNR.Name = 'JOSEDP';
        objPNR.ATG_Trip_Type__c = 'R';
        objPNR.ATG_airlineClass__c = 'E';
        objPNR.ATG_infantSeatOrLap__c = 'lap';
        insert objPNR;
        
        PNR_Passenger__c objPax = new PNR_Passenger__c();
        objPax.ATG_AirLine_Code__c = 'SV';
        objPax.ATG_First_Name__c = 'ABRAHAM/NEETHU MS';
        objPax.ATG_Last_Name__c = 'ABRAHAM/NEETHU MS';
        objPax.ATG_Travel_Order__c = '2290058';
        objPax.ATG_Passenger_Type__c = 'Adult';
        objPax.PNR__c = objPNR.id;
        objPax.ATG_Route__c = 'ELQ-RUH-COK-RUH-ELQ';
        objPax.ATG_Ticket_Number__c = '2783467235';
        objPax.ATG_email__c = 'test@test.com';
        objPax.ATG_Nationality__c = 'Indian';
        objPax.ATG_ID_Type__c = '';
        objPax.ATG_National_ID__c = 'L9686998';
        objPax.ATG_Iqama_ID__c = '';
        objPax.ATG_Passport_Number__c = '';
        // objPax.ATG_Passport_Issue_Country__c = 'IND';
        objPax.ATG_Passport_Expiry_Date__c = system.today().addyears(12);
        objPax.ATG_Mobile_Number__c = '';
        objPax.ATG_Gender__c = 'F';
        objPax.ATG_Date_Of_Birth__c = system.today().addyears(-12);
        objPax.ATG_Payment_Mode__c = 'GOV/GR0000635101';
        objPax.Status__c = 'Original';
        insert objPax;    
        
        PNR_Segment__c objSeg = new PNR_Segment__c();   
        objSeg.ATG_AirLine_Code__c = 'SV';                                        
        objSeg.ATG_Arrival_Date_Time__c = system.now(); 
        objSeg.ATG_Booking_Class__c = 'H'; 
        objSeg.ATG_Departure_Date_Time__c = system.now(); 
        objSeg.ATG_Destination__c = 'RUH'; 
        objSeg.ATG_Origin__c = 'ELQ'; 
        objSeg.ATG_PNR__c = objPNR.id; 
        objSeg.PNR_Passenger__c = objPax.id; 
        objSeg.ATG_Ticket_Number__c = '2783467235'; 
        insert objSeg;
        
        ATG_FetchPNR objClass = new ATG_FetchPNR();
        map<string,string> message = new map<string,string>();
        ATG_FetchPNR.getCredentials('JOSEDP', caseID.id, '123.123.23.232', 'Chrome');
    }
    
     static testMethod void myUnitTest5() {
        ATG_TestUtility objTest = new ATG_TestUtility();
        
        Account corp = objTest.createLeisureAccount();
        
        
        list<ATG_Master__c> masterList = ATG_TestDataFactory.createMaster(1,'c');
        masterList[0].ATG_Type__c = 'CCGR';
        masterList[0].Name  = '635101';
        update  masterList;
        
        
        Case caseID = objTest.createbookingCase(corp.id);
        caseID.ATG_Reference_Number__c = '2290058';
        caseID.ATG_CC_CCGR_Number__c = masterList[0].Id;
        update caseID;
        
        ATG_Requested_Traveller__c objRT = new ATG_Requested_Traveller__c();
        objRT.ATG_Nationality__c = 'SA';
        objRT.ATG_National_ID__c = ' ';
        objRT.ATG_Passport_Expiry_Date__c = system.today().addyears(12);
        objRT.ATG_Mobile_Number__c = '+966 555768909';
        objRT.ATG_Last_Name__c = 'ABRAHAM';
        objRT.ATG_ID_Type__c = 'Passport';
        objRT.ATG_Gender__c = 'Female';
        objRT.ATG_First_Name__c = 'NEETHU';
        objRT.ATG_Passport_ID__c = '567654';
        objRT.ATG_Email_Address__c = 'test@altayyargroup.com';
        objRT.ATG_Date_of_Birth__c = system.today().addyears(-12);
        objRT.ATG_Passport_Issuing_Country__c = 'SA';
        objRT.ATG_PNR_Downloaded__c = false;
        objRT.Request_Number__c = caseID.id;
        objRT.ATG_Employee_Id__c = '4844676';
        objRT.ATG_Grade__c = '79';
        objRT.ATG_Level__c = '3';
        insert objRT;
        
        MOH_Fetch_PNR__c objCredentials = new MOH_Fetch_PNR__c();
        objCredentials.name = 'MOH';
        objCredentials.ATG_UserName__c = 'NAoorble1sNqTrighbAIzImOWRvV8544q3zfDyeRHb8=';
        objCredentials.ATG_Password__c = 'mYGZIahSpgJm6CXK6spSw91xFe+t4VVVN5cykswHF00=';
        objCredentials.ATG_Endpoint_URL__c = 'http://testing.altayyargroup.com/atoservices/';
        insert objCredentials;
        
        PNR__c objPNR = new PNR__c();
        objPNR.Name = 'JOSEDP';
        objPNR.ATG_Trip_Type__c = 'R';
        objPNR.ATG_airlineClass__c = 'E';
        objPNR.ATG_infantSeatOrLap__c = 'lap';
        insert objPNR;
        
        PNR_Passenger__c objPax = new PNR_Passenger__c();
        objPax.ATG_AirLine_Code__c = 'SV';
        objPax.ATG_First_Name__c = 'ABRAHAM/NEETHU MS';
        objPax.ATG_Last_Name__c = 'ABRAHAM/NEETHU MS';
        objPax.ATG_Travel_Order__c = '2290058';
        objPax.ATG_Passenger_Type__c = 'Adult';
        objPax.PNR__c = objPNR.id;
        objPax.ATG_Route__c = 'ELQ-RUH-COK-RUH-ELQ';
        objPax.ATG_Ticket_Number__c = '2783467235';
        objPax.ATG_email__c = 'test@test.com';
        objPax.ATG_Nationality__c = 'Indian';
        objPax.ATG_ID_Type__c = '';
        objPax.ATG_National_ID__c = 'L9686998';
        objPax.ATG_Iqama_ID__c = '';
        objPax.ATG_Passport_Number__c = '';
        // objPax.ATG_Passport_Issue_Country__c = 'IND';
        objPax.ATG_Passport_Expiry_Date__c = system.today().addyears(12);
        objPax.ATG_Mobile_Number__c = '';
        objPax.ATG_Gender__c = 'F';
        objPax.ATG_Date_Of_Birth__c = system.today().addyears(-12);
        objPax.ATG_Payment_Mode__c = 'GOV/GR0000635101';
        objPax.Status__c = 'Original';
        insert objPax;    
        
        PNR_Segment__c objSeg = new PNR_Segment__c();   
        objSeg.ATG_AirLine_Code__c = 'SV';                                        
        objSeg.ATG_Arrival_Date_Time__c = system.now(); 
        objSeg.ATG_Booking_Class__c = 'H'; 
        objSeg.ATG_Departure_Date_Time__c = system.now(); 
        objSeg.ATG_Destination__c = 'RUH'; 
        objSeg.ATG_Origin__c = 'ELQ'; 
        objSeg.ATG_PNR__c = objPNR.id; 
        objSeg.PNR_Passenger__c = objPax.id; 
        objSeg.ATG_Ticket_Number__c = '2783467235'; 
        insert objSeg;
        
        ATG_FetchPNR objClass = new ATG_FetchPNR();
        map<string,string> message = new map<string,string>();
        ATG_FetchPNR.getCredentials('JOSEDP', caseID.id, '123.123.23.232', 'Chrome');
    }
}