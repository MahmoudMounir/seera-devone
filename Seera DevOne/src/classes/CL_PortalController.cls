public without sharing class CL_PortalController  {

    //Fetch required data to show on a list in the CL Portal
    @AuraEnabled
    public static List < sObject > fetchRecords(string listObject, string filter, string columns, Integer ListLimit) {
        
        UserDetailCls userDetailObj = getCurrentUserDetails();
        string vehicleSobjectName = 'cl_vehicle__c';
        
        string caseSobjectName = 'case';
        
        if(listObject.toLowerCase() == vehicleSobjectName.toLowerCase()){
                string businessAccountFilter = ' CL_Business_Account__c = ' + '\''+ userDetailObj.clientAccount.id + '\'';
                string activeVehicleFilter = ' CL_Driver__c != null ';
                if(String.isBlank(filter)){
                    filter = businessAccountFilter;
                    filter += ' AND ' + activeVehicleFilter;
                } else {
                    filter += ' AND ' + businessAccountFilter;
                    filter += ' AND ' +activeVehicleFilter;
                }

        } else if (listObject.toLowerCase() == caseSobjectName.toLowerCase()){
                string businessAccountFilter = ' AccountId = ' + '\''+ userDetailObj.clientAccount.id + '\'';
                string driverAccountFilter = ' CL_Driver__c = ' + '\''+ userDetailObj.driverAccount.id + '\'';
            if(userDetailObj.LumiRole == 'Admin'){
                if(String.isBlank(filter)){
                    filter = businessAccountFilter;
                } else {
                    filter += ' AND ' + businessAccountFilter;
                }
            } else if(userDetailObj.LumiRole == 'Driver'){  
                if(String.isBlank(filter)){
                    filter = driverAccountFilter;
                } else {
                    filter += ' AND ' + driverAccountFilter;
                }
            }   
        }   
        
        string ObjectName = listObject;
        List < sObject > returnList = new List < sObject > ();
      
        // Create a Dynamic SOQL Query For Fetch Record List with LIMIT 5   
        String sQuery =  'select id,' +columns +' from ' +ObjectName;
        if(filter!=null && filter.length()>0){
            sQuery += ' WHERE ' + filter;
        }
        sQuery += ' LIMIT ' + ListLimit;
        System.debug('fetchRecords query: ' + sQuery);  
        List < sObject > lstOfRecords = Database.query(sQuery);
        
        for (sObject obj: lstOfRecords) {
            returnList.add(obj);
        }
        return returnList;
    }

    @AuraEnabled
    public static List<sObject> fetchVehicleLookUpValues(String searchKeyWord,boolean activeVehicleOnly,boolean availableVehicleOnly ){ 
        
        List<sObject> returnList = new List<sObject>();
        List<String> filters = new List<String>();
        
        CL_PortalController.UserDetailCls currentUserDetails = new CL_PortalController.UserDetailCls(userInfo.getUserId()); 
        if(currentUserDetails.clientAccount!=null){
            string businessAccountFilter = ' CL_Business_Account__c = ' + '\''+ currentUserDetails.clientAccount.id + '\'';
            filters.add('(' + businessAccountFilter + ')' );
            
            if(activeVehicleOnly){
                string activeVehicleFilter = ' CL_Driver__c != null ';
                filters.add('(' + activeVehicleFilter + ')' );
            }
            if(availableVehicleOnly && !activeVehicleOnly){
                string availableVehicleFilter = ' CL_Driver__c = null ';
                filters.add('(' + availableVehicleFilter + ')' );
            }           
        }
        
        string LIKEsearchQuery = 'Name' + ' LIKE \'%' + searchKeyWord + '%\'';        
        filters.add('(' + LIKEsearchQuery + ')' );
        
        // Create a Dynamic SOQL Query For Fetch Record List with LIMIT 5   
        String sQuery =  'select id, Name, CL_Driver_Name__c,CL_Driver__c, CL_Driver__r.Phone, Driver_Phone__c from CL_Vehicle__c' ;

        if (filters.size() > 0) {
            sQuery += ' WHERE ' + String.join(filters, ' AND ');
        }
        
        sQuery += ' Order by createdDate DESC LIMIT 10';
System.debug('sQuery-->'+sQuery);
        List<sObject> lstOfRecords = Database.query(sQuery);
        for (sObject obj: lstOfRecords) {
            returnList.add(obj);
       }
         return returnList;
    }

    @AuraEnabled
    public static user  getCurrentUser(){
        user  userObj =  [select id,CL_LUMI_Role__c,FirstName,LastName, Email,MobilePhone, AccountId from user where id = : userInfo.getUserId() limit 1];   
        return userObj;
    }   
    
    @AuraEnabled
    public static UserDetailCls  getCurrentUserDetails(){
        //User currentUser = [select id from User where id = : userInfo.getUserId() limit 1];   
        UserDetailCls  userDetailObj = new UserDetailCls(userInfo.getUserId());      
        return userDetailObj;
    }   
    
  /*  @AuraEnabled
    public static string saveCase(string eventFields, string documentId) {

        case myCase = (case)JSON.deserialize(eventFields,case.Class);

        Savepoint sp= Database.setSavepoint();
        try{

            insert  myCase;
            if(documentId!=null){
                ContentDocumentLink newclnk = new ContentDocumentLink(ContentDocumentId=documentId, LinkedEntityId=myCase.Id,ShareType='V');
                insert newclnk;
            }
            
            return myCase.Id;
        } catch(exception e){               
                system.debug('error: '+e.getStackTraceString());  
                Database.Rollback(sp); 
                return 'error';
        } 


    }*/

    // User Details structure using on the fleet management community screen. 
    public class UserDetailCls {
        @AuraEnabled
        public user userObj; 
        @AuraEnabled
        public Account driverAccount;
        @AuraEnabled        
        public Account clientAccount; 
        @AuraEnabled
        public string LumiRole;
        @AuraEnabled
        public List<cl_vehicle__c> vehicles;
        
        public UserDetailCls(string userId){             
            userObj = new User();           
            driverAccount  = new Account();
            clientAccount  = new Account();
            vehicles = new List<cl_vehicle__c>();
            
            // fetch User details
            if(String.isNotBlank(userId)){
                userObj = [select id,CL_LUMI_Role__c,FirstName,LastName, Email,MobilePhone, AccountId, Account.CL_Business_Account__c  from user where id = : userId limit 1];   
                LumiRole = userObj.CL_LUMI_Role__c;
                
                if(userObj.AccountId!=null ){
 
                    List<Account> tempPersAccount =     new List<Account> ();
                     tempPersAccount = [select id,OwnerId, CL_Business_Account__c,CL_Business_Account__r.Name,CL_Business_Account__r.OwnerId from Account where Id = : userObj.AccountId Limit 1];
                    if(tempPersAccount.size()>0){ 
                            driverAccount = tempPersAccount[0];                 
                    } 
                    
                    if(driverAccount!=null && driverAccount.CL_Business_Account__c!=null && driverAccount.CL_Business_Account__r!=null){
                        clientAccount = driverAccount.CL_Business_Account__r;                 
                    }

                    List<cl_vehicle__c> tempVehicles =  new List<cl_vehicle__c> (); 
                    tempVehicles = [select id, Name, CL_Driver_Name__c,CL_Driver__c, CL_Driver__r.Phone, Driver_Phone__c from CL_Vehicle__c WHERE CL_Driver__c =:  userObj.AccountId];
                    for(cl_vehicle__c temVehicle: tempVehicles){
                        vehicles.add(temVehicle);
                    }   
                    
                }
                 

            }   
            //clientAccount = new Account(Id= driverAccount.CL_Business_Account__c);
            system.debug('UserDetailCls driverAccount: ' +driverAccount);   
            system.debug('UserDetailCls clientAccount: ' +clientAccount);
            system.debug('UserDetailCls vehicles: ' +vehicles);             
        }
    }
      
   @AuraEnabled
    public static CL_FleetManagementController.CommunityMessages deactivateUsers(List<String> vehicleList){
    
        List<Id> accountList=new List<Id>();
        List<Id> contactList=new List<Id>();
        CL_FleetManagementController.CommunityMessages msg=new CL_FleetManagementController.CommunityMessages('',true);
        String resp='';
        List<CL_Vehicle__c> vehList=new  List<CL_Vehicle__c>();
        
        List<CL_Vehicle__c > driverList =[select CL_Driver__c,id from CL_Vehicle__c where id in :vehicleList];
        if(driverList.size()>0 && driverList !=null ){
        for(CL_Vehicle__c driver:driverList){
            accountList.add(driver.CL_Driver__c);
          }
          
           for(CL_Vehicle__c  veh:driverList ){
              veh.CL_Driver__c=null;
              vehList.add(veh);
          }
          
          }
        
        List<Account> contactIdList=[SELECT personcontactId from account where id in:accountList];
         if(contactIdList.size()>0 && contactIdList !=null ){
        for(Account acc:contactIdList){
            contactList.add(acc.personcontactId);
          }
          
         }
          
     
            List<User> userList=[select id,IsActive  from User where ContactId in :contactList];
            system.debug('user list-->'+userList);
            if(userList.size()>0 && userList!=null )
            msg=CL_PortalController.deActivateUser(UserList,vehList);
            return msg;
    }
    
      @AuraEnabled
      public static CL_FleetManagementController.CommunityMessages deActivateUser(List<User> userList,List<CL_Vehicle__c> vehicleList){
        List<User> userListToDeactivate=new List<User>();
        for(User usr:userList){
            user userObjtoDeactivate = new user(id =  usr.id,IsActive = false);
            userListToDeactivate.add(userObjtoDeactivate);
        }
        
        String userId=JSON.Serialize (userListToDeactivate);
        try{
             update vehicleList;
             updateUsers(userId); 
             
        }catch(Exception ex){
             return new CL_FleetManagementController.CommunityMessages('Error',false);
        }
        
         return new CL_FleetManagementController.CommunityMessages(Label.UserDeActivationSuccessMessage,true);            
    }
    
   @future
   public static void updateUsers(string userList){
     List<User> userListToBeUpdated=new List<User>();
     userListToBeUpdated=(List<User>) JSON.deserialize(userList,List<User>.Class);
     update userListToBeUpdated ;
   }
    @AuraEnabled
    public static Case getRecordType(String caseID){
        
        return [select Status, RecordType.DeveloperName from Case where ID=:caseID][0];
        
    }
   
}