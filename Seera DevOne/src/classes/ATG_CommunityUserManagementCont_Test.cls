/**
 * Created by mnazir001 on 11/26/18.
 */

@IsTest
private class ATG_CommunityUserManagementCont_Test {
    @IsTest
    static void testCreateCommunityUser() {
        Account acc = ATG_TestDataFactory.createEmployeeAccount();
        acc.ATG_User_Type__c = '';
        insert acc;

        UserType_Profile_Mapping__c profileMapping = new UserType_Profile_Mapping__c(Name='Requestor', Profile_Name__c='MoH Requester Community Login User');
        insert profileMapping;

        ATG_CommunityUserManagementController.CreateCommunityUser(acc.id, true);
        
        acc.ATG_User_Type__c = 'Requestor';
        Update acc;

        ATG_CommunityUserManagementController.CreateCommunityUser(acc.id, true);        


     
        
        User usrObj = [select id from User limit 1];
        
        
        User requesterUser = ATG_TestDataFactory.createUser(acc, null);
        acc.ATG_Community_User__c = requesterUser.id;
        Update acc;
        
        ATG_CommunityUserManagementController.CreateCommunityUser(acc.id, false);        
        
        // Create Program Department Relationship
        list<ATG_Master__c> masterList = new list<ATG_Master__c>();
        masterList.addAll(ATG_TestDataFactory.createMaster(2,'p'));
        masterList.addAll(ATG_TestDataFactory.createMaster(2,'d'));
        masterList.addAll(ATG_TestDataFactory.createMaster(2,'c'));
        
        
        
        
        
        
        // create Program Department Relationship
        
        ATG_Department_Relationship__c dr = new ATG_Department_Relationship__c();
        list<ATG_Department_Relationship__c> drList = new list<ATG_Department_Relationship__c>();
        drList.add(new ATG_Department_Relationship__c(Department__c=masterList[2].Id,Program__c=masterList[0].Id));
        drList.add(new ATG_Department_Relationship__c(Department__c=masterList[3].Id,Program__c=masterList[1].Id));     
        insert drList;
        
        
        list<string> programList = new list<string>{masterList[0].Id,masterList[1].Id};
        list<string> depList = new list<string>{masterList[3].Id,masterList[2].Id};
        
        User_Department__c ud = new User_Department__c(ATG_Account__c=acc.Id,Program__c=masterList[0].Id,User__c=usrObj.Id);
        insert ud;
        

        ATG_CommunityUserManagementController.UpdateUserRelationships(acc.id,usrObj.Id,programList,depList,'Requestor');  
        ATG_CommunityUserManagementController.updateUserDepartments(acc.id,usrObj.Id,programList,depList);
        ATG_CommunityUserManagementController.FetchPrograms();
        ATG_CommunityUserManagementController.FetchDepartments(programList);         
        ATG_CommunityUserManagementController.GetUserDepartments();
        ATG_CommunityUserManagementController.GetUserDetails(acc.id);    
        ATG_CommunityUserManagementController.RestPassword(acc.id);            

        
        List<ID> accountIds = new List<ID>();
        accountIds.add(acc.id);
        
        ATG_CommunityUserManagementController.updateCommunityUserAsync(accountIds);
                
        
    }
}