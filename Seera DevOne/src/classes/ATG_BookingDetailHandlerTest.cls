/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class ATG_BookingDetailHandlerTest {

    static testMethod void ATG_BookingDetailHandler1() {
        ATG_TestUtility objTest = new ATG_TestUtility();
        string bookingID = objTest.createflightbooking();
        
        ATG_Booking_Detail__c objBD = new ATG_Booking_Detail__c();
        objBD.ATG_Booking__c = bookingID;
        objBD.ATG_Departure_Date__c = system.today().adddays(2);
        objBD.ATG_PNR__c = '456';
        objBD.recordtypeid = Schema.SObjectType.ATG_Booking_Detail__c.getRecordTypeInfosByName().get('ATG Flight').getRecordTypeId();
        
        ATG_Booking_Detail__c objBD0 = new ATG_Booking_Detail__c();
        objBD0.ATG_Booking__c = bookingID;
        objBD0.ATG_Departure_Date__c = system.today().adddays(2);
        objBD0.ATG_PNR__c = '123';
        objBD0.recordtypeid = Schema.SObjectType.ATG_Booking_Detail__c.getRecordTypeInfosByName().get('ATG Flight').getRecordTypeId();
        
        ATG_Booking_Detail__c objBD1 = new ATG_Booking_Detail__c();
        objBD1.ATG_Booking__c = bookingID;
        objBD1.ATG_Check_Out__c = system.today().adddays(2);
        objBD.ATG_Booking_Type__c = 'Flight';
        objBD1.recordtypeid = Schema.SObjectType.ATG_Booking_Detail__c.getRecordTypeInfosByName().get('ATG Hotel').getRecordTypeId();
        
        ATG_Booking_Detail__c objBD2 = new ATG_Booking_Detail__c();
        objBD2.ATG_Booking__c = bookingID;
        objBD2.ATG_Tour_Date__c = system.today().adddays(2);
        objBD.ATG_Booking_Type__c = 'Hotel';
        objBD2.recordtypeid = Schema.SObjectType.ATG_Booking_Detail__c.getRecordTypeInfosByName().get('ATG-Sightseeing').getRecordTypeId();
        
        ATG_Booking_Detail__c objBD3 = new ATG_Booking_Detail__c();
        objBD3.ATG_Booking__c = bookingID;
        objBD3.ATG_End_Date__c = system.today().adddays(2);
        objBD3.recordtypeid = Schema.SObjectType.ATG_Booking_Detail__c.getRecordTypeInfosByName().get('ATG Visa').getRecordTypeId();
        
        
        
        list<ATG_Booking_Detail__c> soNewList = new list<ATG_Booking_Detail__c>();
        soNewList.add(objBD);
        soNewList.add(objBD1);
        soNewList.add(objBD2);
        soNewList.add(objBD3);
        insert soNewList;
        
        map<id,ATG_Booking_Detail__c> soOldMap = new map<id,ATG_Booking_Detail__c>();
        soOldMap.put(objBD.id,objBD);
        
        ATG_BookingDetailHandler objClass = new ATG_BookingDetailHandler();
        objClass.beforeInsert (soNewList);
        objClass.beforeUpdate (soNewList,soOldMap);
        objClass.beforeDelete (soNewList);
        objClass.afterInsert (soNewList);
        objClass.afterUpdate (soNewList,soOldMap);
        objClass.afterDelete (soNewList);
    }
    
    static testMethod void ATG_BookingDetailHandler(){
        Test.startTest();
        
        ATG_Product_Details__c objPD = new ATG_Product_Details__c();
        objPD.Name = 'Flight';
        insert objPD;
        
        objPD = new ATG_Product_Details__c();
        objPD.Name = 'Hotel';
        insert objPD;
        
        list<string> lstSVChargeTypes = new list<string>{'Insurance','Loyalty','Rule','VAT'};
        list<Product_Service_Charge_Types__c> lstChargeTypes = new list<Product_Service_Charge_Types__c>();
        for(string strChargeType:lstSVChargeTypes){
            lstChargeTypes.add(new Product_Service_Charge_Types__c(Name=strChargeType));
        }
        insert lstChargeTypes;
        
        list<ATG_Product_Business_Units__c> lstPBU = new list<ATG_Product_Business_Units__c>();
        lstPBU.add(new ATG_Product_Business_Units__c(Name='Al Mosafer'));
        lstPBU.add(new ATG_Product_Business_Units__c(Name='Tajawal'));
        insert lstPBU;
        
        
        ATG_Booking__c objBooking = new ATG_Booking__c();
        objBooking.Name = 'A295835';
        objBooking.ATG_Business_Unit__c ='Al Mosafer';
        objBooking.ATG_Product_Type__c = 'Flight';
        insert objBooking;

        ATG_Booking_Detail__c objBookingDetail= new ATG_Booking_Detail__c();
        objBookingDetail.ATG_Booking__c = objBooking.Id;
        objBookingDetail.ATG_Trip_Type__c = 'Roudtrip';
        objBookingDEtail.ATG_Booking_Type__c = 'Flight';
        
        objBookingDEtail.ATG_Check_Out__c = System.NOW();
        objBookingDEtail.ATG_Hotel_Name__c = 'test';
        objBookingDEtail.ATG_Is_Domestic__c = true;
        insert objBookingDetail;
        
        ATG_Booking_Detail__c objBD5 = new ATG_Booking_Detail__c();
        objBD5.ATG_Booking__c = objBooking.Id;
        objBD5.ATG_End_Date__c = system.today().adddays(2);
        objBD5.ATG_Booking_Type__c = 'VAT';
        objBD5.ATG_Projected_Selling__c= 567;
        objBD5.recordtypeid = Schema.SObjectType.ATG_Booking_Detail__c.getRecordTypeInfosByName().get('ATG Rule').getRecordTypeId();
        insert objBD5;
        
        ATG_Booking_Detail__c objBD6 = new ATG_Booking_Detail__c();
        objBD6.ATG_Booking__c = objBooking.Id;
        objBD6.ATG_End_Date__c = system.today().adddays(2);
        objBD6.ATG_Booking_Type__c = 'Insurance';
        objBD6.ATG_Projected_Selling__c= 123;
        objBD6.recordtypeid = Schema.SObjectType.ATG_Booking_Detail__c.getRecordTypeInfosByName().get('ATG Rule').getRecordTypeId();
        insert objBD6;
        
        ATG_Booking_Detail__c objBD7 = new ATG_Booking_Detail__c();
        objBD7.ATG_Booking__c = objBooking.Id;
        objBD7.ATG_End_Date__c = system.today().adddays(2);
        objBD7.ATG_Booking_Type__c = 'Loyalty';
        objBD7.ATG_Projected_Selling__c= 113;
        objBD7.recordtypeid = Schema.SObjectType.ATG_Booking_Detail__c.getRecordTypeInfosByName().get('ATG Rule').getRecordTypeId();
        insert objBD7;
        
         ATG_Booking_Detail__c objBD8 = new ATG_Booking_Detail__c();
        objBD8.ATG_Booking__c = objBooking.Id;
        objBD8.ATG_End_Date__c = system.today().adddays(2);
        objBD8.ATG_Booking_Type__c = 'Rule';
        objBD8.ATG_Projected_Selling__c= 213;
        objBD8.recordtypeid = Schema.SObjectType.ATG_Booking_Detail__c.getRecordTypeInfosByName().get('ATG Rule').getRecordTypeId();
        insert objBD8;

        list<ATG_Booking_Detail__c> soNewList = new list<ATG_Booking_Detail__c>();
        soNewList.add(objBookingDetail);
        soNewList.add(objBD5);
        soNewList.add(objBD6);
        soNewList.add(objBD7);
        soNewList.add(objBD8);
        
        ATG_BookingDetailController var = new ATG_BookingDetailController();
        
        
        map<id,ATG_Booking_Detail__c> soOldMap = new map<id,ATG_Booking_Detail__c>();
        soOldMap.put(objBookingDetail.id,objBookingDetail);
                        
        ATG_BookingDetailHandler objClass = new ATG_BookingDetailHandler();
        ATG_BookingDetailcontroller objClass1 = new ATG_BookingDetailController();
        objClass.beforeInsert (soNewList);
        objClass.beforeUpdate (soNewList,soOldMap);
        objClass.beforeDelete (soNewList);
        objClass.afterInsert (soNewList);
        objClass.afterUpdate (soNewList,soOldMap);
        objClass.afterDelete (soNewList);
        
        try{
            ATG_BookingDetailcontroller.UpdatePrimaryBookingDetail(JSON.serialize(soNewList));
        }catch(Exception e){
        
        }
        
        ATG_BookingDetailcontroller.ProductBookingDetail objwrap = new ATG_BookingDetailcontroller.ProductBookingDetail(new ATG_Booking_Detail__c());
        Test.stopTest();
    }
}