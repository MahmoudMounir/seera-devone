@isTest
private class ATG_CaseCommentTriggerHandlerTest {

    static testmethod void test_CaseCommentTriggerHandler(){
        Test.startTest();
        
        ATG_Booking__c booking = new ATG_Booking__C();
        booking.Name = 'H9034035';
        booking.ATG_Product_Type__c = 'Hotel';
        booking.ATG_Booking_Reference_Number__c = 'bdsgj74385436srer';
        booking.ATG_Business_Unit__c = ATG_ConstantsUtility.ALMOSAFER_BusinessUnit;
        insert booking;
        
        
        Case objCase = new Case();
        objCase.RecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByDeveloperName().get('AMH_Amendment').getRecordTypeId();
        objCase.ATG_BU_From_Case__c = ATG_ConstantsUtility.ALMOSAFER_BusinessUnit;
        objCase.ATG_Booking__c = booking.Id;
        insert objCase;
        
        CaseComment objCaseComment = new CaseComment();
        objCaseComment.CommentBody = 'test';
        objCasecomment.ParentId = objCase.Id;
        insert objCaseComment;
        
        list<CaseComment> soNewList = new list<CaseComment>();
        soNewList.add(objCasecomment);
        
      
        ATG_CaseCommentTriggerHandler objClass = new ATG_CaseCommentTriggerHandler();
        objClass.beforeInsert (soNewList);
        
        Test.stopTest();
    }
    
    static testmethod void test_CaseCommentTriggerHandler1(){
        Test.startTest();
        
        Case objCase = new Case();
        objCase.RecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByDeveloperName().get('TJW_Booking_Enquiry').getRecordTypeId();
        objCase.ATG_BU_From_Case__c = ATG_ConstantsUtility.TAJAWAL_BusinessUnit;
        insert objCase;
        
        ATG_Booking__c booking = new ATG_Booking__C();
        booking.Name = 'H9034035';
        booking.ATG_Product_Type__c = 'Hotel';
        booking.ATG_Booking_Reference_Number__c = 'bdsgj74385436srer';
        booking.ATG_Business_Unit__c = ATG_ConstantsUtility.TAJAWAL_BusinessUnit;
        booking.ATG_Case__c = objCase.Id;
        insert booking;
        
        CaseComment objCaseComment = new CaseComment();
        objCaseComment.CommentBody = 'test';
        objCasecomment.ParentId = objCase.Id;
        insert objCaseComment;
        
        list<CaseComment> soNewList = new list<CaseComment>();
        soNewList.add(objCasecomment);
        
      
        ATG_CaseCommentTriggerHandler objClass = new ATG_CaseCommentTriggerHandler();
        objClass.beforeInsert (soNewList);
        objClass.beforeUpdate (soNewList,null);
        objClass.beforeDelete (soNewList);
        objClass.afterInsert (soNewList);
        objClass.afterUpdate (soNewList,null);
        objClass.afterDelete (soNewList);
        Test.stopTest();
    }
    
    static testmethod void test_CaseCommentTriggerHandler2(){
        Test.startTest();
        
        Case objCase = new Case();
        objCase.RecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByDeveloperName().get('AMH_Booking_Enquiry').getRecordTypeId();
        objCase.ATG_BU_From_Case__c = ATG_ConstantsUtility.ALMOSAFER_BusinessUnit;
        insert objCase;
        
        ATG_Booking__c booking = new ATG_Booking__C();
        booking.Name = 'H9034035';
        booking.ATG_Product_Type__c = 'Hotel';
        booking.ATG_Booking_Reference_Number__c = 'bdsgj74385436srer';
        booking.ATG_Business_Unit__c = ATG_ConstantsUtility.ALMOSAFER_BusinessUnit;
        booking.ATG_Case__c = objCase.Id;
        insert booking;
        
        CaseComment objCaseComment = new CaseComment();
        objCaseComment.CommentBody = 'test';
        objCasecomment.ParentId = objCase.Id;
        insert objCaseComment;
        
        list<CaseComment> soNewList = new list<CaseComment>();
        soNewList.add(objCasecomment);
        
      
        ATG_CaseCommentTriggerHandler objClass = new ATG_CaseCommentTriggerHandler();
        objClass.beforeInsert (soNewList);
        objClass.beforeUpdate (soNewList,null);
        objClass.beforeDelete (soNewList);
        objClass.afterInsert (soNewList);
        objClass.afterUpdate (soNewList,null);
        objClass.afterDelete (soNewList);
        Test.stopTest();
    }
}