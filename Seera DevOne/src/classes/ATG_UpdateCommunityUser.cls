/**************************************************************************************************
* Name               : ATG_UpdateCommunityUser
* Description        : Controller to Update community user                                
* Created Date       : 09 Oct 2018                                                              *
* Created By         : PWC Digital                                              
* -------------------------------------------------------------------------------------------------
* Version       Author      Date        Comment                                                               *

**************************************************************************************************/
public class ATG_UpdateCommunityUser{

    @InvocableMethod(label='Update Community User' description='Update community user upon account Update') 
    public static void UpdateCommunityUser(List<ID> ids){
        ATG_CommunityUserManagementController.updateCommunityUserAsync(ids);
    }   

}