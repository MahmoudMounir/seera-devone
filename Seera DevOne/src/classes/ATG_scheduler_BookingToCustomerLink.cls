/**************************************************************************************************
* Name               : ATG_scheduler_BookingToCustomerLink                                                               
* Description        : scheduler to run the batch to link bookings to customers when customers are pulled from Tapro                                       
* Created Date       : 19th June 2018                                                                *
* Created By         : Swati Sehrawat (PWC Digital)                                              
* -------------------------------------------------------------------------------------------------
* Version    Author    Date    Comment                                                                *

**************************************************************************************************/

global class ATG_scheduler_BookingToCustomerLink implements Schedulable { 
    
    global void execute(SchedulableContext sc) {
        ATG_Batch_BookingToCustomerLink obj = new ATG_Batch_BookingToCustomerLink();  
        database.executebatch(obj,500);
    }   
}