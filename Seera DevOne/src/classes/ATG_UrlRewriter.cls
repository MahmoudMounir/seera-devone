global class ATG_UrlRewriter implements Site.UrlRewriter {
    
    global PageReference mapRequestUrl(PageReference friendlyURL){
        
         string url = friendlyURL.getUrl();
                                        system.debug(friendlyURL);
        
        if(null != url && url.contains('socialmedia')){
           
            String[] baseURL =url.split('/');
            
            if(baseURL.size()>=3){
                String caseNumber = baseURL[3];
                String caseId= null;
                for(Case objCase:[SELECT Id FROM Case Where CaseNumber=:caseNumber limit 1]){
                    caseId = objCase.Id;
                }
                String surveyId=baseURL[2];
                String oId= url.substring(url.indexOf(baseURL[3])+baseURL[3].length()+1,url.length());
                url = '?id='+surveyId+'&caId='+caseId+'&oId='+oId;
                system.debug('***'+url);
            }
        }
        else if( null != url && url.contains('email')){
             String[] baseURL =url.split('/');
            
            if(baseURL.size()>=3){
                
                String surveyId=baseURL[2];
                String cId= baseURL[3];
                String caId = baseURL[4];
                String aId = url.substring(url.indexOf(baseURL[4])+baseURL[4].length()+1,url.length());
                
                
                url = '?id='+surveyId+'&aID='+aId+'&cId='+cId+'&caId='+caId;
                system.debug('***'+url);
            }
        }else if( null != url && url.contains('booking')){
             String[] baseURL =url.split('/');
            
            if(baseURL.size()>=3){
                
                String surveyId=baseURL[2];
                String aId= baseURL[3];
                String bId = url.substring(url.indexOf(baseURL[3])+baseURL[3].length()+1,url.length());
               
                url = '?id='+surveyId+'&bID='+bId+'&aID='+aId;
                system.debug('***'+url);
            }
        }
        
      
        return new PageReference(url);
    }
    global PageReference[] generateUrlFor(List<PageReference> salesforceURLs){
       system.debug('>>>> URLReWriter:generateUrlFor: '+salesforceURLs);
        List<PageReference> friendlyUrls = new List<PageReference>();
        return friendlyURLs;   
    }
}