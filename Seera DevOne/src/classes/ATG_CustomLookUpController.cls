/**************************************************************************************************
* Name               : ATG_CustomLookUpController                                                               
* Description        : Custom Lookup Component Controller to fetch the values based on the searchkeyword. 
						This is used in flows.
* Created Date       : 27/12/2018                                                                
* Created By         : Diana Correa                                            
* -------------------------------------------------------------------------------------------------
* Version          Author    Date           Comment                                                              
  MVP 2- 1.1       Leeba      27/12/2018    get look up values based on search keyword,object name and record type
**************************************************************************************************/

public class ATG_CustomLookUpController {
	
    @AuraEnabled
    public static List < sObject > fetchLookUpValues(String searchKeyWord, String ObjectName,String recordType) {
        system.debug('ObjectName-->' + ObjectName);
        String searchKey = '%'+searchKeyWord + '%';
        
        List < sObject > returnList = new List < sObject > ();
      
        // Create a Dynamic SOQL Query For Fetch Record List with LIMIT 5   
        String sQuery =  'select id, Name from ' +ObjectName + ' where Name LIKE: searchKey AND RecordType.DeveloperName=:recordType order by createdDate DESC limit 5';
        List < sObject > lstOfRecords = Database.query(sQuery);
        
        for (sObject obj: lstOfRecords) {
            returnList.add(obj);
        }
        return returnList;
    }
}