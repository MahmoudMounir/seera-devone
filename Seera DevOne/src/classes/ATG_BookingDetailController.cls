/**
 * Created by mnazir001 on 1/31/19.
 */
/**************************************************************************************************
* Name               : ATG_BookingDetailController
* Description        : Booking Detail trigger controller, to calculate the amount for related booking products and service charges.
* Created Date       : 1/31/19                                                            *
* Created By         : Muhammad Nazir (PWC Digital)
* -------------------------------------------------------------------------------------------------
* Version       Author      Date        Comment                                                               *
  v1.0          Leeba       20/3/2019   Updated the code to make the calculations in update scenarios
**************************************************************************************************/


public with sharing class ATG_BookingDetailController {

    public static String PACKAGE_BOOKING_PRODUCT_NAME   = 'Package';
    public static String FLIGHT_BOOKING_PRODUCT_NAME    = 'Flight';
    public static String COUPON_SERVICE_NAME            = 'coupon';
    public static String LOYALTY_SERVICE_NAME           = 'Loyalty';
    public static String INSURANCE_SERVICE_NAME         = 'Insurance';
    public static String VAT_SERVICE_NAME               = 'VAT';
    
    public static boolean IsRecursive = false;


    /***
     * @param bookingDetails - list of booking details
     *******
     * @description update the main product details with amount calculated based on the configured business units and set of rule types
     */
    public static void updateBookingDetails(List<ATG_Booking_Detail__c> bookingDetails){

        // Business Units
        Set<String> businessUnitsSet = new Set<String>();
        for(ATG_Product_Business_Units__c businessUnit: [SELECT Name FROM ATG_Product_Business_Units__c]){
            businessUnitsSet.add(businessUnit.Name);
        }
        System.debug('Businee Unit List ' + businessUnitsSet);

        // Product types
        Set<String> productDetailsSet = new Set<String>();
        for(ATG_Product_Details__c product: [SELECT Name FROM ATG_Product_Details__c]){
            productDetailsSet.add(product.Name);
        }

        System.debug('Products List ' + productDetailsSet);

        // VAT, Rules and other service charges
        Set<String> serviceChargeTypesSet = new Set<String>();
        for(Product_Service_Charge_Types__c serviceChargeType: [SELECT Name FROM Product_Service_Charge_Types__c])
            serviceChargeTypesSet.add(serviceChargeType.Name);

        System.debug('Services List ' + serviceChargeTypesSet);

        // A map from booking product to list of service charges
        Map<String, ProductBookingDetail> bookingDetailMap = new Map<String, ProductBookingDetail>();
        
        set<string> setATGBookingIds = new set<string>();
        
        Set<Id> bookingIds = new Set<Id>();
        for(ATG_Booking_Detail__c bookingDetail: bookingDetails){
            if(bookingDetail.ATG_Booking__c!=null)
                setATGBookingIds.add(bookingDetail.ATG_Booking__c);
            bookingIds.add(bookingDetail.Id);
        }
        
        if(setATGBookingIds.size()>0){
            for(ATG_Booking_Detail__c ATGBD:[Select Id from ATG_Booking_Detail__c where ATG_Booking__c IN:setATGBookingIds and Id NOT IN:bookingIds and (ATG_Booking_Type__c='Flight' or ATG_Booking_Type__c='Hotel')]){
                bookingIds.add(ATGBD.Id);
            }
        }
        System.debug('bookingIds***' + bookingIds);

        List<ATG_Booking_Detail__c> bookingDetailsList = [
                SELECT id, ATG_Booking__c, ATG_Booking_Type__c, ATG_Discount_Amount__c, ATG_Service_Fee__c, ATG_Projected_Selling__c,
                        ATG_Booking__r.ATG_Product_Type__c, ATG_Booking__r.ATG_Business_Unit__c, ATG_Service_Name__c, ATG_Category__c,
                        ATG_Loyalty_Amount__c, ATG_Insurance_Amount__c
                FROM ATG_Booking_Detail__c
                WHERE Id in :bookingIds and (ATG_Booking__r.ATG_Business_Unit__c IN :businessUnitsSet)
        ];

        // Initialize with primary product (Hotel/Flight etc)
        for(ATG_Booking_Detail__c bookingDetail: bookingDetailsList){

            String bookingDetailType = bookingDetail.ATG_Booking_Type__c;
            system.debug('bookingdetailtype===> ' +bookingDetail.ATG_Booking_Type__c);
            
             if(productDetailsSet.contains(bookingDetailType) && !bookingDetailMap.containsKey(bookingDetail.ATG_Booking__c)){               
                if(bookingDetail.ATG_Booking__r.ATG_Product_Type__c == PACKAGE_BOOKING_PRODUCT_NAME ){

                    if(bookingDetail.ATG_Booking_Type__c == FLIGHT_BOOKING_PRODUCT_NAME)
                        bookingDetailMap.put(bookingDetail.ATG_Booking__c, new ProductBookingDetail(bookingDetail));

                }else{
                    bookingDetailMap.put(bookingDetail.ATG_Booking__c, new ProductBookingDetail(bookingDetail));
                }
            }
            
        }
        system.debug('BookingdetailMap==>' +bookingDetailMap);
        
        for(ATG_Booking_Detail__c bookingDetail: bookingDetailsList){

            String bookingType = bookingDetail.ATG_Booking__r.ATG_Product_Type__c;
            String businessUnit = bookingDetail.ATG_Booking__r.ATG_Business_Unit__c;
            String bookingDetailType = bookingDetail.ATG_Booking_Type__c;

            System.debug('Business Unit service ' + businessUnit);
            System.debug('Main Booking type ' + bookingType);
            System.debug('Booking detail type ' + bookingDetailType);

            // If bookingdetail item type is of type service charges, add to the list
            if(bookingDetailMap.containsKey(bookingDetail.ATG_Booking__c) && serviceChargeTypesSet.contains(bookingDetailType)){
                bookingDetailMap.get(bookingDetail.ATG_Booking__c).productServiceCharges.add(bookingDetail);
            }

        }


        System.debug('Printing all the booking details structure' + bookingDetailMap.keySet().size());


        List<ATG_Booking_Detail__c> updatedPrimaryBookingDetails = new List<ATG_Booking_Detail__c>();
        List<ATG_Booking_Detail__c> unusedBookingDetails = new List<ATG_Booking_Detail__c>();

        // Iterate through all the product map items and,
        // it's corresponding list of service charges to calculate the total

        try{
            for(String strKey: bookingDetailMap.keySet()){
                System.debug('Product ID ==>' + strKey);
                System.debug('Booking Service Charge ' +  bookingDetailMap.get(strKey));
                ProductBookingDetail productBooking = bookingDetailMap.get(strKey);
                // Calculate service charges total
                if(productBooking != null && productBooking.primaryProduct != null){
                    System.debug('Updating the prices ....');
                    updatedPrimaryBookingDetails.add(ATG_BookingDetailController.calculateTotalCharges(productBooking));
                    unusedBookingDetails.addAll(productBooking.productServiceCharges);
                }
            }
            System.debug('FinalProductBookings***'+ updatedPrimaryBookingDetails);
            System.debug('Final Product Service Bookings'+ unusedBookingDetails);
            
            IsRecursive = true;
            delete unusedBookingDetails;
            if(updatedPrimaryBookingDetails.size()>0){
                string strPrimaryBookingDetails = JSON.serialize(updatedPrimaryBookingDetails);
                UpdatePrimaryBookingDetail(strPrimaryBookingDetails);
            }
            //update updatedPrimaryBookingDetails;
        }catch(Exception ex){
            System.debug('Error : Unable to update the booking details ==> ' + ex.getStackTraceString());
        }
    }

    @future
    public static void UpdatePrimaryBookingDetail(string strPrimaryBookingDetails){
        
        List<ATG_Booking_Detail__c> lstBDs = (List<ATG_Booking_Detail__c>) JSON.deserialize(strPrimaryBookingDetails, List<ATG_Booking_Detail__c>.class);
        system.debug('lstBDs***'+lstBDs);
        if(lstBDs.size()>0){
            ATG_BookingDetailController.IsRecursive = true;
            try{
                update lstBDs;
            }catch(Exception e){}
        }
    }
    
    /***
     *
     *
     * @param productBookingDetail - ProductBookingDetail wrapper with
     *
     * @return
     */
    public static ATG_Booking_Detail__c calculateTotalCharges(ProductBookingDetail productBookingDetail){

        ATG_Booking_Detail__c primaryProduct = productBookingDetail.primaryProduct;

        Double totalDiscount = 0;//(primaryProduct.ATG_Discount_Amount__c == null ? 0 : Double.valueOf(primaryProduct.ATG_Discount_Amount__c));
        Double totalServiceCharges = 0;//(primaryProduct.ATG_Service_Fee__c == null ? 0 : Double.valueOf(primaryProduct.ATG_Service_Fee__c));
        Double TotalInsurance = 0;//(primaryProduct.ATG_Insurance_Amount__c == null ? 0 : Double.valueOf(primaryProduct.ATG_Insurance_Amount__c));
        Double LoyaltyAmount = 0;//(primaryProduct.ATG_Loyalty_Amount__c == null ? 0 : Double.valueOf(primaryProduct.ATG_Loyalty_Amount__c));
        Double VATAmount = 0;
        
        System.debug('Total Service Charges Items => '+ productBookingDetail.productServiceCharges.size() );

        for(ATG_Booking_Detail__c bookingDetail: productBookingDetail.productServiceCharges){

            Double projectedSellingValue = (bookingDetail.ATG_Projected_Selling__c == null ? 0 : Double.valueOf(bookingDetail.ATG_Projected_Selling__c));
            System.debug('Booking Detail Value***' + projectedSellingValue );

            if(bookingDetail.ATG_Booking_Type__c == LOYALTY_SERVICE_NAME){
                System.debug('Loyalty Value***' + bookingDetail.ATG_Loyalty_Amount__c);
                if(bookingDetail.ATG_Loyalty_Amount__c!=null)
                    LoyaltyAmount = LoyaltyAmount + bookingDetail.ATG_Loyalty_Amount__c;
                //primaryProduct.ATG_Loyalty_Amount__c = bookingDetail.ATG_Projected_Selling__c;
            }else if(bookingDetail.ATG_Booking_Type__c == INSURANCE_SERVICE_NAME){
                System.debug('Insurance Value***'+bookingDetail.ATG_Projected_Selling__c);
                if(bookingDetail.ATG_Projected_Selling__c!=null)
                    TotalInsurance = TotalInsurance + bookingDetail.ATG_Projected_Selling__c;
                //primaryProduct.ATG_Insurance_Amount__c = bookingDetail.ATG_Projected_Selling__c;
            }else if(bookingDetail.ATG_Booking_Type__c == VAT_SERVICE_NAME){
                System.debug('VAT Value***'+bookingDetail.ATG_Projected_Selling__c);
                if(bookingDetail.ATG_Projected_Selling__c!=null)
                    VATAmount = VATAmount + bookingDetail.ATG_Projected_Selling__c;
                //primaryProduct.ATG_Insurance_Amount__c = bookingDetail.ATG_Projected_Selling__c;
            }else{
                if(projectedSellingValue < 0){
                    totalDiscount += projectedSellingValue;
                }else{
                    totalServiceCharges += projectedSellingValue;
                }
            }
            if(bookingDetail.ATG_Category__c == COUPON_SERVICE_NAME){
                primaryProduct.ATG_Coupon_Name__c = bookingDetail.ATG_Service_Name__c;
            }
        }

        System.debug('Total Discount Value ==>' + totalDiscount );
        System.debug('LoyaltyAmount==>' + LoyaltyAmount);
        System.debug('TotalInsurance==>' + TotalInsurance);
        System.debug('VATAmount==>' + VATAmount);
        System.debug('Total Service Charges Value ==>' + totalServiceCharges );



        primaryProduct.ATG_Discount_Amount__c = totalDiscount;
        primaryProduct.ATG_Service_Fee__c = totalServiceCharges;
        primaryProduct.ATG_Insurance_Amount__c = TotalInsurance;
        primaryProduct.ATG_Loyalty_Amount__c = LoyaltyAmount;
        primaryProduct.ATG_Supplier_VAT__c = VATAmount;
        return primaryProduct;
    }


    /***
     * @PrimaryProduct - Hotel, Flight etc.
     *
     * @productServiceCharges - List of service charges, VAT, Rule etc.
     *
     * @description A wrapper class to structure related booking and service charges
     */
    public class ProductBookingDetail {
        public ATG_Booking_Detail__c primaryProduct {get; set;}
        public List<ATG_Booking_Detail__c> productServiceCharges {get; set;}
        public ProductBookingDetail(ATG_Booking_Detail__c productBooking){
            primaryProduct = productBooking;
            productServiceCharges = new List<ATG_Booking_Detail__c>();
        }
    }

}