/**************************************************************************************************
* Name               : ATG_WhatsAppIntegration                                                               
* Description        : WhatsApp Integration with salesforce Integration with LiveEngage                                        
* Created Date       : 17th July 2019                                                                
* Created By         : Srinivas Mannem
***************************************************************************************************/

public class ATG_WhatsAppIntegration {
    
    private static string whatsAppKey= system.label.WhatsApp_Key;
    private static string subject= 'WhatsApp Notification';
    private static string folderName= 'WA_folder';
    private static string recordTypeName= 'WhatsApp';
    private static string endpointURL= system.label.WhatsApp_Endpoint + whatsAppKey;
    private static string contentType= 'application/json';
    private static string countryCode;
    private static string phone;
    private static string template;
        
    @AuraEnabled
     public static Map<String, String> getselectOptions(sObject objObject, string fld) {
      system.debug('objObject --->' + objObject);
      system.debug('fld --->' + fld);
      List < String > allOpts = new list < String > ();
         
      Map<String, String> options = new Map<String, String>();   
      // Get the object type of the SObject.
      Schema.sObjectType objType = objObject.getSObjectType();
     
      // Describe the SObject using its object type.
      Schema.DescribeSObjectResult objDescribe = objType.getDescribe();
     
      // Get a map of fields for the SObject
      map < String, Schema.SObjectField > fieldMap = objDescribe.fields.getMap();
     
      // Get the list of picklist values for this field.
      list < Schema.PicklistEntry > values =
       fieldMap.get(fld).getDescribe().getPickListValues();
     
      // Add these values to the selectoption list.
      for (Schema.PicklistEntry a: values) {
		options.put(a.getValue(), a.getLabel());
      }
      system.debug('options ---->' + options);       
      //allOpts.sort();
      return options;
	}
    
    
	@AuraEnabled
    public static List<String> getTemplateName(){
        
        List <String> allOpts = new list < String > ();
        
        for(QuickText  txtObj :[select name from QuickText where Folder.name=:folderName AND is_Active__c=true]){
        	allOpts.add(txtObj.name);    
        }
		allOpts.sort();        
        return allOpts;
    }
    
    @AuraEnabled
    public static String getTemplateDataStr(string templateNameStr) {
         
         list<QuickText> selectedTemplate= new list<quickText>();
         selectedTemplate = [select id, name, Message from quicktext where name= :templateNameStr and Folder.name=:folderName AND is_Active__c=true];
         
         string tempMessage='';
         if(selectedTemplate.size() > 0){
             tempMessage = selectedTemplate[0].Message;
             system.debug('*** tempMessage '+ tempMessage);
             return tempMessage;
         }         
         return '';
     }
    
    @AuraEnabled
    public static String replaceTempVariable(string templateDataStr,list<string> varList){
        
        if(varList.size() > 0){
            for(string str :varList){
                if(str.contains('=>')){
                    string key= str.split('=>')[0];
                    if(templateDataStr.contains(key)){
                        system.debug(' templateDataStr '+ templateDataStr);
                        templateDataStr = templateDataStr.replace(key,str.split('=>')[1]);
                        system.debug(' replace variable '+ templateDataStr);
                    }
                }
            }
            return templateDataStr;
        }        
        return '';
    }
    
    @AuraEnabled
    public static String sendNotificationWhatsApp(string recordIdVal, string templateNameStr, string tempDataWithOutVarObj, string tempDataVarStr, string reasonForNotification, string otherReason, string countryCodeStr, string phoneNumberStr, list<string> varValueList){
        
        try{
            task tskObj = new task();
        	
            tskObj.whatId= recordIdVal;
            tskObj.subject=subject;
            tskObj.status='Sent';
            tskObj.Template_Name__c=templateNameStr;
            tskObj.template__c= tempDataWithOutVarObj;
            tskObj.Reason_for_sending_notification__c= reasonForNotification;
            tskObj.Other_Reason__c= otherReason;            
            tskObj.Phone_Mobile_Number__c = countryCodeStr + '-'+ phoneNumberStr;        
            tskObj.Template_Data__c=tempDataVarStr;
            tskObj.recordTypeId= Schema.SObjectType.Task.getRecordTypeInfosByDeveloperName().get(recordTypeName).getRecordTypeId();
            
            if(varValueList.size() > 0){
                for(string strObj :varValueList){
                    string key = strObj.split('=>')[0];
                    string value = strObj.split('=>')[1];
                    if(key=='{{1}}'){
                    	tskObj.Variable1__c=value;
                    }else if(key=='{{2}}'){
                    	tskObj.Variable2__c= value;   
                    }else if(key=='{{3}}'){
                    	tskObj.Variable3__c= value;    
                    }else if(key=='{{4}}'){
                    	tskObj.Variable4__c= value;    
                    }else if(key=='{{5}}'){
                    	tskObj.Variable5__c= value;    
                    }
                }
            }
              
            Database.SaveResult sr = database.insert(tskObj,true);

            if (sr.isSuccess()) {
                list<id> taskId= new list<Id>();
                taskId.add(tskObj.id);                    
                callWhatsAppAPI(taskId);    

                return 'SUCCESS';
            }else{
                for(Database.Error err : sr.getErrors()) {
                    System.debug(err.getStatusCode() + ': ' + err.getMessage());
                    return err.getMessage();
                }

            }            
            
            /*list<id> taskId= new list<Id>();
            taskId.add(tskObj.id);
            
            callWhatsAppAPI(taskId);*/
            
            return 'SUCCESS';
            
        }catch(exception ex){
            return ex.getMessage();
        }        
    }

    @future(callout=true)
    public static void callWhatsAppAPI(list<id> taskIds){
        
        postWhatsAppMsg(taskIds);

    }
    
    public static void postWhatsAppMsg(list<id> taskIds){
        
        list<task> taskList = [select id, whatId, Description, template__c,	Variable1__c, Variable2__c, Variable3__c, Variable4__c, Variable5__c, 	Request_Id__c, Message_Code__c, Error_Message__c, Message_Id__c, Template_Name__c, Phone_Mobile_Number__c, Reason_for_sending_notification__c, Other_Reason__c from Task where id IN :taskIds ];
        
        task tskObj = new task();
        if(taskList.size() > 0){
            tskObj= taskList[0];
        }
        
        if(tskObj.Phone_Mobile_Number__c.contains('-')){
            string [] phoneNumberList = tskObj.Phone_Mobile_Number__c.split('-');
            countryCode= phoneNumberList[0];
            phone = phoneNumberList[1];                
        }
        
        Http http = new Http();
        HttpRequest request = new HttpRequest();

        request.setEndpoint(endpointURL);
        request.setHeader('Content-Type', contentType);
        string bodyStr = '{"to": {"code": "'+ countryCode + '", "num": "' + phone + '"}, "template": { "id": "' + tskObj.Template_Name__c +'"';

        if(tskObj.Variable1__c !=null && tskObj.Variable1__c !=''){
            bodyStr = bodyStr + ',"variables": { "1": "'+ tskObj.Variable1__c +'"';
        }

        if(tskObj.Variable2__c !=null && tskObj.Variable2__c !=''){
            bodyStr = bodyStr + ',"2": "' + tskObj.Variable2__c +'"';
        }
		
		if(tskObj.Variable3__c !=null && tskObj.Variable3__c !=''){
            bodyStr = bodyStr + ',"3": "' + tskObj.Variable3__c +'"';
        }
		
        if(tskObj.Variable4__c !=null && tskObj.Variable4__c !=''){
            bodyStr = bodyStr + ',"4": "' + tskObj.Variable4__c +'"';
        }
		
		if(tskObj.Variable5__c !=null && tskObj.Variable5__c !=''){
            bodyStr = bodyStr + ',"5": "' + tskObj.Variable5__c +'"';
        }
		
		bodyStr = bodyStr + '} }, "meta": {} }';		
		
        system.debug(' *** bodyStr '+ bodyStr);

        request.setBody(bodyStr);
        request.setMethod('POST');
        system.debug('request--->>'+request.getBody());
        HttpResponse response = http.send(request);

        // If the request is successful, parse the JSON response.
        if (response.getStatusCode() == 200) {
            // Deserialize the JSON string into collections of primitive data types.
            JSONParser parser = JSON.createParser(response.getBody());            
            system.debug(' response1.getBody() '+ response.getBody());
            tskObj.status='Success';    
            tskObj.Message_Code__c= string.valueOf(response.getStatusCode());                
            ResponseBdy rs =  (ResponseBdy)JSON.deserialize(response.getBody(),ResponseBdy.class);   
            system.debug(' *** rs'+ rs);
            tskObj.Message_Id__c= rs.id;
            tskObj.request_Id__c= rs.requestId;
        }else{
            tskObj.status='Failed';    
            tskObj.Message_Code__c= string.valueOf(response.getStatusCode());
        }   
        try{
            database.update(tskObj,false);
        }catch(Exception ex){
            system.debug('*** eeror/exception message '+ ex.getMessage());
        }
        
    }
    
    public class RequestBdy{
        public ToInfo to {set;get;}
        public Template template {set;get;}
        public Meta meta {set;get;}
    }
    
    public class ToInfo{
        public String code {set;get;}
        public String num {set;get;} // number is a reserved keyword
    }
    
    public class Template{
        public String Id {set;get;}
        public Variable variable {set;get;}
    }
    
    public class Variable{
        public String num1 {set;get;}
    }
    
    public class Meta{
        public String MetaData {set;get;}
    }
    
    public class ResponseBdy{
        public ToInfo to {set;get;}
        public Template template {set;get;}
        public Meta meta {set;get;}
        public String id {set;get;}
        public String provider {set;get;}
        public String code {set;get;}   
        public String message {set;get;}   
        public String connectionId {set;get;}
        public String requestId {set;get;}
        public String processDate {set;get;}
        public String updatedAt {set;get;}
        public String createdAt {set;get;}
    }
}