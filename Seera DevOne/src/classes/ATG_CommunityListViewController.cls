/**************************************************************************************************
* Name               : ATG_CommunityListViewController 
* Description        : Controller used for the CUstom list view component                                
* Created Date       : 30 Jul 2018                                                              *
* Created By         : PWC Digital                                              
* -------------------------------------------------------------------------------------------------
* Version       Author      Date        Comment                                                               *

**************************************************************************************************/

public with sharing class ATG_CommunityListViewController {

    public static String STATUS_APPROVAL_PENDING = 'Approval Pending';
    
    
    @AuraEnabled
    public static list<dataTableColumn> GetColumnDetails(string TabName){
        
        // Use custom meta data types and fetch related fields 
        map<string,string> fieldName_LabelMap = new map<string,string>();
        Map<String, Schema.SObjectType> global_describe = Schema.getGlobalDescribe();
        Map<String, Schema.SObjectField> object_fields_map;
        string sobject_type;
        map<string,ATG_Community_Tab_Related_Fields__mdt> fieldsAPICasesensitiveMap = new map<string,ATG_Community_Tab_Related_Fields__mdt>();
        
        list<dataTableColumn> columnDetails = new list<dataTableColumn>();
        list<string> actions = new list<string>();
        
        // Get field labels
        String language = UserInfo.getLanguage();

        for(ATG_CommunityTabName__mdt tabDtls : [
                select id,DeveloperName,ATG_Object_API_Name__c,Actions__c,
                    (select id,Order__c,ATG_Community_Tab_Name__c,ATG_Field_API_Name__c,ATG_Field_API_Name_Arabic__c,
                            Lex_Column_Type__c,sortable__c,URL_Label_Attribute__c,Is_Hidden__c
                                                                            from ATG_Community_Tab_Related_Fields__r order by Order__c ASC
                    )

                from ATG_CommunityTabName__mdt where DeveloperName=:TabName]){
            sobject_type = tabDtls.ATG_Object_API_Name__c;
            
            system.debug('****'+sobject_type);
            if(!string.IsEmpty(tabDtls.Actions__c))
                actions = tabDtls.Actions__c.split(',');          
            
            for(ATG_Community_Tab_Related_Fields__mdt fl : tabDtls.ATG_Community_Tab_Related_Fields__r){
                fieldName_LabelMap.put(fl.ATG_Field_API_Name__c.tolowerCase(),'');


                fieldsAPICasesensitiveMap.put(fl.ATG_Field_API_Name__c.tolowerCase(),fl);

            }       
        }       
        
        if(!string.IsEmpty(sobject_type))
            object_fields_map = global_describe.get(sobject_type).getDescribe().fields.getMap();
        
        map<integer,dataTableColumn> dataColMapForSort = new map<integer,dataTableColumn>();
        for(string fldName : fieldsAPICasesensitiveMap.keySet()){
            if(object_fields_map.containsKey(fldName)){
                dataTableColumn dtCol = new dataTableColumn();

                dtCol.fieldNameAR = fieldsAPICasesensitiveMap.get(fldName).ATG_Field_API_Name_Arabic__c;
                dtCol.fieldName = fieldsAPICasesensitiveMap.get(fldName).ATG_Field_API_Name__c;
                dtCol.label = object_fields_map.get(fldName).getDescribe().getLabel();
                dtCol.Type = fieldsAPICasesensitiveMap.get(fldName).Lex_Column_Type__c;
                dtCol.isAction = false;  
                dtCol.sortable =  fieldsAPICasesensitiveMap.get(fldName).Sortable__c;  
                dtCol.URLLabelAttribute= fieldsAPICasesensitiveMap.get(fldName).URL_Label_Attribute__c ;
                dtCol.isHidden  = fieldsAPICasesensitiveMap.get(fldName).is_Hidden__c;        
                dataColMapForSort.put(fieldsAPICasesensitiveMap.get(fldName).Order__c.IntValue(),dtCol);                
            }   
        }
        
        if(dataColMapForSort !=null && dataColMapForSort.size()>0 && dataColMapForSort.values().size()>0)
            columnDetails.addAll(dataColMapForSort.values());
        
        
        if(actions !=null && !actions.isEmpty()){
                for(string act :actions){
                    dataTableColumn dtCol = new dataTableColumn();
                    dtCol.fieldName = act.containsany(' ') ? act.replaceAll(' ','_') : act;
                    dtCol.label = act;                    
                    dtCol.isAction = true;
                    columnDetails.add(dtCol);
                }
            }       
        
        return columnDetails;           
    }
    
    
    @AuraEnabled
    public static list<sobject> GetTableData(string TabName,list<string> fields,integer LimitSizeVal,integer offsetSizeVal,string searchString,list<string> searchablefields){      
         
        string queryString;               
        queryString = 'select id,'+string.join(fields,',');
        string queryCondition = ''; 
        
        if(!string.isEmpty(searchString) && Searchablefields !=null && !Searchablefields.isEmpty()){
            searchString = '\'%'+searchString+'%\'';
            list<string> searchParams = new list<string>();            
            for(string fld : Searchablefields)
                searchParams.add(fld +' Like '+searchString);            
            
            queryCondition += ' where ('+string.Join(searchParams,' or ')+')';
        }
        
       
        if(TabName == 'Pending_Approvals'){        
            queryString += ',MOH_Request_Number__c from Case ';
            
            if(!string.IsEmpty(queryCondition))
              queryCondition += ' and status =\''+STATUS_APPROVAL_PENDING+'\'';
            else
              queryCondition += ' where status =\''+STATUS_APPROVAL_PENDING+'\'';
                           
            queryString += queryCondition +'Order By CaseNumber Desc  LIMIT  '+LimitSizeVal+' OFFSET '+offsetSizeVal;
             
         }
         system.debug('QSTRING'+queryString);          
        return database.Query(queryString); 
            
    }
    
    
    // wrapper for columns structure
    public class dataTableColumn{
        @AuraEnabled
        public string label,fieldName,fieldNameAR,type,URLLabelAttribute;
        @AuraEnabled
        public boolean isAction,sortable,isHidden;     
    }
    
    
    
    
}