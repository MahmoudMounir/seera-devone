/**************************************************************************************************
* Name               : ATG_CommunityApprovalProcessController 
* Description        : Controller with methods to manage Approval process for Community                                 
* Created Date       : 30 Aug 2018                                                              *
* Created By         : PWC Digital                                              
* -------------------------------------------------------------------------------------------------
* Version       Author      Date        Comment                                                               *

**************************************************************************************************/


public without sharing class ATG_CommunityApprovalProcessController {

    public static String USERTYPE_APPROVER = 'Approver';

    public static list<Case> caseList;

    public static string SubmitETOForApproval(string csId,Id currentUserId, string comments){

        list<case> caseListToProcess = ATG_CommunityApprovalProcessController.SplitETO(csId);
        ATG_CommunityApprovalProcessController.caseList = caseListToProcess;
        List<Approval.ProcessSubmitRequest> requests = new List<Approval.ProcessSubmitRequest>();
        // Submit on behalf of a specific submitter
        list<id> approverIds = ATG_CommunityApprovalProcessController.GetApproversList(csId);

        if(approverIds !=null && approverIds.size()>0){
            Savepoint sp;
            try{
                Database.setSavepoint();

                // Update Case Ownership
                for(case caseObj : caseListToProcess){
                    caseObj.ownerId=approverIds[0];
                    caseObj.Case_Closure_Comments__c = comments;
                    integer approverCount = 1;
                    for(id approver : approverIds){

                        if(approverCount == 1){
                            caseObj.MOH_Approver_1__c = approver ;
                            caseObj.MOH_Approver_2__c = approver ;
                            caseObj.MOH_Approver_3__c = approver ;
                            caseObj.MOH_Approver_4__c = approver ;
                            caseObj.MOH_Approver_5__c = approver ;

                        }
                        else if(approverCount == 2){
                            caseObj.MOH_Approver_2__c = approver ;
                            caseObj.MOH_Approver_3__c = approver ;
                            caseObj.MOH_Approver_4__c = approver ;
                            caseObj.MOH_Approver_5__c = approver ;
                        }else if(approverCount == 3){
                            caseObj.MOH_Approver_3__c = approver ;
                            caseObj.MOH_Approver_4__c = approver ;
                            caseObj.MOH_Approver_5__c = approver ;
                        }else if(approverCount == 4){
                            caseObj.MOH_Approver_4__c = approver ;
                            caseObj.MOH_Approver_5__c = approver ;
                        }else if(approverCount == 5)
                            caseObj.MOH_Approver_5__c = approver ;

                        approverCount++ ;
                    }
                }

                update caseListToProcess;
                ATG_CommunityApprovalProcessController.UpdateCaseTeams(caseListToProcess);

                list<Approval.ProcessSubmitRequest> reqList = new list<Approval.ProcessSubmitRequest>();

                for(Case casObj : caseListToProcess){
                    Approval.ProcessSubmitRequest req1 = new Approval.ProcessSubmitRequest();
                    req1.setComments(comments);
                    req1.setObjectId(casObj.Id);
                    req1.setSubmitterId(currentUserId);
                    req1.setProcessDefinitionNameOrId('MOH_Approval_Process');
                    reqList.add(req1);
                }
                list<Approval.ProcessResult> result = Approval.process(reqList);
                return 'Success';
            }catch(exception e){
                Database.RollBack(sp);
                ATG_Utility.LogException(e);
                return 'Error';
            }
        }

        return 'Error';
    }



    // Update CaseTeams 
    private static void UpdateCaseTeams(list<Case> casesList){
        list<CaseTeamMember> CaseTeamMemberList = new list<CaseTeamMember>();
        map<string,set<string>> caseApproversMap = new map<string,set<string>>();
        list<CaseTeamRole> caseTeamRole = new list<CaseTeamRole>();
        caseTeamRole = [select id from CaseTeamRole where name = 'MOH Approver' limit 1];
        for(list<CaseTeamMember> cml : [select id from CaseTeamMember where  TeamRole.Name = 'MOH Approver' and parentId in : casesList] )
            CaseTeamMemberList.addAll(cml);

        if(CaseTeamMemberList !=null && !CaseTeamMemberList.isEmpty()){
            Delete CaseTeamMemberList;
            CaseTeamMemberList.clear();
        }

        // Prepare CaseTeams

        for(Case cas : casesList){
            if(!caseApproversMap.containsKey(cas.Id)){
                caseApproversMap.put(cas.Id,new set<string>());
                caseApproversMap.get(cas.Id).add(cas.MOH_Approver_1__c);
                caseApproversMap.get(cas.Id).add(cas.MOH_Approver_2__c);
                caseApproversMap.get(cas.Id).add(cas.MOH_Approver_3__c);
                caseApproversMap.get(cas.Id).add(cas.MOH_Approver_4__c);
                caseApproversMap.get(cas.Id).add(cas.MOH_Approver_5__c);
            }else if(caseApproversMap.containsKey(cas.Id)){
                caseApproversMap.get(cas.Id).add(cas.MOH_Approver_1__c);
                caseApproversMap.get(cas.Id).add(cas.MOH_Approver_2__c);
                caseApproversMap.get(cas.Id).add(cas.MOH_Approver_3__c);
                caseApproversMap.get(cas.Id).add(cas.MOH_Approver_4__c);
                caseApproversMap.get(cas.Id).add(cas.MOH_Approver_5__c);
            }

        }

        // Create Case Teams
        for(string casId : caseApproversMap.keySet()){
            if(caseTeamRole !=null && caseTeamRole.size()>0){
                for(string usr :  caseApproversMap.get(casId)){
                    CaseTeamMemberList.add(new CaseTeamMember(MemberId=usr,ParentId=casId,TeamRoleId= caseTeamRole[0].Id));
                }
            }
        }

        if(CaseTeamMemberList !=null && CaseTeamMemberList.size()>0)
            insert CaseTeamMemberList;



    }





    public static list<Id> GetApproversList(id CaseId){
        list<id> userIds;
        string programId;
        for(Case caseObj : [select Id,ATG_Program__c from Case where id =: CaseId])
            programId = caseObj.ATG_Program__c   ;

        if(!string.IsEmpty(programId)){
            set<id> tempUsrIdSet = new set<id>();
            for(User_Department__c ud : [
                    select id,User__c
                    from User_Department__c
                    where Program__c =:programId  and User__c !=null and user__r.IsActive = true
                    and user__r.User_Type__c =: USERTYPE_APPROVER
            ])
                tempUsrIdSet.add(ud.user__c);
            if(tempUsrIdSet !=null && !tempUsrIdSet.IsEmpty())
                userIds = new list<id>(tempUsrIdSet);
        }

        return userIds;
    }



    // Split Travel Order


    public static list<case> SplitETO(string caseId){

        ATG_CommunityETORequestController.ETORequestDetail etoDetail = ATG_CommunityETORequestController.GetETODetails(caseId);
        map<id,list<ATG_Requested_Traveller__c>> requestedTravellersMap = new map<id,list<ATG_Requested_Traveller__c>>();
        case caseDtlsObj;
        list<ATG_Case_Details__c> caseDetailsToInsert = new list<ATG_Case_Details__c>();
        list<list<ATG_Requested_Traveller__c>> RTListEmployee = new list<list<ATG_Requested_Traveller__c>>();
        list<ATG_Requested_Traveller__c> RTListToUpdate = new list<ATG_Requested_Traveller__c>();

        list<case> caseListToInsert = new list<case>();




        if(etoDetail !=null && etoDetail.bookingCase !=null)
            caseDtlsObj = etoDetail.bookingCase;

        system.debug('** Case detail'+caseDtlsObj);

        // Split the Traveller Sets 

        if(etoDetail.requestedTravelerInfo !=null && !etoDetail.requestedTravelerInfo.isEmpty()){
            for(ATG_Requested_Traveller__c rt : etoDetail.requestedTravelerInfo){
                if((rt.RecordTypeId == ATG_ConstantsUtility.RequestedTraveller_Employee_RT || rt.RecordTypeId == ATG_ConstantsUtility.RequestedTraveller_Visitor_RT) &&  requestedTravellersMap.containsKey(rt.id))
                    requestedTravellersMap.get(rt.id).add(rt);
                else if((rt.RecordTypeId == ATG_ConstantsUtility.RequestedTraveller_Visitor_RT || rt.RecordTypeId == ATG_ConstantsUtility.RequestedTraveller_Employee_RT) &&  !requestedTravellersMap.containsKey(rt.id)){
                    requestedTravellersMap.put(rt.id,new list<ATG_Requested_Traveller__c>());
                    requestedTravellersMap.get(rt.id).add(rt);
                }else if(rt.RecordTypeId == ATG_ConstantsUtility.RequestedTraveller_Dependant_RT && !requestedTravellersMap.containsKey(rt.ATG_Employee_Traveler__c)){
                    requestedTravellersMap.put(rt.ATG_Employee_Traveler__c,new list<ATG_Requested_Traveller__c>());
                    requestedTravellersMap.get(rt.ATG_Employee_Traveler__c).add(rt);
                }else if(rt.RecordTypeId == ATG_ConstantsUtility.RequestedTraveller_Dependant_RT && requestedTravellersMap.containsKey(rt.ATG_Employee_Traveler__c))
                    requestedTravellersMap.get(rt.ATG_Employee_Traveler__c).add(rt);
            }

        }

        system.debug('RequestedTraveller Info ** '+requestedTravellersMap.size());

        list<id> employeeIds = new list<id>();

        if(requestedTravellersMap.keyset().size() > 1){
            employeeIds.addAll(requestedTravellersMap.keyset());

            for(integer i=0 ; i < requestedTravellersMap.keyset().size() - 1 ; i++){
                caseDtlsObj.ATG_Request_Number__c = null;
                caseListToInsert.add(caseDtlsObj.clone(false,false,false,false));
            }

            system.debug('CaseListToInsert****'+caseListToInsert);

            if(CaseListToInsert !=null && !CaseListToInsert.isEmpty())
                insert caseListToInsert;

            // Distribute Travelers and Other details 

            ATG_Case_Details__c cdObj ;

            integer cntr=0;

            for(case cs : caseListToInsert){

                // Add Case Details                
                for(ATG_Case_Details__c cd : etoDetail.caseDetails){
                    cdObj  = new ATG_Case_Details__c();
                    cdObj = cd.clone(false,false,false,false);
                    cdObj.ATG_Case_Number__c = cs.Id;
                    caseDetailsToInsert.add(cdObj);
                }

                // Update requested travelers 


                for(ATG_Requested_Traveller__c rt : requestedTravellersMap.get(employeeIds[cntr])){
                    rt.Request_Number__c = cs.Id;
                    RTListToUpdate.add(rt);
                }


                cntr++ ;
            }

            if(caseDetailsToInsert !=null && caseDetailsToInsert.size()>0)
                insert caseDetailsToInsert;

            if(RTListToUpdate !=null && !RTListToUpdate.isEmpty())
                update RTListToUpdate;




        }

        // Add parent case

        caseListToInsert.add(etoDetail.bookingCase);


        list<Case> caseListToUpdate = new list<case>();
        string employeeRecordTypeId = ATG_ConstantsUtility.RequestedTraveller_Employee_RT;
        string vistorRecType = ATG_ConstantsUtility.RequestedTraveller_Visitor_RT;
        for(Case caseObj : [select Id,ATG_Primary_Traveller__c,ATG_No_Of_Adults__c,ATG_No_of_Children_Travelling__c,ownerId,MOH_Approver_1__c,MOH_Approver_2__c,MOH_Approver_3__c,MOH_Request_Number__c,(select id,ATG_Employee__c,ATG_Traveller_Full_Name__c,ATG_Mobile_Number__c,ATG_Email_Address__c,recordTypeId,ATG_Adult_Child__c from Requested_Travelers__r where recordTypeId =: ATG_ConstantsUtility.RequestedTraveller_Employee_RT limit 1) from Case where id In : caseListToInsert]){
            if(caseObj.Requested_Travelers__r.size()>0){

                for(ATG_Requested_Traveller__c rt : caseObj.Requested_Travelers__r){
                    if((caseObj.Requested_Travelers__r[0].ATG_Employee__c !=null && rt.RecordTypeId == employeeRecordTypeId) || rt.RecordTypeId == vistorRecType){
                        caseObj.ATG_Primary_Traveller__c = caseObj.Requested_Travelers__r[0].ATG_Employee__c;
                        caseObj.ATG_Primary_Traveller_Nam__c = caseObj.Requested_Travelers__r[0].ATG_Traveller_Full_Name__c;
                        caseObj.ATG_Primary_Traveller_Email__c = caseObj.Requested_Travelers__r[0].ATG_Email_Address__c;
                        caseObj.ATG_Primary_Traveller_Mobile_No__c = caseObj.Requested_Travelers__r[0].ATG_Mobile_Number__c;
                    }

                }
            }
            caseListToUpdate.add(caseObj);
        }
        if(caseListToUpdate !=null && caseListToUpdate.size()>0)
            update caseListToUpdate;

        return  caseListToUpdate;
    }

    //@InvocableMethod
    //public static void CreateAssignees(list<id> approvalStepId){}   

    @AuraEnabled
    public static string ProcessApproval(list<string> CaseIds,string comments,string status){

        string ActionType;

        if(status == 'Approve'){
            Status = 'Approved';
            actionType = 'Approve';
            ATG_CommunityApprovalProcessController.updateEmployeeDocuments(CaseIds);
        }
        else if(status == 'Reject'){
            status = 'Rejected';
            actionType = 'Reject';
        }
        else if(status == 'Return'){
            status = 'Returned';
            actionType = 'Reject';
        }
        system.debug('ActionType**'+actionType);
        system.debug('Status**'+status);
        // Approve/Reject Cases 
        map<Id,Id> caseWorkItemMap = new map<Id,Id>();
        list<case> caseListToUpdate = new list<case>();
        SavePoint sp;
        try{
            Database.setSavePoint();
            if(CaseIds !=null && CaseIds.size()>0){
                for(processInstance pi : [select Id,TargetObjectId,(select id from workItems where ActorId =: userInfo.getUserId()) from processInstance where TargetObjectId in : CaseIds and Status = 'Pending' and ProcessDefinition.Name='MOH Approval Process']){
                    if(pi.workItems !=null && !pi.workItems.isEmpty()){
                        for(ProcessInstanceWorkitem pwi : pi.workItems)
                            caseWorkItemMap.put(pi.TargetObjectId,pwi.Id);
                    }
                }
            }

            list<Approval.ProcessWorkitemRequest> processWorkItems = new list<Approval.ProcessWorkitemRequest>();

            if(caseWorkItemMap !=null && caseWorkItemMap.size()>0){
                for(Id i : caseWorkItemMap.keySet()){
                    Approval.ProcessWorkitemRequest pwr = new Approval.ProcessWorkitemRequest();
                    pwr.setComments(comments);
                    pwr.setAction(actionType);
                    pwr.setWorkitemId(caseWorkItemMap.get(i));
                    processWorkItems.add(pwr);
                    system.debug('^^^^'+caseWorkItemMap.get(i));
                    caseListToUpdate.add(new case(id=i,ownerId=userInfo.getUserId(),status=Status,Case_Closure_Comments__c = comments));
                }
            }
            system.debug('&*&****');
            if(processWorkItems !=null && !processWorkItems.isEmpty()){
                list<Approval.ProcessResult> result2 =  Approval.process(processWorkItems);
                update caseListToUpdate;
                return 'Success';
            }
            return 'Error';

        }catch(exception e){
            Database.RollBack(sp);
            ATG_Utility.LogException(e);
            return 'Error' ;
        }
    }



    // Method to return all the case Details

    @AuraEnabled
    public static ATG_CommunityETORequestController.ETORequestDetail GetCaseDetails(string CaseId){

        ATG_CommunityETORequestController.ETORequestDetail etoReqObj = new ATG_CommunityETORequestController.ETORequestDetail();
        // Query All the Details 
        List<string> fieldsList = new List<string>(ATG_utility.GetAccessibleFields('Case'));
        string CaseQueryString = 'Select ';
        CaseQueryString +=string.join(fieldsList,',');


        // Query Case detail fields
        string CaseDetailsQueryString ='Select ';
        fieldsList = new List<string>(ATG_utility.GetAccessibleFields('ATG_Case_Details__c'));
        CaseDetailsQueryString += string.join(fieldsList,',')+' from Case_Details__r ORDER BY ATG_Date__C ASC ';



        CaseQueryString += ',('+CaseDetailsQueryString +') from case where id=\''+caseId+'\'';


        for(Case caseObj : Database.Query(CaseQuerySTring)){
            etoReqObj.bookingCase = caseObj;
            etoReqObj.requestedTravelerInfo = caseObj.Requested_Travelers__r;
            etoReqObj.caseDetails = caseObj.Case_Details__r;
        }


        return etoReqObj ;
    }


    // Update Employee Documents

    public static void updateEmployeeDocuments(list<string> caseIdList){
        string mohRectype = ATG_ConstantsUtility.ATG_CASE_ETO_RECORDTYPE;
        string employeeRT = ATG_ConstantsUtility.RequestedTraveller_Employee_RT;
        string EmployeeId;
        map<string,string> caseEmployeeMap = new map<string,string>();
        map<id,map<string,ATG_Document_Details__c>> employeeDocMap = new map<id,map<string,ATG_Document_Details__c>>();
        map<id,map<string,ATG_Document_Details__c>> RTDocMap = new map<id,map<string,ATG_Document_Details__c>>();
        list<id> ContentDocumentIds = new list<id>();

        list<ATG_Document_Details__c> docDetailsToInsert = new list<ATG_Document_Details__c>();
        map<id,ATG_Document_Details__c> old_NewDocDetails = new map<id,ATG_Document_Details__c>();

        for(Case cs : [select Id,ATG_Primary_Traveller__c from Case where Id in : caseIdList and ATG_Primary_Traveller__c  !=null]){
            caseEmployeeMap.put(cs.Id,cs.ATG_Primary_Traveller__c);
            employeeDocMap.put(cs.ATG_Primary_Traveller__c,new map<string,ATG_Document_Details__c>());
        }

        system.debug('Docs Emp Check ##'+caseEmployeeMap);
        set<string> docToCopy = new set<string>();


        if(!caseIdList.isEmpty() && !caseEmployeeMap.Isempty()){
            for(RT_Required_Docs_Community__c rt : RT_Required_Docs_Community__c.getAll().values()){
                if(rt.Copy_to_Account__c)docToCopy.add(rt.Name);
            }


            system.debug('Document Details **'+docToCopy);

            for(ATG_Document_Details__c dd : [select id,ATG_Document_ID__c,ATG_Document_Name__c,ATG_Employee__c,ATG_Request_Number__c,ATG_Document_Description__c  from ATG_Document_Details__c  where (ATG_Request_Number__c in : caseEmployeeMap.keySet() and ATG_Request_Number__r.RecordTypeId =: mohRectype and Requested_Travelers__r.RecordTypeId = : employeeRT and ATG_Is_Changed__c = true and ATG_Document_Name__c in : docToCopy) or  ATG_Employee__c in : caseEmployeeMap.values()]){

                if(dd.ATG_Employee__c !=null){
                    if(!employeeDocMap.containsKey(dd.ATG_Employee__c)){
                        employeeDocMap.put(dd.ATG_Employee__c,new map<string,ATG_Document_Details__c>());
                        employeeDocMap.get(dd.ATG_Employee__c).put(dd.ATG_Document_Name__c,dd);
                    }else if(employeeDocMap.containsKey(dd.ATG_Employee__c)){
                        employeeDocMap.get(dd.ATG_Employee__c).put(dd.ATG_Document_Name__c,dd);
                    }
                }

                if(dd.ATG_Request_Number__c !=null){
                    if(!RTDocMap.containsKey(dd.ATG_Request_Number__c) && dd.ATG_Document_ID__c !=null){
                        RTDocMap.put(dd.ATG_Request_Number__c,new map<string,ATG_Document_Details__c>());
                        RTDocMap.get(dd.ATG_Request_Number__c).put(dd.ATG_Document_Name__c,dd);
                        ContentDocumentIds.add(dd.ATG_Document_ID__c);
                    }else if(RTDocMap.containsKey(dd.ATG_Request_Number__c) && dd.ATG_Document_ID__c !=null){
                        RTDocMap.get(dd.ATG_Request_Number__c).put(dd.ATG_Document_Name__c,dd);
                        ContentDocumentIds.add(dd.ATG_Document_ID__c);
                    }
                }
            }

            system.debug('RT DocsEmp Check ##'+RTDocMap);

            for(string caseId : RTDocMap.keySet()){
                if(caseEmployeeMap.containsKey(caseId)){
                    for(string DocNames : RTDocMap.get(caseId).keySet()){
                        system.debug('---^^.'+employeeDocMap);

                        if(employeeDocMap.containsKey(caseEmployeeMap.get(caseId)) && !employeeDocMap.get(caseEmployeeMap.get(caseId)).containsKey(DocNames)){
                            employeeDocMap.get(caseEmployeeMap.get(caseId)).put(DocNames,new ATG_Document_Details__c(ATG_Document_Name__c = DocNames,ATG_Document_Description__c=RTDocMap.get(caseId).get(DocNames).ATG_Document_Description__c,ATG_Employee__c = caseEmployeeMap.get(caseId)));
                            system.debug('---^^'+employeeDocMap);
                        }
                    }
                }
            }

            system.debug('Emp DocsEmp Check ##'+employeeDocMap);

            // Handle Update 
            List<ATG_Document_Details__c> docsToUpsert = new list<ATG_Document_Details__c>();
            for(string empId : employeeDocMap.keySet()){
                for(string docName : employeeDocMap.get(empId).keySet())
                    docsToUpsert.add(employeeDocMap.get(empId).get(docName));
            }

            if(docsToUpsert !=null && !docsToUpsert.isEmpty())
                upsert docsToUpsert;

            system.debug('****TestDocs'+docsToUpsert);
            system.debug('*##*TestDocs'+employeeDocMap);

            //Map old and New Document 
            for(string caseId: RTDocMap.keySet()){
                for(string DocName : RTDocMap.get(caseId).keySet())
                    old_NewDocDetails.put(RTDocMap.get(caseId).get(DocName).Id,employeeDocMap.get(caseEmployeeMap.get(caseId)).get(DocName));
            }

            system.debug('%%'+old_NewDocDetails);
            system.debug('%%----'+ContentDocumentIds);


            if(old_NewDocDetails !=null && ContentDocumentIds !=null && !ContentDocumentIds.isEmpty() && old_NewDocDetails.size() >0)
                ATG_CommunityETORequestController.CreateContentDocuments(old_NewDocDetails,ContentDocumentIds);



        }


      

    }




}