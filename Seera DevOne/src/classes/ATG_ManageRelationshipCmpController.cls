/**************************************************************************************************
* Name               : ATG_ManageRelationshipCmpController 
* Description        : Controller used in quick action lightning components of Link CCGR Departments,Programs on ATG Master                                  
* Created Date       : 30 Jul 2018                                                              *
* Created By         : PWC Digital                                              
* -------------------------------------------------------------------------------------------------
* Version       Author      Date        Comment                                                               *

**************************************************************************************************/

public without sharing class ATG_ManageRelationshipCmpController {
    
    
    
    @AuraEnabled
    public static SelectValuesClass GetSelectableValues(string parentId,string processNo){
        
        if(processNo == '1')
            return ATG_ManageRelationshipCmpController.GetDepartmentsListForProgram(parentId);
        else if(processNo == '2')
            return ATG_ManageRelationshipCmpController.GetProgramsForDepartment(parentId);
        else if(processNo == '3')
            return ATG_ManageRelationshipCmpController.GetCCGRForDepartments(parentId);
        else
            return ATG_ManageRelationshipCmpController.GetDepartmentsForCCGR(parentId); 
            
    }
    
    
    @AuraEnabled
    public static string SaveSelectedValues(string parentId,list<string> selectedValues,string processNo){
         system.debug('______'+parentId+'-----'+selectedValues+'**'+processNo);
        Savepoint sp;        
        try{
            Database.setSavepoint();
            if(processNo == '1')
                return ATG_ManageRelationshipCmpController.SaveDepartmentsListforPrograms(parentId,selectedValues);
            else if(processNo == '2')
                return ATG_ManageRelationshipCmpController.SaveProgramsforDepartments(parentId,selectedValues);
            else if(processNo == '3')
                return ATG_ManageRelationshipCmpController.SaveCCGRForDepartments(parentId,selectedValues);
            else if(processNo =='4')
                return ATG_ManageRelationshipCmpController.SaveDepartmentsForCCGR(parentId,selectedValues);
            else
                return 'Error';    
        }catch(exception e){            
            Database.RollBack(sp);
             ATG_utility.LogException(e);
            return 'Error';            
        }       
    }
    
    
    
    
    public static SelectValuesClass GetDepartmentsListForProgram(string parentId){ 
       SelectValuesClass departDetail = new SelectValuesClass();
        string depName ;
        for(ATG_Department_Relationship__c atgd : [select id,Name,Department__c,ATG_Department_Name__c,Department__r.Name_in_Arabic__c  ,Department__r.Name from ATG_Department_Relationship__c where  Program__c= : parentId]){          
            depName = userInfo.getLanguage().containsAny('Arabic') ? atgd.Department__r.Name_in_Arabic__c : atgd.Department__r.Name;//atgd.Department__r.ATG_Arabic_Name__c;
            departDetail.selectedValues.add(new ATG_Master__c(id=atgd.Department__c,Name=depName));                        
        }
        departDetail.AvailableValues = ATG_ManageRelationshipCmpController.GetMasterList(ATG_ConstantsUtility.ATG_MASTER_Department_RT);
        
        return departDetail;
    }   
    
     // fetch Programs for Department 
  
    public static SelectValuesClass GetProgramsForDepartment(string parentId){ 
       SelectValuesClass progDetail = new SelectValuesClass();
        
        string progName;
        for(ATG_Department_Relationship__c atgd : [select id,Name,Department__c,ATG_Program_Name__c,Program__c,Program_Name_Arabic__c,Program__r.Name from ATG_Department_Relationship__c where  Department__c = : parentId]) {       
            progName = userInfo.getLanguage().containsAny('Arabic') ? atgd.Program_Name_Arabic__c : atgd.Program__r.Name;//atgd.ATG_Program_Name__c;
            progDetail.selectedValues.add(new ATG_Master__c(id=atgd.Program__c,Name=progName));                        
        }
        progDetail.AvailableValues = ATG_ManageRelationshipCmpController.GetMasterList(ATG_ConstantsUtility.ATG_MASTER_Program_RT);
        
        return progDetail;
    }   
    
    
    // Fetch CCGR for Department 
   
     public static SelectValuesClass GetCCGRForDepartments(string parentId){ 
       SelectValuesClass ccgrDetail = new SelectValuesClass();
        
        // Get Programs Details
        list<id> programIds = new list<id>();
        for(ATG_Department_Relationship__c depRel : [select id,Program__c from ATG_Department_Relationship__c where Department__c = : parentId])
            programIds.add(depRel.Program__c);
        
        
        for(ATG_CCGR_CC_Relationship__c atccgrRel : [select id,CC_CCGR_Number__c,Department__c,Program__c,CC_CCGR_Number__r.ATG_Type__c,
                                                            CC_CCGR_Number__r.Name from ATG_CCGR_CC_Relationship__c where  Department__c= : parentId and program__c in : programIds])          
            ccgrDetail.selectedValues.add(new ATG_Master__c(id=atccgrRel.CC_CCGR_Number__c,Name=atccgrRel.CC_CCGR_Number__r.Name));        
            ccgrDetail.AvailableValues = ATG_ManageRelationshipCmpController.GetCCGRList(programIds);
        
        return ccgrDetail;
    }
        
    
    
    // fetch Departments for CCGR
    public static SelectValuesClass GetDepartmentsForCCGR(string parentId){ 
         map<id,ATG_Master__c>  ccgrMap = new map<id,ATG_Master__c>([select id,Name,ATG_Program__c from ATG_Master__c where id =: parentId]);
        SelectValuesClass ccgrDetail = new SelectValuesClass();        
        string DepName;
        for(ATG_CCGR_CC_Relationship__c atccgrRel : [select id,CC_CCGR_Number__c,Department__c,Program__c,Department__r.Name_in_Arabic__c,
                                                             Department__r.Name from ATG_CCGR_CC_Relationship__c where  CC_CCGR_Number__c = : parentId]) {
            DepName =   userInfo.getLanguage().containsAny('Arabic') ? atccgrRel.Department__r.Name_in_Arabic__c : atccgrRel.Department__r.Name;                                        
            ccgrDetail.selectedValues.add(new ATG_Master__c(id=atccgrRel.Department__c,Name=DepName));        
         }   
         //ccgrDetail.AvailableValues = ATG_ManageRelationshipCmpController.GetMasterList(ATG_ConstantsUtility.ATG_MASTER_Department_RT,string programId);
        
        // Get Available values for Program Department Relation
        for(ATG_Department_Relationship__c atgd : [select id,Name,Department__c,Department__r.Name,Department__r.Name_in_Arabic__c from ATG_Department_Relationship__c where  Program__c = : ccgrMap.get(parentId).ATG_Program__c]) {       
            ATG_master__c dep= new ATG_master__c(id=atgD.Department__c);
            dep.Name = userInfo.getLanguage().containsAny('Arabic') ? atgd.Department__r.Name_in_Arabic__c : atgd.Department__r.Name;
            ccgrDetail.AvailableValues.add(dep); 
        }
        return ccgrDetail;
    }
    
    
    
    
    
    
    @AuraEnabled
    public static string SaveDepartmentsListforPrograms(string parentId,list<String> DepartmentsList){
        
        list<ATG_Department_Relationship__c> DepartmentRelsToDelete = new list<ATG_Department_Relationship__c>();
        list<ATG_Department_Relationship__c> DepartmentRelsToInsert = new list<ATG_Department_Relationship__c>();   
       
            for(ATG_Department_Relationship__c rel : [select id from ATG_Department_Relationship__c where  Program__c =:parentID])          
                DepartmentRelsToDelete.add(rel);
            
            if(DepartmentRelsToDelete !=null && !DepartmentRelsToDelete.isEmpty())
                Delete DepartmentRelsToDelete;
            
            if(DepartmentsList!=null && !DepartmentsList.isEmpty()){
                for(Id depId : DepartmentsList)
                DepartmentRelsToInsert.add(new ATG_Department_Relationship__c(Department__c=depId,Program__c=parentID));
                
                
                insert DepartmentRelsToInsert;
            }

            return 'Success' ;   
       
    } 
    
    
   
    public static string SaveProgramsforDepartments(string parentId,list<string> programsList){
        list<ATG_Department_Relationship__c> DepartmentRelsToDelete = new list<ATG_Department_Relationship__c>();
        list<ATG_Department_Relationship__c> DepartmentRelsToInsert = new list<ATG_Department_Relationship__c>();   
        
        list<ATG_CCGR_CC_Relationship__c> ccgrRelListtoDel= new list<ATG_CCGR_CC_Relationship__c>();  
        
        
         
       
       
            for(ATG_Department_Relationship__c rel : [select id from ATG_Department_Relationship__c where  Department__c =:parentID])          
                DepartmentRelsToDelete.add(rel);
                
                
              
            
            if(DepartmentRelsToDelete !=null && !DepartmentRelsToDelete.isEmpty())
                Delete DepartmentRelsToDelete;
            
            if(programsList!=null && !programsList.isEmpty()){
                for(Id progId : programsList)
                    DepartmentRelsToInsert.add(new ATG_Department_Relationship__c(Department__c=parentId,Program__c=progId));
                
                
                 for(ATG_CCGR_CC_Relationship__c crel : [select id from ATG_CCGR_CC_Relationship__c where program__c not in : programsList and Department__c = : parentId]) 
                     ccgrRelListtoDel.add(crel);  
                
                insert DepartmentRelsToInsert;
                
                
                Delete ccgrRelListtoDel;
            }

            return 'Success' ;   
       
    } 
    
    
    // Save CCGRs for Department 
    
    public static string SaveCCGRForDepartments(string parentID,list<string> selectedCCGR){
        
        map<id,ATG_Master__c>  ccgrMap = new map<id,ATG_Master__c>([select id,Name,ATG_Program__c from ATG_Master__c where id in : selectedCCGR]);
        list<ATG_CCGR_CC_Relationship__c> ccGRRelsToDelete = new list<ATG_CCGR_CC_Relationship__c>();
        list<ATG_CCGR_CC_Relationship__c> ccGRRelsToInsert = new list<ATG_CCGR_CC_Relationship__c>();   
       
            for(ATG_CCGR_CC_Relationship__c rel : [select id from ATG_CCGR_CC_Relationship__c where  Department__c =:parentID])          
                ccGRRelsToDelete.add(rel);
            
            if(ccGRRelsToDelete !=null && !ccGRRelsToDelete.isEmpty())
                Delete ccGRRelsToDelete;
            
            if(selectedCCGR !=null && !selectedCCGR.isEmpty()){
                for(Id ccgrID : selectedCCGR){
                    ccGRRelsToInsert.add(new ATG_CCGR_CC_Relationship__c(Department__c=parentID,Program__c=ccgrMap.get(ccgrID).ATG_Program__c,CC_CCGR_Number__c=ccgrID));
                }
                
                insert ccGRRelsToInsert;
            }

            return 'Success' ;  
        
    } 
    
    
    
    // Save Departments for CCGR 
    
    public static string SaveDepartmentsForCCGR(string parentID,list<string> selectedDepartments){
        
        map<id,ATG_Master__c>  ccgrMap = new map<id,ATG_Master__c>([select id,Name,ATG_Program__c from ATG_Master__c where id = :parentID]);
        list<ATG_CCGR_CC_Relationship__c> ccGRRelsToDelete = new list<ATG_CCGR_CC_Relationship__c>();
        list<ATG_CCGR_CC_Relationship__c> ccGRRelsToInsert = new list<ATG_CCGR_CC_Relationship__c>();   
        
            for(ATG_CCGR_CC_Relationship__c rel : [select id from ATG_CCGR_CC_Relationship__c where  CC_CCGR_Number__c =:parentID])          
                ccGRRelsToDelete.add(rel);
            
            if(ccGRRelsToDelete !=null && !ccGRRelsToDelete.isEmpty())
                Delete ccGRRelsToDelete;
            
            if(selectedDepartments!=null && !selectedDepartments.isEmpty()){
                for(Id depID : selectedDepartments){
                    ccGRRelsToInsert.add(new ATG_CCGR_CC_Relationship__c(Department__c=depID,Program__c=ccgrMap.get(parentId).ATG_Program__c,CC_CCGR_Number__c=parentID));
                }
                
                insert ccGRRelsToInsert;
            }

            return 'Success'  ;  
        
        
    } 
    
    
    
    
    
   
    public static  list<ATG_Master__c> GetMasterList(string recordTypeId){        
        list<ATG_Master__c> masterList = new list<ATG_Master__c>();     
        string masterName;
        for(ATG_Master__c atgm : [select id,Name,Name_in_Arabic__c from ATG_Master__c where recordTypeId = : recordTypeId  and ATG_Active__c = true]) {//and id not in : excludeList
            masterName = userInfo.getLanguage().containsAny('Arabic') ? atgm.Name_in_Arabic__c  : atgm.Name;
            masterList.add(new ATG_Master__c(id=atgm.id,Name=masterName));            
            
        }
        return masterList;          
    }   
    
    
    public static  list<ATG_Master__c> GetCCGRList(list<id> programID){        
        list<ATG_Master__c> masterList = new list<ATG_Master__c>();     
        string masterName;
        for(ATG_Master__c atgm : [select id,Name,Name_in_Arabic__c from ATG_Master__c where recordTypeId = : ATG_ConstantsUtility.ATG_MASTER_CCGR_RT and ATG_Active__c = true and ATG_Program__c in : programID]) {//and id not in : excludeList
            masterName = userInfo.getLanguage().containsAny('Arabic') ? atgm.Name_in_Arabic__c  : atgm.Name;
            masterList.add(new ATG_Master__c(id=atgm.id,Name=masterName));            
            
        }
        return masterList;          
    }   
    
    
    
    public class SelectValuesClass{
        
        @AuraEnabled
        public list<ATG_Master__c> selectedValues,AvailableValues;
        
        public SelectValuesClass(){
            selectedValues = new list<ATG_Master__c>();
            AvailableValues = new list<ATG_Master__c>();
        }
        
    }
    
    
   
    
    
    
}