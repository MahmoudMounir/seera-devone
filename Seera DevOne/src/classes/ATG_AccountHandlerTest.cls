/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class ATG_AccountHandlerTest {

    
    @testSetUp
    static void ATG_AccountHandler_TestData(){
        
        
        User thisUser = [SELECT Id FROM User WHERE Id = :UserInfo.getUserId()];
       
        System.runAs (thisUser) {
            Account Acc = ATG_TestDataFactory.createEmployeeAccount();        
            insert acc;
            
            Account personAcc = [select Id,Name,PersonContactId from Account where Id = : acc.Id];
            Profile p = [SELECT Id FROM Profile WHERE Name='MoH Admin Community Login User'];
                User u = new User(Alias = 'standt', Email='standarduser@testorg.com',
                EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US',
                LocaleSidKey='en_US', ProfileId = p.Id,
                TimeZoneSidKey='America/Los_Angeles',
                UserName='standarduser@testorg.com',contactId = personAcc.PersonContactId);
                insert u;
                
                PermissionSet ps = [SELECT ID From PermissionSet WHERE Name = 'Manager_Users'];
                insert new PermissionSetAssignment(AssigneeId = u.id, PermissionSetId = ps.Id );
        }
    }
    
    static testMethod void AccountHandler1() {
        
        ATG_Emails_To_Exclude__c objTemp = new ATG_Emails_To_Exclude__c();
        objTemp.name = 'dsdfsd@asda.sada';
        insert objTemp;
        
        ATG_Account_To_Account_Search__c objCustom = new ATG_Account_To_Account_Search__c();
        objCustom.name = 'PersonEmail';
        objCustom.Is_Available_For_Person_Account__c = true;
        insert objCustom;
        
        ATG_BU_Configuration__c objCustom2 = new ATG_BU_Configuration__c();
        objCustom2.name = 'Al Tayyar';
        objCustom2.ATG_Last_Generated_Unique_ID__c = '0';
        insert objCustom2;
        
        ATG_Master_Account_Fields__c objCustom1 = new ATG_Master_Account_Fields__c();
        objCustom1.name = 'LastName';
        insert objCustom1;
        
        Account obj = new Account();
        obj.firstName = 'Test';
        obj.lastName = 'Test';
        obj.personemail = 'test@test.com';
        obj.personMobilePhone = '+971 555553456';
        obj.recordTypeID = ATG_ConstantsUtility.PERSON_ACCOUNT_RECORDTYPEID;
        obj.ATG_Business_Unit_Picklist__c = 'Tajawal';
        obj.ATG_Tapro_ID__c = 'sasasa';
        obj.Mobile_Number__pc = '898989898';
        obj.Country_Code__pc = 'United Arab Emirates (+971)';
        insert obj;
        
        obj.personMobilePhone = '+971 555233456';
        update obj;
        
        Account obj2 = new Account();
        obj2.firstName = 'Test';
        obj2.lastName = 'Test';
        obj2.personemail = 'test@test.com';
        obj2.recordTypeID = ATG_ConstantsUtility.PERSON_ACCOUNT_RECORDTYPEID;
        obj2.ATG_Business_Unit_Picklist__c = 'Al Mosafer';
        insert obj2;
                
        Account obj3 = new Account();
        obj3.firstName = 'Test';
        obj3.lastName = 'Test2';
        obj3.personemail = 'test2@test.com';
        obj3.recordTypeID = ATG_ConstantsUtility.PERSON_ACCOUNT_RECORDTYPEID;
        obj3.ATG_Business_Unit_Picklist__c = 'Al Tayyar';
        insert obj3;
        
        list<Account> soNewList = new list<Account>();
        soNewList.add(obj);
        soNewList.add(obj2);
        soNewList.add(obj3);
        
        ATG_TestUtility objTest = new ATG_TestUtility();
        Account corp = objTest.createLeisureAccount();
        soNewList.add(corp);
        
        ATG_AccountHandler objClass = new ATG_AccountHandler();
        objClass.beforeInsert (soNewList);
        objClass.beforeDelete (soNewList);
        objClass.afterDelete (soNewList);
        
        ATG_ConstantsUtility.runOnceForAccount = false;
        
        map<id,Account> soOldMap = new map<id,Account>();
        soOldMap.put(corp.id,corp);
        
        corp.personemail = 'testing@testing.testing';
        update corp;
        
        objClass.beforeUpdate (soNewList, soOldMap);
        
        ATG_ConstantsUtility.runOnceForAccount = false;
        objClass.afterUpdate (soNewList, soOldMap);
    }
    
    
    static testmethod void accountTestCase1(){
        
        ATG_AccountHandler.formatPhoneNumber('+9715265156543');
        ATG_AccountHandler.formatPhoneNumber('+9665265156543');
        ATG_AccountHandler.formatPhoneNumber('+915265156543');
        ATG_AccountHandler.formatPhoneNumber('+925265156543');
        ATG_AccountHandler.formatPhoneNumber('+205265156543');
        ATG_AccountHandler.formatPhoneNumber('+15265156543');
	}
    
    
}