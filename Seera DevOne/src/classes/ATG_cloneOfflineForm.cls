global without sharing class ATG_cloneOfflineForm {
    
    @InvocableMethod(label='cloneOldBooking' description='cloneOldBooking')
    public static list<string> cloneOldBooking(list<id> caseID){
        
        try{
            system.debug('-----caseID----'+caseID); 
            
            list<case> tempCaseList = new list<case>();
            
            list<string> successMsg = new list<string>();
            
            if(caseID!=null && caseID.size()>0){
                
                tempCaseList = [select id,ATG_Amended_Booking__c, ATG_Booking__c from case where id IN : caseId];
            }       
            
            if(tempCaseList!=null && tempCaseList.size()>0){
                case objCase = tempCaseList[0];
                
                system.debug('-----objCase----'+objCase);    
                ATG_Booking__c objBookingHeader =  new ATG_Booking__c();
                
                string bookingHeaderQuery = 'select ';
                string bookingDetailQuery = ' , (select ';
                string travellerQuery = ' , (select ';
                
                list<ATG_Booking__c> tempBookingList = new list<ATG_Booking__c>();
                list<string> bookingFieldsList = new list<string>(ATG_Utility.getallAPINames('ATG_Booking__c'));
                list<string> bookingDetailFieldsList = new list<string>(ATG_Utility.getallAPINames('ATG_Booking_Detail__c'));
                list<string> travellerFieldsList = new list<string>(ATG_Utility.getallAPINames('ATG_Traveller__c'));
                
                bookingDetailQuery += string.join(bookingDetailFieldsList,',')+' from Itineraries__r )';
                
                travellerQuery += string.join(travellerFieldsList,',')+' from Travellers__r )';
                
                string tempCaseId = objCase.ATG_Booking__c;
                
                system.debug('-----tempCaseId----'+tempCaseId);
                bookingHeaderQuery += string.join(bookingFieldsList,',') + bookingDetailQuery + travellerQuery +' from ATG_Booking__c Where id =: tempCaseId';
                
                tempBookingList = Database.query(bookingHeaderQuery);
                
                if(tempBookingList!=null && tempBookingList.size()>0){
                    objBookingHeader = tempBookingList[0];
                    
                    ATG_Booking__c objClonedBooking = new ATG_Booking__c();
                    objClonedBooking = objBookingHeader.clone(false, false, false, false);
                    objClonedBooking.ATG_Booking_Reference_Number__c = '';
                    system.debug('-----objClonedBooking----'+objClonedBooking);
                    insert objClonedBooking; 
                    
                    objCase.ATG_Amended_Booking__c = objClonedBooking.id;
                    update objCase;
                    
                    objClonedBooking.name = objClonedBooking.ATG_Order_ID__c;
                    objClonedBooking.ATG_Case__c = objCase.id;
                    update objClonedBooking;
                    
                    if(objBookingHeader.Itineraries__r!=null && objBookingHeader.Itineraries__r.size()>0){
                        list<ATG_Booking_Detail__c> tempBookingDetails = new list<ATG_Booking_Detail__c>();
                    
                        for(ATG_Booking_Detail__c obj : objBookingHeader.Itineraries__r){
                            ATG_Booking_Detail__c tempObj = new ATG_Booking_Detail__c();
                            tempObj = obj.clone(false, false, false, false);
                            tempObj.ATG_Booking__c = objClonedBooking.id;
                            tempBookingDetails.add(tempObj);    
                        }
                        
                        insert tempBookingDetails;
                    }
                    
                    if(objBookingHeader.Travellers__r!=null && objBookingHeader.Travellers__r.size()>0){
                        list<ATG_Traveller__c> tempTravellers = new list<ATG_Traveller__c>();
                    
                        for(ATG_Traveller__c obj : objBookingHeader.Travellers__r){
                            ATG_Traveller__c tempObj = new ATG_Traveller__c();
                            tempObj = obj.clone(false, false, false, false);
                            tempObj.ATG_Booking__c = objClonedBooking.id;
                            tempTravellers.add(tempObj);
                        }   
                        
                        insert tempTravellers;
                    }
                    
                    objBookingHeader.ATG_Booking_has_Amended__c = true;
                    update objBookingHeader;
                    successMsg.add(objClonedBooking.id);
                    return successMsg;
                }
            }
        }
        catch(exception ex){
            system.debug('-----------'+ex);
        }
        return null;
    }
}