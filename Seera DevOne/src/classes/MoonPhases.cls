/**
 * Created by mnazir001 on 9/27/18.
 */

public abstract class MoonPhases {
    public  abstract double calculatePhase(double T);
}