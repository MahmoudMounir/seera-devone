@isTest
public class ATG_TravellerTicketControllerTest{

     private static testmethod void TestMethod1(){
         
         list<Case> Caselist= ATG_TestDataFactory.CreateETOCase(1);
         Caselist[0].ATG_Travel_Order_Number__c = '1234';
         insert Caselist;
         
         list<ATG_Master__c> masterList = ATG_TestDataFactory.createMaster(1,'c');
         Caselist[0].ATG_CC_CCGR_Number__c = masterList[0].Id;
         update Caselist;
         
         list<ATG_Requested_Traveller__c> rtObj = ATG_TestDataFactory.CreateRTraveller(1);
         rtObj[0].Request_Number__c = Caselist[0].Id;
         rtObj[0].ATG_Ticket_Number__c = '1234';
         Insert rtObj;              
         ATG_TravellerTicketController at = new ATG_TravellerTicketController();
         at.type = 'CCGR';
         at.caseObj =    Caselist[0]   ;
         at.caseId =  Caselist[0].Id;
         at.getData();
         
         PNR__c pn = new  PNR__c();
         pn.Name = 'testpnr';
         
         insert pn;
         
         PNR_Passenger__c pnPass = new PNR_Passenger__c();
         pnPass.PNR__c = pn.Id;
         pnPass.ATG_Ticket_Number__c = '1234';
         pnPass.ATG_Travel_Order__c = '1234';
         insert pnPass;
         
         PNR_Segment__c  ps = new PNR_Segment__c();
         ps.ATG_PNR__c = pn.Id;
         ps.PNR_Passenger__c =  pnPass.Id;
         ps.ATG_Ticket_Number__c  = '1234';
         insert ps;
         
         
         
         
         
         //seglist 
     
     }


}