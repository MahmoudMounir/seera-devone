global class ATG_MOHEmployeeCreateBatch implements Database.Batchable<sobject>,Schedulable{

    
    private static map<string,string> rtAccMapping = new map<string,string>() ;
    private static map<string,Account> empIDAccMapping = new map<string,Account>();
    
    
    global database.QueryLocator start(database.BatchableContext bc){
            
        
        list<string> rtList = new list<string>{ATG_ConstantsUtility.RequestedTraveller_Employee_RT ,ATG_ConstantsUtility.RequestedTraveller_Dependant_RT};
        rtAccMapping = new map<string,string>();
        
        list<string> rtFieldList = new list<string>(); 
        set<string> rtFieldSet = new set<string>{'id','Name','Request_Number__c','Request_Number__r.ATG_Primary_Traveller__c','ATG_Employee_Traveler__c','ATG_Employee_Traveler__r.ATG_Employee_Id__c','RecordTypeId'}; 
                
        string queryString  = 'select ';
        for(Request_Traveller_Account_Mapping__mdt rtfld : [select Acc_field_name__c,RT_Field_Name__c from Request_Traveller_Account_Mapping__mdt])
            ATG_MOHEmployeeCreateBatch.rtAccMapping.put(rtfld.RT_Field_Name__c,rtfld.Acc_field_name__c);
        
        rtFieldSet.addAll(ATG_MOHEmployeeCreateBatch.rtAccMapping.keySet());
        rtFieldList.addAll(rtFieldSet);      
        queryString += string.join(rtFieldList,',');        
        if(!test.isRunningTest())
            queryString  += ' from ATG_Requested_Traveller__c where ATG_Synced_with_Master__c = false and Request_Number__r.ATG_Approved_Date__c !=null and RecordTypeId in : rtList';
        else 
            queryString  += ' from ATG_Requested_Traveller__c where ATG_Synced_with_Master__c = false and Request_Number__r.ATG_Approved_Date__c !=null and RecordTypeId in : rtList limit 200';   
        system.debug('QueryStr__'+queryString);
        return Database.getQueryLocator(queryString);
    }
    
    
    
    global void execute (Database.BatchableContext BC, List<ATG_Requested_Traveller__c> requestTravellerList){
        
        Account accObj;
        map<string,Account> EmpID_AccMap = new map<string,Account>();
        
        map<string,list<Account>> empID_dependantMap = new map<string,list<Account>>();
        map<string,list<ATG_Requested_Traveller__c>> empID_dependant_RTMap = new map<string,list<ATG_Requested_Traveller__c>>();
        
        map<string,ATG_Requested_Traveller__c> rt_Map = new map<string,ATG_Requested_Traveller__c>();
        list<Account> accounts_forRels = new list<Account>();
        list<ATG_Requested_Traveller__c > rtListToUpdate = new list<ATG_Requested_Traveller__c >();
        list<Case> caseListToUpdate = new list<Case>();
        list<string> caseIDList = new list<string>();
        
        // RecordType IDs
        string employeeRT = ATG_ConstantsUtility.RequestedTraveller_Employee_RT;
        string dependantRT = ATG_ConstantsUtility.RequestedTraveller_Dependant_RT;
        
        
        for(Request_Traveller_Account_Mapping__mdt rtfld : [select Acc_field_name__c,RT_Field_Name__c from Request_Traveller_Account_Mapping__mdt])
            ATG_MOHEmployeeCreateBatch.rtAccMapping.put(rtfld.RT_Field_Name__c,rtfld.Acc_field_name__c);
        
        
        for(ATG_Requested_Traveller__c rt : requestTravellerList){
            accObj = new Account();
            accObj.put('AccountSource','MOH');
            if(employeeRT == rt.RecordTypeId)
                accObj.ATG_company_Name__c = Label.MOH_Account;            
            
            for(string rtfield : ATG_MOHEmployeeCreateBatch.rtAccMapping.keySet()){
                if(rt.get(rtfield) !=null && !String.IsBlank(string.valueOf(rt.get(rtfield))))
                    accObj.put(rtAccMapping.get(rtfield),rt.get(rtfield));
                       
            }
            
            if(rt.RecordTypeId == employeeRT){
                    EmpID_AccMap.put(string.valueOf(rt.get('ATG_Employee_Id__c')),accObj);
                    rt_Map.put(string.valueOf(rt.get('ATG_Employee_Id__c')),rt);      
            }else if(rt.RecordTypeId == dependantRT){                    
                if(!empID_dependantMap.containsKey(rt.ATG_Employee_Traveler__r.ATG_Employee_Id__c)){
                    empID_dependantMap.put(rt.ATG_Employee_Traveler__r.ATG_Employee_Id__c,new list<Account>());
                    empID_dependantMap.get(rt.ATG_Employee_Traveler__r.ATG_Employee_Id__c).add(accObj);     
                }else if(empID_dependantMap.containsKey(rt.ATG_Employee_Traveler__r.ATG_Employee_Id__c))
                    empID_dependantMap.get(rt.ATG_Employee_Traveler__r.ATG_Employee_Id__c).add(accObj); 
                    
                // Employee ID  --- > RT Dependant Map
                
                if(!empID_dependant_RTMap.containsKey(rt.ATG_Employee_Traveler__r.ATG_Employee_Id__c)){
                    empID_dependant_RTMap.put(rt.ATG_Employee_Traveler__r.ATG_Employee_Id__c,new list<ATG_Requested_Traveller__c>());
                    empID_dependant_RTMap.get(rt.ATG_Employee_Traveler__r.ATG_Employee_Id__c).add(rt);     
                }else if(empID_dependant_RTMap.containsKey(rt.ATG_Employee_Traveler__r.ATG_Employee_Id__c))
                    empID_dependant_RTMap.get(rt.ATG_Employee_Traveler__r.ATG_Employee_Id__c).add(rt);      
                        
            }
                
        }
        Savepoint sp;
       try{  
            sp = Database.SetSavePoint();
             
            List<Database.UpsertResult> accUpsertResult;  
            if(EmpID_AccMap !=null && !EmpID_AccMap.isEmpty())
                accUpsertResult  = Database.upsert(EmpID_AccMap.values(),Account.Fields.ATG_Employee_Id__c,false);
            // Check Success and Errors
            for(integer i =0;i < accUpsertResult.size() ; i++){
                if(accUpsertResult[i].IsSuccess()){
                    accounts_forRels.add(EmpID_AccMap.values()[i]);
                    rtListToUpdate.add(new ATG_Requested_Traveller__c(id=rt_Map.get(EmpID_AccMap.values()[i].ATG_Employee_Id__c).Id,ATG_Synced_with_Master__c=true,ATG_Error_Message__c='',ATG_Employee__c=EmpID_AccMap.values()[i].Id));
                    
                    empIDAccMapping.put(EmpID_AccMap.values()[i].ATG_Employee_Id__c,EmpID_AccMap.values()[i]);
                    
                    
                    // Update Case with Primary Traveller IDLEntity
                    if(rt_Map.get(EmpID_AccMap.values()[i].ATG_Employee_Id__c).Request_Number__r.ATG_Primary_Traveller__c == null)
                        caseListToUpdate.add(new Case(id=rt_Map.get(EmpID_AccMap.values()[i].ATG_Employee_Id__c).Request_Number__c,ATG_Primary_Traveller__c=EmpID_AccMap.values()[i].Id));
                        caseIDList.add(rt_Map.get(EmpID_AccMap.values()[i].ATG_Employee_Id__c).Request_Number__c);
                    
                }else{
                    string errorMessage = '';
                    for(Database.Error error : accUpsertResult[i].getErrors())
                        errorMessage+=error.getMessage()+'\r\n';                
                    rtListToUpdate.add(new ATG_Requested_Traveller__c(id=rt_Map.get(EmpID_AccMap.values()[i].ATG_Employee_Id__c).Id,ATG_Synced_with_Master__c=false,ATG_Error_Message__c=errorMessage));
                }   
            }   
            
            // Update Case
            if(caseListToUpdate !=null && !caseListToUpdate.isEmpty())
                Database.Update(caseListToUpdate,false);
                
            // Update Docs on Account
            if(caseIDList!=null && !caseIDList.isEmpty())
                ATG_CommunityApprovalProcessController.updateEmployeeDocuments(caseIDList);    
            
           system.debug('EMPMap=='+empID_dependantMap);
           system.debug('DepMap=='+empID_dependant_RTMap);
           // Process Dependants
            empID_dependantMap = ATG_MOHEmployeeCreateBatch.processDependantAcc(empID_dependantMap,empID_dependant_RTMap );
                


           // Create Relationships
            
            system.debug('accounts_forRels**'+accounts_forRels);        
            ATG_MOHEmployeeCreateBatch.CreateRelationships(empIDAccMapping,empID_dependantMap);                      
            
            
           
            if(rtListToUpdate !=null && !rtListToUpdate.isEmpty())
                Database.Update(rtListToUpdate,false);
        }catch(exception e){
            system.debug('Exception Roll Back'+e.getStackTraceString()+e.getMessage());
            Database.RollBack(sp);
        }
        
    }
    
    global void finish(Database.BatchableContext BC){
        
    }  
    
    public static void CreateRelationships(map<string,Account> empAccMap,map<string,list<Account>> employeeDependantMap){       
        
             list<AccountContactRelation> accContactRels = new list<AccountContactRelation>();
             set<string> personContactIDSet = new set<string>();
             map<string,string> dependantAccContactMap = new map<string,string>();
             map<string,string> empAccContactMap = new map<string,string>();
             
            list<Account> dependantList = new list<Account>();
            
            for(list<Account> accL:  employeeDependantMap.values())
                dependantList.addAll(accL);
            
            
            if((empAccMap !=null && !empAccMap.isEmpty()) || (employeeDependantMap !=null && !employeeDependantMap.isEmpty())){
                for(Account acc : [select id,PersonContactId,ATG_Employee_Id__c from account where id in : empAccMap.values() or id in : dependantList]){
                    if(acc.ATG_Employee_Id__c != null){
                        personContactIDSet.add(acc.PersonContactId);
                        empAccContactMap.put(acc.id,acc.PersonContactId);
                    }else if(acc.ATG_Employee_Id__c == null)
                        dependantAccContactMap.put(acc.id,acc.PersonContactId);
                }   
            } 
            
            
            for(string empId : employeeDependantMap.keySet()){
                for(Account acc: employeeDependantMap.get(empId)){                    
                    system.debug('__1 '+dependantAccContactMap);
                    system.debug('__2 '+empAccMap);
                    if(empAccMap.containsKey(empId) && empAccContactMap.containsKey(acc.Id)){
                        accContactRels.add(new AccountContactRelation(ContactID=empAccContactMap.get(acc.Id),AccountId = Label.MOH_Account,Roles='Employee',IsActive=true,AccountContactUniqueKey__c=empAccMap.get(empId).Id+Label.MOH_Account));  
                        
                    }
                    if(empAccMap.containsKey(empId) &&  empAccMap.containsKey(empId)){
                        accContactRels.add(new AccountContactRelation(ContactID=dependantAccContactMap.get(acc.Id),AccountId = empAccMap.get(empId).Id,Roles=acc.ATG_Sys_Related_By__c,IsActive=true,AccountContactUniqueKey__c=empAccMap.get(empId).Id+dependantAccContactMap.get(acc.Id)));                       
                    }
                
                }
            }       
          
            Database.upsert(accContactRels,AccountContactRelation.Fields.AccountContactUniqueKey__c,true);
       
    }

    public static map<string,list<Account>>  processDependantAcc(map<string,list<Account>> employeeidDepMap,map<string,list<ATG_Requested_Traveller__c>> employeeidDep_RTMap){
        map<string,string> passportAccMap = new map<string,string>();
        map<string,string> NationalIDAccMap = new map<string,string>();
        map<string,string> emailAccMap = new map<string,string>();
        map<string,string> fullnameAccMap = new map<string,string>();
        list<Account> accountList = new list<Account>();
        list<ATG_Requested_Traveller__c> rtList = new list<ATG_Requested_Traveller__c>();
        map<string,list<Account>> dependantMapToProcessRelationship = new map<string,list<Account>>();
        list<ATG_Requested_Traveller__c> rtResListToUpdate = new list<ATG_Requested_Traveller__c>();
        
      system.debug('***employeeidDepMap'+employeeidDepMap);        
            for(AccountContactRelation ac : [select id,AccountId,ContactId,contact.AccountID,contact.Account.ATG_Passport_Number__c,contact.Account.ATG_National_ID__c,contact.Account.personEmail from AccountContactRelation where Account.ATG_Employee_Id__c in : employeeidDepMap.keySet()
                                                and contact.account.ATG_Company_Name__c =: Label.MOH_Account]){
            
                passportAccMap.put(ac.contact.Account.ATG_Passport_Number__c,ac.contact.AccountID);
                NationalIDAccMap.put(ac.contact.Account.ATG_National_ID__c,ac.contact.AccountID);
                emailAccMap.put(ac.contact.Account.personEmail,ac.contact.AccountID);
                //fullnameAccMap.put(ac.contact.Account.ATG_Passport_Number__c,ac.contact.AccountID);
                system.debug('passportAccMap****'+passportAccMap+'**'+NationalIDAccMap+'&&'+emailAccMap);
            }
            
            // Map Account Ids
            system.debug('==employeeidDepMap'+employeeidDepMap);
            for(string empID : employeeidDepMap.keySet()){
                for(Account acc : employeeidDepMap.get(empId)){                
                        if(acc.ATG_Passport_Number__c !=null && !string.isEmpty(acc.ATG_Passport_Number__c) && passportAccMap.containsKey(acc.ATG_Passport_Number__c))
                            acc.Id = passportAccMap.get(acc.ATG_Passport_Number__c);
                        else if(acc.ATG_National_ID__c !=null && !string.isEmpty(acc.ATG_National_ID__c) && NationalIDAccMap.containsKey(acc.ATG_National_ID__c))
                            acc.Id = passportAccMap.get(acc.ATG_National_ID__c);                        
                        else If(acc.PersonEmail !=null && !string.isEmpty(acc.PersonEmail) && emailAccMap.containsKey(acc.PersonEmail))
                            acc.Id = passportAccMap.get(acc.PersonEmail);                       

                        accountList.add(acc); 
                       
                }
                
                for(ATG_Requested_Traveller__c rt : employeeidDep_RTMap.get(empID))
                    rtList.add(rt);
                
                
            }         
                  
           
        system.debug('Dependant List___'+accountList);  
        List<Database.UpsertResult> accUpsertResult;   
          if(accountList !=null && accountList.size()>0)
             accUpsertResult = Database.upsert(accountList,false); 
            
        if(accUpsertResult !=null && accUpsertResult.size()>0){
            for(integer i =0;i < accUpsertResult.size() ; i++){
                if(accUpsertResult[i].IsSuccess()){             
                    if(!dependantMapToProcessRelationship.containsKey(rtList[i].ATG_Employee_Traveler__r.ATG_Employee_Id__c)){
                        dependantMapToProcessRelationship.put(rtList[i].ATG_Employee_Traveler__r.ATG_Employee_Id__c,new list<Account>());
                        dependantMapToProcessRelationship.get(rtList[i].ATG_Employee_Traveler__r.ATG_Employee_Id__c).add(accountList[i]);                              
                    }else if(dependantMapToProcessRelationship.containsKey(rtList[i].ATG_Employee_Traveler__r.ATG_Employee_Id__c))
                        dependantMapToProcessRelationship.get(rtList[i].ATG_Employee_Traveler__r.ATG_Employee_Id__c).add(accountList[i]);                  
                    rtResListToUpdate.add(new ATG_Requested_Traveller__c(id=rtList[i].Id,ATG_Synced_with_Master__c=true,ATG_Error_Message__c='',ATG_Employee__c=accountList[i].Id));
                }else{
                    string errorMessage = '';
                    for(Database.Error error : accUpsertResult[i].getErrors())
                        errorMessage+=error.getMessage()+'\r\n';                
                    rtResListToUpdate.add(new ATG_Requested_Traveller__c(id=rtList[i].Id,ATG_Synced_with_Master__c=false,ATG_Error_Message__c=errorMessage));
                    system.debug('Dependant Error Message'+errorMessage);
                }   
            }
        }
            if(rtResListToUpdate!=null && !rtResListToUpdate.isEmpty())
                update rtResListToUpdate;
              
        return dependantMapToProcessRelationship;      
    }
    
    
    global void execute(SchedulableContext ctx) {
        Database.executeBatch(new ATG_MOHEmployeeCreateBatch(),10);
    }
    
    

}