public without sharing class ATG_TestUtility {
    
    public Account createCorporateAccount(){
        Account obj = new Account();
        obj.name = 'Test Corporate Account';
        obj.ATG_Unique_ID__c = '12345';
        obj.ATG_Business_Unit_Picklist__c = 'Al Tayyar';
        obj.recordTypeID = ATG_ConstantsUtility.BUSINESS_ACCOUNT_RECORDTYPEID;
        insert obj;
        
        return obj;
    }  
    
    public Account createLeisureAccount(){
        Account obj = new Account();
        obj.firstName = 'Test';
        obj.lastName = 'Test';
        obj.personemail = 'qwe@qwe.qwe';
        obj.PersonMobilePhone = '+966 898767567';
        obj.ATG_Business_Unit_Picklist__c = 'Al Tayyar';
        obj.recordTypeID = ATG_ConstantsUtility.PERSON_ACCOUNT_RECORDTYPEID;
        insert obj;
        
        return obj;
    }  
    
    public AccountContactRelation createRelation(string accountID, String ContactID){
        AccountContactRelation obj = new AccountContactRelation();
        obj.accountID = accountID;
        obj.ContactID = ContactID;
        obj.Roles = 'Primary Contact; Employee';
        obj.isActive = true;
        insert obj;
        
        return obj;
    }
    
    public Case createbookingCase(string accid){
    Case bookingCase = new case(accountid=accid,status = 'New',
                            recordtypeid = ATG_ConstantsUtility.Case_booking_RECORDTYPEID,
                            ATG_Product_Type__c = 'Flight',
                            Origin = 'Phone',
                            ATG_BU_From_Case__c = 'Al Tayyar',
                            SuppliedEmail='test@test.com',
                            ownerID = userinfo.getuserID());
    insert bookingCase;
    return bookingCase;
    }
    
    public string createlivechatCase(string accid){
    Case livechatCase = new case(accountid=accid,status = 'New',
                            recordtypeid = ATG_ConstantsUtility.Case_Generic_RECORDTYPEID ,
                            Origin = 'Live Chat',
                            ATG_BU_From_Case__c = 'Al Tayyar',
                            SuppliedEmail='test@test.com');
    insert livechatCase;
    return livechatCase.id;
    }
    
    public string createamendmentCase(string accid){
    
    Case amendmentCase = new case(accountid=accid,status = 'New',
                            recordtypeid = ATG_ConstantsUtility.Case_Amendment_RECORDTYPEID,
                            ATG_Product_Type__c = 'Flight',
                            Origin = 'Phone',
                            ATG_Amendment_Type__c = 'Transactional',
                            ATG_BU_From_Case__c = 'Al Tayyar',
                            ATG_Reason__c = 'Change of Date',
                            ATG_Booking__c = null,
                            SuppliedEmail='test@test.com');
    insert amendmentCase;
    return amendmentCase.id;
    }
    
    public string createcancellationCase(string accid,string bookingid){
    
    Case cancellationCase = new case(accountid=accid,status = 'New',
                            recordtypeid = ATG_ConstantsUtility.Case_Cancellation_RECORDTYPEID ,
                            ATG_Product_Type__c = 'Flight',
                            Origin = 'Phone',
                            ATG_BU_From_Case__c = 'Al Tayyar',
                            ATG_Booking__c = bookingid,
                            SuppliedEmail='test@test.com');
    insert cancellationCase;
    return cancellationCase.id;
    }   
    
    public string createcompliantCase(string accid){
    
                    
    Case compliantCase = new case(accountid=accid,status = 'New',
                            recordtypeid = ATG_ConstantsUtility.Case_Complaint_RECORDTYPEID ,
                            ATG_Product_Type__c = 'Flight',
                            Origin = 'Phone',
                            ATG_BU_From_Case__c = 'Al Tayyar',
                            ATG_Complaint_Category__c = 'Pre-booking',
                            Complaint_Sub_Category__c = 'No availability',
                            SuppliedEmail='test@test.com');
    insert compliantCase;
    return compliantCase.id;
    }
    
    public string createflightbooking(){
    
    ATG_Booking__c book = new ATG_Booking__c(Name = 'TestBook',
                              ATG_Product_Type__c='Flight',
                              ATG_Email__c ='test@test.com',
                              ATG_Customer_Id__c = '12345',
                              ATG_Travel_End_Date__c = system.today());
    insert book;
    return book.id;
    }
    
}