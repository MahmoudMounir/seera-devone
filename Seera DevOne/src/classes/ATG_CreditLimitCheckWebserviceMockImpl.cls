@isTest
global class ATG_CreditLimitCheckWebserviceMockImpl implements WebServiceMock{
    
    public String type;
    
     global ATG_CreditLimitCheckWebserviceMockImpl(final String type){
             this.type = type;
    }

    
    global void doInvoke(
        Object stub,
        Object request,
        Map<String, Object> response,
        String endpoint,
        String soapAction,
        String requestName,
        String responseNS,
        String responseName,
        String responseType) {
            
            system.debug('****request'+request);
            ATG_FinCreditlimitcheck.Record_element res = new ATG_FinCreditlimitcheck.Record_element();
            if(type == 'A'){        
                
                res.CreditAvailable = '17606';
                res.CreditResult = 'Yes';
                res.Message=null;
            }else if(type == 'B'){
                res.CreditAvailable = '';
                res.CreditResult = 'No';
                res.Message='Customer does not exist.';
            }
            ATG_FinCreditlimitcheck.CreditLimitCheckRes crRes= new ATG_FinCreditlimitcheck.CreditLimitCheckRes();
            crRes.Record = res;    
            response.put('response_x', crRes); 
            
        }
    
}