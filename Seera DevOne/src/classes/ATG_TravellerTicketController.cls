Global class ATG_TravellerTicketController{


    global Case caseObj{get;set;}
    global string caseId{get;set;}    
    global list<PNR_Segment__c> seglist{get;set;}
    global string type{get;set;}
    global list<ATG_Requested_Traveller__c> rtList{get;set;}
    
    
    global string getData(){        
       init();
       return '';
    }
    
    
    public void init(){    
        set<string> ticketNumbers = new set<string>();        
        caseObj = new Case();
        if(string.IsnotBlank(caseId))
        caseObj = [select Id,ATG_Reference_Number__c,ATG_Trip_Type__c,MOH_Request_Number__c,ATG_CC_CCGR_Number__r.ATG_Type__c from Case where Id = :caseId limit 1];
        seglist = new list<PNR_Segment__c >();
      
        if(!string.isBlank(caseObj.Id)){
            for(ATG_Requested_Traveller__c rt : [select id,ATG_Ticket_Number__c from ATG_Requested_Traveller__c where Request_Number__c =: caseObj.Id and ATG_Ticket_Number__c !=null])
                ticketNumbers.add(rt.ATG_Ticket_Number__c);             
               
            if(ticketNumbers.size() > 0){
                
                for(PNR_Segment__c seg : [select id,ATG_AirLine_Code__c,ATG_Arrival_Date_Time__c,ATG_Booking_Class__c,ATG_Departure_Date_Time__c,ATG_Destination__c,
                                                 ATG_Origin__c,ATG_PNR__c,ATG_PNR__r.Name,PNR_Passenger__c,PNR_Passenger__r.ATG_Passenger_Full_Name__c,ATG_Ticket_Number__c,PNR_Passenger__r.ATG_Travel_Order__c from PNR_Segment__c   where  ATG_Ticket_Number__c in : ticketNumbers]){   
                    if((caseObj.ATG_CC_CCGR_Number__r.ATG_Type__c == 'CCGR' && seg.PNR_Passenger__r.ATG_Travel_Order__c == caseObj.ATG_Reference_Number__c) || caseObj.ATG_CC_CCGR_Number__r.ATG_Type__c == 'CC No')
                        seglist.add(seg); 
                 }     
            }            
              
        }
    
    }
    
    
    
    
    
    
    
    
    
    
}