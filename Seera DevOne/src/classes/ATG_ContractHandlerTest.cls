/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class ATG_ContractHandlerTest {
	static testMethod void CoETestCase1() {
        Account objAccount = new Account();
        objAccount.Name = 'Test Vendor';
        objAccount.RecordTypeId = ATG_ConstantsUtility.Account_CoE_Air_Vendor_RecordType;
        objAccount.Type = 'Contracted';
        objAccount.ATG_Status__c = 'Active';
        insert objAccount;
        
         Case objCase = new case(Status = 'New',
                            recordtypeid = ATG_ConstantsUtility.Case_Contract_Generic_RecordType,
                            Origin = 'Phone',
                            AccountId = objAccount.Id,
                            ownerID = userinfo.getuserID());
        insert objCase;
        
        Contract objContract = new Contract();
        objContract.AccountId = objAccount.Id;
        objContract.Status = 'Draft';
        objContract.sys_Case_Id__c = objCase.Id;
        insert objContract;
        
        objContract.StartDate = system.today();
        objContract.EndDate = System.today().addYears(1);
        objContract.Status = 'Activated';
        try{
            update objContract;
        }catch(Exception ex){}
        
        ContentVersion objCV = new Contentversion(); 
        objCV.Title = 'CZDSTOU'; 
        objCV.PathOnClient = 'test'; 
        objCV.VersionData = EncodingUtil.base64Decode('Unit Test Attachment Body'); 
        List<ContentVersion> lstCV = new List<ContentVersion>(); 
        lstCV.add(objCV); 
        insert lstCV;
        
        list<ContentDocumentLink> lstCDL = new list<ContentDocumentLink>();
        for(ContentVersion obj : [select ContentDocumentId from ContentVersion where Id IN : lstCV]){
            lstCDL.add(
            	new ContentDocumentLink(
                	ContentDocumentId = obj.ContentDocumentId,
                    LinkedEntityId = objContract.Id,
                    ShareType = 'I'
                )
            );
        }
        insert lstCDL;
        
        update objContract;
        
        objContract.Status = 'Live';
        update objContract;
        
        objContract.Status = 'Expired';
        update objContract;
        
    }

}