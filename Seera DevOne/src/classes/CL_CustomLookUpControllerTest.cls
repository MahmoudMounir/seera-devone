/**************************************************************************************************
* Name               : CL_CustomLookUpControllerTest                                                               
* Description        : CL_CustomLookUpControllerTest class is a Test class to test the code coverage of all the methods of this class.                                        
* Created Date       : 17th June 2019                                                                *
* Created By         : Dushyant Singh Parihar (Accenture)                                              
* -------------------------------------------------------------------------------------------------
Version     Author      Date            Comment                                                               *
v1.1        Dushyant    17/06/2019      Created from scratch

**************************************************************************************************/
@IsTest
private class CL_CustomLookUpControllerTest {
    
    @TestSetup
    static void setup() {
        
        Account ac = new Account(Name='Test Account');
        insert ac;
        CL_Vehicle__c VehicleDataOfUtilClass = new CL_Vehicle__c(Name = 'Test Vehicle');
        VehicleDataOfUtilClass.CL_Driver__c = ac.Id;
        insert VehicleDataOfUtilClass;
       
         List<sObject> VehicleList = new List<sObject>();
         CL_Vehicle__c vehicle;  
          for(integer i=0; i<1; i++){
            vehicle = new CL_Vehicle__c();
            vehicle.Name = String.valueOf(8000+i);
            VehicleList.add(vehicle);
        }
        
        List<sObject> DataOfUtilClass = VehicleList;
        insert DataOfUtilClass;
        User UsrOfUtilClassForFMProfile = new User();
        User UsrOfUtilClassForDriverProfile = new User();
        UsrOfUtilClassForDriverProfile = new User();
       
    }
    
    
    @IsTest
    private static void fetchLookUpValuesTest(){
        
        sObject sObjs = [SELECT Id, Name FROM CL_Vehicle__c WHERE Name='8000'];
        Test.startTest();
        List<sObject> dataOfMainClass = CL_customLookUpController.fetchLookUpValues('8000');
        Test.stopTest();
        CL_Vehicle__c clVeh1 = (CL_Vehicle__c)sObjs;
        List<CL_Vehicle__c> clVeh2 = (List<CL_Vehicle__c>)dataOfMainClass;
        system.assertEquals(clVeh1.Name, clVeh2[0].Name);
    }
    
    @IsTest
    private static void fetchVehicleValuesTest(){
        
        sObject sObjs = [SELECT Id, Name FROM CL_Vehicle__c WHERE Name='8000'];
        Test.startTest();
        List<sObject> dataOfMainClass = CL_customLookUpController.fetchVehicleValues('8000');
        Test.stopTest();
        CL_Vehicle__c clVeh1 = (CL_Vehicle__c)sObjs;
        List<CL_Vehicle__c> clVeh2 = (List<CL_Vehicle__c>)dataOfMainClass;
        system.assertEquals(clVeh1.Name, clVeh2[0].Name);
    } 
    @IsTest   
    private static void fetchUserTest(){
        String dataOfMainclass = CL_customLookUpController.fetchUser();
        //User usr = [Select LastName from User where LastName = 'Test User1'];
        System.runAs(new User(Id = UserInfo.getUserId())){
            Test.startTest();
            List<Profile> PROF = [SELECT Id, Name FROM Profile WHERE Id=:userinfo.getProfileId() LIMIT 1];
            Test.stopTest();
            String profileName = PROF[0].Name;
            system.assertEquals(profileName, dataOfMainclass);
        }
    }
    
    @IsTest   
    public static void getAllContentFilesTest(){
        
        CL_Vehicle__c utildata = [Select Id, CL_Driver__c from CL_Vehicle__c WHERE Name = 'Test Vehicle'];
        ContentVersion contentVersion = new ContentVersion(
            Title = 'Test ContentVers',
            PathOnClient = 'test.jpg',
            VersionData = Blob.valueOf('Test Content'),
            IsMajorVersion = true
        );
        insert contentVersion;    
        List<ContentDocument> documents = [SELECT Id, Title, LatestPublishedVersionId FROM ContentDocument];
        ContentDocumentLink cdl = New ContentDocumentLink();
        cdl.LinkedEntityId = utildata.CL_Driver__c;
        cdl.ContentDocumentId = documents[0].Id;
        cdl.shareType = 'V';
        insert cdl;
        Test.startTest();
        CL_customLookUpController.getAllContentFiles(utildata.Id);
        Test.stopTest();
       // system.assertEquals(cdl.LinkedEntityId, CL_customLookUpController.getAllContentFiles(utildata.Id)[0]);
    } 
    
    @isTest
    private static void getAllFilesTest (){
        //user us1 = ATG_utility.testUserCLProfile();
        List<CL_Vehicle__c> veh = [SELECT Id, Name, CL_Driver__c FROM CL_Vehicle__c WHERE Name='Test Vehicle' LIMIT 1];
        system.debug('veh --> '+veh);
        Attachment attach=new Attachment();   	
    	attach.Name='Unit Test Attachment';
    	Blob bodyBlob=Blob.valueOf('Unit Test Attachment Body');
    	attach.body=bodyBlob;
        attach.parentId=veh[0].CL_Driver__c;
        insert attach;
        Profile prf = [SELECT Id, Name FROM Profile WHERE Name='CL Fleet Manager'];
        List<User> usr= [SELECT Id, Name FROM User WHERE ProfileId =:prf.Id AND isActive=true];
        
        if(usr!=null && usr.size()>0){
        System.runAs(usr[0]){
            test.startTest();
            CL_customLookUpController.getAllFiles(veh[0].Id);
            test.stopTest();
            } 
        }
    }
}