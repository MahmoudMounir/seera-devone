public class TOExpireErrorMsg {
   @AuraEnabled
   public static Boolean showpopup (Id caseid){
        Boolean Booleanvalue = false;
        Date TOExpiryDt = [select id,Patient_TO_Expiration_Date__c from Case  where Id =:caseid limit 1].Patient_TO_Expiration_Date__c;
        if(TOExpiryDt != null){
            
                if(TOExpiryDt <= date.today()){
                   Booleanvalue = true; 
                }
               
        } return Booleanvalue;

}

}