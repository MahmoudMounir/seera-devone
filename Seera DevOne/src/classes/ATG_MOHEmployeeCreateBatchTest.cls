@istest
public class ATG_MOHEmployeeCreateBatchTest{

    static testmethod void TestMethod1(){
      
      
       // create case 
       Case caseObj = ATG_TestDataFactory.CreateETOCase(1)[0];
       insert caseObj;
       
       // Create Request Travellers 
       
       Account acc = ATG_TestDataFactory.createEmployeeAccount();
       insert acc;
       
       // create Requested Traveller
       list<ATG_Requested_Traveller__c> rtList = ATG_TestDataFactory.CreateRTraveller(2);
       rtList[0].ATG_Employee__c  = acc.Id;
       rtList[0].Request_Number__c = caseObj.Id;
       rtList[0].RecordTypeId = ATG_ConstantsUtility.RequestedTraveller_Employee_RT;
       rtList[0].ATG_Employee_Id__c = 'MOH11';
       
       insert  rtList[0];
       
       rtList[1].Request_Number__c  = caseObj.Id;
       rtList[1].RecordTypeId = ATG_ConstantsUtility.RequestedTraveller_Dependant_RT;
       
       
       insert rtList[1];
       
       rtList[1].ATG_Employee_Traveler__c = rtList[0].Id;
       update rtList;
       
      
       
       caseObj.status = 'Approved';
       caseObj.ATG_Approved_Date__c = Date.Today();
       update caseObj;
       
       test.startTest();
       
           Database.executeBatch(new ATG_MOHEmployeeCreateBatch());
       
       test.stopTest();
       
       
       
        
       
       
       
    
    
    }

}