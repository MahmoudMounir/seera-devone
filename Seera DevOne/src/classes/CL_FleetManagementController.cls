public without sharing class CL_FleetManagementController {

    @AuraEnabled
    public static CL_PortalController.UserDetailCls  getUserDetails(string userId){
        CL_PortalController.UserDetailCls userDetails = new CL_PortalController.UserDetailCls(userId);    
        return userDetails;
    } 

     @AuraEnabled
    public static CL_PortalController.UserDetailCls  getReassigntUserDetails(string selectedRassignVehicle){
        
        List<cl_vehicle__c> lstOfVeihcles = new List<cl_vehicle__c>();
        cl_vehicle__c selectedVehicle = (cl_vehicle__c)JSON.deserialize(selectedRassignVehicle,cl_vehicle__c.class);

        System.debug('FleetMgtCtrl selectedVehicle' + selectedVehicle);
        lstOfVeihcles = [select id, Name,CL_Driver__c from CL_Vehicle__c WHERE Id = : selectedVehicle.Id limit 1];
        if(lstOfVeihcles.size()>0){
            List<user> lUsers = new List<user>();   
            lUsers = [select Id from User WHERE AccountId = : lstOfVeihcles[0].CL_Driver__c];
            if(lUsers.size()>0){
                CL_PortalController.UserDetailCls userDetails = new CL_PortalController.UserDetailCls(lUsers[0].Id);
                return userDetails; 
            } else {
                return null;
            }   
        } else {
            return null;    
        }   
        
    }   
    // Method to reassign driver
    @AuraEnabled
    public static CommunityMessages reassignVehicle(CL_Vehicle__c oldSelectedVehicle, CL_Vehicle__c selectedVehicle, string driverAccountId){
        
        List<CL_Vehicle__c> vehiclesToUpdate = new List<CL_Vehicle__c>();
        
        Savepoint sp= Database.setSavepoint();
        try{   
            //Remove assignment from Old Vehicle
            oldSelectedVehicle.CL_Driver__c = null;
            vehiclesToUpdate.add(oldSelectedVehicle);
            //assign new vehicle
            selectedVehicle.CL_Driver__c = driverAccountId;
            vehiclesToUpdate.add(selectedVehicle);
            
            update vehiclesToUpdate;
            
            return new CommunityMessages(Label.CL_vehicleSuccessfullyReassigned,true);
        }   catch(DmlException e){               
                    system.debug('error: '+e.getStackTraceString());  
                    Database.Rollback(sp); 
                    //ATG_utility.LogException(e);
                    string errorMessage = '';
                    for (Integer i = 0; i < e.getNumDml(); i++) {
                        // Process exception here
                        errorMessage += e.getDmlMessage(i) + '\n'; 
                    }           
                    return new CommunityMessages('Error: ' + errorMessage,false);                     
            } 
    }

    
     @AuraEnabled
    public static CommunityMessages createCommunityUser(CL_Vehicle__c selectedVehicle, string userDetail){    
        
        CL_PortalController.UserDetailCls userDetailObj =  (CL_PortalController.UserDetailCls)JSON.deserialize(userDetail,CL_PortalController.UserDetailCls.class);
        System.debug('selectedVehicle'+selectedVehicle);
        System.debug('userDetailObj'+userDetailObj);
        boolean isExistingInactiveUser = false;
        
        //Get the Business Account from current user
        CL_PortalController.UserDetailCls currentUserDetails = new CL_PortalController.UserDetailCls(userInfo.getUserId());
        Account businessAccount = currentUserDetails.clientAccount;
        userDetailObj.clientAccount = businessAccount;      

        //check if the email address exists
        List<user> lUsers = new List<user>();
        lUsers = [select id,Name,isActive,AccountId, Account.CL_Business_Account__c,  CL_LUMI_Role__c, Username, Email from User WHERE email =: userDetailObj.userObj.email AND CL_LUMI_Role__c!=null AND AccountId!=null AND Account.CL_Business_Account__c = : businessAccount.Id LIMIT 1];              
        //if user exist - ask the KAM to activate it
        if(lUsers.size()>0){
            if(lUsers[0].isActive){
                List<cl_vehicle__c> lstOfVeihcles = new List<cl_vehicle__c>();
                lstOfVeihcles = [select id, Name,CL_Driver__c, CL_Driver_Name__c from CL_Vehicle__c WHERE CL_Driver__c=: lUsers[0].AccountId limit 1];
                if(lstOfVeihcles.size()>0){
                    List<String> parameters = new List<String>();
                    parameters.add(lUsers[0].Name);
                    parameters.add(lstOfVeihcles[0].Name);
                    String userAlreadyExistsLabelwithParam = String.format(Label.CL_userAlreadyExists, parameters); 
                    return new CommunityMessages(userAlreadyExistsLabelwithParam, false);                   
                }   
            
            } else {
                isExistingInactiveUser = true;
                userDetailObj.driverAccount = new Account(id=lUsers[0].AccountId);
                userDetailObj.userObj.id = lUsers[0].id;
            }   
        }   
        // Create user Only if user is not existing 
        Id LumiPersonAccountRecordTypeID = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('CL_Driver_Person_Account').getRecordTypeId();

        Savepoint sp= Database.setSavepoint();
        try{            
            if(isExistingInactiveUser && userDetailObj.userObj.id !=null){
                //Assign Vehicle
                selectedVehicle.CL_Driver__c = userDetailObj.driverAccount.id;
                update selectedVehicle;
                
                //update Accout Details
                Account driverAccount = new Account(id= userDetailObj.driverAccount.Id,OwnerId=businessAccount.OwnerId, RecordTypeId=LumiPersonAccountRecordTypeID, CL_Business_Account__c=userDetailObj.clientAccount.id, firstName=userDetailObj.userObj.firstName,LastName=userDetailObj.userObj.LastName,PersonMobilePhone= userDetailObj.userObj.MobilePhone,PersonEmail= userDetailObj.userObj.Email);    
                update driverAccount;
                
                //activate the user
                userDetailObj.userObj.IsActive = true;
                Database.DMLOptions dmo = new Database.DMLOptions();
                dmo.EmailHeader.triggerUserEmail = true;       
                dmo.EmailHeader.triggerOtherEmail = false;
                dmo.EmailHeader.triggerAutoResponseEmail = false;       
                dmo.optAllOrNone = false;
                
                userDetailObj.userObj.setOptions(dmo);
                
                string userDetailObjString = JSON.serialize(userDetailObj);
                updateUserAsync(userDetailObjString);
                
                return new CommunityMessages(Label.UserActivationSuccessMessage,true);
            } else {

                //Create the driver
                string profId ;

                for(Profile profObj :[select id,Name from profile where Name= : Label.CL_DriverProfileName LIMIT 1])
                    profId =  profObj.Id;
                     system.debug('&&&&&'+profId);
                 if(!string.isBlank(profId)){
                     userDetailObj.userObj.ProfileId = profId;
                     userDetailObj.userObj.communitynickname  = userDetailObj.userObj.FirstName;
                     userDetailObj.userObj.UserName = userDetailObj.userObj.Email;        
                     userDetailObj.userObj.timezonesidkey = 'Asia/Riyadh';          
                     userDetailObj.userObj.IsActive = true;
                     userDetailObj.userObj.languagelocalekey = 'en_US'; 
                     userDetailObj.userObj.localesidkey = 'en_US';
                     userDetailObj.userObj.Alias  = userDetailObj.userObj.UserName.split('@')[0].substring(0,2);
                     userDetailObj.userObj.EmailEncodingKey = 'ISO-8859-1'; 
                    
                   
                    Database.DMLOptions dmo = new Database.DMLOptions();
                    dmo.EmailHeader.triggerUserEmail = true;       
                    dmo.EmailHeader.triggerOtherEmail = false;
                    dmo.EmailHeader.triggerAutoResponseEmail = false;       
                    dmo.optAllOrNone = false;
                    
                    userDetailObj.userObj.setOptions(dmo);
                    
                    
                    
                    
                    //assign  user person Account

                    Account driverAccount = new Account(OwnerId=businessAccount.OwnerId, RecordTypeId=LumiPersonAccountRecordTypeID, CL_Business_Account__c=userDetailObj.clientAccount.id, firstName=userDetailObj.userObj.firstName,LastName=userDetailObj.userObj.LastName,PersonMobilePhone= userDetailObj.userObj.MobilePhone,PersonEmail= userDetailObj.userObj.Email);    
                    //assign Account Owner to Business Accoun owner (KAM)
                    insert driverAccount;
                    userDetailObj.driverAccount = driverAccount;
                    //get PersonContactId
                    Account insertedAccount = new Account();
                    for(Account acc : [select id, personContactId from Account WHERE id =: driverAccount.id LIMIT 1])
                        insertedAccount = acc;                  
                    userDetailObj.userObj.ContactId = insertedAccount.personContactId;
 
                    
                    //Assign Vehicle
                    selectedVehicle.CL_Driver__c = userDetailObj.driverAccount.id;
                    update selectedVehicle;
                    
                    insert userDetailObj.userObj;                 
        
                    return new CommunityMessages(Label.UserActivationSuccessMessage,true);
                }else {
                    return new CommunityMessages('Error: please contact your Key Account Manager',false);
                }
                    
            }
            
            
            
        }   catch(DmlException e){               
                    system.debug('error: '+e.getStackTraceString());  
                    Database.Rollback(sp); 
                    //ATG_utility.LogException(e);
                    string errorMessage = '';
                    for (Integer i = 0; i < e.getNumDml(); i++) {
                        // Process exception here
                        errorMessage += e.getDmlMessage(i) + '\n'; 
                    }           
                    return new CommunityMessages('Error: ' + errorMessage,false);                     
            }   
            
          
    }


    @AuraEnabled
    public static List<CL_Vehicle__c> fetchVehicles(String searchKeyWord){    
           List<sObject> returnList = CL_PortalController.fetchVehicleLookUpValues(searchKeyWord,false, true);
           return returnList;           
    } 
        
    
 
    @AuraEnabled
    public static string RestPassword(Id accountId){
        return null;
    }    
    

    public class CommunityMessages{
        @AuraEnabled
        public string communitymsg; 
        @AuraEnabled
        public boolean IsSuccess;  
 
        public  CommunityMessages(string communitymsg, boolean IsSuccess){
            this.communitymsg = communitymsg;
            this.IsSuccess = IsSuccess;
            //Below commented code can be placed in community to show the message
            /*  var msg = response.getReturnValue();              
                helper.showToastMsg(cmp,event,msg.communitymsg); */
        }       
    }
  
     @future
    public static void updateUserAsync(String userDetailsString){  
        Savepoint sp= Database.setSavepoint();
       try{     
            CL_PortalController.UserDetailCls userDetailObj =  (CL_PortalController.UserDetailCls)JSON.deserialize(userDetailsString,CL_PortalController.UserDetailCls.class);
            update userDetailObj.userObj;

        }   catch(exception e){               
                system.debug('error: '+e.getStackTraceString());  
                Database.Rollback(sp); 
                //ATG_utility.LogException(e);
                string errorMessage = '';
                for (Integer i = 0; i < e.getNumDml(); i++) {
                    // Process exception here
                    errorMessage += e.getDmlMessage(i) + '\n'; 
                }
                system.debug('errorMessage: ' +errorMessage);       
            }   
    }  

}