@isTest
public class ATG_CommunityETORequestControllerTest {
    
    private static testmethod void TestMethod1(){
        
        
        
        
        
        Account empAcc = ATG_TestDataFactory.createEmployeeAccount();
        insert empAcc;
        
        
        
        ATG_Document_Details__c doc = new ATG_Document_Details__c();
        doc.ATG_Document_Description__c = 'test';
        doc.ATG_Document_Name__c = 'Passport';
        doc.ATG_Employee__c = empAcc.Id;
        insert doc;
        ATG_TestDataFactory.createContentDocument(doc.Id);
        
        
        
        // Documents
        
        list<RT_Required_Docs_Community__c> rtDocs = new list<RT_Required_Docs_Community__c>();
        rtDocs.add(new RT_Required_Docs_Community__c(Active__c=true,Copy_to_Account__c=true,Is_Default__c=true,Name='Passport'));
        rtDocs.add(new RT_Required_Docs_Community__c(Active__c=true,Copy_to_Account__c=true,Is_Default__c=true,Name='National ID / IQAMA'));
        rtDocs.add(new RT_Required_Docs_Community__c(Active__c=true,Copy_to_Account__c=true,Is_Default__c=true,Name='Other Documents'));        
        insert rtDocs;
        
        
        list<CCGR_CC_Requestor_Settings__c> ccsList = new list<CCGR_CC_Requestor_Settings__c>();
        ccsList.add(new CCGR_CC_Requestor_Settings__c(name='CC No',ATG_Searchable__c=false));
        ccsList.add(new CCGR_CC_Requestor_Settings__c(name='CCGR',ATG_Searchable__c=true));
        insert  ccsList;     
        
        
        
        Case Etocase = ATG_TestDataFactory.CreateETOCase(1)[0];
        ATG_CommunityETORequestController.ETORequestDetail etoReqDObj = ATG_CommunityETORequestController.CreateNewETORequest(Etocase);
        ATG_CommunityETORequestController.getBooking(etoReqDObj.bookingCase.Id);
        
        
       ATG_Requested_Traveller__c rt = ATG_TestDataFactory.CreateRTraveller(1)[0];
        rt.ATG_Employee__c  = empAcc.Id;
        rt.ATG_Employee_Id__c = '888';
        ATG_CommunityETORequestController.SaveTravellerInfo(etoReqDObj.bookingCase.Id,rt,'Employee');
        ATG_CommunityETORequestController.SaveTravellerInfo(etoReqDObj.bookingCase.Id,rt,'Dependant');
        
        etoReqDObj.bookingCase.Cabin_Class__c = 'Economy';
        etoReqDObj.bookingCase.ATG_Purpose_of_Visit__c= 'Vacation';
        etoReqDObj.bookingCase.ATG_Trip_Type__c = 'RoundTrip';
        list<ATG_Case_Details__c> caseDetailsList =   ATG_TestDataFactory.createCaseDetails();        
        ATG_CommunityETORequestController.ETORequestDetail etoReqDObj2 = ATG_CommunityETORequestController.UpdateCaseDetails(etoReqDObj.bookingCase,caseDetailsList);
        //ATG_CommunityETORequestController.UpdateRequestedTravelerRelationship(etoReqDObj2.requestedTravelerInfo[0].Id,etoReqDObj2.requestedTravelerInfo[1].Id);
        ATG_CommunityETORequestController.getTravellerFieldMetadata();        
       
    }
    
    
     private static testmethod void TestMethod2(){ 
        
        Account empAcc = ATG_TestDataFactory.createEmployeeAccount();
        insert empAcc;
        
        
        
        ATG_Document_Details__c doc = new ATG_Document_Details__c();
        doc.ATG_Document_Description__c = 'test';
        doc.ATG_Document_Name__c = 'Passport';
        doc.ATG_Employee__c = empAcc.Id;
        insert doc;
        ATG_TestDataFactory.createContentDocument(doc.Id);
        
        
        
        // Documents
        
        list<RT_Required_Docs_Community__c> rtDocs = new list<RT_Required_Docs_Community__c>();
        rtDocs.add(new RT_Required_Docs_Community__c(Active__c=true,Copy_to_Account__c=true,Is_Default__c=true,Name='Passport'));
        rtDocs.add(new RT_Required_Docs_Community__c(Active__c=true,Copy_to_Account__c=true,Is_Default__c=true,Name='National ID / IQAMA'));
        rtDocs.add(new RT_Required_Docs_Community__c(Active__c=true,Copy_to_Account__c=true,Is_Default__c=true,Name='Other Documents'));        
        insert rtDocs;
        
        
        list<CCGR_CC_Requestor_Settings__c> ccsList = new list<CCGR_CC_Requestor_Settings__c>();
        ccsList.add(new CCGR_CC_Requestor_Settings__c(name='CC No',ATG_Searchable__c=false));
        ccsList.add(new CCGR_CC_Requestor_Settings__c(name='CCGR',ATG_Searchable__c=true));
        insert  ccsList;     
        
        
        
        Case Etocase = ATG_TestDataFactory.CreateETOCase(1)[0];
        ATG_CommunityETORequestController.ETORequestDetail etoReqDObj = ATG_CommunityETORequestController.CreateNewETORequest(Etocase);
        ATG_CommunityETORequestController.getBooking(etoReqDObj.bookingCase.Id);
        
        
       ATG_Requested_Traveller__c rt = ATG_TestDataFactory.CreateRTraveller(1)[0];
        rt.ATG_Employee__c  = empAcc.Id;
        rt.ATG_Employee_Id__c = '888';
        ATG_CommunityETORequestController.SaveTravellerInfo(etoReqDObj.bookingCase.Id,rt,'Employee');
        ATG_CommunityETORequestController.SaveTravellerInfo(etoReqDObj.bookingCase.Id,rt,'Dependant');
        
        etoReqDObj.bookingCase.Cabin_Class__c = 'Economy';
        etoReqDObj.bookingCase.ATG_Purpose_of_Visit__c= 'Vacation';
        etoReqDObj.bookingCase.ATG_Trip_Type__c = 'RoundTrip';
        list<ATG_Case_Details__c> caseDetailsList =   ATG_TestDataFactory.createCaseDetails();        
        ATG_CommunityETORequestController.ETORequestDetail etoReqDObj2 = ATG_CommunityETORequestController.UpdateCaseDetails(etoReqDObj.bookingCase,caseDetailsList);
        //ATG_CommunityETORequestController.UpdateRequestedTravelerRelationship(etoReqDObj2.requestedTravelerInfo[0].Id,etoReqDObj2.requestedTravelerInfo[1].Id);
        ATG_CommunityETORequestController.getTravellerFieldMetadata();        
        ATG_CommunityETORequestController.FetchCCGRDetails();       
        
        
        
        ATG_CommunityETORequestController.submitForApproval(etoReqDObj2.bookingCase.Id, 'Please approve the request.');        
        ATG_CommunityETORequestController.getInterviews('5000E000007zWIo');
        ATG_CommunityETORequestController.deleteInterview('5000E000007zWIo');
        
         etoReqDObj.bookingCase.status ='Assigned';
         update  etoReqDObj.bookingCase;
         
        
       
        
    }
    
}