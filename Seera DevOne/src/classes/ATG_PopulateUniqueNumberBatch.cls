global without sharing class ATG_PopulateUniqueNumberBatch implements Database.Batchable<sObject>, Database.Stateful, Schedulable {  //
	
	public map<string, long> mapMaxUniqueNumbers = new map<string, long>();
	
	global void execute(SchedulableContext sc){
		database.executeBatch(new ATG_PopulateUniqueNumberBatch());
    }
	
	global Database.QueryLocator start(Database.BatchableContext BC){
		
		set<Id> setRTIDs = new set<Id>();
		for(Schema.RecordTypeInfo obj : Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().values()){
			if(obj.getRecordTypeId() != ATG_ConstantsUtility.Master_Person_Account_RECORDTYPEID && obj.getRecordTypeId() != ATG_ConstantsUtility.MASTER_BUSINESS_ACCOUNT_RECORDTYPEID){
				setRTIDs.add( obj.getRecordTypeId());
				//mapMaxUniqueNumbers.put(obj.getRecordTypeId(), 0);
			}			
		}
		
		mapMaxUniqueNumbers.put('Al Tayyar', 0);
		mapMaxUniqueNumbers.put('Al Mosafer', 0);
		mapMaxUniqueNumbers.put('Tajawal', 0);
		mapMaxUniqueNumbers.put('Master', 0);
		
		string query = 'select Id,RecordTypeId,ATG_Unique_ID__c,ATG_Golden_ID__c,ATG_Business_Unit_Picklist__c from Account where (ATG_Unique_ID__c = null AND ATG_Golden_ID__c = null) AND RecordTypeId != null order by CreatedDate' ;
		//string query = 'select Id,RecordTypeId,ATG_Unique_ID__c,ATG_Golden_ID__c,ATG_Business_Unit_Picklist__c from Account where ATG_Master_Account__c != null AND Id NOT IN : ContectDocLinkIds  order by CreatedDate' ;
        return Database.getQueryLocator(query); 
	}
	
	global void execute(Database.BatchableContext BC, list<Account> lstAccounts){ 
    	try{
    	
    		populateUniqueIds(lstAccounts);
    		
    	}catch(Exception ex){ 
			ATG_utility.LogException(ex);
        } 
    }
    
    global void finish(Database.BatchableContext BC){
    	
    }
    
    public void populateUniqueIds(list<Account> lstAccounts){
    	string PreceedingId = ATG_ConstantsUtility.MASTER_GOLD_ID;
    	string sTemp = '';
		map<string, string> mapRecordTypes = new map<string, string>();
			
		for(Schema.RecordTypeInfo obj : Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().values()){
    		mapRecordTypes.put( obj.getRecordTypeId(), obj.getDeveloperName() );
    	}
		
		string strBU = '';
        for(AggregateResult obj : [select MAX(ATG_Unique_ID__c) MaxUniqueId, MAX(ATG_Golden_ID__c) MaxGoldenId, RecordTypeId from Account where (ATG_Unique_ID__c != null OR ATG_Golden_ID__c != null) Group by RecordTypeId ]){
		    system.debug(''+obj.get('RecordTypeId')+' '+obj.get('MaxUniqueId')+ ' '+obj.get('MaxGoldenId'));
		    long num = 0;
		    
		    strBU = getBUName(obj.get('RecordTypeId')+'');
		    
		    if(obj.get('RecordTypeId')+'' == ATG_ConstantsUtility.MASTER_PERSON_ACCOUNT_RECORDTYPEID  || obj.get('RecordTypeId')+'' == ATG_ConstantsUtility.MASTER_BUSINESS_ACCOUNT_RECORDTYPEID){
			   num = obj.get('MaxGoldenId') != null ? ( long.valueOf(obj.get('MaxGoldenId')+'') ) : 0;
		    }else{
		    	num = getNumber( obj.get('MaxUniqueId')+'' );
		    }
			
			if(mapMaxUniqueNumbers.get(strBU) == null || mapMaxUniqueNumbers.get(strBU) < num )
	    		mapMaxUniqueNumbers.put(strBU, num);
		}
		
		
		for(Account objAccount : lstAccounts){
			long l;
			if(mapMaxUniqueNumbers.containsKey(objAccount.ATG_Business_Unit_Picklist__c)){
        		if(objAccount.RecordTypeId != ATG_ConstantsUtility.MASTER_PERSON_ACCOUNT_RECORDTYPEID && objAccount.RecordTypeId != ATG_ConstantsUtility.MASTER_BUSINESS_ACCOUNT_RECORDTYPEID){
        			l = mapMaxUniqueNumbers.get(objAccount.ATG_Business_Unit_Picklist__c)+1;
	        		sTemp = string.valueOf(PreceedingId + l ).right(9);
	        		if(objAccount.ATG_Business_Unit_Picklist__c != null){
		        		objAccount.ATG_Unique_ID__c = ATG_ConstantsUtility.MAP_SHORT_BU.get(objAccount.ATG_Business_Unit_Picklist__c)+'-'+sTemp;
		        		mapMaxUniqueNumbers.put(objAccount.ATG_Business_Unit_Picklist__c, l);
	        		}
        		}else{
        			l = mapMaxUniqueNumbers.get('Master')+1;
                	objAccount.ATG_Golden_ID__c = string.valueOf(PreceedingId+ l ).right(9);
        			mapMaxUniqueNumbers.put('Master', l);
        		}
        		system.debug('objAccount '+objAccount);
        	}else if(objAccount.RecordTypeId == ATG_ConstantsUtility.MASTER_PERSON_ACCOUNT_RECORDTYPEID || objAccount.RecordTypeId == ATG_ConstantsUtility.MASTER_BUSINESS_ACCOUNT_RECORDTYPEID){
    			l = mapMaxUniqueNumbers.get('Master')+1;
            	objAccount.ATG_Golden_ID__c = string.valueOf(PreceedingId+ l ).right(9);
    			mapMaxUniqueNumbers.put('Master', l);
    		}
        	system.debug('2 objAccount '+objAccount);
		}
		
		ATG_AccountHandler.ByPassUpdateTrigger = true;
		list<Database.SaveResult> updateResult = Database.update(lstAccounts,false);
		string errors = '';
		
		for(Integer i=0;i<updateResult.size();i++){
		    if (!updateResult.get(i).isSuccess()){
		        // DML operation failed
		        Database.Error error = updateResult.get(i).getErrors().get(0);
		        errors += error.getMessage()+' id : '+lstAccounts.get(i).Id+'\n';
		     }
		}
		
		if(errors != '' || test.isRunningTest()){
			if(errors.length() >= 131070)
				errors = errors.substring(0, 131070);
			ATG_utility.LogException(errors, 'Unique Number Population');
		}
    }
    
    public string getBUName(string RecordTypeId){
    	if(RecordTypeId == ATG_ConstantsUtility.MASTER_PERSON_ACCOUNT_RECORDTYPEID || 
    		RecordTypeId == ATG_ConstantsUtility.MASTER_BUSINESS_ACCOUNT_RECORDTYPEID ){
    		return 'Master';
    	}else{
    		if(RecordTypeId == ATG_ConstantsUtility.AMH_PERSON_ACCOUNT_RECORDTYPEID)
    			return ATG_ConstantsUtility.ALMOSAFER_BusinessUnit;
    		else if(RecordTypeId == ATG_ConstantsUtility.TJW_PERSON_ACCOUNT_RECORDTYPEID)
    			return ATG_ConstantsUtility.TAJAWAL_BusinessUnit;
    		else
    			return ATG_ConstantsUtility.ALTAYYAR_BusinessUnit;
    	}
    }
    
    public long getNumber(string uniqueNo){
    	return uniqueNo.indexOf('-') != -1 ? ( long.valueOf(uniqueNo.split('-')[1] )) : 0;
    }
    
}