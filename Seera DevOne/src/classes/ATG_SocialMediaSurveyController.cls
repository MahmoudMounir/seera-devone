public class ATG_SocialMediaSurveyController {
    
    @AuraEnabled
    public static string generateSurveyURL(String recordId){    
        String surveyURL = '';
        for(Case objCase:[SELECT CaseNumber,ATG_BU_From_Case__c,Owner.Name FROM Case WHERE Id=:recordId AND Origin IN ('Twitter','Facebook','Instagram') LIMIT 1]){
            if(objCase.ATG_BU_From_Case__c != null && objCase.ATG_BU_From_Case__c.equalsIgnoreCase(ATG_ConstantsUtility.ALMOSAFER_BusinessUnit)){
              surveyURL = +Label.AlMosafer_Social_Post_Survey_URL_Base+'/'+objCase.CaseNumber+'/'+ATG_utility.EncryptPassword(objCase.Owner.Name);  
            }else if(objCase.ATG_BU_From_Case__c != null && objCase.ATG_BU_From_Case__c.equalsIgnoreCase(ATG_ConstantsUtility.TAJAWAL_BusinessUnit)){
              surveyURL = Label.Tajawal_Social_Media_Survey_Base+'/'+objCase.CaseNumber+'/'+ATG_utility.EncryptPassword(objCase.Owner.Name);  
            }
        }
        return surveyURL;
    }

}