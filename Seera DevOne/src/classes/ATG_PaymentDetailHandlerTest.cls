/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class ATG_PaymentDetailHandlerTest {
	
	 static testMethod void ATG_PaymentDetailHandlerTest0() {
	 	Account obj = new Account();
        obj.firstName = 'Test';
        obj.lastName = 'Test';
        obj.personemail = 'testw@testw.com';
        obj.personMobilePhone = '+971 456576896';
        obj.recordTypeID = ATG_ConstantsUtility.PERSON_ACCOUNT_RECORDTYPEID;
        obj.ATG_Business_Unit_Picklist__c = 'Al Tayyar';
        insert obj;
        
        Account obj1 = new Account();
        obj1.firstName = 'Test';
        obj1.lastName = 'Test';
        obj1.personemail = 'testw@testw.com';
        obj1.personMobilePhone = '+971 456576896';
        obj1.recordTypeID = ATG_ConstantsUtility.TJW_PERSON_ACCOUNT_RECORDTYPEID;
        obj1.ATG_Business_Unit_Picklist__c = ATG_ConstantsUtility.TAJAWAL_BusinessUnit;
        insert obj1;
        
        Account obj2 = new Account();
        obj2.firstName = 'Test';
        obj2.lastName = 'Test';
        obj2.personemail = 'testw@testw.com';
        obj2.personMobilePhone = '+971 456576896';
        obj2.recordTypeID = ATG_ConstantsUtility.AMH_PERSON_ACCOUNT_RECORDTYPEID;
        obj2.ATG_Business_Unit_Picklist__c = ATG_ConstantsUtility.ALMOSAFER_BusinessUnit;
        insert obj2;
        
        Payment_Details__c pd1 = new Payment_Details__c ();
        pd1.ATG_Status__c ='Payment Pending';
        pd1.ATG_Business_Unit__c = ATG_ConstantsUtility.ALTAYYAR_BusinessUnit;
        pd1.ATG_Customer_Email__c = 'testw@testw.com';
        insert pd1;
        
        Payment_Details__c pd2 = new Payment_Details__c ();
        pd2.ATG_Status__c ='Payment Pending';
        pd2.ATG_Business_Unit__c = ATG_ConstantsUtility.ALMOSAFER_BusinessUnit;
        pd2.ATG_Customer_Email__c = 'testw@testw.com';
        insert pd2;
        
        Payment_Details__c pd3 = new Payment_Details__c ();
        pd3.ATG_Status__c ='Payment Pending';
        pd3.ATG_Business_Unit__c = ATG_ConstantsUtility.TAJAWAL_BusinessUnit;
        pd3.ATG_Customer_Email__c = 'testw@testw.com';
        insert pd3;
        
        Payment_Details__c pd4 = new Payment_Details__c ();
        pd4.ATG_Status__c ='Payment Pending';
        pd4.ATG_Business_Unit__c = ATG_ConstantsUtility.ALTAYYAR_BusinessUnit;
        pd4.ATG_Customer_Mobile__c = '+971 456576896';
        insert pd4;
        
        Payment_Details__c pd5 = new Payment_Details__c ();
        pd5.ATG_Status__c ='Payment Pending';
        pd5.ATG_Business_Unit__c = ATG_ConstantsUtility.ALMOSAFER_BusinessUnit;
        pd5.ATG_Customer_Mobile__c = '+971 456576896';
        insert pd5;
        
        Payment_Details__c pd6 = new Payment_Details__c ();
        pd6.ATG_Status__c ='Payment Pending';
        pd6.ATG_Business_Unit__c = ATG_ConstantsUtility.TAJAWAL_BusinessUnit;
        pd6.ATG_Customer_Mobile__c = '+971 456576896';
        insert pd6;
	 }

    static testMethod void ATG_PaymentDetailHandlerTest1() {
        
        Account obj = new Account();
        obj.firstName = 'Test';
        obj.lastName = 'Test';
        obj.personemail = 'testw@testw.com';
        obj.personMobilePhone = '+971 456576896';
        obj.recordTypeID = ATG_ConstantsUtility.PERSON_ACCOUNT_RECORDTYPEID;
        obj.ATG_Business_Unit_Picklist__c = 'Al Tayyar';
        obj.ATG_Tapro_ID__c = 'dfgf';
        obj.Mobile_Number__pc = '909098089';
        obj.Country_Code__pc = 'United Arab Emirates (+971)';
        insert obj;
        
        Case c = new Case();
        c.Subject = 'Test Case';  
        c.Status ='New';
        c.Priority = 'Medium';
        c.Origin = 'Email';
        c.ATG_BU_From_Case__c = ATG_ConstantsUtility.ALTAYYAR_BusinessUnit;
        c.recordTypeId = ATG_ConstantsUtility.Case_Generic_RECORDTYPEID;
        c.accountID = obj.id;
        insert c;
        
        Case c1 = new Case();
        c1.Subject = 'Test Case';  
        c1.Status ='New';
        c1.Priority = 'Medium';
        c1.Origin = 'Email';
        c1.ATG_BU_From_Case__c = ATG_ConstantsUtility.ALMOSAFER_BusinessUnit;
        c1.recordTypeId = ATG_ConstantsUtility.AMH_CASE_RECORDTYPE;
        insert c1;
        
        ATG_Booking__c book = new ATG_Booking__c(Name = 'T12345',
                              ATG_Product_Type__c='Flight',
                              ATG_Email__c ='test@test.com',
                              ATG_Customer__c = obj.id,
                              ATG_Travel_End_Date__c = system.today(),ATG_Customer_Name1__c = 'asdsadsad', ATG_Business_Unit__c = 'Al Mosafer');
        insert book;
        
        Payment_Details__c pd = new Payment_Details__c ();
        pd.ATG_Case__c = c1.id;
        pd.ATG_Status__c ='Payment Pending';
        pd.ATG_Booking__c = book.id;
        pd.ATG_Business_Unit__c = 'Al Mosafer';
        pd.ATG_Customer_Email__c = 'testw@testw.com';
        pd.ATG_Payment_Mode__c = 'Cash';
        pd.ATG_Tapro_Flight_Reference_Number__c = 'T1234';
        pd.ATG_Tapro_Hotel_Reference_Number__c = 'T1234';
        pd.ATG_Other_Sales_Tapro_Reference_Number__c = 'T1234';
        
        Payment_Details__c pd3 = new Payment_Details__c ();
        pd3.ATG_Case__c = c.id;
        pd3.ATG_Status__c ='Payment Pending';
        pd3.ATG_Business_Unit__c = 'Al Tayyar';
        pd3.ATG_Customer_Email__c = 'testw@testw.com';
        pd3.ATG_Payment_Mode__c = 'SPAN';
        pd3.ATG_Tapro_Flight_Reference_Number__c = 'T12346';
        pd3.ATG_Tapro_Hotel_Reference_Number__c = 'T12346';
        pd3.ATG_Other_Sales_Tapro_Reference_Number__c = 'T12346';
        
        Payment_Details__c pd2 = new Payment_Details__c ();
        pd2.ATG_Case__c = c.id;
        pd2.ATG_Status__c ='Payment Pending';
        pd2.ATG_Business_Unit__c = 'Tajawal';
        pd2.ATG_Customer_Email__c = 'testw@testw.com';
        
        List<Payment_Details__c> soNewList = new List<Payment_Details__c>();
        soNewList.add(pd);
        soNewList.add(pd2);
        soNewList.add(pd3);
        insert soNewList;
        
        
        pd3.ATG_Payment_Mode__c = 'Cash';
        update pd3;
        
        pd.ATG_Payment_Mode__c = 'Cash';
        pd.ATG_Status__c = 'Payment Verified';
        pd.ownerID = userinfo.getuserid();
     	pd.ATG_Tapro_Flight_Reference_Number__c = 'T12345';
        pd.ATG_Tapro_Hotel_Reference_Number__c = 'T12345';
        pd.ATG_Other_Sales_Tapro_Reference_Number__c = 'T12345';
        update pd;
        
        pd3.ATG_Status__c = 'Payment Received';
        update pd3;
            
        pd3.ATG_Status__c = 'Completed';
        update pd3;
        
        pd3.ATG_Status__c = 'Expired';
        update pd3;
        
        Map<Id,Payment_Details__c> soOldMap = new Map<Id,Payment_Details__c>();
        soOldMap.put(pd.Id,pd);
        
        ATG_PaymentDetailHandler objClass = new ATG_PaymentDetailHandler();
        
    }
    
}