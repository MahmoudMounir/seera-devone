/**************************************************************************************************
Name               : ATG_QAMonitor_Share                                                               
Description        : Trigger on QA_Monitoring__c Object is used to share the QA monitor record to
           the agent(user lookup) on the record.
Created Date       : Feb 11th 2019                                                               
Created By         : Lakshmi Priya                                              
**************************************************************************************************
 Version   Author      Date            Comment                                                        *
MVP2 v1.0   Diana    07/03/2019        Added the logic to invoke Handler method 
**************************************************************************************************/

trigger ATG_QAMonitor_Share on QA_Monitoring__c (after delete, after insert, after undelete, after update, before delete, before insert, before update) {
 ATG_TriggerFactory.createHandler(QA_Monitoring__c.sObjectType);   
 
}