/**************************************************************************************************
* Name               : ATG_LiveChatTranscriptTrigger 
* Description        : Trigger for LivechatTranscript
* Created Date       : 16th August 2018                                                                *
* Created By         : Sravan Booragadda (PWC Digital)                                              
* -------------------------------------------------------------------------------------------------
* Version       Author      Date        Comment                                                               *

**************************************************************************************************/
trigger ATG_LiveChatTranscriptTrigger on LiveChatTranscript (after insert,before insert,after update,before update) {    
    ATG_TriggerFactory.createHandler(LiveChatTranscript.sObjectType); 
}