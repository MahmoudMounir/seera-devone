/**************************************************************************************************
* Name               : ATG_CaseTrigger                                                               
* Description        : Trigger for Case                                      
* Created Date       : 19th June 2018                                                                *
* Created By         : Leeba shibu (PWC Digital)                                              
* -------------------------------------------------------------------------------------------------
* Version       Author      Date        Comment                                                               *

**************************************************************************************************/

trigger ATG_CaseTrigger on Case (after delete, after insert, after undelete, after update, before delete, before insert, before update) {
    ATG_TriggerFactory.createHandler(Case.sObjectType);    
}