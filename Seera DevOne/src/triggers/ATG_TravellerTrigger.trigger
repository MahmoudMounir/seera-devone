/**************************************************************************************************
* Name               : ATG_TravellerTrigger                                                               
* Description        : Trigger for Traveller Header                                      
* Created Date       : 4th Sept 2018                                                                *
* Created By         : Swati Sehrawat (PWC Digital)                                              
* -------------------------------------------------------------------------------------------------
* Version       Author      Date        Comment                                                               *

**************************************************************************************************/
trigger ATG_TravellerTrigger on ATG_Traveller__c (after delete, after insert, after undelete, after update, before delete, before insert, before update) {
    ATG_TriggerFactory.createHandler(ATG_Traveller__c.sObjectType);    
}