/**************************************************************************************************
* Name               : ATG_ContractTrigger                                                               
* Description        : Trigger for Contract                                     
* Created Date       : 24th June 2019                                                                *
* Created By         : Leeba shibu (PWC Digital)                                              
* -------------------------------------------------------------------------------------------------
* Version       Author      Date        Comment                                                               *

**************************************************************************************************/

trigger ATG_ContractTrigger on Contract(after delete, after insert, after undelete, after update, before delete, before insert, before update) {
    ATG_TriggerFactory.createHandler(Contract.sObjectType);    
}