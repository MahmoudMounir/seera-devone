/**************************************************************************************************
* Name               : ATG_ContentDocumentLinkTrigger 
* Description        : Trigger for ContentDocumentLink                                       
* Created Date       : 16th August 2018                                                                *
* Created By         : Sravan Booragadda (PWC Digital)                                              
* -------------------------------------------------------------------------------------------------
* Version       Author      Date        Comment                                                               *

**************************************************************************************************/
trigger ATG_ContentDocumentLinkTrigger on ContentDocumentLink (after insert,before insert,before update,after update,after delete) {
    ATG_TriggerFactory.createHandler(ContentDocumentLink.sObjectType);  
}