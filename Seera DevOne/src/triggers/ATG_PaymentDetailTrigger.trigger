/**************************************************************************************************
* Name               : ATG_PaymentDetailTrigger                                                               
* Description        : Trigger for Case                                      
* Created Date       : 20th Sept 2018                                                                *
* Created By         : Swati Sehrawat (PWC Digital)                                              
* -------------------------------------------------------------------------------------------------
* Version       Author      Date        Comment                                                               *

**************************************************************************************************/

trigger ATG_PaymentDetailTrigger on Payment_Details__c (after insert, after update, before insert, before update) {
     ATG_TriggerFactory.createHandler(Payment_Details__c.sObjectType); 
}