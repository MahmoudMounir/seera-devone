/**************************************************************************************************
* Name               : ATG_SocialPostTrigger                                                               
* Description        : Trigger for SocialPost
* Created Date       : 22nd October 2018                                                                *
* Created By         : Paramasivan (PWC Digital)                                              
* -------------------------------------------------------------------------------------------------
* Version       Author      Date        Comment                                                               *

**************************************************************************************************/

trigger ATG_SocialPostTrigger on SocialPost(after delete, after insert, after update, before delete, before insert, before update) {
	ATG_TriggerFactory.createHandler(SocialPost.sObjectType); 
}