/**************************************************************************************************
* Name               : ATG_AccountContactRelationTrigger                                                               
* Description        : Trigger for AccountContactRelation                                       
* Created Date       : 16th August 2018                                                                *
* Created By         : Swati Sehrawat (PWC Digital)                                              
* -------------------------------------------------------------------------------------------------
* Version       Author      Date        Comment                                                               *

**************************************************************************************************/
trigger ATG_AccountContactRelationTrigger on AccountContactRelation (after delete, after insert, after update, before delete, before insert, before update) {
    ATG_TriggerFactory.createHandler(AccountContactRelation.sObjectType);    
}