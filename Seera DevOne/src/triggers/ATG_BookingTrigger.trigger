/**************************************************************************************************
* Name               : ATG_BookingTrigger                                                               
* Description        : Trigger for Booking Header                                      
* Created Date       : 7th August 2018                                                                *
* Created By         : Swati Sehrawat (PWC Digital)                                              
* -------------------------------------------------------------------------------------------------
* Version       Author      Date        Comment                                                               *

**************************************************************************************************/
trigger ATG_BookingTrigger on ATG_Booking__c (after delete, after insert, after undelete, after update, before delete, before insert, before update) {
    ATG_TriggerFactory.createHandler(ATG_Booking__c.sObjectType);    
}