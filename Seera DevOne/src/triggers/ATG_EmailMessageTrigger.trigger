/**************************************************************************************************
* Name               : ATG_EmailMessageTrigger
* Description        : Trigger for Email Message
* Created Date       : 14th-Oct-2018                                                                *
* Created By         : Leeba (PWC Digital)                                              
* -------------------------------------------------------------------------------------------------
* Version       Author      Date        Comment                                                               *

**************************************************************************************************/

trigger ATG_EmailMessageTrigger on EmailMessage(after delete, after insert, after update, before delete, before insert, before update) {
    ATG_TriggerFactory.createHandler(EmailMessage.sObjectType); 
}