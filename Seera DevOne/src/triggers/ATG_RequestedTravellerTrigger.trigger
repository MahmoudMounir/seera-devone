trigger ATG_RequestedTravellerTrigger on ATG_Requested_Traveller__c (before insert,before update,after insert,after update,after delete,before delete) {
    ATG_TriggerFactory.createHandler(ATG_Requested_Traveller__c.sObjectType); 
}