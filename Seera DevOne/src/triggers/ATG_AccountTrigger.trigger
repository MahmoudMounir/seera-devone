/**************************************************************************************************
* Name               : ATG_AccountTrigger                                                               
* Description        : Trigger for Account                                       
* Created Date       : 10th June 2018                                                                *
* Created By         : Swati Sehrawat (PWC Digital)                                              
* -------------------------------------------------------------------------------------------------
* Version       Author      Date        Comment                                                               *

**************************************************************************************************/

trigger ATG_AccountTrigger on Account (after delete, after insert, after update, before delete, before insert, before update) {
    ATG_TriggerFactory.createHandler(Account.sObjectType); 
}