/**************************************************************************************************
* Name               : ATG_BookingDetailTrigger                                                               
* Description        : Trigger for Booking Detail                                      
* Created Date       : 22nd Sept 2018                                                                *
* Created By         : Swati Sehrawat (PWC Digital)                                              
* -------------------------------------------------------------------------------------------------
* Version       Author      Date        Comment                                                               *

**************************************************************************************************/

trigger ATG_BookingDetailTrigger on ATG_Booking_Detail__c (after insert, after update, before insert, before update) {
    ATG_TriggerFactory.createHandler(ATG_Booking_Detail__c.sObjectType); 
}